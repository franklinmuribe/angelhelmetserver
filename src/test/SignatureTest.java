package test;

import java.util.Collections;

import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToLongException;
import com.mchange.v2.lang.StringUtils;

public class SignatureTest {

	public static void main(String[] args)
			throws NumberFormatException, ParseStringToBigIntegerException, ParseStringToLongException {

		Long signatureSequence = 1418205121L;
		//Long signatureSequence = 1397L;
		System.out.println("Signature->Sequence:    " + signatureSequence);

		String signatureHex = parseLongToHex(signatureSequence);
		System.out.println("Signature->Hex:         " + signatureHex);

		String signatureReverse = hexReverse(signatureHex);
		System.out.println("Signature->Hex Reverse: " + signatureReverse);

		String codeSingHexReverse = hexReverse(signatureReverse);
		System.out.println("\nDecode Sign.->Hex Reverse: " + codeSingHexReverse);

		Long codeSing = parseStringToLong16(codeSingHexReverse);
		System.out.println("Decode Sign.->Sequence:    " + codeSing);

		String signatureReverse2 = "76050000";

		String codeSingHexReverse2 = hexReverse(signatureReverse2);
		System.out.println("\nDecode Sign.->Hex Reverse: " + codeSingHexReverse2);

		Long codeSing2 = parseStringToLong16(codeSingHexReverse2);
		System.out.println("Decode Sign.->Sequence:    " + codeSing2);

	}

	public static String parseLongToHex(Long intValue) {
		String ret = "";

		ret = Long.toHexString(intValue).toUpperCase();

		// if (ret.length() % 2 != 0) {
		ret = String.join("", Collections.nCopies(8 - ret.length(), "0")) + ret;
		// }

		return ret;
	}

	public static String hexReverse(String hex) {
		StringBuilder sb = new StringBuilder();
		char[] bits_hex = hex.toCharArray();

		for (int j = hex.length() - 1; j > -1; j -= 2) {
			sb.append(bits_hex[j - 1]);
			sb.append(bits_hex[j]);
		}
		return sb.toString();
	}

	public static Long parseStringToLong16(String s) {
		Long ret = 0L;

		ret = Long.parseLong(s, 16);

		return ret;
	}

}
