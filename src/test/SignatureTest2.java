package test;

import java.util.Collections;

import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToLongException;
import com.mchange.v2.lang.StringUtils;

public class SignatureTest2 {

	public static void main(String[] args)
			throws NumberFormatException, ParseStringToBigIntegerException, ParseStringToLongException {

		Integer signatureSequence = 1;
		System.out.println("Signature->Sequence:    " + signatureSequence);

		String signatureHex = parseIntegerToHex(signatureSequence);
		System.out.println("Signature->Hex:         " + signatureHex);

	

	}

	public static String parseIntegerToHex(Integer intValue) {
		String ret = "";

		ret = Integer.toHexString(intValue).toUpperCase();

		ret = String.join("", Collections.nCopies(2 - ret.length(), "0")) + ret;

		return ret;
	}

	public static String hexReverse(String hex) {
		StringBuilder sb = new StringBuilder();
		char[] bits_hex = hex.toCharArray();

		for (int j = hex.length() - 1; j > -1; j -= 2) {
			sb.append(bits_hex[j - 1]);
			sb.append(bits_hex[j]);
		}
		return sb.toString();
	}

	public static Long parseStringToLong16(String s) {
		Long ret = 0L;

		ret = Long.parseLong(s, 16);

		return ret;
	}

}
