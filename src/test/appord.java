package test;

import java.nio.charset.StandardCharsets;
import java.util.zip.CRC32;

import com.angelhelmet.server.util.excepciones.ConvertHexToStringException;

public class appord {

	public static void main(String[] args) {

	/*	System.out.println(ord("PHP"));
		System.out.println(ord("HTML"));
		System.out.println(ord("Java"));
		System.out.println(ord("SQL"));
		System.out.println(ord("CSS"));*/

		int crc = 0;
		crc = crc ^ ord("PHP");
		System.out.println("crc: " + crc);
		crc = crc ^ ord("HTML");
		System.out.println("crc: " + crc);
		crc = crc ^ ord("│Java");
		System.out.println("crc: " + crc);
		crc = crc ^ ord(" SQL");
		System.out.println("crc: " + crc);
		crc = crc ^ ord(" CSS");
		System.out.println("crc: " + crc);
		crc = crc ^ ord("é");
		System.out.println("crc: " + crc);
		
		crc = ord("PHP");
		System.out.println("PHP: " + crc);
		crc =  ord("HTML");
		System.out.println("HTML: " + crc);
		crc =  ord("│Java");
		System.out.println("│Java: " + crc);
		crc = ord(" SQL");
		System.out.println(" SQL: " + crc);
		crc = ord(" CSS");
		System.out.println(" CSS: " + crc);
		crc = ord("é");
		System.out.println("é: " + crc);
		
		int crc2 = 0;

		crc2 = crc2 ^ ord3("PHP");
		System.out.println("crc2: " + crc2);
		crc2 = crc2 ^ ord3("HTML");
		System.out.println("crc2: " + crc2);
		crc2 = crc2 ^ ord3("│Java");
		System.out.println("crc2: " + crc2);
		crc2 = crc2 ^ ord3(" SQL");
		System.out.println("crc2: " + crc2);
		crc2 = crc2 ^ ord3(" CSS");
		System.out.println("crc2: " + crc2);
		crc2 = crc2 ^ ord3("é");
		System.out.println("crc2: " + crc2);
		
		
	}

	public static int ord(char c) {
		if (c < 0x80) {
			return c;
		} else if (c < 0x800) {
			return 0xc0 | c >> 6;
		} else if (c < 0x10000) {
			return 0xe0 | c >> 12;
		} else {
			return 0xf0 | c >> 18;
		}
	}

	public static int ord(String s) {
		return s.length() > 0 ? ord(s.charAt(0)) : 0;
	}
	
	public static int ord3(String s) {
	    return s.length() > 0 ? (s.getBytes(StandardCharsets.UTF_8)[0] & 0xff) : 0;
	}
	
	public static int ord3(char c) {
	    return c < 0x80 ? c : ord3(Character.toString(c));
	}
}
