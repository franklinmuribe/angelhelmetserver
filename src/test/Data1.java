package test;

import com.angelhelmet.server.util.excepciones.ConvertHexToStringException;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToLongException;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.util.ServidorUtil;

public class Data1 {

	private Logger _log;

	public static void main(String[] args) throws ConvertHexToStringException, Exception {

		// System.out.println("Canal:" + ServidorUtil.parseStringToInt16("8", "Canal"));
		// System.out.println("Modo:" + ServidorUtil.parseStringToInt16("AA", "Modo"));

		getAudioData("860854021309868", 100L, 2L);
	}

	public static void getAudioData(String serie, Long firma, Long canal)
			throws UnsupportedAudioFileException, IOException {

		File audioFile = new File("C:\\net\\media\\" + serie + ".wav");

		AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
		int BUFFER_SIZE = 4096;

		byte[] bytesBuffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;

		int i = 0;
		int crc = 0;
		
		System.out.println(String.format("0x%08X", firma));
		System.out.println(String.format("0x%02X", canal));
		System.out.println(String.format("0x%06X", audioStream.available() + 2));
		
		System.out.append("#");
		System.out.append("%");
		System.out.format("%08X", firma);
		System.out.format("%02X", canal);
		System.out.format("%06X", audioStream.available() + 2);
		System.out.append("X");
		System.out.append("X");

		while ((bytesRead = audioStream.read(bytesBuffer)) != -1) {

			for (int x = 0; x < bytesRead; x++) {
				crc = crc ^ ord((char) bytesBuffer[x]);
			}
		}

		System.out.format("%02X", crc);
		audioStream.close();
	}

	public static int ord(char c) {
		if (c < 0x80) {
			return c;
		} else if (c < 0x800) {
			return 0xc0 | c >> 6;
		} else if (c < 0x10000) {
			return 0xe0 | c >> 12;
		} else {
			return 0xf0 | c >> 18;
		}
	}

}
