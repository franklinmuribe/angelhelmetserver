package com.angelhelmet.server.rest.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton
@Startup
public class AppProperties {
	 private Properties properties;

	@PostConstruct
	public void init() {
		try {
		InputStream inputStream = getClass().getClassLoader().getResourceAsStream("/resources/application.properties");	
		setProperties(new Properties());
		System.out.println("InputStream is: " + inputStream);
		// Loading the properties
		
			getProperties().load(inputStream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("PostConstruct reading application.properties: " + e.getStackTrace());
			e.printStackTrace();
		}

		// Printing the properties
		System.out.println("Read Properties." + getProperties());
	}

	public Properties getProperties() {
		return properties;
	}

	public void setProperties(Properties properties) {
		this.properties = properties;
	}
}
