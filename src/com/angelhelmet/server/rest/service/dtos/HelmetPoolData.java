package com.angelhelmet.server.rest.service.dtos;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

import com.angelhelmet.server.datos.datosOperarioWS;
import com.angelhelmet.server.datos.datosTiempoVida;

// http://javarevisited.blogspot.com.es/2013/02/concurrent-collections-from-jdk-56-java-example-tutorial.html
public class HelmetPoolData
{
	
	private int id_unidad;
	private String serie;
	private int sensor_x;
	private int sensor_y;
	private int sensor_z;
	private int bateria;
	private int gas;
	private datosTiempoVida tiempo_vida;
	private int id_mapa;
	
	private DatosOperario operario;
	

	//private String supervisor;
	//private String blood;
	//private String emergency;
	//private String smsNumber;
	
	public HelmetPoolData()
	{
		
	}
//
//	public int get_id_unidad() {
//		return _id_unidad;
//	}
//
//	public void set_id_unidad(int _id_unidad) {
//		this._id_unidad = _id_unidad;
//	}
//
//	public String get_serie() {
//		return _serie;
//	}
//
//	public void set_serie(String _serie) {
//		this._serie = _serie;
//	}
//
//	public int get_sensor_x() {
//		return _sensor_x;
//	}
//
//	public void set_sensor_x(int _sensor_x) {
//		this._sensor_x = _sensor_x;
//	}
//
//	public int get_sensor_y() {
//		return _sensor_y;
//	}
//
//	public void set_sensor_y(int _sensor_y) {
//		this._sensor_y = _sensor_y;
//	}
//
//	public int get_sensor_z() {
//		return _sensor_z;
//	}
//
//	public void set_sensor_z(int _sensor_z) {
//		this._sensor_z = _sensor_z;
//	}
//
//	public int get_bateria() {
//		return _bateria;
//	}
//
//	public void set_bateria(int _bateria) {
//		this._bateria = _bateria;
//	}
//
//	public int get_gas() {
//		return _gas;
//	}
//
//	public void set_gas(int _gas) {
//		this._gas = _gas;
//	}
//
//	public datosTiempoVida get_tiempo_vida() {
//		return _tiempo_vida;
//	}
//
//	public void set_tiempo_vida(datosTiempoVida _tiempo_vida) {
//		this._tiempo_vida = _tiempo_vida;
//	}
//
//	public int get_id_mapa() {
//		return _id_mapa;
//	}
//
//	public void set_id_mapa(int _id_mapa) {
//		this._id_mapa = _id_mapa;
//	}

	public int getId_unidad() {
		return id_unidad;
	}

	public void setId_unidad(int id_unidad) {
		this.id_unidad = id_unidad;
	}

	public String getSerie() {
		return serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public int getSensor_x() {
		return sensor_x;
	}

	public void setSensor_x(int sensor_x) {
		this.sensor_x = sensor_x;
	}

	public int getSensor_y() {
		return sensor_y;
	}

	public void setSensor_y(int sensor_y) {
		this.sensor_y = sensor_y;
	}

	public int getSensor_z() {
		return sensor_z;
	}

	public void setSensor_z(int sensor_z) {
		this.sensor_z = sensor_z;
	}

	public int getBateria() {
		return bateria;
	}

	public void setBateria(int bateria) {
		this.bateria = bateria;
	}

	public int getGas() {
		return gas;
	}

	public void setGas(int gas) {
		this.gas = gas;
	}

	public datosTiempoVida getTiempo_vida() {
		return tiempo_vida;
	}

	public void setTiempo_vida(datosTiempoVida tiempo_vida) {
		this.tiempo_vida = tiempo_vida;
	}

	public int getId_mapa() {
		return id_mapa;
	}

	public void setId_mapa(int id_mapa) {
		this.id_mapa = id_mapa;
	}

	public DatosOperario getOperario() {
		return operario;
	}

	public void setOperario(DatosOperario operario) {
		this.operario = operario;
	}
	
}
