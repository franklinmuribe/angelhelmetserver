package com.angelhelmet.server.rest.service.dtos;

public class DatosOperario {

	private int idOperario;
	private String nombre;
	private String apellidos;
	private String identificacion;
	private String empresa;
	private String cargo;

	public int getIdOperario() {
		return idOperario;
	}

	public void setIdOperario(int idOperario) {
		this.idOperario = idOperario;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getIdentificacion() {
		return identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}
}
