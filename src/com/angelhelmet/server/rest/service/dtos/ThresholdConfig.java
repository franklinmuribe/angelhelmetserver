package com.angelhelmet.server.rest.service.dtos;

import java.util.Map;

public class ThresholdConfig {

	private Integer configId;
	private Integer sessionId;
	private Map<Integer, Integer> helmetList;
//helmetId, workerId
	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	public Map<Integer, Integer> getHelmetList() {
		return helmetList;
	}

	public void setHelmetList(Map<Integer, Integer> helmetList) {
		this.helmetList = helmetList;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

}
