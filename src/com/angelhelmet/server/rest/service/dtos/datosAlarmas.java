package com.angelhelmet.server.rest.service.dtos;

public class datosAlarmas {
	private String alarm;
	private String cancelTo;
	private long alarmId;
	private long count;
	private String question;
	private Integer answerNumber;
	private String answerDesc;
	private String answerText;
	private long eventId;
	private long eventDate;
	private long eventResponseDate;

	public String getAlarm() {
		return alarm;
	}

	public void setAlarm(String alarm) {
		this.alarm = alarm;
	}

	public String getCancelTo() {
		return cancelTo;
	}

	public void setCancelTo(String cancelTo) {
		this.cancelTo = cancelTo;
	}

	public long getAlarmId() {
		return alarmId;
	}

	public void setAlarmId(long alarmId) {
		this.alarmId = alarmId;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getAnswerNumber() {
		return answerNumber;
	}

	public void setAnswerNumber(Integer answerNumber) {
		this.answerNumber = answerNumber;
	}

	public String getAnswerDesc() {
		return answerDesc;
	}

	public void setAnswerDesc(String answerDesc) {
		this.answerDesc = answerDesc;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public long getEventDate() {
		return eventDate;
	}

	public void setEventDate(long eventDate) {
		this.eventDate = eventDate;
	}

	public long getEventResponseDate() {
		return eventResponseDate;
	}

	public void setEventResponseDate(long eventResponseDate) {
		this.eventResponseDate = eventResponseDate;
	}

}
