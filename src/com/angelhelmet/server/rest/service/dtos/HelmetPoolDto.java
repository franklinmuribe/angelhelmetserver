package com.angelhelmet.server.rest.service.dtos;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class HelmetPoolDto {

	private List<datosAlarmas> alarmsList;

	@JsonProperty("IndoorPosition")
	public IndoorPositions IndoorPosition;
	@JsonProperty("OutdoorPosition")
	public OutdoorPositions OutdoorPosition;
	private String mode;
	private Map<String, Map<String, Integer>> map;

	@JsonProperty("Helmet")
	public HelmetData Helmet;
	@JsonProperty("MenssageStatus")
	public String MenssageStatus;
	private WorkerData worker;
	private Integer helmetId;
	private int mapId;

	public List<datosAlarmas> getAlarmsList() {
		return alarmsList;
	}

	public void setAlarmsList(List<datosAlarmas> alarmsList) {
		this.alarmsList = alarmsList;
	}

	public int getMapId() {
		return mapId;
	}

	public void setMapId(int mapId) {
		this.mapId = mapId;
	}

	public Integer getHelmetId() {
		return helmetId;
	}

	public void setHelmetId(Integer helmetId) {
		this.helmetId = helmetId;
	}

	public String getMode() {
		return mode;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public Map<String, Map<String, Integer>> getMap() {
		return map;
	}

	public void setMap(Map<String, Map<String, Integer>> map) {
		this.map = map;
	}

	public WorkerData getWorker() {
		return worker;
	}

	public void setWorker(WorkerData worker) {
		this.worker = worker;
	}

	public class IndoorPositions {
		public double X;
		public double Y;
	}

	public class OutdoorPositions {
		double latitude;
		double longitude;

		public double getLatitude() {
			return latitude;
		}

		public void setLatitude(double latitude) {
			this.latitude = latitude;
		}

		public double getLongitude() {
			return longitude;
		}

		public void setLongitude(double longitude) {
			this.longitude = longitude;
		}

	}

	public class HelmetData {
		private Integer battery;
		private Integer gas;
		private Integer helmetId;
		private Integer xSensor;
		private Integer ySensor;
		private Integer zSensor;
		private String serial;
		private String telephone;
		private LifeTime lifeTime;
		private Long timeLastReport;

		public Integer getBattery() {
			return battery;
		}

		public void setBattery(Integer battery) {
			this.battery = battery;
		}

		public Integer getGas() {
			return gas;
		}

		public void setGas(Integer gas) {
			this.gas = gas;
		}

		public Integer getHelmetId() {
			return helmetId;
		}

		public void setHelmetId(Integer helmetId) {
			this.helmetId = helmetId;
		}

		public Integer getxSensor() {
			return xSensor;
		}

		public void setxSensor(Integer xSensor) {
			this.xSensor = xSensor;
		}

		public Integer getySensor() {
			return ySensor;
		}

		public void setySensor(Integer ySensor) {
			this.ySensor = ySensor;
		}

		public Integer getzSensor() {
			return zSensor;
		}

		public void setzSensor(Integer zSensor) {
			this.zSensor = zSensor;
		}

		public String getSerial() {
			return serial;
		}

		public void setSerial(String serial) {
			this.serial = serial;
		}

		public LifeTime getLifeTime() {
			return lifeTime;
		}

		public void setLifeTime(LifeTime lifeTime) {
			this.lifeTime = lifeTime;
		}

		public class LifeTime {
			private Integer h;
			private Integer m;
			private Integer s;

			public Integer getH() {
				return h;
			}

			public void setH(Integer h) {
				this.h = h;
			}

			public Integer getM() {
				return m;
			}

			public void setM(Integer m) {
				this.m = m;
			}

			public Integer getS() {
				return s;
			}

			public void setS(Integer s) {
				this.s = s;
			}
		}

		public String getTelephone() {
			return telephone;
		}

		public void setTelephone(String telephone) {
			this.telephone = telephone;
		}

		public Long getTimeLastReport() {
			return timeLastReport;
		}

		public void setTimeLastReport(Long timeLastReport) {
			this.timeLastReport = timeLastReport;
		}

	}

	public class WorkerData {
		private String bussiness;
		private String identification;
		private Integer workerId;
		private String name;
		private String emergency;
		private String blood;
		private String supervisor;
		private String workerposition;
		private Integer chargeId;

		public String getBussiness() {
			return bussiness;
		}

		public void setBussiness(String bussiness) {
			this.bussiness = bussiness;
		}

		public String getIdentification() {
			return identification;
		}

		public void setIdentification(String identification) {
			this.identification = identification;
		}

		public Integer getWorkerId() {
			return workerId;
		}

		public void setWorkerId(Integer workerId) {
			this.workerId = workerId;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getEmergency() {
			return emergency;
		}

		public void setEmergency(String emergency) {
			this.emergency = emergency;
		}

		public String getBlood() {
			return blood;
		}

		public void setBlood(String blood) {
			this.blood = blood;
		}

		public String getSupervisor() {
			return supervisor;
		}

		public void setSupervisor(String supervisor) {
			this.supervisor = supervisor;
		}

		public String getWorkerposition() {
			return workerposition;
		}

		public void setWorkerposition(String workerposition) {
			this.workerposition = workerposition;
		}

		public Integer getChargeId() {
			return chargeId;
		}

		public void setChargeId(Integer chargeId) {
			this.chargeId = chargeId;
		}

	}

}
