package com.angelhelmet.server.rest.service.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringStyle;

public class ToStringPersonalizado extends ToStringStyle
{
	private static final long serialVersionUID = 5592247845531278099L;

	protected void appendDetail(StringBuffer buffer, String campo, Object valor)
	{
		if (valor instanceof Date)
		{
			valor = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(valor);
		}
		buffer.append(valor);
	}

}
