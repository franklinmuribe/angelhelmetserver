package com.angelhelmet.server.rest.service.utils;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class Utils
{
	public static Timestamp converCalendarToTimestamp(Calendar c)
	{
		return new Timestamp(c.getTimeInMillis());
	}

	public static Timestamp converDateToTimestamp(Date d)
	{
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return converCalendarToTimestamp(c);
	}

	public static Date converTimestampToDate(Timestamp t)
	{
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(t.getTime());
		return c.getTime();
	}
}
