package com.angelhelmet.server.rest.service;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HttpMethod;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadMensajeBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaDiferidoBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.rest.service.api.MensajeUnico;
import com.angelhelmet.server.rest.service.api.RegisterWorker;
import com.angelhelmet.server.rest.service.api.Threshold;
import com.angelhelmet.server.rest.service.api.ThresholdList;
import com.angelhelmet.server.rest.service.api.Worker;
import com.angelhelmet.server.rest.service.api.WorkerList;
import com.angelhelmet.server.rest.service.api.exception.Error;
import com.angelhelmet.server.rest.service.bean.EnvioMensaje;
import com.angelhelmet.server.rest.service.bean.RegisterWorkerBean;
import com.angelhelmet.server.rest.service.bean.ThresholdBean;
import com.angelhelmet.server.rest.service.bean.WorkerBean;
import com.angelhelmet.server.rest.service.dao.exceptions.GetEntityException;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolDto;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolData;
import com.angelhelmet.server.rest.service.dtos.ThresholdConfig;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Path("/")
public class ServiceDemo {
	@EJB
	RegisterWorkerBean _register_worker_bean;

	@EJB
	WorkerBean _worker_bean;

	@EJB
	EnvioMensaje _envio_mensaje;

	@EJB
	ThresholdBean _threshold_bean;

	private Logger _log;
	/* added by ysanjay */
	@EJB
	ILogBean _log_bean;
	@EJB
	IUnidadMensajeBean _um_bean;
	@EJB
	IUnidadMapaDiferidoBean _cliente_diferido;
	@EJB
	IUnidadMapaRealBean _cliente_real;

	public ServiceDemo() {
		_log = LogManager.getLogger(ServiceDemo.class);
	}

	@Target({ ElementType.METHOD })
	@Retention(RetentionPolicy.RUNTIME)
	@HttpMethod("PATCH")
	public @interface PATCH {
	}

	@GET
	@Path("/sayhello")
	public String sayhello() {
		return "Hello World REST";
	}

	/*
	 * WORKERS
	 */

	@POST
	@Path("/worker")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public RegisterWorker addWorker(RegisterWorker r) {
		RegisterWorker ret = null;
		try {
			_log.info(r.toString());
			ret = _register_worker_bean.Register(r);
		} catch (Exception e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return ret;
	}

	@GET
	@Path("/worker")
	@Produces(MediaType.APPLICATION_JSON)
	public WorkerList getAllWorkers() {

		List<Worker> lista = new ArrayList<Worker>();
		try {
			lista = _worker_bean.getAllWorkers();
		} catch (GetEntityException e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return new WorkerList(lista);
	}

	@GET
	@Path("/worker/{supervisor}")
	@Produces(MediaType.APPLICATION_JSON)
	public WorkerList getWorkes(@PathParam("supervisor") String supervisor) {
		List<Worker> lista = new ArrayList<Worker>();
		try {
			lista = _worker_bean.getWorkers(supervisor);
		} catch (Exception e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return new WorkerList(lista);
	}

	@GET
	@Path("/worker/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Worker getWorkes(@PathParam("id") Integer id) {
		Worker ret = null;
		try {
			ret = _worker_bean.getWorker(id);
		} catch (Exception e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return ret;
	}

	/*
	 * MESSAGES
	 */

	@POST
	@Path("/message")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MensajeUnico sendMenssage(MensajeUnico mensaje) {
		MensajeUnico ret = null;
		try {
			_log.info(mensaje.toString());
			ret = _envio_mensaje.addMensaje(mensaje);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return ret;
	}

	@POST
	@Path("/messages")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public MensajeMasivo sendMenssage(MensajeMasivo mensaje) {
		MensajeMasivo ret = null;
		try {
			_log.info(mensaje.toString());
			ret = _envio_mensaje.addMensajes(mensaje);
		} catch (Exception e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return ret;
	}

	/*
	 * THRESHOLDS
	 */

	// @POST
	// @Path("/threshold")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	// public ThresholdList addThresholds(List<Threshold> thresholds) {
	// List<Threshold> lista = null;
	// try {
	// _log.info(thresholds.toString());
	// lista = _threshold_bean.addThreshold(thresholds);
	// } catch (Exception e) {
	// throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
	// }
	// return new ThresholdList(lista);
	// }

	@POST
	@Path("/thresholdconfig")
	// @Produces(MediaType.APPLICATION_JSON)
	// @Consumes(MediaType.APPLICATION_JSON)
	public String addThresholdConfig(ThresholdConfig threshold) {
		String result = null;
		try {
			result = _threshold_bean.addThresholdConfig(threshold);
			return result;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
	}

	@GET
	@Path("/threshold/{serial}")
	@Produces(MediaType.APPLICATION_JSON)
	public Threshold getLastCofiguration(@PathParam("serial") String serial) {
		Threshold ret = null;
		try {
			ret = _threshold_bean.getLastThreshold(serial.trim());
		} catch (Exception e) {
			throw new Error(HttpURLConnection.HTTP_INTERNAL_ERROR, e).asException();
		}
		return ret;
	}

	// Added by Gunjan
	@DELETE
	@Path("/clearPoolAlarm/{id_unidad}/{firma}")
	public String clearAlarmPool(@PathParam("id_unidad") int id_unidad, @PathParam("firma") long firma)
			throws Exception {
		String ret;
		try {
			_envio_mensaje.clearAlarmPool(id_unidad, firma);
			return "OK";
		} catch (Exception e) {
			return e.getMessage();
		}
	}

	@GET
	@Path("/getUnidadesTotalWithMapa")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Integer, Integer> getUnidadesTotalWithMapa() {
		Map<Integer, Integer> ret = null;
		try {
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesTotalWithMapa(null);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios de la mina", ex);
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new HashMap<>(); // ArrayList<datosUnidadMapaWS>();
			// return "test";
		} else {
			return ret;
			// return "test";
		}
	}

	@GET
	@Path("/getUsuarios/{id_mapa}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<datosUnidadMapaWS> getUsuarios(@PathParam("id_mapa") int id_mapa) {
		List<datosUnidadMapaWS> ret = null;
		try {
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesPorMapa(id_mapa);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios del mapa: {}", id_mapa, ex);
			// _log.error(Level.forName("ERROR_WS", 501),
			// "No se puede obtener la lista de operarios del mapa:");
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new ArrayList<datosUnidadMapaWS>();
		} else {
			return ret;
		}
	}

	@GET
	@Path("/getUnidadesByMapa/{id_mapa}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<HelmetPoolData> getUnidadesByMapa(@PathParam("id_mapa") int id_mapa) {
		/* getting worker list from pool by map id */
		List<HelmetPoolData> ret = null;
		try {
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesByMapa(id_mapa);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios del mapa: {}", id_mapa, ex);
			// _log.error(Level.forName("ERROR_WS", 501),
			// "No se puede obtener la lista de operarios del mapa:");
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new ArrayList<HelmetPoolData>();
		} else {
			return ret;
		}
	}

	@GET
	@Path("/getAlarmasByMapa/{id_mapa}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<HelmetPoolDto> getAlarmasByMapa(@PathParam("id_mapa") int id_mapa) {
		List<HelmetPoolDto> ret = null;
		try {
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getAlarmasByMapa(id_mapa);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios del mapa: {}", id_mapa, ex);
			// _log.error(Level.forName("ERROR_WS", 501),
			// "No se puede obtener la lista de operarios del mapa:");
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new ArrayList<HelmetPoolDto>();
		} else {
			return ret;
		}
	}

	@GET
	@Path("/getAlarmWorkerTotal/{lstMaps}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<String, Integer> getAlarmWorkerTotal(@PathParam("lstMaps") String lstMaps) {
		Map<String, Integer> ret = null;
		try {

			String[] literalNumbers = lstMaps.split(",");
			List<Integer> maps = new ArrayList();

			for (int i = 0; i < literalNumbers.length; i++) {
				maps.add(Integer.valueOf(literalNumbers[i]));
			}

			ret = _cliente_real.getPool().getAlarmWorkerTotal(maps);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios de la mina", ex);
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new HashMap<String, Integer>();
		} else {
			return ret;
		}

	}

	@GET
	@Path("/getUsuariosForLockedMap/{lstMaps}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Integer> getUsuariosForLockedMap(@PathParam("lstMaps") String lstMaps) {
		List<Integer> ret = null;
		try {

			String[] literalNumbers = lstMaps.split(",");
			List<Integer> maps = new ArrayList();

			for (int i = 0; i < literalNumbers.length; i++) {
				maps.add(Integer.valueOf(literalNumbers[i]));
			}

			ret = _cliente_real.getPool().getUsuariosForLockedMap(maps);
		} catch (Exception ex) {
			_log.error("No se puede obtener la lista de operarios de la mina", ex);
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new ArrayList<Integer>();
		} else {
			return ret;
		}
	}

	@GET
	@Path("/getCompaniesForLockedMap/{lstMaps}")
	@Produces(MediaType.APPLICATION_JSON)
	public Map<Integer, String> getCompaniesForLockedMap(@PathParam("lstMaps") String lstMaps) {
		Map<Integer, String> ret = null;
		try {
			String[] literalNumbers = lstMaps.split(",");
			List<Integer> maps = new ArrayList<>();

			for (int i = 0; i < literalNumbers.length; i++) {
				maps.add(Integer.valueOf(literalNumbers[i]));
			}

			ret = _cliente_real.getPool().getCompaniesForLockedMap(maps);
		} catch (Exception ex) {

			_log.error("No se puede obtener la lista de operarios de la mina", ex);

		} finally {
			// lock.unlock();
		}
		return ret;
	}

	@GET
	@Path("/getUsuariosByCompanyForLockedMap/{empresas}/{lstMaps}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Integer> getUsuariosByCompanyForLockedMap(@PathParam("empresas") String lstEmpresa,
			@PathParam("lstMaps") String lstMaps) {
		List<Integer> ret = null;
		try {
			String[] empresa = lstEmpresa.split(",");
			List<Integer> empresas = new ArrayList<>();

			for (int i = 0; i < empresa.length; i++) {
				empresas.add(Integer.valueOf(empresa[i]));
			}

			String[] arrMaps = lstMaps.split(",");
			List<Integer> maps = new ArrayList<>();

			for (int i = 0; i < arrMaps.length; i++) {
				maps.add(Integer.valueOf(arrMaps[i]));
			}

			ret = _cliente_real.getPool().getUsuariosByCompanyForLockedMap(empresas, maps);
		} catch (Exception ex) {
			_log.error("Exception in getUsuariosByCompanyForLockedMap: ", ex);
		} finally {
			// lock.unlock();
		}
		if (ret == null) {
			return new ArrayList<Integer>();
		} else {
			return ret;
		}
	}
}
