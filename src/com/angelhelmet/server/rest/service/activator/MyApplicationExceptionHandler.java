package com.angelhelmet.server.rest.service.activator;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class MyApplicationExceptionHandler implements ExceptionMapper<MyApplicationException>
{

	@Override
	public Response toResponse(MyApplicationException ex)
	{
		return Response.status(Status.BAD_REQUEST).entity(ex.getMessage()).type(MediaType.APPLICATION_JSON).build();
		
	}

}
