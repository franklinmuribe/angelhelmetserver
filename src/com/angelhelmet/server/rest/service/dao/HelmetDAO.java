package com.angelhelmet.server.rest.service.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.dao.exceptions.AddEntityException;
import com.angelhelmet.server.rest.service.dao.exceptions.GetEntityException;

@Stateless
public class HelmetDAO
{
	@PersistenceContext
	private EntityManager _em;

	public HelmetDAO()
	{
	}

	public UnidadEB addUnidad(UnidadEB unidad) throws AddEntityException
	{
		try
		{
			_em.persist(unidad);
		}
		catch (Exception ex)
		{
			throw new AddEntityException("No se ha podido a�adir la unidad:" + ex);
		}
		return unidad;
	}

	public UnidadEB getUnidad(int id) throws GetEntityException
	{
		UnidadEB ret = null;
		try
		{
			ret = _em.find(UnidadEB.class, id);
		}
		catch (Exception ex)
		{
			throw new GetEntityException("No se ha podido recuperar la unidad por id " + id, ex);
		}
		return ret;
	}

	public List<UnidadEB> getUnidades() throws GetEntityException
	{
		List<UnidadEB> ret = null;
		try
		{
			TypedQuery<UnidadEB> query = _em.createNamedQuery("OperarioEB.findAll", UnidadEB.class);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			throw new GetEntityException("No se ha podido recuperar la lista de operarios", ex);
		}
		return ret;
	}
}
