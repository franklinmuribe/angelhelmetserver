package com.angelhelmet.server.rest.service.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.rest.service.dao.exceptions.AddEntityException;
import com.angelhelmet.server.rest.service.dao.exceptions.GetEntityException;

@Stateless
public class WorkerDAO
{
	@PersistenceContext
	private EntityManager _em;

	public WorkerDAO()
	{
	}

	public OperarioEB addOperario(OperarioEB operario) throws AddEntityException
	{
		try
		{
			_em.persist(operario);
		}
		catch (Exception ex)
		{
			throw new AddEntityException("No se ha podido a�adir el operario:" + ex);
		}
		return operario;
	}

	public OperarioEB getOperario(int id) throws GetEntityException
	{
		OperarioEB ret = null;
		try
		{
			ret = _em.find(OperarioEB.class, id);
		}
		catch (Exception ex)
		{
			throw new GetEntityException("No se ha podido recuperar el operario por id " + id, ex);
		}
		return ret;
	}

	public List<OperarioEB> getOperarios() throws GetEntityException
	{
		List<OperarioEB> ret = null;
		try
		{
			TypedQuery<OperarioEB> query = _em.createNamedQuery("OperarioEB.findAll", OperarioEB.class);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			throw new GetEntityException("No se ha podido recuperar la lista de operarios", ex);
		}
		return ret;
	}
}
