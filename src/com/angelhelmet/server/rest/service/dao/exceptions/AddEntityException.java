package com.angelhelmet.server.rest.service.dao.exceptions;

public class AddEntityException extends Exception
{
	private static final long serialVersionUID = -1547319058365290005L;

	public AddEntityException()
	{
		super();
	}

	public AddEntityException(String s)
	{
		super(s);
	}

	public AddEntityException(String s, Exception e)
	{
		super(s, e);
	}

}
