package com.angelhelmet.server.rest.service.dao.exceptions;

public class DelEntityException extends Exception
{
	private static final long serialVersionUID = -1067174932986511897L;

	public DelEntityException()
	{
		super();
	}

	public DelEntityException(String s)
	{
		super(s);
	}

	public DelEntityException(String s, Exception e)
	{
		super(s, e);
	}

}
