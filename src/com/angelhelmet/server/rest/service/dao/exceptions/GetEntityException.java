package com.angelhelmet.server.rest.service.dao.exceptions;

public class GetEntityException extends Exception
{
	private static final long serialVersionUID = -6843043555775253629L;
	
	public GetEntityException() {
		super();
	}
	
	public GetEntityException(String s) {
		super(s);
	}
	
	public GetEntityException(String s, Exception e) {
		super(s, e);
	}

}
