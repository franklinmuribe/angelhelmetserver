package com.angelhelmet.server.rest.service.dao.exceptions;

public class UpdateEntityException extends Exception
{
	private static final long serialVersionUID = -6531555460135211158L;
	
	public UpdateEntityException() {
		super();
	}
	
	public UpdateEntityException(String s) {
		super(s);
	}
	
	public UpdateEntityException(String s, Exception e) {
		super(s, e);
	}

}
