package com.angelhelmet.server.rest.service.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.api.ConnectionInfo;
import com.angelhelmet.server.rest.service.api.Helmet;
import com.angelhelmet.server.rest.service.dao.HelmetDAO;
import com.angelhelmet.server.rest.service.dao.exceptions.AddEntityException;
import com.angelhelmet.server.rest.service.dao.exceptions.GetEntityException;
import com.angelhelmet.server.rest.service.utils.Utils;

@Stateless
public class HelmetBean
{
	@EJB
	HelmetDAO _helmet_dao;

	private ConnectionInfo connectionInfo;

	public HelmetBean()
	{
		connectionInfo = new ConnectionInfo();
	}

	public UnidadEB addHelmet(Helmet helmet, ConnectionInfo info) throws AddEntityException
	{
//		Helmet ret = null;
//
//		UnidadEB u = parseHelmet2HelmetEB(helmet, info);
//		u.setIdUnidad(null);
//		u = _helmet_dao.addUnidad(u);
//		setConnectionInfo(u);
//		ret = parseHelmetEB2Helmet(u);

		UnidadEB ret = parseHelmet2HelmetEB(helmet, info);
		ret.setIdUnidad(null);
		ret = _helmet_dao.addUnidad(ret);
		setConnectionInfo(ret);
		
		return ret;
	}

	public Helmet getHelmet(Integer id) throws GetEntityException
	{
		Helmet ret = null;
		UnidadEB u = _helmet_dao.getUnidad(id);		
		ret = parseHelmetEB2Helmet(u);
		return ret;
	}

	public boolean existHelmet(int id) throws GetEntityException
	{
		boolean ret = false;

		Helmet h = getHelmet(id);
		if (h != null) ret = true;

		return ret;
	}

	public UnidadEB parseHelmet2HelmetEB(Helmet h, ConnectionInfo i)
	{
		UnidadEB ret = new UnidadEB();
		//Timestamp t = ServidorUtil.getTimestamp();

		//String idenficacion = i.getDeviceId();
		
		ret.setActivo(true);
		ret.setAsignada(true);
		ret.setFechaActivacion(Utils.converDateToTimestamp(h.getLastActivationDate()));
		
		ret.setFechaAlta(Utils.converDateToTimestamp(h.getResgistionDate()) );
		ret.setFechaBaja(null);
		ret.setIdUnidad(h.getId());
		ret.setNumeroSerie(h.getSerialNumber());
		ret.setIdMina(i.getDeviceId());

		ret.setIp(h.getIp());
		ret.setMascara(h.getMask());
		ret.setPuertEnlace(h.getGateway());
		ret.setTelefono(h.getGsnNumber());

		ret.setOrganizationID(i.getOrganizationId());
		ret.setAuthenticationMethod(i.getAuthenticationMethod());
		ret.setAPIKey(i.getApikey());
		ret.setAuthenticationToken(i.getAuthenticationToken());
		ret.setDeviceType(i.getDeviceType());
		ret.setDeviceID(i.getDeviceId());
		ret.setSharedSubscription(i.getSharedSubscription());
		ret.setCleanSession(i.getCleanSession());
		ret.setAuthtoken(i.getAuthtoken());
		ret.setWsbroker(i.getWsbroker());

		return ret;
	}

	public Helmet parseHelmetEB2Helmet(UnidadEB u)
	{
		Helmet ret = new Helmet();

		ret.setGateway(u.getMascara());
		ret.setGsnNumber(u.getTelefono());
		ret.setId(u.getIdUnidad());
		ret.setIdClient(u.getIdMina());
		ret.setIp(u.getIp());
		ret.setLastActivationDate(u.getFechaActivacion());
		ret.setMask(u.getMascara());
		ret.setResgistionDate(u.getFechaAlta());
		ret.setSerialNumber(u.getNumeroSerie());

		return ret;
	}

	public ConnectionInfo getConnectionInfo()
	{
		return this.connectionInfo;
	}

	private void setConnectionInfo(UnidadEB u)
	{
		connectionInfo.setApikey(u.getAPIKey());
		connectionInfo.setAuthenticationMethod(u.getAuthenticationMethod());
		connectionInfo.setAuthenticationToken(u.getAuthenticationToken());
		connectionInfo.setCleanSession(u.isCleanSession());
		connectionInfo.setDeviceId(u.getDeviceID());
		connectionInfo.setDeviceType(u.getDeviceType());
		connectionInfo.setOrganizationId(u.getOrganizationID());
		connectionInfo.setSharedSubscription(u.isSharedSubscription());
		connectionInfo.setWsbroker(u.getWsbroker());
		connectionInfo.setAuthtoken(u.getAuthtoken());
	}

}
