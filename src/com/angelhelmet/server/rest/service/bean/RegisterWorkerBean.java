package com.angelhelmet.server.rest.service.bean;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.negocio.interfaces.IUnidadOperarioBean;
import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadOperarioEB;
import com.angelhelmet.server.rest.service.api.Helmet;
import com.angelhelmet.server.rest.service.api.RegisterWorker;
import com.angelhelmet.server.rest.service.api.Worker;
import com.angelhelmet.server.rest.service.utils.Utils;

;

@Stateless
public class RegisterWorkerBean
{
	@EJB
	WorkerBean _worker_bean;

	@EJB
	HelmetBean _helmet_bean;

	@EJB
	IUnidadOperarioBean _uo_bean;

	public RegisterWorker Register(RegisterWorker rw) throws Exception
	{
		RegisterWorker ret = new RegisterWorker();

		Worker w = null;
		Helmet h = null;

		// Worker w = _worker_bean.addWorker(rw.getWorkerInfo());
		// Helmet h = _helmet_bean.addHelmet(rw.getHelmetInfo(),
		// rw.getConnectionInfo());

		OperarioEB operario = _worker_bean.addWorker(rw.getWorkerInfo());
		if (operario != null)
		{
			UnidadEB unidad = _helmet_bean.addHelmet(rw.getHelmetInfo(), rw.getConnectionInfo());
			if (unidad != null)
			{
				UnidadOperarioEB uo_eb = _uo_bean.addUnidadOperario(unidad, operario, Utils.converDateToTimestamp(rw.getHelmetInfo().getResgistionDate()));
				if (uo_eb != null)
				{
					w = _worker_bean.parseWorkerEB2Worker(operario);
					h = _helmet_bean.parseHelmetEB2Helmet(unidad);
				}
			}
		}

		ret.setConnectionInfo(_helmet_bean.getConnectionInfo());
		ret.setHelmetInfo(h);
		ret.setWorkerInfo(w);

		return ret;
	}

}
