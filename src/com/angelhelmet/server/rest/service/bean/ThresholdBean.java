package com.angelhelmet.server.rest.service.bean;

import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.excepciones.UnidadNoEncontradaException;
import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.negocio.PeticionBean;
import com.angelhelmet.server.negocio.interfaces.IMensajeBean;
import com.angelhelmet.server.negocio.interfaces.IUmbralBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.api.Threshold;
import com.angelhelmet.server.rest.service.dtos.ThresholdConfig;
import com.angelhelmet.server.util.TipoMensaje;

@SuppressWarnings("deprecation")
@Stateless
public class ThresholdBean {

	private Logger _log = LogManager.getLogger(PeticionBean.class);

	@EJB
	WorkerBean _worker_bean;

	@EJB
	HelmetBean _helmet_bean;

	@EJB
	IUnidadBean _unidad_bean;

	@EJB
	IMensajeBean _mensaje_bean;

	@EJB
	IUmbralBean _umbral_bean;

	public String addThresholdConfig(ThresholdConfig threshold) throws UnidadNoEncontradaException, Exception {

		Integer success = 0;
		Integer failed = 0;
		Threshold thresholds = null;

		Threshold item = _umbral_bean.getThresholdConfiguration(threshold.getConfigId());

		try {
			for (Map.Entry<Integer, Integer> helmet : threshold.getHelmetList().entrySet()) {
				item.setHelmetId(helmet.getKey());
				item.setWorkerId(helmet.getValue());

				UnidadEB unidad = null;
				datosUmbral umbral = new datosUmbral();
				int modo_comunicacion = 0;

				String modo = item.getCommunicationMode();

				switch (item.getCommunicationModeId()) {
				case 1:
					modo_comunicacion = 0;
					break;
				case 2:
					modo_comunicacion = 1;
					break;
				case 3:
					modo_comunicacion = 2;
					break;
				case 4:
					modo_comunicacion = 3;
					break;
				case 5:
					modo_comunicacion = 4;
					break;

				}

				umbral.setId_tipoumbral(modo_comunicacion);

				if (item.getHelmetId() != null && item.getHelmetId() > 0) {
					unidad = _unidad_bean.getUnidad(item.getHelmetId());
				} else {
					if (item.getSerialNumber() != null) {
						unidad = _unidad_bean.getUnidad(item.getSerialNumber());
					}
				}

				if (unidad != null) {

					StringBuilder texto = new StringBuilder();
					texto.append("|");
					texto.append(unidad.getNumeroSerie());
					texto.append(padRight(unidad.getIp(), 15));
					texto.append(padRight(unidad.getMascara(), 15));
					texto.append(padRight(unidad.getPuertEnlace(), 15));

					if (modo_comunicacion == 1 || modo_comunicacion == 3 || modo_comunicacion == 4) {
						texto.append(padRight(item.getGprsTelephone(), 15));
						umbral.setTelefono(item.getGprsTelephone());
					} else {
						texto.append(new String(new char[15]).replace("\0", " "));
					}

					umbral.setImpacto_x(item.getxImpact());
					umbral.setImpacto_y(item.getyImpact());
					umbral.setImpacto_z(item.getzImpact());
					if (item.getGasSensor() == null) {
						umbral.setGas(0);
					} else {
						umbral.setGas(item.getGasSensor());
					}

					umbral.setPrealarma(item.getPreAlarmTime());
					umbral.setEstatico(item.getStaticTime());
					umbral.setCasco_caido(item.getTurnedDown());
					if (item.getTrackingTime() != null) {
						Integer trackingTime = item.getTrackingTime();
						umbral.setTracking(trackingTime);
					}
					texto.append(padLeft(String.valueOf(item.getxImpact()), 3).replace(' ', '0'));
					texto.append(padLeft(String.valueOf(item.getyImpact()), 3).replace(' ', '0'));
					texto.append(padLeft(String.valueOf(item.getzImpact()), 3).replace(' ', '0'));
					texto.append(padLeft(String.valueOf(item.getGasSensor()), 3).replace(' ', '0'));
					texto.append(padLeft(String.valueOf(item.getPreAlarmTime()), 3).replace(' ', '0'));
					texto.append(padLeft(String.valueOf(item.getStaticTime()), 3).replace(' ', '0'));

					if (item.getTurnedDown()) {
						texto.append(1);
					} else {
						texto.append(0);
					}

					texto.append(padLeft(String.valueOf(item.getTrackingTime()), 3).replace(' ', '0'));

					if (modo_comunicacion == 4 || modo_comunicacion == 1) {
						texto.append(padRight(item.getGprsAPN(), 32));
						texto.append(padRight(item.getGprskey(), 16));
						texto.append(padRight(item.getGprsUserName(), 16));
						texto.append(padRight(item.getGprsHost(), 32));
						umbral.setHost_gprs(item.getGprsHost());
						umbral.setApn(item.getGprsAPN());
						umbral.setApn_user(item.getGprsUserName());
						umbral.setApn_pwd(item.getGprskey());

					} else {
						texto.append(new String(new char[96]).replace("\0", " "));
					}

					texto.append(padLeft(String.valueOf(18080), 5).replace(' ', '0'));
					if (modo_comunicacion == 4 || modo_comunicacion == 2) {
						texto.append(padRight(item.getWifiSsid(), 16));
						texto.append(padRight(item.getWifiPassword(), 16));
						texto.append(3);
						texto.append(padRight(item.getWifiHost(), 15));
						umbral.setSsid(item.getWifiSsid());
						umbral.setHost_wifi(item.getWifiHost());
						umbral.setWifi_pwd(item.getWifiPassword());
					} else {
						texto.append(new String(new char[32]).replace("\0", " "));
						texto.append(0);
						texto.append(new String(new char[15]).replace("\0", " "));
					}

					texto.append(modo_comunicacion);

					if (item.getHwVersion() == 2) {

						if (item.getWalkieTalkieUnit()) {

							texto.append(com.angelhelmet.server.util.ServidorUtil
									.parseIntegerToHex(item.getWalkieTalkieChannel()));

							if (!item.getWalkieTalkieBlocked()) {
								texto.append("01"); // Normal
							} else {
								texto.append("02"); // Bloqueado
							}

						} else {
							texto.append("0000"); // Normal
						}
					}

					texto.append("|");

					Long unidadUmbrale = _umbral_bean.addUnidadUmbral(item.getHelmetId(), threshold.getSessionId(),
							item.getThresholdId());

					long id = _mensaje_bean.addMensaje(texto.toString(), TipoMensaje.CONFIGURACION,
							unidad.getIdUnidad(), "", threshold.getSessionId(), unidadUmbrale, -1L);

					_log.info("Threshold assigned: message id= " + id);

					_log.info(texto.toString());

				}

				success = success + 1;
			}
		} catch (Exception e) {
			failed = failed + 1;
		}

		return success + "," + failed + "," + threshold.getHelmetList().size();
	}

	public Threshold getLastThreshold(String serie) {

		_log.info("getLastThreshold - serie: " + serie);

		Threshold ret = new Threshold();
		MensajeEB mensaje = _mensaje_bean.getMensajeConfiguracion(serie);

		if (mensaje != null) {

			String texto = mensaje.getTexto();
			_log.info("getLastThreshold - texto: " + texto);

			ret.setCommunicationMode("MIXMODE");
			ret.setGasSensor(Integer.parseInt(texto.substring(85, 88)));
			ret.setxImpact(Integer.parseInt(texto.substring(76, 79)));
			ret.setyImpact(Integer.parseInt(texto.substring(79, 82)));
			ret.setzImpact(Integer.parseInt(texto.substring(82, 85)));
			ret.setTurnedDown(texto.substring(95, 95).equals("0") ? false : true);
			ret.setCasetUnit(true);
			ret.setStaticTime(Integer.parseInt(texto.substring(91, 94)));
			ret.setPreAlarmTime(Integer.parseInt(texto.substring(88, 91)));
			ret.setGprsTelephone(texto.substring(61, 76).trim());
			ret.setGprsAPN(texto.substring(98, 130).trim());
			ret.setGprsUserName(texto.substring(146, 162).trim());
			ret.setGprskey(texto.substring(130, 146).trim());
			ret.setGprsHost(texto.substring(162, 194).trim());
			ret.setWifiSsid(texto.substring(199, 215).trim());
			ret.setWifiPassword(texto.substring(215, 231).trim());
			ret.setWifiHost(texto.substring(232, 247).trim());
			ret.setThresholdId(mensaje.getIdMensaje().intValue());
			ret.setSerialNumber(serie);
		}

		return ret;
	}

	private String padRight(String s, int n) {
		return String.format("%1$-" + n + "s", s);
	}

	private String padLeft(String s, int n) {
		return String.format("%1$" + n + "s", s);
	}

}
