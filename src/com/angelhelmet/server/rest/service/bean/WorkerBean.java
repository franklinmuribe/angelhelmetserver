package com.angelhelmet.server.rest.service.bean;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.negocio.interfaces.IOperarioBean;
import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.rest.service.api.Worker;
import com.angelhelmet.server.rest.service.dao.WorkerDAO;
import com.angelhelmet.server.rest.service.dao.exceptions.AddEntityException;
import com.angelhelmet.server.rest.service.dao.exceptions.GetEntityException;
import com.angelhelmet.server.util.ServidorUtil;

@Stateless
public class WorkerBean
{
	@EJB
	WorkerDAO _worker_dao;
	
	@EJB
	IOperarioBean _operario_bean;
	
	public OperarioEB addWorker(Worker worker) throws AddEntityException
	{
//		Worker ret = null;
//		OperarioEB w = parseWorker2WorkerEB(worker);
//		w.setIdOperario(null);
//		w = _worker_dao.addOperario(w);
//		ret = parseWorkerEB2Worker(w);
		
		OperarioEB ret = parseWorker2WorkerEB(worker);
		ret.setIdOperario(null);
		ret = _worker_dao.addOperario(ret);

		return ret;
	}
	
	public List<Worker> getAllWorkers() throws GetEntityException
	{
		List<Worker> ret = new ArrayList<Worker>();

		List<OperarioEB> lista = _worker_dao.getOperarios();
		if (lista != null)
		{
			for (OperarioEB item : lista)
			{
				ret.add(parseWorkerEB2Worker(item));
			}
		}
		return ret;
	}
	
	public boolean existWorker(int id) throws GetEntityException
	{
		boolean ret = false;
		OperarioEB w = _worker_dao.getOperario(id);
		if (w != null) ret = true;
		return ret;
	}
	
	public Worker getWorker(Integer id) throws GetEntityException
	{
		Worker ret = null;
		OperarioEB w = _worker_dao.getOperario(id);
		ret = parseWorkerEB2Worker(w);
		return ret;
	} 
	
	public List<Worker> getWorkers(String supervisor) throws Exception {
		List<Worker> ret = new ArrayList<Worker>();

		List<OperarioEB> lista = _operario_bean.getOperarios(supervisor);
		if (lista != null)
		{
			for (OperarioEB item : lista)
			{
				ret.add(parseWorkerEB2Worker(item));
			}
		}
		return ret;
	}
	
	public OperarioEB parseWorker2WorkerEB(Worker w)
	{
		OperarioEB ret = new OperarioEB();		
		Timestamp t = ServidorUtil.getTimestamp();
		
		ret.setActivo(true);
		ret.setApellidos("-");
		ret.setBlood(w.getBlood());
		ret.setColor(null);
		ret.setEmergency(w.getEmergency());
		ret.setEsRoot(false);
		ret.setFechaActivacion(t);
		ret.setFechaAlta(t);
		ret.setFechaBaja(null);
		ret.setFechaNacimiento(null);
		ret.setIdentificacion(w.getEmployeeId());
		//ret.setIdOperario(w.getEmployeeId());
		ret.setNombre(w.getName());
		ret.setNotas(null);
		ret.setSmsNumber(w.getSmsNumber());
		ret.setSupervisor(w.getSupervisor());
		ret.setPais(w.getLocation());
		ret.setRol(w.getRole());

		return ret;
	}

	public Worker parseWorkerEB2Worker(OperarioEB w)
	{
		Worker ret = new Worker();

		ret.setBlood(w.getBlood());
		ret.setEmergency(ret.getEmergency());
		ret.setEmployeeId(w.getIdOperario().toString());
		ret.setLocation(w.getPais());
		ret.setName(w.getNombre());
		ret.setRole(w.getRol());
		ret.setSmsNumber(w.getSmsNumber());
		ret.setSupervisor(w.getSupervisor());

		return ret;
	}
}
