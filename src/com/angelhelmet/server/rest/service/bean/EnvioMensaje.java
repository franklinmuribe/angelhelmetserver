package com.angelhelmet.server.rest.service.bean;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.Random;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.PathParam;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.interfaces.IUnidadDAO;
import com.angelhelmet.server.negocio.interfaces.IMensajeBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.api.ListaUnidadesMensaje;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.rest.service.api.MensajeUnico;
import com.angelhelmet.server.servlet.Server;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.TipoMensaje;

@Stateless
public class EnvioMensaje {
	
	private Logger _log;
	
	@EJB
	IMensajeBean _mensaje_bean;

	@EJB
	private IUnidadDAO _unidad_dao;

	@EJB
	IUnidadMapaRealBean _cliente_real1;
	
	

	public EnvioMensaje() {
		super();
		_log = LogManager.getLogger(Server.class);
	}

	public MensajeUnico addMensaje(MensajeUnico mensaje) throws Exception {
		// _log.info(this.getClass().getName() + " - addMensaje");
		String mensaje_text = mensaje.getText();
		Long idFirmaCasco = 0L;
		Formatter obj = new Formatter();
		
		idFirmaCasco = _mensaje_bean.getIdFirmaCasco();
		
		try {
			TipoMensaje tipo;
			// _log.info(this.getClass().getName() + " - addMensaje - messageType " + mensaje.getMessageType());
			if (mensaje.getMessageType().toLowerCase().equalsIgnoreCase("alarm")) {
				tipo = TipoMensaje.ALARMA;
				mensaje_text = "1" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			} else if (mensaje.getMessageType().toLowerCase().equalsIgnoreCase("alarmreply")) {
				tipo = TipoMensaje.ALARMA;
				mensaje_text = "3" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			} else if (mensaje.getMessageType().toLowerCase().equalsIgnoreCase("normal")) {
				tipo = TipoMensaje.SMS;
				mensaje_text = "2" + mensaje_text;
				
			}else if (mensaje.getMessageType().toLowerCase().equalsIgnoreCase("salud")) {
				tipo = TipoMensaje.SALUD;
				mensaje_text = mensaje.getText();
				
			}else {
				tipo = TipoMensaje.SMS;
				mensaje_text = "4" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			}
			
			
			// _log.info(this.getClass().getName() + " - addMensaje - idFirmaCasco " + idFirmaCasco);
			// _log.info(this.getClass().getName() + " - addMensaje - mensaje_text " + mensaje_text);
			
			_mensaje_bean.addMensaje(mensaje_text, tipo, mensaje.getHelmetId(), mensaje.getHelmetSerial(),
					idFirmaCasco);
			mensaje.setStatus(true);

		} catch (Exception ex) {
			throw ex;
		}
		return mensaje;
	}

	public static String getStreamOfRandomIntsWithRange(int num, int min, int max) {
		int numero = new Random().ints(num, min, max).iterator().nextInt();
		String numeroString = String.format("%04d", numero);
		return numeroString;
	}

	public static String convertNumberToHex(Long number) {
		Formatter obj = new Formatter();

		return obj.format("%8s", Long.toHexString(number).toUpperCase()).toString().replace(" ", "0");
	}

	public static String convertNumberToHexLitteEndian(Long number) {
		Formatter obj = new Formatter();

		return ServidorUtil.hexReverse(obj.format("%8s", Long.toHexString(number).toUpperCase()).toString().replace(" ", "0"));
	}
	

	public MensajeMasivo addMensajes(MensajeMasivo mensajes) throws Exception {
		// _log.info(this.getClass().getName() + " - addMensajes");
		List<Integer> unidades = new ArrayList<Integer>();
		String mensaje_text = mensajes.getText();

		Long idFirmaCasco = 0L;
		Formatter obj = new Formatter();
		
		idFirmaCasco = _mensaje_bean.getIdFirmaCasco();

		try {
			TipoMensaje tipo;
			// _log.info(this.getClass().getName() + " - addMensajes - MessageType " + mensajes.getMessageType());
			if (mensajes.getMessageType().toLowerCase().equalsIgnoreCase("alarm")) {
				tipo = TipoMensaje.ALARMA;
				mensaje_text = "1" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			} else if (mensajes.getMessageType().toLowerCase().equalsIgnoreCase("alarmreply")) {
				tipo = TipoMensaje.ALARMA;
				mensaje_text = "3" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			} else if (mensajes.getMessageType().toLowerCase().equalsIgnoreCase("normal")) {
				tipo = TipoMensaje.SMS;
				mensaje_text = "2" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			} else {
				tipo = TipoMensaje.SMS;
				mensaje_text = "4" + convertNumberToHexLitteEndian(idFirmaCasco) + mensaje_text;
				
			}

			for (ListaUnidadesMensaje item : mensajes.getHelmets()) {
				if (item.getHelmetId() != null) {
					unidades.add(item.getHelmetId());
				} else {
					UnidadEB unidad = _unidad_dao.getUnidad(item.getHelmetSerial());
					unidades.add(unidad.getIdUnidad());
				}
			}
			// _log.info(this.getClass().getName() + " - addMensajes - idFirmaCasco " + idFirmaCasco);
			// _log.info(this.getClass().getName() + " - addMensajes - mensaje_text " + mensaje_text);
			
			_mensaje_bean.addMensajesMasivos(mensaje_text, tipo, unidades, mensajes, idFirmaCasco);
			mensajes.setStatus(true);
		} catch (Exception ex) {
			throw ex;
		}
		return mensajes;
	}

	public boolean clearAlarmPool(int id_unidad, long firma) throws Exception {
		boolean ret = true;
		try {
			datosUnidadMapa datos = _cliente_real1.getPool().getUnidad(id_unidad);
			if (null == datos) {
				System.out.println("No datosUnidadMapa info");
				ret = false;
				throw new Exception("not found");
			}
			if (datos.getAlarmas().remove(firma) == null) {
				ret = false;
			}
		} catch (Exception ex) {
			throw ex;
		}

		return ret;
	}
}
