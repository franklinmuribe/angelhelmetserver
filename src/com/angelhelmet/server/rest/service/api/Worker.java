package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class Worker
{
	// "name": "Arduino Uno",
	// "role": "truckdriver",
	// "employeeid": "12345",
	// "supervisor": "polly",
	// "blood": "A+",
	// "Emergency": "45678990",
	// "Location": "south africa"

	private String name;
	private String role;
	private String employeeId;
	private String supervisor;
	private String blood;
	private String emergency;
	private String smsNumber;
	private String location;

	@XmlElement
	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@XmlElement
	public String getRole()
	{
		return role;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	@XmlElement
	public String getEmployeeId()
	{
		return employeeId;
	}

	public void setEmployeeId(String employeeId)
	{
		this.employeeId = employeeId;
	}

	@XmlElement
	public String getSupervisor()
	{
		return supervisor;
	}

	public void setSupervisor(String supervisor)
	{
		this.supervisor = supervisor;
	}

	@XmlElement
	public String getBlood()
	{
		return blood;
	}

	public void setBlood(String blood)
	{
		this.blood = blood;
	}

	@XmlElement
	public String getEmergency()
	{
		return emergency;
	}

	public void setEmergency(String emergency)
	{
		this.emergency = emergency;
	}

	@XmlElement
	public String getLocation()
	{
		return location;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	@XmlElement
	public String getSmsNumber()
	{
		return smsNumber;
	}

	public void setSmsNumber(String smsNumber)
	{
		this.smsNumber = smsNumber;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
