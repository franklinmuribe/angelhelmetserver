package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class Threshold {
	private Integer configId;
	private Integer thresholdId;
	private String communicationMode;
	private Integer communicationModeId;
	private Integer gasSensor;
	private Integer xImpact;
	private Integer yImpact;
	private Integer zImpact;
	private Boolean casetUnit;
	private Boolean turnedDown;
	private Integer staticTime;
	private Integer preAlarmTime;
	private String gprsAPN;
	private String gprsUserName;
	private String gprskey;
	private String gprsTelephone;
	private String gprsHost;
	private String wifiSsid;
	private String wifiPassword;
	private String wifiHost;
	private Integer trackingTime;
	private String serialNumber;

	private String trackingTimeToggle;
	private Integer workerId;
	private Integer helmetId;

	private Boolean walkieTalkieUnit;
	private Integer walkieTalkieChannel;
	private Boolean walkieTalkieBlocked;
	private Boolean coronavirusProximityNotice;
	private Boolean bleHealthCommunication;

	private Integer hwVersion;

	@XmlElement
	public Integer getTrackingTime() {
		return trackingTime;
	}

	public void setTrackingTime(Integer trackingTime) {
		this.trackingTime = trackingTime;
	}

	@XmlElement
	public String getTrackingTimeToggle() {
		return trackingTimeToggle;
	}

	public void setTrackingTimeToggle(String trackingTimeToggle) {
		this.trackingTimeToggle = trackingTimeToggle;
	}

	@XmlElement
	public String getCommunicationMode() {
		return communicationMode;
	}

	public void setCommunicationMode(String communicationMode) {
		this.communicationMode = communicationMode;
	}

	@XmlElement
	public Integer getGasSensor() {
		return gasSensor;
	}

	public void setGasSensor(Integer gasSensor) {
		this.gasSensor = gasSensor;
	}

	@XmlElement
	public Integer getxImpact() {
		return xImpact;
	}

	public void setxImpact(Integer xImpact) {
		this.xImpact = xImpact;
	}

	@XmlElement
	public Integer getyImpact() {
		return yImpact;
	}

	public void setyImpact(Integer yImpact) {
		this.yImpact = yImpact;
	}

	@XmlElement
	public Integer getzImpact() {
		return zImpact;
	}

	public void setzImpact(Integer zImpact) {
		this.zImpact = zImpact;
	}

	@XmlElement
	public Boolean getTurnedDown() {
		return turnedDown;
	}

	public void setTurnedDown(Boolean turnedDown) {
		this.turnedDown = turnedDown;
	}

	@XmlElement
	public Boolean getCasetUnit() {
		return casetUnit;
	}

	public void setCasetUnit(Boolean casetUnit) {
		this.casetUnit = casetUnit;
	}

	@XmlElement
	public Integer getStaticTime() {
		return staticTime;
	}

	public void setStaticTime(Integer staticTime) {
		this.staticTime = staticTime;
	}

	@XmlElement
	public Integer getPreAlarmTime() {
		return preAlarmTime;
	}

	public void setPreAlarmTime(Integer preAlarmTime) {
		this.preAlarmTime = preAlarmTime;
	}

	@XmlElement
	public Integer getWorkerId() {
		return workerId;
	}

	public void setWorkerId(Integer workerId) {
		this.workerId = workerId;
	}

	@XmlElement
	public Integer getHelmetId() {
		return helmetId;
	}

	public void setHelmetId(Integer helmetId) {
		this.helmetId = helmetId;
	}

	@XmlElement
	public String getGprsAPN() {
		return gprsAPN;
	}

	public void setGprsAPN(String gprsAPN) {
		this.gprsAPN = gprsAPN;
	}

	@XmlElement
	public String getGprsUserName() {
		return gprsUserName;
	}

	public void setGprsUserName(String gprsUserName) {
		this.gprsUserName = gprsUserName;
	}

	@XmlElement
	public String getGprskey() {
		return gprskey;
	}

	public void setGprskey(String gprskey) {
		this.gprskey = gprskey;
	}

	@XmlElement
	public String getGprsTelephone() {
		return gprsTelephone;
	}

	public void setGprsTelephone(String gprsTelephone) {
		this.gprsTelephone = gprsTelephone;
	}

	@XmlElement
	public String getWifiSsid() {
		return wifiSsid;
	}

	public void setWifiSsid(String wifiSsid) {
		this.wifiSsid = wifiSsid;
	}

	@XmlElement
	public String getWifiPassword() {
		return wifiPassword;
	}

	public void setWifiPassword(String wifiPassword) {
		this.wifiPassword = wifiPassword;
	}

	@XmlElement
	public String getWifiHost() {
		return wifiHost;
	}

	public void setWifiHost(String wifiHost) {
		this.wifiHost = wifiHost;
	}

	@XmlElement
	public Integer getThresholdId() {
		return thresholdId;
	}

	public void setThresholdId(Integer thresholdId) {
		this.thresholdId = thresholdId;
	}

	@XmlElement
	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@XmlElement
	public Integer getCommunicationModeId() {
		return communicationModeId;
	}

	public void setCommunicationModeId(Integer communicationModeId) {
		this.communicationModeId = communicationModeId;
	}

	@XmlElement
	public String getGprsHost() {
		return gprsHost;
	}

	public void setGprsHost(String gprsHost) {
		this.gprsHost = gprsHost;
	}

	@XmlElement
	public Integer getConfigId() {
		return configId;
	}

	public void setConfigId(Integer configId) {
		this.configId = configId;
	}

	@XmlElement
	public Boolean getWalkieTalkieUnit() {
		return walkieTalkieUnit;
	}

	public void setWalkieTalkieUnit(Boolean walkieTalkieUnit) {
		this.walkieTalkieUnit = walkieTalkieUnit;
	}

	@XmlElement
	public Integer getWalkieTalkieChannel() {
		return walkieTalkieChannel;
	}

	public void setWalkieTalkieChannel(Integer walkieTalkieChannel) {
		this.walkieTalkieChannel = walkieTalkieChannel;
	}

	@XmlElement
	public Boolean getWalkieTalkieBlocked() {
		return walkieTalkieBlocked;
	}

	public void setWalkieTalkieBlocked(Boolean walkieTalkieBlocked) {
		this.walkieTalkieBlocked = walkieTalkieBlocked;
	}

	@XmlElement
	public Boolean getCoronavirusProximityNotice() {
		return coronavirusProximityNotice;
	}

	public void setCoronavirusProximityNotice(Boolean coronavirusProximityNotice) {
		this.coronavirusProximityNotice = coronavirusProximityNotice;
	}

	@XmlElement
	public Boolean getBleHealthCommunication() {
		return bleHealthCommunication;
	}

	public void setBleHealthCommunication(Boolean bleHealthCommunication) {
		this.bleHealthCommunication = bleHealthCommunication;
	}

	@XmlElement
	public Integer getHwVersion() {
		return hwVersion;
	}

	public void setHwVersion(Integer hwVersion) {
		this.hwVersion = hwVersion;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

}
