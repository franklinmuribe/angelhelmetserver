package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class RegisterWorker
{
	private Worker workerInfo;
	private Helmet helmetInfo;
	private ConnectionInfo connectionInfo;

	@XmlElement()
	public Worker getWorkerInfo()
	{
		return workerInfo;
	}

	public void setWorkerInfo(Worker workerInfo)
	{
		this.workerInfo = workerInfo;
	}

	@XmlElement()
	public Helmet getHelmetInfo()
	{
		return helmetInfo;
	}

	public void setHelmetInfo(Helmet helmetInfo)
	{
		this.helmetInfo = helmetInfo;
	}

	@XmlElement()
	public ConnectionInfo getConnectionInfo()
	{
		return connectionInfo;
	}

	public void setConnectionInfo(ConnectionInfo connectionInfo)
	{
		this.connectionInfo = connectionInfo;
	}

	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
}
