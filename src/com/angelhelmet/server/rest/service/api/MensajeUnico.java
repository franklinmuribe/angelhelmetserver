package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class MensajeUnico
{
	private String text;
	private Integer helmetId;
	private String helmetSerial;
	private String messageType;
	private boolean status;

	@XmlElement()
	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	@XmlElement()
	public Integer getHelmetId()
	{
		return helmetId;
	}

	public void setHelmetId(Integer helmetId)
	{
		this.helmetId = helmetId;
	}

	@XmlElement()
	public String getHelmetSerial()
	{
		return helmetSerial;
	}

	public void setHelmetSerial(String helmetSerial)
	{
		this.helmetSerial = helmetSerial;
	}

	@XmlElement()
	public String getMessageType()
	{
		return messageType;
	}

	public void setMessageType(String messageType)
	{
		this.messageType = messageType;
	}

	@XmlElement()
	public boolean isStatus()
	{
		return status;
	}

	public void setStatus(boolean status)
	{
		this.status = status;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
}
