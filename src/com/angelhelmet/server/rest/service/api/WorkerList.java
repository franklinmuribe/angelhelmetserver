package com.angelhelmet.server.rest.service.api;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class WorkerList
{
	private List<Worker> workers;

	public WorkerList()
	{
	}

	public WorkerList(List<Worker> lista)
	{
		this.workers = lista;
	}

	@XmlElement(name = "workers")
	public List<Worker> getWorkers()
	{
		return this.workers;
	}
}
