package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class ApiStatus
{
	private int code;
	private String message;
	private String details;

	public ApiStatus()
	{
	}

	@XmlElement(name = "code")
	public int getCode()
	{
		return code;
	}

	public void setCode(int code)
	{
		this.code = code;
	}

	@XmlElement(name = "message")
	public String getMessage()
	{
		return message;
	}

	public void setMessage(String message)
	{
		this.message = message;
	}

	@XmlElement(name = "details")
	public String getDetails()
	{
		return details;
	}

	public void setDetails(String details)
	{
		this.details = details;
	}
}
