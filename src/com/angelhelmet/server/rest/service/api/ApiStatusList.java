package com.angelhelmet.server.rest.service.api;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "ApiStatusList")
public class ApiStatusList
{
	List<ApiStatus> lista;

	@SuppressWarnings("unused")
	private ApiStatusList()
	{
	}

	public ApiStatusList(List<ApiStatus> lista)
	{
		this.lista = lista;
	}

	@XmlElement()
	public List<ApiStatus> getApiStatus()
	{
		return this.lista;
	}

}
