package com.angelhelmet.server.rest.service.api;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class ConnectionInfo
{
	// "Organization ID": "r6tic8",
	// "Device Type": "SmartHelmet",
	// "Device ID": "AngelHelmet2",
	// "Authentication Method": "token",
	// "Authentication Token": "(ji3T9ycCKUCG0sxhu",
	// "wsbroker":"r6tic8.messaging.internetofthings.ibmcloud.com",
	// "apikey":"a-r6tic8-i1lwnsbmen",
	// "authtoken":"J-?bkktx4c4n1ck6FC"

	private String organizationId;
	private String deviceType;
	private String deviceId;
	private String authenticationMethod;
	private String wsbroker;
	private String apikey;
	private String authenticationToken;
	private String authtoken;
	private Boolean sharedSubscription;
	private Boolean cleanSession;

	@XmlElement
	public String getOrganizationId()
	{
		return organizationId;
	}

	public void setOrganizationId(String organizationId)
	{
		this.organizationId = organizationId;
	}

	@XmlElement
	public String getDeviceType()
	{
		return deviceType;
	}

	public void setDeviceType(String deviceType)
	{
		this.deviceType = deviceType;
	}

	@XmlElement
	public String getDeviceId()
	{
		return deviceId;
	}

	public void setDeviceId(String deviceId)
	{
		this.deviceId = deviceId;
	}

	@XmlElement
	public String getAuthenticationMethod()
	{
		return authenticationMethod;
	}

	public void setAuthenticationMethod(String authenticationMethod)
	{
		this.authenticationMethod = authenticationMethod;
	}

	@XmlElement
	public String getWsbroker()
	{
		return wsbroker;
	}

	public void setWsbroker(String wsbroker)
	{
		this.wsbroker = wsbroker;
	}

	@XmlElement
	public String getApikey()
	{
		return apikey;
	}

	public void setApikey(String apikey)
	{
		this.apikey = apikey;
	}

	@XmlElement
	public String getAuthenticationToken()
	{
		return authenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken)
	{
		this.authenticationToken = authenticationToken;
	}

	@XmlElement
	public Boolean getSharedSubscription()
	{
		return sharedSubscription;
	}

	public void setSharedSubscription(Boolean sharedSubscription)
	{
		this.sharedSubscription = sharedSubscription;
	}

	@XmlElement
	public Boolean getCleanSession()
	{
		return cleanSession;
	}

	public void setCleanSession(Boolean cleanSession)
	{
		this.cleanSession = cleanSession;
	}
	
	@XmlElement
	public String getAuthtoken()
	{
		return authtoken;
	}

	public void setAuthtoken(String authtoken)
	{
		this.authtoken = authtoken;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
	}
}
