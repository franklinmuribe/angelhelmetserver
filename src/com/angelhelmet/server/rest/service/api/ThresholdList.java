package com.angelhelmet.server.rest.service.api;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
public class ThresholdList
{
	List<Threshold> thresholds;

	@SuppressWarnings("unused")
	private ThresholdList()
	{
	}

	public ThresholdList(List<Threshold> thresholds)
	{
		this.thresholds = thresholds;
	}

	@XmlElement
	public List<Threshold> getThresholdList()
	{
		return this.thresholds;
	}
}
