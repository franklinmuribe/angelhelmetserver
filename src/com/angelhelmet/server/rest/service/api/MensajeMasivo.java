package com.angelhelmet.server.rest.service.api;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

@XmlRootElement()
public class MensajeMasivo {
	private String text;
	private String messageType;
	private boolean status;
	List<ListaUnidadesMensaje> helmets;
	private Integer sessionId;
	private Long firma;

	@XmlElement
	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	@XmlElement
	public String getMessageType() {
		return messageType;
	}

	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}

	@XmlElement
	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	@XmlElement(name = "helmets")
	public List<ListaUnidadesMensaje> getHelmets() {
		return helmets;
	}

	public void setLista(List<ListaUnidadesMensaje> helmets) {
		this.helmets = helmets;
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public Long getFirma() {
		return firma;
	}

	public void setFirma(Long firma) {
		this.firma = firma;
	}

}
