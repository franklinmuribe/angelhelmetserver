package com.angelhelmet.server.rest.service.api;

import java.util.Date;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.angelhelmet.server.rest.service.utils.ToStringPersonalizado;

@XmlRootElement()
public class Helmet
{
	// "Serial Number": "787788787878",
	// "ID": "754646",
	// "GSN Number": "826869862396",
	// "APN Number": "Not Mandaotry",
	// "ResgistionDate": "10/09/2016",
	// "Last Activation Date": "03/10/2016",
	// "Last Deactivation Date": "08/11/2015",
	// "ip": "10.92.63.44",
	// "mask": "0.0.0.8",
	// "gateway": "221.221.445.78"

	private String serialNumber;
	private Integer id;
	private String idClient;
	private String gsnNumber;
	private String apnNumber;
	private Date resgistionDate;
	private Date lastActivationDate;
	private Date lastDeactivationDate;
	private String ip;
	private String mask;
	private String gateway;

	@XmlElement
	public String getSerialNumber()
	{
		return serialNumber;
	}

	@XmlElement
	public void setSerialNumber(String serialNumber)
	{
		this.serialNumber = serialNumber;
	}

	@XmlElement
	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	@XmlElement
	public String getGsnNumber()
	{
		return gsnNumber;
	}

	public void setGsnNumber(String gsnNumber)
	{
		this.gsnNumber = gsnNumber;
	}

	@XmlElement
	public String getApnNumber()
	{
		return apnNumber;
	}

	public void setApnNumber(String apnNumber)
	{
		this.apnNumber = apnNumber;
	}

	@XmlElement
	public Date getResgistionDate()
	{
		return resgistionDate;
	}

	public void setResgistionDate(Date resgistionDate)
	{
		this.resgistionDate = resgistionDate;
	}

	@XmlElement
	public Date getLastActivationDate()
	{
		return lastActivationDate;
	}

	public void setLastActivationDate(Date lastActivationDate)
	{
		this.lastActivationDate = lastActivationDate;
	}

	@XmlElement
	public Date getLastDeactivationDate()
	{
		return lastDeactivationDate;
	}

	public void setLastDeactivationDate(Date lastDeactivationDate)
	{
		this.lastDeactivationDate = lastDeactivationDate;
	}

	@XmlElement
	public String getIp()
	{
		return ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	@XmlElement
	public String getMask()
	{
		return mask;
	}

	public void setMask(String mask)
	{
		this.mask = mask;
	}

	@XmlElement
	public String getGateway()
	{
		return gateway;
	}

	public void setGateway(String gateway)
	{
		this.gateway = gateway;
	}

	public String getIdClient()
	{
		return idClient;
	}

	public void setIdClient(String idClient)
	{
		this.idClient = idClient;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, new ToStringPersonalizado());
	}

}
