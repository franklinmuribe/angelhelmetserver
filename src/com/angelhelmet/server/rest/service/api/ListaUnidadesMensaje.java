package com.angelhelmet.server.rest.service.api;

import javax.xml.bind.annotation.XmlElement;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

public class ListaUnidadesMensaje
{
	private Integer helmetId;
	private String helmetSerial;
	
	@XmlElement
	public Integer getHelmetId()
	{
		return helmetId;
	}
	public void setHelmetId(Integer helmetId)
	{
		this.helmetId = helmetId;
	}
	@XmlElement
	public String getHelmetSerial()
	{
		return helmetSerial;
	}
	public void setHelmetSerial(String helmetSerial)
	{
		this.helmetSerial = helmetSerial;
	}
	
	@Override
	public String toString()
	{
		return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
	}
}
