package com.angelhelmet.server.datos;

import com.angelhelmet.server.negocio.kalman.KalmanOut;

public class datosPunto
{
	private datosCoordenada _coordenadas;

	private KalmanOut _valor_inicial_x;
	private KalmanOut _valor_inicial_y;

	private boolean _primer_punto_x;
	private boolean _primer_punto_y;
	
	private String _mode;

	public String get_mode() {
		return _mode;
	}

	public void set_mode(String _mode) {
		this._mode = _mode;
	}

	public datosPunto()
	{
		_valor_inicial_x = new KalmanOut();
		_valor_inicial_y = new KalmanOut();
		_coordenadas = new datosCoordenada();

		_primer_punto_x = true;
		_primer_punto_y = true;
	}

	public datosPunto(double x, double y)
	{
		this._coordenadas.setX(x);
		this._coordenadas.setY(y);
	}

	public datosPunto toInt()
	{
		this._coordenadas.setX((int) Math.round(this._coordenadas.getX()));
		this._coordenadas.setY((int) Math.round(this._coordenadas.getY()));
		return this;
	}

	public KalmanOut getValorInicialX()
	{
		return _valor_inicial_x;
	}

	public void setValorInicialX(KalmanOut valor_inicial_x)
	{
		this._valor_inicial_x = valor_inicial_x;
	}

	public KalmanOut getValorInicialY()
	{
		return _valor_inicial_y;
	}

	public void setValorInicialY(KalmanOut valor_inicial_y)
	{
		this._valor_inicial_y = valor_inicial_y;
	}

	public boolean esPrimerPuntoX()
	{
		return _primer_punto_x;
	}

	public void setPrimerPuntoX(boolean primer_punto_x)
	{
		this._primer_punto_x = primer_punto_x;
	}

	public boolean esPrimerPuntoY()
	{
		return _primer_punto_y;
	}

	public void setPrimerPuntoY(boolean primer_punto_y)
	{
		this._primer_punto_y = primer_punto_y;
	}

	public datosCoordenada getCoordenadas()
	{
		return _coordenadas;
	}

	public void setCoordenadas(datosCoordenada coordenadas)
	{
		this._coordenadas = coordenadas;
	}
}
