package com.angelhelmet.server.datos;

public class datosGestion
{
	private int _id_unidad;
	private long _id_log;
	private String _texto_mensaje;

	public datosGestion()
	{
	}

	public int getIdUnidad()
	{
		return _id_unidad;
	}

	public void setIdUnidad(int id_unidad)
	{
		this._id_unidad = id_unidad;
	}

	public long getIdLog()
	{
		return _id_log;
	}

	public void setIdLog(long id_log)
	{
		this._id_log = id_log;
	}

	public String getTextoMensaje()
	{
		return _texto_mensaje;
	}

	public void setTextoMensaje(String texto_mensaje)
	{
		this._texto_mensaje = texto_mensaje;
	}
}
