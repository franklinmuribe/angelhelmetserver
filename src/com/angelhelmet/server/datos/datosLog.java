package com.angelhelmet.server.datos;

import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.MapaEB;
import com.angelhelmet.server.persistencia.UnidadEB;

public class datosLog
{
	private datosComunicacion _datos;
	private UnidadEB _unidad;
	private AlarmaEB _alarma;
	private MapaEB _mapa;
	private LogEB _log;

	public datosLog() {
		_datos = new datosComunicacion();
		_unidad = new UnidadEB();
		_alarma = new AlarmaEB();
		_mapa = new MapaEB();
		_log = new LogEB();
	}
	
	public datosComunicacion getDatos()
	{
		return _datos;
	}

	public void setDatos(datosComunicacion _datos)
	{
		this._datos = _datos;
	}

	public UnidadEB getUnidad()
	{
		return _unidad;
	}

	public void setUnidad(UnidadEB _unidad)
	{
		this._unidad = _unidad;
	}

	public AlarmaEB getAlarma()
	{
		return _alarma;
	}

	public void setAlarma(AlarmaEB _alarma)
	{
		this._alarma = _alarma;
	}

	public MapaEB getMapa()
	{
		return _mapa;
	}

	public void setMapa(MapaEB _mapa)
	{
		this._mapa = _mapa;
	}

	public LogEB getLog()
	{
		return _log;
	}

	public void setLog(LogEB _log)
	{
		this._log = _log;
	}

}
