package com.angelhelmet.server.datos;

import com.angelhelmet.server.util.TipoAlarma;

public class datosPermisoZona
{
	private int _id_zona_alarma;
	private TipoAlarma _alarma;

	public datosPermisoZona()
	{
	}

	public int getIdZonaAlarma()
	{
		return _id_zona_alarma;
	}

	public void setIdZonaAlarma(int id_zona_alarma)
	{
		this._id_zona_alarma = id_zona_alarma;
	}

	public TipoAlarma getAlarma()
	{
		return _alarma;
	}

	public void setAlarma(int id_alarma)
	{
		switch (id_alarma)
		{
		case 5:
			this._alarma = TipoAlarma.ENTRADAZONA;
			break;
		case 6:
			this._alarma = TipoAlarma.SALIDAZONA;
			break;
		default:
			this._alarma = TipoAlarma.PERMANENCIAZONA;
			break;
		}
	}

}
