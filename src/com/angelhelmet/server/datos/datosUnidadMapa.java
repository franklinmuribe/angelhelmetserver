package com.angelhelmet.server.datos;

import java.util.Map;

public class datosUnidadMapa extends datosUnidadMapaWS
{
	private datosMapa _mapa;
	private String _texto_mensaje;
	private datosAlarma _alarma_anterior;
	private datosAlarma _alarma_unidad_asignada;
	private datosAlarma _alarma_unidad_activa;
	private boolean _hay_gas;
	private boolean _gas_otros;
	private long _firma_gas;
	//private datosAlarma currAlarm ;

	public datosUnidadMapa()
	{
		_mapa = new datosMapa();
		_alarma_anterior = new datosAlarma();
		_alarma_unidad_asignada = new datosAlarma();
		_alarma_unidad_activa = new datosAlarma();
	}

	public datosMapa getMapa()
	{
		return _mapa;
	}

	public void setMapa(datosMapa _mapa)
	{
		this._mapa = _mapa;
	}

	public String getTextoMensaje()
	{
		return _texto_mensaje;
	}

	public void setTextoMensaje(String _texto_mensaje)
	{
		this._texto_mensaje = _texto_mensaje;
	}
	
	public datosAlarma getAlarmaAnterior()
	{
		return _alarma_anterior;
	}

	public void setAlarmaAnterior(datosAlarma alarma_anterior)
	{
		this._alarma_anterior = alarma_anterior;
	}
	
	public boolean HayGas()
	{
		return _hay_gas;
	}

	public void setHayGas(boolean hay_gas)
	{
		this._hay_gas = hay_gas;
	}

	public boolean GasOtros()
	{
		return _gas_otros;
	}

	public void setGasOtros(boolean gas_otros)
	{
		this._gas_otros = gas_otros;
	}

	public long getFirmaGas()
	{
		return _firma_gas;
	}

	public void setFirmaGas(long firma_gas)
	{
		this._firma_gas = firma_gas;
	}
	
	public datosAlarma getAlarmaUnidadAsignada()
	{
		return _alarma_unidad_asignada;
	}

	public void setAlarmaUnidadAsignada(datosAlarma alarma_unidad_asignada)
	{
		this._alarma_unidad_asignada = alarma_unidad_asignada;
	}

	public datosAlarma getAlarmaUnidadActiva()
	{
		return _alarma_unidad_activa;
	}

	public void setAlarmaUnidadActiva(datosAlarma alarma_unidad_activa)
	{
		this._alarma_unidad_activa = alarma_unidad_activa;
	}

//	public datosAlarma getCurrAlarm() {
//		return currAlarm;
//	}
//
//	public void setCurrAlarm(datosAlarma currAlarm) {
//		this.currAlarm = currAlarm;
//	}
	
	@Override
	public String toString()
	{
		StringBuilder ret = new StringBuilder();

		ret.append("Mapa: " + this.getIdMapa() + "\n");
		ret.append("Unidad: " + this.getUnidad().getIdUnidad() + "\n");
		ret.append("Posicion: X=" + this.getPosicion().getCoordenadas().getX() + " Y=" + this.getPosicion().getCoordenadas().getY() + "\n");
		ret.append("id_unidadmnesaje: " + this.getDatosMensajeWS().getIdUnidadMensaje() + "\n");
		ret.append("Estado mensaje: " + this.getDatosMensajeWS().getEstadoMensaje() + "\n");
		ret.append("Estado mensaje anterior: " + this.getDatosMensajeWS().getEstadoMensajeAnterior() + "\n");
		ret.append("*******" + "\n");
		ret.append("ALARMAS" + "\n");
		ret.append("*******" + "\n");
		
		for (Map.Entry<Long, datosAlarma> item : getAlarmas().entrySet()) {
			ret.append("Alarma: " + item.getValue().getAlarma() + "\n");
			ret.append("\tFirma: " +item.getValue().getFirma() + "\n");
			ret.append("\tCancela a " + item.getValue().getCancelaAlarma() + "\n");
		}
		
		ret.append("======================================================================");
		return ret.toString();
	}
}
