package com.angelhelmet.server.datos;

public class datosGps
{
	private double latitud;
	private double longitud;

	public datosGps()
	{
	}

	public datosGps(double latitud, double longitud)
	{
		this.latitud = latitud;
		this.longitud = longitud;
	}

	public double getLatitud()
	{
		return latitud;
	}

	public void setLatitud(double latitud)
	{
		this.latitud = latitud;
	}

	public double getLongitud()
	{
		return longitud;
	}

	public void setLongitud(double longitud)
	{
		this.longitud = longitud;
	}
}
