package com.angelhelmet.server.datos;

public class datosTiempoVida
{
	private int _horas;
	private int _minutos;
	private int _segundos;
	
	public datosTiempoVida(int tiempo)
	{
		_horas = tiempo / 3600;
		_minutos = (tiempo - (3600 * _horas)) / 60;
		_segundos = tiempo - ((_horas * 3600) + (_minutos * 60));
	}

	public int getHoras()
	{
		return _horas;
	}

	public void setHoras(int horas)
	{
		this._horas = horas;
	}

	public int getMinutos()
	{
		return _minutos;
	}

	public void setMinutos(int minutos)
	{
		this._minutos = minutos;
	}

	public int getSegundos()
	{
		return _segundos;
	}

	public void setSegundos(int segundos)
	{
		this._segundos = segundos;
	}

}
