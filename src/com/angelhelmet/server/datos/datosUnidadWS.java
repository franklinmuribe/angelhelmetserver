package com.angelhelmet.server.datos;

public class datosUnidadWS {
	private int _id_unidad;
	private String _serie;
	private int _sensor_x;
	private int _sensor_y;
	private int _sensor_z;
	private int _bateria;
	private int _gas;
	private datosTiempoVida _tiempo_vida;
	private Long _timeLastReport;
	// private TipoAlarma _casco_apagado;

	public datosUnidadWS() {
	}

	public int getIdUnidad() {
		return _id_unidad;
	}

	public void setIdUnidad(int id_unidad) {
		this._id_unidad = id_unidad;
	}

	public String getSerie() {
		return _serie;
	}

	public void setSerie(String serie) {
		this._serie = serie;
	}

	public int getSensorX() {
		return _sensor_x;
	}

	public void setSensorX(int sensor_x) {
		this._sensor_x = sensor_x;
	}

	public int getSensorY() {
		return _sensor_y;
	}

	public void setSensorY(int sensor_y) {
		this._sensor_y = sensor_y;
	}

	public int getSensorZ() {
		return _sensor_z;
	}

	public void setSensorZ(int sensor_z) {
		this._sensor_z = sensor_z;
	}

//	public TipoAlarma getCascoApagado()
//	{
//		return _casco_apagado;
//	}
//
//	public void setCascoApagado(TipoAlarma casco_apagado)
//	{
//		this._casco_apagado = casco_apagado;
//	}

	public int getBateria() {
		return _bateria;
	}

	public void setBateria(int bateria) {
		this._bateria = bateria;
	}

	public int getGas() {
		return _gas;
	}

	public void setGas(int gas) {
		this._gas = gas;
	}

	public datosTiempoVida getTiempoVida() {
		return _tiempo_vida;
	}

	public void setTiempoVida(datosTiempoVida tiempo) {
		this._tiempo_vida = tiempo;
	}

	public Long getTimeLastReport() {
		return _timeLastReport;
	}

	public void setTimeLastReport(Long _timeLastReport) {
		this._timeLastReport = _timeLastReport;
	}
	
	
	
}
