package com.angelhelmet.server.datos;

public class datosUmbral {
	private int id_umbral;
	private  int impacto_x;
	private int impacto_y;
	private int impacto_z;
	private Integer gas;
	private int prealarma;
	private int estatico;
	private boolean casco_caido;
	private int tracking;
	private String apn;
	private String apn_user;
	private String apn_pwd;
	private String host_gprs;
	private String puerto;
	private String ssid;
	private String wifi_pwd;
	private int id_encriptacion;
	private String host_wifi;
	private int canales;
	private boolean comunicacion_casset;
	private String telefono;
	private String telefono_sos;
	private int id_tipoumbral; //tipoumbral table
	private boolean coronavirus_proximity;
	private boolean ble_health;
	
	
	public int getId_umbral() {
		return id_umbral;
	}
	public void setId_umbral(int id_umbral) {
		this.id_umbral = id_umbral;
	}
	public int getImpacto_x() {
		return impacto_x;
	}
	public void setImpacto_x(int impacto_x) {
		this.impacto_x = impacto_x;
	}
	public int getImpacto_y() {
		return impacto_y;
	}
	public void setImpacto_y(int impacto_y) {
		this.impacto_y = impacto_y;
	}
	public int getImpacto_z() {
		return impacto_z;
	}
	public void setImpacto_z(int impacto_z) {
		this.impacto_z = impacto_z;
	}
	public Integer getGas() {
		return gas;
	}
	public void setGas(Integer gas) {
		this.gas = gas;
	}
	public int getPrealarma() {
		return prealarma;
	}
	public void setPrealarma(int prealarma) {
		this.prealarma = prealarma;
	}
	public int getEstatico() {
		return estatico;
	}
	public void setEstatico(int estatico) {
		this.estatico = estatico;
	}
	public boolean isCasco_caido() {
		return casco_caido;
	}
	public void setCasco_caido(boolean casco_caido) {
		this.casco_caido = casco_caido;
	}
	public int getTracking() {
		return tracking;
	}
	public void setTracking(int tracking) {
		this.tracking = tracking;
	}
	public String getApn() {
		return apn;
	}
	public void setApn(String apn) {
		this.apn = apn;
	}
	public String getApn_user() {
		return apn_user;
	}
	public void setApn_user(String apn_user) {
		this.apn_user = apn_user;
	}
	public String getApn_pwd() {
		return apn_pwd;
	}
	public void setApn_pwd(String apn_pwd) {
		this.apn_pwd = apn_pwd;
	}
	public String getHost_gprs() {
		return host_gprs;
	}
	public void setHost_gprs(String host_gprs) {
		this.host_gprs = host_gprs;
	}
	public String getPuerto() {
		return puerto;
	}
	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public String getWifi_pwd() {
		return wifi_pwd;
	}
	public void setWifi_pwd(String wifi_pwd) {
		this.wifi_pwd = wifi_pwd;
	}
	public int getId_encriptacion() {
		return id_encriptacion;
	}
	public void setId_encriptacion(int id_encriptacion) {
		this.id_encriptacion = id_encriptacion;
	}
	public String getHost_wifi() {
		return host_wifi;
	}
	public void setHost_wifi(String host_wifi) {
		this.host_wifi = host_wifi;
	}
	public int getCanales() {
		return canales;
	}
	public void setCanales(int canales) {
		this.canales = canales;
	}
	public boolean isComunicacion_casset() {
		return comunicacion_casset;
	}
	public void setComunicacion_casset(boolean comunicacion_casset) {
		this.comunicacion_casset = comunicacion_casset;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getTelefono_sos() {
		return telefono_sos;
	}
	public void setTelefono_sos(String telefono_sos) {
		this.telefono_sos = telefono_sos;
	}
	public int getId_tipoumbral() {
		return id_tipoumbral;
	}
	public void setId_tipoumbral(int id_tipoumbral) {
		this.id_tipoumbral = id_tipoumbral;
	}
	public boolean isCoronavirus_proximity() {
		return coronavirus_proximity;
	}
	public void setCoronavirus_proximity(boolean coronavirus_proximity) {
		this.coronavirus_proximity = coronavirus_proximity;
	}
	public boolean isBle_health() {
		return ble_health;
	}
	public void setBle_health(boolean ble_health) {
		this.ble_health = ble_health;
	}
	
	
	

}
