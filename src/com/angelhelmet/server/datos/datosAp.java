package com.angelhelmet.server.datos;

import java.util.ArrayList;
import java.util.List;

import com.angelhelmet.server.negocio.kalman.KalmanOut;
import com.angelhelmet.server.util.TipoPotencia;

public class datosAp
{
	private int _id;
	private String _mac;
	private int _coordenada_x;
	private int _coordenada_y;
	private double _potencia;

	private KalmanOut _valor_inicial;
	private datosCoordenada _ponderado_adyacente;
	private List<datosAp> _aps_adyacentes;

	public datosAp()
	{
		_aps_adyacentes = new ArrayList<datosAp>();
		_valor_inicial = new KalmanOut();
		_ponderado_adyacente = null;
	}

	public int getId()
	{
		return _id;
	}

	public void setId(int id)
	{
		this._id = id;
	}

	public String getMac()
	{
		return _mac;
	}

	public void setMac(String _mac)
	{
		this._mac = _mac;
	}

	public int getCoordenadaX()
	{
		return _coordenada_x;
	}

	public void setCoordenadaX(int _coordenada_x)
	{
		this._coordenada_x = _coordenada_x;
	}

	public int getCoordenadaY()
	{
		return _coordenada_y;
	}

	public void setCoordenadaY(int _coordenada_y)
	{
		this._coordenada_y = _coordenada_y;
	}

	public List<datosAp> getApsAdyacentes()
	{
		return _aps_adyacentes;
	}

	public void setApsAdyacentes(List<datosAp> aps_adyacentes)
	{
		this._aps_adyacentes = aps_adyacentes;
	}

	public KalmanOut getValorInicial()
	{
		return _valor_inicial;
	}

	public void setValorInicial(KalmanOut valor)
	{
		this._valor_inicial = valor;
	}

	public datosCoordenada getPonderadoAdyacente()
	{
		return _ponderado_adyacente;
	}

	public void setPonderadoAdyacente(datosCoordenada ponderado_adyacente)
	{
		this._ponderado_adyacente = ponderado_adyacente;
	}

	public double getPotencia()
	{
		return _potencia;
	}

	public void setPotencia(double potencia, TipoPotencia tipo)
	{
		if (TipoPotencia.Real == tipo)
		{
			if (potencia == 0) potencia = 80;
			potencia = (80 - potencia);
		}
		this._potencia = potencia;
	}

	public void setPotencia(double potencia)
	{
		this._potencia = potencia;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();

		sb.append("AP " + this._mac + "\n");
		sb.append("\t" + "ID: " + this._id + "\n");
		sb.append("\t" + "Coordenadas: (" + this._coordenada_x + "," + this._coordenada_y + ")" + "\n");
		sb.append("\t" + "Potencia: " + this._potencia + "\n");
		sb.append("\t" + this._valor_inicial.toString() + "\n");
		sb.append("\t" + this._ponderado_adyacente.toString() + "\n");

		sb.append("\tLista de APs Adyacentes\n");

		for (datosAp item : this._aps_adyacentes)
		{
			sb.append("\t\tAP ID: " + item.getId() + "\n");
		}
		return sb.toString();
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (!(obj instanceof datosAp)) return false;

		datosAp aux = (datosAp) obj;
		return this._id == aux._id;
	}

	@Override
	public int hashCode()
	{
		int hash = 1;
		hash = hash * 31 + _aps_adyacentes.hashCode();
		hash = hash * 31 + (_valor_inicial == null ? 0 : _valor_inicial.hashCode());

		return hash;
	}

}
