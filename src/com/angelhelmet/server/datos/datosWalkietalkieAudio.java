package com.angelhelmet.server.datos;

import java.sql.Timestamp;

public class datosWalkietalkieAudio {

	private Long _id_mensaje_helmet_wt;
	private Long _id_mensajeaudio_wt;

	private Long _id_estadomensaje_wt;
	private String _estadomensaje_wt;

	private String _id_tipomensajeaudio_wt;
	private String _tipomensajeaudio_wt;

	private String _nombre_archivo;

	private Long _id_unidad;
	private String _numero_serie;

	private Long _id_operario;

	private Long _firma;

	private Integer _reenvio;

	private Timestamp _fecha_creacion;
	private Timestamp _fecha_envio;
	private Timestamp _fecha_confirmacion;
	private Timestamp _fecha_error;

	private Long _id_transaccion;
	private Long _id_estadoenvio_wt;

	public Long get_id_mensaje_helmet_wt() {
		return _id_mensaje_helmet_wt;
	}

	public void set_id_mensaje_helmet_wt(Long _id_mensaje_helmet_wt) {
		this._id_mensaje_helmet_wt = _id_mensaje_helmet_wt;
	}

	public Long get_id_mensajeaudio_wt() {
		return _id_mensajeaudio_wt;
	}

	public void set_id_mensajeaudio_wt(Long _id_mensajeaudio_wt) {
		this._id_mensajeaudio_wt = _id_mensajeaudio_wt;
	}

	public Long get_id_estadomensaje_wt() {
		return _id_estadomensaje_wt;
	}

	public void set_id_estadomensaje_wt(Long _id_estadomensaje_wt) {
		this._id_estadomensaje_wt = _id_estadomensaje_wt;
	}

	public String get_estadomensaje_wt() {
		return _estadomensaje_wt;
	}

	public void set_estadomensaje_wt(String _estadomensaje_wt) {
		this._estadomensaje_wt = _estadomensaje_wt;
	}

	public String get_id_tipomensajeaudio_wt() {
		return _id_tipomensajeaudio_wt;
	}

	public void set_id_tipomensajeaudio_wt(String _id_tipomensajeaudio_wt) {
		this._id_tipomensajeaudio_wt = _id_tipomensajeaudio_wt;
	}

	public String get_tipomensajeaudio_wt() {
		return _tipomensajeaudio_wt;
	}

	public void set_tipomensajeaudio_wt(String _tipomensajeaudio_wt) {
		this._tipomensajeaudio_wt = _tipomensajeaudio_wt;
	}

	public String get_nombre_archivo() {
		return _nombre_archivo;
	}

	public void set_nombre_archivo(String _nombre_archivo) {
		this._nombre_archivo = _nombre_archivo;
	}

	public Long get_id_unidad() {
		return _id_unidad;
	}

	public void set_id_unidad(Long _id_unidad) {
		this._id_unidad = _id_unidad;
	}

	public String get_numero_serie() {
		return _numero_serie;
	}

	public void set_numero_serie(String _numero_serie) {
		this._numero_serie = _numero_serie;
	}

	public Long get_id_operario() {
		return _id_operario;
	}

	public void set_id_operario(Long _id_operario) {
		this._id_operario = _id_operario;
	}

	public Long get_firma() {
		return _firma;
	}

	public void set_firma(Long _firma) {
		this._firma = _firma;
	}

	public Integer get_reenvio() {
		return _reenvio;
	}

	public void set_reenvio(Integer _reenvio) {
		this._reenvio = _reenvio;
	}

	public Timestamp get_fecha_creacion() {
		return _fecha_creacion;
	}

	public void set_fecha_creacion(Timestamp _fecha_creacion) {
		this._fecha_creacion = _fecha_creacion;
	}

	public Timestamp get_fecha_envio() {
		return _fecha_envio;
	}

	public void set_fecha_envio(Timestamp _fecha_envio) {
		this._fecha_envio = _fecha_envio;
	}

	public Timestamp get_fecha_confirmacion() {
		return _fecha_confirmacion;
	}

	public void set_fecha_confirmacion(Timestamp _fecha_confirmacion) {
		this._fecha_confirmacion = _fecha_confirmacion;
	}

	public Timestamp get_fecha_error() {
		return _fecha_error;
	}

	public void set_fecha_error(Timestamp _fecha_error) {
		this._fecha_error = _fecha_error;
	}

	public Long get_id_transaccion() {
		return _id_transaccion;
	}

	public void set_id_transaccion(Long _id_transaccion) {
		this._id_transaccion = _id_transaccion;
	}

	public Long get_id_estadoenvio_wt() {
		return _id_estadoenvio_wt;
	}

	public void set_id_estadoenvio_wt(Long _id_estadoenvio_wt) {
		this._id_estadoenvio_wt = _id_estadoenvio_wt;
	}

}
