package com.angelhelmet.server.datos;

import java.util.Comparator;

public class datosCoordenada implements Comparator<datosCoordenada>
{
	private double _x;
	private double _y;

	public datosCoordenada()
	{
	}

	public datosCoordenada(double x, double y)
	{
		this._x = x;
		this._y = y;
	}

	public double getX()
	{
		return _x;
	}

	public void setX(double x)
	{
		this._x = x;
	}

	public double getY()
	{
		return _y;
	}

	public void setY(double y)
	{
		this._y = y;
	}

	public datosCoordenada toInt()
	{
		this._x = (int) Math.round(this._x);
		this._y = (int) Math.round(_y);
		return this;
	}

	@Override
	public String toString()
	{
		return "Coordenadas (X: " + this._x + ", Y: " + this._y + ")";
	}

	@Override
	public boolean equals(Object obj)
	{
		if (obj == null) return false;
		if (!(obj instanceof datosCoordenada)) return false;

		datosCoordenada aux = (datosCoordenada) obj;
		return this._x == aux._x && this._y == aux._y;
	}

	@Override
	public int hashCode()
	{
		int hash = 1;
		hash = hash * 17 + (int) this._x;
		hash = hash * 31 + (int) this._y;

		return hash;
	}

	@Override
	public int compare(datosCoordenada a, datosCoordenada b)
	{
		if (a._x < b._x)
		{
			return -1;
		}
		else if (a._x > b._x)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}

}
