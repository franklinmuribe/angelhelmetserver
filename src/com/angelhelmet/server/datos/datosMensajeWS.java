package com.angelhelmet.server.datos;

import com.angelhelmet.server.util.EstadoMensajeEntrada;

public class datosMensajeWS
{
	private EstadoMensajeEntrada _estado_mensaje;
	private EstadoMensajeEntrada _estado_mensaje_anterior;
	private long _id_unidadmensaje;

	public datosMensajeWS()
	{
	}

	public void setEstadoMensaje(EstadoMensajeEntrada estado)
	{
		this._estado_mensaje = estado;
	}

	public EstadoMensajeEntrada getEstadoMensaje()
	{
		return this._estado_mensaje;
	}

	public long getIdUnidadMensaje()
	{
		return _id_unidadmensaje;
	}

	public void setIdUnidadMensaje(long id_unidadmensaje)
	{
		this._id_unidadmensaje = id_unidadmensaje;
	}

	public EstadoMensajeEntrada getEstadoMensajeAnterior()
	{
		return _estado_mensaje_anterior;
	}

	public void setEstadoMensajeAnterior(EstadoMensajeEntrada estado_mensaje_anterior)
	{
		this._estado_mensaje_anterior = estado_mensaje_anterior;
	}
}
