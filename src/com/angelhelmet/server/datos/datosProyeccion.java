package com.angelhelmet.server.datos;

public class datosProyeccion
{
	private datosCoordenada _punto;
	private int _indice_nodo;

	public datosProyeccion()
	{
		_punto = new datosCoordenada();
	}

	public datosCoordenada getPunto()
	{
		return _punto;
	}

	public void setPunto(datosCoordenada punto)
	{
		this._punto = punto;
	}

	public int getIndiceNodo()
	{
		return _indice_nodo;
	}

	public void setIndiceNodo(int indice_nodo)
	{
		this._indice_nodo = indice_nodo;
	}

}
