package com.angelhelmet.server.datos;

import java.sql.Timestamp;

public class datosWalkietalkieImei {

	private Long _id_unidad;
	private String _nmro_serie;
	private Long _id_operario;
	private boolean _wt_unit;
	private Integer _wt_channel;
	private boolean _wt_blocked;
	private String _descripcion;
	private Timestamp _fecha_alta;

	public Long get_id_unidad() {
		return _id_unidad;
	}

	public void set_id_unidad(Long _id_unidad) {
		this._id_unidad = _id_unidad;
	}

	public String get_nmro_serie() {
		return _nmro_serie;
	}

	public void set_nmro_serie(String _nmro_serie) {
		this._nmro_serie = _nmro_serie;
	}

	public Long get_id_operario() {
		return _id_operario;
	}

	public void set_id_operario(Long _id_operario) {
		this._id_operario = _id_operario;
	}

	public boolean is_wt_unit() {
		return _wt_unit;
	}

	public void set_wt_unit(boolean _wt_unit) {
		this._wt_unit = _wt_unit;
	}

	public Integer get_wt_channel() {
		return _wt_channel;
	}

	public void set_wt_channel(Integer _wt_channel) {
		this._wt_channel = _wt_channel;
	}

	public boolean is_wt_blocked() {
		return _wt_blocked;
	}

	public void set_wt_blocked(boolean _wt_blocked) {
		this._wt_blocked = _wt_blocked;
	}

	public String get_descripcion() {
		return _descripcion;
	}

	public void set_descripcion(String _descripcion) {
		this._descripcion = _descripcion;
	}

	public Timestamp get_fecha_alta() {
		return _fecha_alta;
	}

	public void set_fecha_alta(Timestamp _fecha_alta) {
		this._fecha_alta = _fecha_alta;
	}

}
