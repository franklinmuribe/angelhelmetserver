package com.angelhelmet.server.datos;


public class datosOperarioWS
{
	private int idOperario;
	private String nombre;
	private String apellidos;
	private String identificacion;
	private String empresa;
	private Integer empresaId;
	private String cargo;
	private Integer cargoId;
	
	private String supervisor;
	private String blood;
	private String emergency;
	private String smsNumber;
	
	public int getIdOperario()
	{
		return idOperario;
	}
	public void setIdOperario(int idOperario)
	{
		this.idOperario = idOperario;
	}
	public String getNombre()
	{
		return nombre;
	}
	public void setNombre(String nombre)
	{
		this.nombre = nombre;
	}
	public String getApellidos()
	{
		return apellidos;
	}
	public void setApellidos(String apellidos)
	{
		this.apellidos = apellidos;
	}
	public String getIdentificacion()
	{
		return identificacion;
	}
	public void setIdentificacion(String identificacion)
	{
		this.identificacion = identificacion;
	}
	public String getEmpresa()
	{
		return empresa;
	}
	public void setEmpresa(String empresa)
	{
		this.empresa = empresa;
	}
	public Integer getEmpresaId() {
		return empresaId;
	}
	public void setEmpresaId(Integer integer) {
		this.empresaId = integer;
	}
	public String getCargo()
	{
		return cargo;
	}
	public void setCargo(String cargo)
	{
		this.cargo = cargo;
	}
	public String getSupervisor()
	{
		return supervisor;
	}
	public void setSupervisor(String supervisor)
	{
		this.supervisor = supervisor;
	}
	public String getBlood()
	{
		return blood;
	}
	public void setBlood(String blood)
	{
		this.blood = blood;
	}
	public String getEmergency()
	{
		return emergency;
	}
	public void setEmergency(String emergency)
	{
		this.emergency = emergency;
	}
	public String getSmsNumber()
	{
		return smsNumber;
	}
	public void setSmsNumber(String smsNumber)
	{
		this.smsNumber = smsNumber;
	}
	public Integer getCargoId() {
		return cargoId;
	}
	public void setCargoId(Integer cargoId) {
		this.cargoId = cargoId;
	}
}
