package com.angelhelmet.server.datos;

import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.TreeMap;

// http://javarevisited.blogspot.com.es/2013/02/concurrent-collections-from-jdk-56-java-example-tutorial.html
public class datosUnidadMapaWS
{
	private Map<Long, datosAlarma> _alarmas;
	private datosPunto _posicion;
	private datosZona _zona;
	private datosUnidadWS _unidad;
	private long _fecha_log;
	private datosMensajeWS _datos_mensaje_ws;
	private datosGps _datos_gps;
	private datosOperarioWS _operario;

	private int _id_mapa;
	private long _id_log;

	public datosUnidadMapaWS()
	{
		_posicion = new datosPunto();
		_zona = new datosZona();
		_datos_gps = new datosGps();
		_operario = new datosOperarioWS();
		
		_alarmas = Collections.synchronizedMap(new TreeMap<Long, datosAlarma>(new Comparator<Long>()
		{
			@Override
			public int compare(Long o1, Long o2)
			{
				return o1.compareTo(o2);
			}
		}));
		_unidad = new datosUnidadWS();
		_datos_mensaje_ws = new datosMensajeWS();
	}

	public Map<Long, datosAlarma> getAlarmas()
	{
		return _alarmas;
	}

	public void setAlarmas(Map<Long, datosAlarma> alarmas)
	{
		this._alarmas = alarmas;
	}

	public datosPunto getPosicion()
	{
		return _posicion;
	}

	public void setPosicion(datosPunto posicion)
	{
		this._posicion = posicion;
	}

	public int getIdMapa()
	{
		return _id_mapa;
	}

	public void setIdMapa(int id_mapa)
	{
		this._id_mapa = id_mapa;
	}

	public long getFechaLog()
	{
		return _fecha_log;
	}

	public void setFechaLog(long fecha_log)
	{
		this._fecha_log = fecha_log;
	}

	public datosUnidadWS getUnidad()
	{
		return _unidad;
	}

	public void setUnidad(datosUnidadWS unidad)
	{
		this._unidad = unidad;
	}

	public datosMensajeWS getDatosMensajeWS()
	{
		return _datos_mensaje_ws;
	}

	public void setDatosMensajeWS(datosMensajeWS datos_mensaje_ws)
	{
		this._datos_mensaje_ws = datos_mensaje_ws;
	}

	public long getIdLog()
	{
		return _id_log;
	}

	public void setIdLog(long id_log)
	{
		this._id_log = id_log;
	}

	public datosZona getZona()
	{
		return _zona;
	}

	public void setZona(datosZona zona)
	{
		this._zona = zona;
	}

	public datosGps getDatosGps()
	{
		return _datos_gps;
	}

	public void setDatosGps(datosGps datos_gps)
	{
		this._datos_gps = datos_gps;
	}

	public datosOperarioWS getOperario()
	{
		return _operario;
	}

	public void setOperario(datosOperarioWS operario)
	{
		this._operario = operario;
	}
}
