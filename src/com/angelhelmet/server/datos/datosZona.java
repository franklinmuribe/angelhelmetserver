package com.angelhelmet.server.datos;

import java.util.Date;

import com.angelhelmet.server.util.EstadoAlarma;

public class datosZona
{
	private Date _tiempo_entrada;
	private String _nombre_zona;
	private int _id_zona;
	private int _ultima_zona;
	private EstadoAlarma _alarma_entrada;
	private EstadoAlarma _alarma_salida;
	private EstadoAlarma _alarma_permanencia;
	
	private boolean _esta_en_zona;

	public datosZona()
	{
		_alarma_entrada = EstadoAlarma.NINGUNA;
		_alarma_permanencia = EstadoAlarma.NINGUNA;
		_alarma_salida = EstadoAlarma.NINGUNA;
	}

	public String getNombreZona()
	{
		return _nombre_zona;
	}

	public void setNombreZona(String nombre_zona)
	{
		this._nombre_zona = nombre_zona;
	}

	public int getIdZona()
	{
		return _id_zona;
	}

	public void setIdZona(int id_zona)
	{
		this._id_zona = id_zona;
	}

	public EstadoAlarma getAlarmaEntrada()
	{
		return _alarma_entrada;
	}

	public void setAlarmaEntrada(EstadoAlarma alarma_entrada)
	{
		this._alarma_entrada = alarma_entrada;
	}

	public EstadoAlarma getAlarmaSalida()
	{
		return _alarma_salida;
	}

	public void setAlarmaSalida(EstadoAlarma alarma_salida)
	{
		this._alarma_salida = alarma_salida;
	}
	
	public EstadoAlarma getAlarmaPermanencia()
	{
		return _alarma_permanencia;
	}

	public void setAlarmaPermanencia(EstadoAlarma alarma_permanencia)
	{
		this._alarma_permanencia = alarma_permanencia;
	}

	public Date getTiempoEntrada()
	{
		return _tiempo_entrada;
	}

	public void setTiempoEntrada(Date _tiempo_entrada)
	{
		this._tiempo_entrada = _tiempo_entrada;
	}

	public int getUltimaZona()
	{
		return _ultima_zona;
	}

	public void setUltimaZona(int ultima_zona)
	{
		this._ultima_zona = ultima_zona;
	}

	public boolean estaEnZona()
	{
		return _esta_en_zona;
	}

	public void setEstaEnZona(boolean esta_en_zona)
	{
		this._esta_en_zona = esta_en_zona;
	}
}
