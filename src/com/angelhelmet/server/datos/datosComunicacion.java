package com.angelhelmet.server.datos;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;

import com.angelhelmet.server.util.TipoAlarma;
import com.angelhelmet.server.util.EstadoMensajeEntrada;
import com.angelhelmet.server.util.ModoFuncionamiento;

public class datosComunicacion {
	private String serie;
	private ModoFuncionamiento modo;
	private TipoAlarma alarma;
	private int tiempo;
	private int gas;
	private int sensor_x;
	private int sensor_y;
	private int sensor_z;
	private float exactitud;
	private int satelites;

	private double latitud;
	private String latitud_c;
	private double longitud;
	private String longitud_c;

	private String mac1;
	private String mac2;
	private String mac3;
	private String mac4;
	private int rssi1;
	private int rssi2;
	private int rssi3;
	private int rssi4;

	private Date fecha;
	private int bateria;
	private EstadoMensajeEntrada mensaje;
	private int temperatura;
	private boolean volteo;
	private String debug;

	private Timestamp fechaLog;
	private long id_log;

	private long firma;
	private int respuesta;
	private String respuestaSms;

	// Walkie Talkie

	private Integer canalWt;
	private Integer modoWt;

	// Salud

	private Integer healthTemperature;
	private Integer healthPressureSistole;
	private Integer healthPressureDiastole;
	private Integer healthOxygenSp02;
	private Integer healthHeartbeat;

	public datosComunicacion() {
	}

	public String getSerie() {
		return this.serie;
	}

	public void setSerie(String serie) {
		this.serie = serie;
	}

	public TipoAlarma getAlarma() {
		return this.alarma;
	}

	public void setAlarma(TipoAlarma alarma) {
		this.alarma = alarma;
	}

	public int getTiempo() {
		return this.tiempo;
	}

	public void setTiempo(int tiempo) {
		this.tiempo = tiempo;
	}

	public int getGas() {
		return this.gas;
	}

	public void setGas(int gas) {
		this.gas = gas;
	}

	public int getSensorX() {
		return this.sensor_x;
	}

	public void setSensorX(int sensorX) {
		this.sensor_x = sensorX;
	}

	public int getSensorY() {
		return this.sensor_y;
	}

	public void setSensorY(int sensorY) {
		this.sensor_y = sensorY;
	}

	public int getSensorZ() {
		return this.sensor_z;
	}

	public void setSensorZ(int sensorZ) {
		this.sensor_z = sensorZ;
	}

	public double getLatitud() {
		return this.latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public String getLatitudC() {
		return this.latitud_c;
	}

	public void setLatitudC(String c) {
		this.latitud_c = c;
	}

	public double getLongitud() {
		return this.longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public String getLongitudC() {
		return this.longitud_c;
	}

	public void setLongitudC(String c) {
		this.longitud_c = c;
	}

	public String getMac1() {
		return this.mac1;
	}

	public void setMac1(String mac1) {
		this.mac1 = mac1;
	}

	public String getMac2() {
		return this.mac2;
	}

	public void setMac2(String mac2) {
		this.mac2 = mac2;
	}

	public String getMac3() {
		return this.mac3;
	}

	public void setMac3(String mac3) {
		this.mac3 = mac3;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getRssi1() {
		return this.rssi1;
	}

	public void setRssi1(int rssi1) {
		this.rssi1 = rssi1;
	}

	public int getRssi2() {
		return this.rssi2;
	}

	public void setRssi2(int rssi2) {
		this.rssi2 = rssi2;
	}

	public int getRssi3() {
		return this.rssi3;
	}

	public void setRssi3(int rssi3) {
		this.rssi3 = rssi3;
	}

	public int getBateria() {
		return bateria;
	}

	public void setBateria(int bateria) {
		this.bateria = bateria;
	}

	public EstadoMensajeEntrada getMensaje() {
		return mensaje;
	}

	public void setMensaje(EstadoMensajeEntrada mensaje) {
		this.mensaje = mensaje;
	}

	public Timestamp getFechaLog() {
		return this.fechaLog;
	}

	public void setFechaLog(Timestamp t) {
		this.fechaLog = t;
		// this.fechaLog_date.setTime(t.getTime());
	}

	public ModoFuncionamiento getModo() {
		return this.modo;
	}

	public void setModo(ModoFuncionamiento modo) {
		this.modo = modo;
	}

	public int getSatelites() {
		return satelites;
	}

	public void setSatelites(int satelites) {
		this.satelites = satelites;
	}

	public float getExactitud() {
		return exactitud;
	}

	public void setExactitud(float exactitud) {
		this.exactitud = exactitud;
	}

	public long getIdLog() {
		return id_log;
	}

	public void setIdLog(long id_log) {
		this.id_log = id_log;
	}

	public String getMac4() {
		return mac4;
	}

	public void setMac4(String mac4) {
		this.mac4 = mac4;
	}

	public int getRssi4() {
		return rssi4;
	}

	public void setRssi4(int rssi4) {
		this.rssi4 = rssi4;
	}

	public int getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(int temperatura) {
		this.temperatura = temperatura;
	}

	public boolean isVolteo() {
		return volteo;
	}

	public void setVolteo(boolean volteo) {
		this.volteo = volteo;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public Long getFirma() {
		return firma;
	}

	public void setFirma(Long firma) {
		this.firma = firma;
	}

	public int getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}

	public String getRespuestaSms() {
		return respuestaSms;
	}

	public void setRespuestaSms(String respuestaSms) {
		this.respuestaSms = respuestaSms;
	}

	public Integer getCanalWt() {
		return canalWt;
	}

	public void setCanalWt(Integer canalWt) {
		this.canalWt = canalWt;
	}

	public Integer getModoWt() {
		return modoWt;
	}

	public void setModoWt(Integer modoWt) {
		this.modoWt = modoWt;
	}

	public Integer getHealthTemperature() {
		return healthTemperature;
	}

	public void setHealthTemperature(Integer healthTemperature) {
		this.healthTemperature = healthTemperature;
	}

	public Integer getHealthPressureSistole() {
		return healthPressureSistole;
	}

	public void setHealthPressureSistole(Integer healthPressureSistole) {
		this.healthPressureSistole = healthPressureSistole;
	}

	public Integer getHealthPressureDiastole() {
		return healthPressureDiastole;
	}

	public void setHealthPressureDiastole(Integer healthPressureDiastole) {
		this.healthPressureDiastole = healthPressureDiastole;
	}

	public Integer getHealthOxygenSp02() {
		return healthOxygenSp02;
	}

	public void setHealthOxygenSp02(Integer healthOxygenSp02) {
		this.healthOxygenSp02 = healthOxygenSp02;
	}

	public Integer getHealthHeartbeat() {
		return healthHeartbeat;
	}

	public void setHealthHeartbeat(Integer healthHeartbeat) {
		this.healthHeartbeat = healthHeartbeat;
	}

}
