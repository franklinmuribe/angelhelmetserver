package com.angelhelmet.server.datos;

import java.util.ArrayList;
import java.util.List;

public class datosMapa
{
	private List<datosAp> _aps_mapa;

	public datosMapa()
	{
		_aps_mapa = new ArrayList<datosAp>();
	}

	public List<datosAp> getApsMapa()
	{
		return this._aps_mapa;
	}

	public void addApMapa(datosAp ap)
	{
		_aps_mapa.add(ap);
	}

	public datosAp getAp(int id)
	{
		datosAp ret = null;
		for (datosAp item : _aps_mapa)
		{
			if (item.getId() == id)
			{
				ret = item;
				break;
			}
		}
		return ret;
	}

	public int getIdApByMac(String mac)
	{
		int ret = 0;
		for (datosAp item : _aps_mapa)
		{
			if (item.getMac().equals(mac))
			{
				ret = item.getId();
				break;
			}
		}
		return ret;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		
		for(datosAp item : _aps_mapa) {
			sb.append(item.toString());
		}
		
		return sb.toString();
	}
}
