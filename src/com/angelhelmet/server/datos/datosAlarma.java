package com.angelhelmet.server.datos;

import com.angelhelmet.server.util.TipoAlarma;

public class datosAlarma {
	private TipoAlarma _alarma;
	private long _firma;
	private long _cancela_alarma;
	private long _id_log;
	private long contador;

	private String question;
	private Integer answerNumber;
	private String answerDesc;
	private String answerText;
	
	private long eventId;
	private long eventDate;
	private long eventResponseDate;

	public datosAlarma() {
		_alarma = TipoAlarma.NINGUNA;
	}

	public TipoAlarma getAlarma() {
		return _alarma;
	}

	public int getValorAlarma() {
		return _alarma.getAlarma();
	}

	public void setAlarma(TipoAlarma alarma) {
		this._alarma = alarma;
	}

	public long getFirma() {
		return _firma;
	}

	public void setFirma(long firma) {
		this._firma = firma;
	}

	public long getCancelaAlarma() {
		return _cancela_alarma;
	}

	public void setCancelaAlarma(long cancela_alarma) {
		this._cancela_alarma = cancela_alarma;
	}

	public long getIdLog() {
		return _id_log;
	}

	public void setIdLog(long id_log) {
		this._id_log = id_log;
	}

	public long getContador() {
		return contador;
	}

	public void setContador(long contador) {
		this.contador = contador;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public Integer getAnswerNumber() {
		return answerNumber;
	}

	public void setAnswerNumber(Integer answerNumber) {
		this.answerNumber = answerNumber;
	}

	public String getAnswerDesc() {
		return answerDesc;
	}

	public void setAnswerDesc(String answerDesc) {
		this.answerDesc = answerDesc;
	}

	public String getAnswerText() {
		return answerText;
	}

	public void setAnswerText(String answerText) {
		this.answerText = answerText;
	}

	public long getEventId() {
		return eventId;
	}

	public void setEventId(long eventId) {
		this.eventId = eventId;
	}

	public long getEventDate() {
		return eventDate;
	}

	public void setEventDate(long eventDate) {
		this.eventDate = eventDate;
	}

	public long getEventResponseDate() {
		return eventResponseDate;
	}

	public void setEventResponseDate(long eventResponseDate) {
		this.eventResponseDate = eventResponseDate;
	}
	
	

}
