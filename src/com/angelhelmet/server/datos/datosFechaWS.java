package com.angelhelmet.server.datos;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.angelhelmet.server.util.ServidorUtil;

public class datosFechaWS
{
	private int _ano;
	private int _mes;
	private int _dia;
	private int _hora;
	private int _minutos;
	private int _segundos;
	private int _milisegundos;

	public datosFechaWS()
	{
	}

	public void setFechaFromTimestamp(Timestamp fecha)
	{
		GregorianCalendar gc = new GregorianCalendar();
		Date aux = ServidorUtil.getFecha(fecha.getTime());
		gc.setTime(aux);

		this._ano = gc.get(Calendar.YEAR);
		this._mes = gc.get(Calendar.MONTH) + 1;
		this._dia = gc.get(Calendar.DAY_OF_MONTH);

		this._hora = gc.get(Calendar.HOUR_OF_DAY);
		this._minutos = gc.get(Calendar.MINUTE);
		this._segundos = gc.get(Calendar.SECOND);
		this._milisegundos = gc.get(Calendar.MILLISECOND);
	}

	public int getAno()
	{
		return _ano;
	}

	public void setAno(int ano)
	{
		this._ano = ano;
	}

	public int getMes()
	{
		return _mes;
	}

	public void setMes(int mes)
	{
		this._mes = mes;
	}

	public int getDia()
	{
		return this._dia;
	}

	public void setDia(int dia)
	{
		this._dia = dia;
	}

	public int getHora()
	{
		return _hora;
	}

	public void setHora(int hora)
	{
		this._hora = hora;
	}

	public int getMinutos()
	{
		return _minutos;
	}

	public void setMinutos(int minutos)
	{
		this._minutos = minutos;
	}

	public int getSegundos()
	{
		return _segundos;
	}

	public void setSegundos(int segundos)
	{
		this._segundos = segundos;
	}

	public int getMilisegundos()
	{
		return _milisegundos;
	}

	public void setMilisegundos(int milisegundos)
	{
		this._milisegundos = milisegundos;
	}
}
