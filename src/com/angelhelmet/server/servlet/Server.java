package com.angelhelmet.server.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;

// https://logging.apache.org/log4j/2.x/manual/webapp.html
// http://www.codejava.net/coding/how-to-initialize-log4j-for-java-web-application
// http://www.redmine.org/projects/redmine/wiki/How_to_Install_Redmine_on_CentOS_%28Detailed%29
// https://logging.apache.org/log4j/2.x/maven-artifacts.html

// http://www.journaldev.com/1997/servlet-example-in-java-with-database-connection-and-log4j-integration
// http://www.journaldev.com/7128/apache-log4j-2-tutorial-configuration-levels-appenders-lookup-layouts-and-filters-example

//Prueba ce creacion de trunk 06/09/2016 11:27 (commit svn)

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.Scanner;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sound.sampled.AudioFileFormat.Type;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.http.HttpHeaders;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.excepciones.AddMensajeApagadoException;
import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsAdyacentesNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.GetZonaException;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.dao.excepciones.UnidadNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosGestion;
import com.angelhelmet.server.datos.datosWalkietalkieAudio;
import com.angelhelmet.server.datos.datosWalkietalkieImei;
import com.angelhelmet.server.negocio.excepciones.IndiceFueraDelStringException;
import com.angelhelmet.server.negocio.excepciones.PeticionSinDatosException;
import com.angelhelmet.server.negocio.interfaces.IDecodificadorBean;
import com.angelhelmet.server.negocio.interfaces.IMensajeBean;
import com.angelhelmet.server.negocio.interfaces.IPeticionBean;
import com.angelhelmet.server.negocio.interfaces.IUsaGlobalesBean;
import com.angelhelmet.server.negocio.interfaces.IWalkietalkie;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;
import com.angelhelmet.server.negocio.pool.excepciones.AddUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.DelUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.EditUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ExisteUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.LimpiaPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ReemplazaUnidadPoolException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PonderaPuntosException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PosicionExteriorException;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.excepciones.ConvertHexToStringException;
import com.angelhelmet.server.util.excepciones.DentroDelPoligonoException;
import com.angelhelmet.server.util.excepciones.GeneraFirmaException;
import com.angelhelmet.server.util.excepciones.GetFechaByStringException;
import com.angelhelmet.server.util.excepciones.ModoDeFuncionamientoException;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToDoubleException;
import com.angelhelmet.server.util.excepciones.ParseStringToFloatException;
import com.angelhelmet.server.util.excepciones.ParseStringToIntException;

@WebServlet({ "/Server", "/server", "/Server/", "/server/" })
@MultipartConfig(
		  fileSizeThreshold = 1024 * 1024 * 1, // 1 MB
		  maxFileSize = 1024 * 1024 * 10,      // 10 MB
		  maxRequestSize = 1024 * 1024 * 100   // 100 MB
		)
public class Server extends HttpServlet {
	private static final long serialVersionUID = -8567589191193816311L;
	private Logger _log;

	final Long AUDIO_PREPARACION = 1L;
	final Long AUDIO_ENVIADO = 3L;
	final Long AUDIO_CONFIRMACION = 4L;
	final Long AUDIO_ERROR = 6L;

	final Integer ENVIAR_AUDIO_NA = 0;
	final Integer ENVIAR_AUDIO_OK = 1;
	final Integer ENVIAR_AUDIO_ERROR = 2;

	@EJB
	private IPeticionBean _peticion_bean;

	@EJB
	private IDecodificadorBean _decodificador_bean;

	@EJB
	private IUsaGlobalesBean _globales;

	@EJB
	private IWalkietalkie _walkie_talkie;

	@EJB
	IMensajeBean _mensaje_bean;

	public Server() {
		super();

		_log = LogManager.getLogger(Server.class);
	}

	@Override
	public void destroy() {
		_log.debug("Servlet Destroy");
		_log = null;
	}

	public int ord2(char c) {
		if (c < 0x80) {
			return c;
		} else if (c < 0x800) {
			return 0xc0 | c >> 6;
		} else if (c < 0x10000) {
			return 0xe0 | c >> 12;
		} else {
			return 0xf0 | c >> 18;
		}
	}

	public int CalculateCrc(String data) {
		char[] dataArray = data.toCharArray();
		int crc = 0;
		for (int i = 0; i < dataArray.length; i++) {
			crc ^= ord(dataArray[i]);
		}
		return crc;
	}

	public String getDateTime() {
		Date dNow = new Date();
		SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd_hhmmss");
		return ft.format(dNow);
	}

	private static final String SAVE_DIR = "uploadFiles";

	private String extractFileName(Part part) {
		String contentDisp = part.getHeader("content-disposition");
		String[] items = contentDisp.split(";");
		for (String s : items) {
			_log.info("s: " + s);
			if (s.trim().startsWith("filename")) {
				return s.substring(s.indexOf("=") + 2, s.length() - 1);
			}
		}
		return "";
	}

	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	    /* Receive file uploaded to the Servlet from the HTML5 form */
	    Part filePart = request.getPart("file");
	    String fileName = filePart.getSubmittedFileName();
	    for (Part part : request.getParts()) {
	      part.write("C:\\ah\\web\\helmet\\media\\helmet\\" + fileName);
	    }
	    response.getWriter().print("The file uploaded sucessfully.");
	  }
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) {

		_log.info("doPost");
		String responseCode = "#$9000$ACK";
		boolean errorStatus = false;

		_log.info(request.getRequestURL() + "?" + request.getQueryString());

		String pathMediaHelmet = "C:\\ah\\web\\helmet\\media\\helmet\\";

		_log.info(pathMediaHelmet);

		File fileSaveDir = new File(pathMediaHelmet);

		if (!fileSaveDir.exists()) {
			fileSaveDir.mkdir();
			_log.info("crear directorio");
		}

		String fileName = "";

		try {

			_log.info("inicio proceso");

	//		if (!request.getParts().isEmpty()) {

				_log.info("no esta vacio el parts");

				for (Part part : request.getParts()) {

					_log.info("inicio proceso part");

					fileName = extractFileName(part);
					fileName = new File(fileName).getName();

					_log.info(pathMediaHelmet + File.separator + fileName);

					byte[] b = new byte[1024];

					_log.info("iniciando grabado de datos");

					String name = part.getName();

					_log.info("name: " + name);

					InputStream is = part.getInputStream();

					Scanner sc = new Scanner(is);

					StringBuffer sbOut = new StringBuffer();
					StringBuffer sbData = new StringBuffer();
					sbOut.append("data:");
					while (sc.hasNext()) {
						sbData.append(sc.nextLine());
					}
					sbOut.append(sbData + "\r\n");
					// System.out.println(sbOut.toString());
					_log.info(sbOut.toString());

					String imei = sbData.substring(0, 15);
					String signature = sbData.substring(15, 23);
					String channel = sbData.substring(23, 25);
					int headerLength = sbData.length() + 5;

					_log.info("headerLength: " + headerLength);

					sbOut.append("imei: " + imei + "\r\n");
					sbOut.append("signature: " + signature + "\r\n");
					sbOut.append("channel: " + channel + "\r\n");
					sbOut.append("header length: " + headerLength + "\r\n");

					int crcInputData = Integer.parseInt((sbData.substring(sbData.length() - 2)), 16);
					int crcCalculated = CalculateCrc(sbData.substring(0, sbData.length() - 2));

					sbOut.append("crc audio : " + crcInputData + "\r\n");
					sbOut.append("crc calculated : " + crcCalculated + "\r\n");

					_log.info(sbOut.toString());

					if (crcInputData == crcCalculated) {

						if (headerLength > 32) { // 15 (ID) + 8 (firma) + 2 (canal) + 2 (CRC)
							_log.info("largo permitido");

							if (!(imei.equals("860854021000000") || imei.equals("800000000000000"))) {
								// if (imei.equals("860854021603468")) {
								try {
									File file = new File(pathMediaHelmet + getDateTime() + ".wav");

									String audioString = sbData.substring(15 + 8 + 2, sbData.length() - 2);
									AudioInputStream inputStream = null;
									ByteArrayOutputStream byt = new ByteArrayOutputStream();
									byt.write(audioString.getBytes());

									AudioFormat format = new AudioFormat(AudioFormat.Encoding.ALAW, 8000, 8, 1, 1, 8000,
											true);

									InputStream input = new ByteArrayInputStream(byt.toByteArray());
									AudioInputStream ais = new AudioInputStream(input, format,
											byt.toByteArray().length / format.getFrameSize());
									AudioSystem.write(ais, Type.WAVE, file);
									sbOut.append("Audio stored correctly.\r\n");

									_log.info(sbOut.toString());

								} catch (IOException ex) {
									_log.info("error: " + ex.getLocalizedMessage());
									sbOut.append("An error occurred while saving the audio.\r\n");
								}
							} else if (imei.equals("800000000000000")) {
								errorStatus = true;
							} else {
								responseCode = "#$6A86$ACK "; // error en los parametros
							}

						} else {
							_log.info("largo minimo");
							responseCode = "#$6700$ACK "; // error en longitud
						}
					} else {

						responseCode = "#$6281$ACK"; // error CRC

					}

				}

		//	} else {
		//		_log.info("part vacio");
		//		errorStatus = true;
//
		//	}
		} catch (IOException | ServletException e) {
			_log.info("IOException | ServletException");
			_log.info(e.getLocalizedMessage());
			e.printStackTrace();
			errorStatus = true;
		} catch (Exception e) {
			_log.info("Exception");
			_log.info(e.getLocalizedMessage());
			e.printStackTrace();
			errorStatus = true;
		}

		try {

			if (!errorStatus) {
				_log.info(responseCode);
				_log.info("Upload has been done successfully!");
				response.setStatus(200);
				response.addHeader(HttpHeaders.CONTENT_TYPE, "application/text; charset=UTF-8");
				response.getWriter().write(responseCode);
			} else {
				response.setStatus(500);
			}
		} catch (IOException e) {
			_log.info("response error: " + e.getLocalizedMessage());
			response.setStatus(500);
			e.printStackTrace();
		}
	}

	// mensajeria con el casco
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		_log.info("doGet");

		String peticion = "";
		Long firmaHelmetAudio = 0L;
		try {

			peticion = request.getParameter("DATA");

			_log.info(request.getRequestURL() + "?" + request.getQueryString());

			datosComunicacion datos_comunicacion = null;
			datosGestion datos_gestion = null;

			datos_comunicacion = _decodificador_bean.DecodificaGet(request);

			_log.info(_decodificador_bean.toString());

			datos_gestion = _peticion_bean.gestionaPeticion(datos_comunicacion);

			_log.info(ToStringBuilder.reflectionToString(datos_comunicacion));

			_log.info("mensaje casco: " + datos_comunicacion.getMensaje());

			if (SendAudioToHelmet(datos_comunicacion.getMensaje().getTipoMensage(), datos_comunicacion.getSerie(),
					datos_comunicacion.getFirma(), request, response) == 0) {

				if (datos_gestion.getTextoMensaje() != null && !datos_gestion.getTextoMensaje().isEmpty()) {

					_log.info("Mensaje enviado a la unidad: ", datos_gestion.getTextoMensaje());

					PrintWriter out = response.getWriter();
					out.print(datos_gestion.getTextoMensaje());

				}
			}

			_log.debug("Hilo: ", Thread.currentThread().getId());
			_log.debug(_globales.getGlobales().PIE_CONSOLA);

		} catch (PeticionSinDatosException ex) {
			_log.info(ex.getMessage());
		} catch (ConvertHexToStringException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al parsear Hext to String", ex);
		} catch (ParseStringToIntException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al parsear Hext to String", ex);
		} catch (ParseStringToBigIntegerException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al parsear Hext to BigInteger", ex);
		} catch (ParseStringToDoubleException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al parsear Hext to Double", ex);
		} catch (ParseStringToFloatException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al parsear Hext to Float", ex);
		} catch (GetFechaByStringException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR al conseguir la fecha del string", ex);
		} catch (IndiceFueraDelStringException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR decodificar la peticion", ex);
		} catch (ModoDeFuncionamientoException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR El modo de funcionamiento no es correcto", ex);
		} catch (UnidadNoEncontradaException ex) {
			_log.info(ex.getMessage());
		} catch (AlarmaNoEncontradaException ex) {
			_log.info(ex.getMessage());
			_log.info("ERROR alarma no encontrada", ex);
		} catch (ApNoEncontradoException ex) {
			if (ex.getCause() != null) {
				_log.info(ex.getCause());
			} else {
				_log.info(ex.getMessage());
			}
		} catch (MapaNoEncontradoException ex) {
			_log.info(ex.getMessage());
			_log.debug(ex.getMessage());
		} catch (ParseLogException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR no se puede parsear el log", ex);
		} catch (GrabaLogException ex) {
			_log.info(ex.getMessage());
			_log.error("ERROR no se puede grabar el log", ex);
		} catch (ExisteUnidadPoolException ex) {
			_log.error("ERROR no se puede comprobar la existencia de la unidad en el pool", ex);
		} catch (AddUnidadPoolException ex) {
			_log.error("ERROR no se puede añadir la unidad en el pool", ex);
		} catch (DelUnidadPoolException ex) {
			_log.error("ERROR no se puede eliminar la unidad del pool", ex);
		} catch (GetUnidadPoolException ex) {
			_log.error("ERROR no se puede recuperar la unidad del pool", ex);
		} catch (ReemplazaUnidadPoolException ex) {
			_log.error("ERROR no se puede reemplazar la unidad del pool", ex);
		} catch (EditUnidadPoolException ex) {
			_log.error("ERROR no se puede editar la unidad del pool", ex);
		} catch (LimpiaPoolException ex) {
			_log.error("ERROR no se puede limpiar el pool", ex);
		} catch (UnidadMensajeNoEncontradoException ex) {
			_log.error("No se puede recuperar el mensaje para la unidad", ex);
		} catch (GetUnidadMensajeByIdException ex) {
			_log.error("No se puede recuperar UnidadMensaje", ex);
		} catch (UnidadMensajeSetLeidoException ex) {
			_log.error("No se puede poner a leido el mensaje", ex);
		} catch (GetMensajesPurgarException ex) {
			_log.error("No se puede recuperar los mensajes a purgar", ex);
		} catch (PurgarMensajeException ex) {
			_log.error("No se puede prugra los mensajes", ex);
		} catch (AddMensajeApagadoException ex) {
			_log.error("No se puede crear el mensaje de apagado");
		} catch (ApsMapaNoEncontradosException ex) {
			_log.error("No se han encontrados APs para el mapa", ex);
		} catch (ApsAdyacentesNoEncontradosException ex) {
			_log.error("No se han encontrados APs adyacentes", ex);
		} catch (GetZonasMapaException ex) {
			_log.error("No se pruede recuperar las zonas para el mapa", ex);
		} catch (GetZonaException ex) {
			_log.error("No se puede recuperar la zona", ex);
		} catch (ZonaSinCoordenadasException ex) {
			_log.error("Zona sin coordenadas", ex);
		} catch (GetZonaCoordenadas ex) {
			_log.error("Error al recuperar las coordenadas de la zona", ex);
		} catch (GetPermisosZonaException ex) {
			_log.error("No se puede recuperar los permisos de la zona", ex);
		} catch (DentroDelPoligonoException ex) {
			_log.error("No se puede calcular si el punto esta dentro del poligono", ex);
		} catch (PosicionExteriorException ex) {
			_log.error("No se puede calcular la posicion exterior del mapa", ex);
		} catch (CalculaKalmanException ex) {
			_log.error("No se puede calcular Kalman", ex);
		} catch (GetNodosProyeccionException ex) {
			_log.error("No se han encontrado nodos para proyectar", ex);
		} catch (PonderaPuntosException ex) {
			_log.error("No se puede ponderar", ex);
		} catch (GeneraFirmaException ex) {
			_log.error("No se puede generar la firma", ex);
		} catch (Exception ex) {
			if (ex.getCause() != null) {
				_log.info("doGet(: {}", ex.getCause());
			} else {
				_log.info("doGet(: {}", ex.getMessage());
			}

			_log.error("ERROR GENERAL en la peticion {}", peticion, ex);
		}
	}

	public Integer SendAudioToHelmet(Integer mensaje, String imei, Long firmaHelmet, HttpServletRequest request,
			HttpServletResponse response) {

		// valida mensajes del casco a considerar
		if (!(mensaje == 0 || mensaje == 2)) {
			return ENVIAR_AUDIO_NA;
		}

		boolean walkietalkieMode = false;

		boolean wtAudioPreparacion = false;
		boolean wtAudioEnviado = false;
		boolean wtActualizaEstadoAudio = false;
		datosWalkietalkieImei dataWalkietalkieImei = null;

		datosWalkietalkieAudio dataWalkietalkieAudio = null;

		Long firmaCasco = 0L;

		try {
			dataWalkietalkieImei = _walkie_talkie.obtenerImeiWT(imei);
			walkietalkieMode = true;
		} catch (Exception e) {
			dataWalkietalkieImei = null;
			_log.info(e.getLocalizedMessage());
		}

		// valida si consulta trajo datos
		if (dataWalkietalkieImei == null)
			return ENVIAR_AUDIO_NA;

		// valida si unidad esta activa
		if (!dataWalkietalkieImei.is_wt_unit())
			return ENVIAR_AUDIO_NA;

		if (mensaje == 0) {

			try {

				dataWalkietalkieAudio = _walkie_talkie.obtenerAudioWT(imei, AUDIO_ENVIADO);
				wtAudioEnviado = true;
			} catch (Exception e) {
				wtAudioEnviado = false;
				_log.info("AUDIO ENVIADO" + "+" + e.getLocalizedMessage());
			}

			if (wtAudioEnviado) {
				// reenviar mensaje
				Integer res = sendAudioToHelmet(dataWalkietalkieAudio.get_nombre_archivo(),
						dataWalkietalkieImei.get_wt_channel(), dataWalkietalkieAudio.get_firma(), request, response);

				System.out.println("res audio: " + res);
				// actualizar estado audio
				try {
					if (res == ENVIAR_AUDIO_OK) {

						_walkie_talkie.actualizarEstadoWT(dataWalkietalkieAudio.get_id_mensaje_helmet_wt(),
								AUDIO_ENVIADO, firmaCasco);
					} else {
						_walkie_talkie.actualizarEstadoWT(dataWalkietalkieAudio.get_id_mensaje_helmet_wt(), AUDIO_ERROR,
								firmaCasco);
					}
					wtActualizaEstadoAudio = true;

				} catch (Exception e) {
					wtActualizaEstadoAudio = false;
					_log.info(e.getLocalizedMessage());
					e.printStackTrace();
				}

				if (!wtActualizaEstadoAudio) {
					return ENVIAR_AUDIO_ERROR;
				}

				return ENVIAR_AUDIO_OK;
			}

			try {

				dataWalkietalkieAudio = _walkie_talkie.obtenerAudioWT(imei, AUDIO_PREPARACION);
				wtAudioPreparacion = true;
			} catch (Exception e) {
				wtAudioPreparacion = false;
				_log.info("AUDIO PREPARACION" + "+" + e.getLocalizedMessage());
			}

			if (wtAudioPreparacion) {

				// obtener firma
				try {
					firmaCasco = _mensaje_bean.getIdFirmaCasco();
				} catch (Exception e) {
					firmaCasco = 0L;
					_log.info("getAudioData-Error de Obtencion de Firma: " + e.getLocalizedMessage());
					e.printStackTrace();
				}

				if (firmaCasco == 0) {
					return ENVIAR_AUDIO_ERROR;
				}

				Integer res = sendAudioToHelmet(dataWalkietalkieAudio.get_nombre_archivo(),
						dataWalkietalkieImei.get_wt_channel(), firmaCasco, request, response);

				System.out.println("res audio2: " + res);
				// actualizar estado audio
				try {
					if (res == ENVIAR_AUDIO_OK) {

						_walkie_talkie.actualizarEstadoWT(dataWalkietalkieAudio.get_id_mensaje_helmet_wt(),
								AUDIO_ENVIADO, firmaCasco);
					} else {
						_walkie_talkie.actualizarEstadoWT(dataWalkietalkieAudio.get_id_mensaje_helmet_wt(), AUDIO_ERROR,
								firmaCasco);
					}
					wtActualizaEstadoAudio = true;
				} catch (Exception e) {
					wtActualizaEstadoAudio = false;
					_log.info(e.getLocalizedMessage());
					e.printStackTrace();
				}

				if (!wtActualizaEstadoAudio) {
					return ENVIAR_AUDIO_ERROR;
				}

				return ENVIAR_AUDIO_OK;
			}

			return ENVIAR_AUDIO_NA;
		}

		if (mensaje == 2) {

			try {
// agregar firma a filtro
				dataWalkietalkieAudio = _walkie_talkie.obtenerAudioWT(imei, AUDIO_ENVIADO, firmaHelmet);
				wtAudioEnviado = true;
			} catch (Exception e) {
				wtAudioEnviado = false;
				_log.info("AUDIO ENVIADO" + "+" + e.getLocalizedMessage());
			}

			if (wtAudioEnviado) {

				try {
					_walkie_talkie.actualizarEstadoWT(dataWalkietalkieAudio.get_id_mensaje_helmet_wt(),
							AUDIO_CONFIRMACION, firmaHelmet);
				} catch (Exception e) {
					_log.info(e.getLocalizedMessage());
					e.printStackTrace();
				}
			}

		}

		return ENVIAR_AUDIO_NA;

	}

	public int sendAudioToHelmet(String nombreArchivo, int canal, Long firmaCasco, HttpServletRequest request,
			HttpServletResponse response) {
		boolean status = false;
		try {
			_log.info("sendAudioToHelmet-start");

			_log.info("nombreArchivo: " + nombreArchivo);
			_log.info("canal        : " + canal);
			_log.info("firmaCasco   : " + firmaCasco);

			getAudioData(nombreArchivo, canal, firmaCasco, response);

			_log.info("sendAudioToHelmet-end");
			// guardar estado de envio.
			status = true;
		} catch (UnsupportedAudioFileException ex) {
			try {
				_log.info("sendAudioToHelmet-error1: " + ex.getLocalizedMessage());
				ex.printStackTrace();
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, request.getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException ex) {
			try {
				_log.info("sendAudioToHelmet-error2: " + ex.getLocalizedMessage());
				ex.printStackTrace();
				response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			}

		} catch (Exception ex) {
			try {
				_log.info("sendAudioToHelmet-error3: " + ex.getLocalizedMessage());
				ex.printStackTrace();
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, request.getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (status) {
			return ENVIAR_AUDIO_OK;
		}
		return ENVIAR_AUDIO_ERROR;
	}

	public void getAudioData(String serie, Integer canal, Long firmaCasco, HttpServletResponse response)
			throws UnsupportedAudioFileException, IOException {

		_log.info("getAudioData-start");

		String firmaCascoHex = "";
		int bytesRead = -1;
		int crc = 0;

		_log.info("firma         : " + firmaCasco);
		firmaCascoHex = convertNumberToHexLitteEndian(firmaCasco);
		_log.info("hexLitteEndian: " + firmaCascoHex);

		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");

		ServletOutputStream out = response.getOutputStream();
		File audioFile = new File("C:\\ah\\web\\helmet\\media\\" + serie + ".wav");

		_log.info("archivo       : " + "C:\\ah\\web\\helmet\\media\\" + serie + ".wav");

		_log.info("canal         : " + canal);
		_log.info("serie         : " + serie);
		AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);

		_log.info("len hex       : " + String.format("0x%06X", audioStream.available() + 2));
		_log.info("len           : " + audioStream.available());

		out.write("#".getBytes());
		out.write("%".getBytes());
		out.write(firmaCascoHex.getBytes());
		out.write(String.format("%02X", canal).getBytes());
		out.write(String.format("%06X", audioStream.available() + 2).getBytes());
		out.write("X".getBytes());
		out.write("X".getBytes());

		int contador = 0;

		byte[] buffer = new byte[2048];

		while ((bytesRead = audioStream.read(buffer)) != -1) {

			out.write(buffer, 0, bytesRead);

			for (int x = 0; x < bytesRead; x++) {
				crc ^= (buffer[x] & 0xff);
				contador++;
			}

		}

		_log.info("crc           : " + crc);
		_log.info("contador      : " + contador);
		out.write(String.format("%02X", crc).getBytes());
		out.flush();
		audioStream.close();
		_log.info("getAudioData-end");

	}

	public static String byteToHex(byte num) {
		char[] hexDigits = new char[2];
		hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
		hexDigits[1] = Character.forDigit((num & 0xF), 16);
		return new String(hexDigits);
	}

	public static String encodeHexString(byte[] byteArray) {
		StringBuffer hexStringBuffer = new StringBuffer();
		for (int i = 0; i < byteArray.length; i++) {
			hexStringBuffer.append(byteToHex(byteArray[i]));
		}
		return hexStringBuffer.toString();
	}

	public static int ord4(String s) {
		return s.length() > 0 ? (s.getBytes(StandardCharsets.UTF_8)[0] & 0xff) : 0;
	}

	public static int ord4(char c) {
		return c < 0x80 ? c : ord4(Character.toString(c));
	}

	public static int ord3(String s) {
		return s.length() > 0 ? (s.getBytes()[0]) : 0;
	}

	public static int ord3(char c) {
		return c < 0x80 ? c : ord3(Character.toString(c));
	}

	public int ord(char c) {
		if (c < 0x80) {
			return c;
		} else if (c < 0x800) {
			return 0xc0 | c >> 6;
		} else if (c < 0x10000) {
			return 0xe0 | c >> 12;
		} else {
			return 0xf0 | c >> 18;
		}
	}

	public static String convertNumberToHexLitteEndian(Long number) {
		Formatter obj = new Formatter();

		return ServidorUtil
				.hexReverse(obj.format("%8s", Long.toHexString(number).toUpperCase()).toString().replace(" ", "0"));
	}

}
