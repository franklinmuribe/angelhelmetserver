package com.angelhelmet.server.servlet;

import java.io.File;
import java.io.IOException;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebServlet({ "/Media", "/media", "/Media/", "/media/" })
public class Media extends HttpServlet {
	private static final long serialVersionUID = -8567589191193816311L;
	private Logger _log;

	public int ord(char c) {
		if (c < 0x80) {
			return c;
		} else if (c < 0x800) {
			return 0xc0 | c >> 6;
		} else if (c < 0x10000) {
			return 0xe0 | c >> 12;
		} else {
			return 0xf0 | c >> 18;
		}
	}

	public Media() {
		super();
		_log = LogManager.getLogger(Media.class);
	}

	@Override
	public void destroy() {
		_log.debug("Servlet Destroy");
		_log = null;
	}

	public void getAudioData(String id, HttpServletResponse response)
			throws UnsupportedAudioFileException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		File audioFile = new File("C:\\ah\\web\\helmet\\media\\" + id + ".wav");

		AudioInputStream audioStream = AudioSystem.getAudioInputStream(audioFile);
		int BUFFER_SIZE = 4096;

		byte[] bytesBuffer = new byte[BUFFER_SIZE];
		int bytesRead = -1;

		int i = 0;
		int crc = 0;
		out.format("%06XXX", audioStream.available());

		while ((bytesRead = audioStream.read(bytesBuffer)) != -1) {

			for (int x = 0; x < bytesRead; x++) {
				out.print((char) bytesBuffer[x]);
				crc = crc ^ ord((char) bytesBuffer[x]);
			}
		}

		out.format("%02X", crc);
		audioStream.close();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException {
		String id = "";

		try {

			id = request.getParameter("ID");
			if (!id.isEmpty()) {
				getAudioData(id, response);
			}
		} catch (UnsupportedAudioFileException ex) {
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, request.getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (IOException ex) {
			try {
				response.sendError(HttpServletResponse.SC_NOT_FOUND, request.getRequestURI());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}
