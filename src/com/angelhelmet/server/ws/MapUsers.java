package com.angelhelmet.server.ws;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
//import java.util.concurrent.locks.Lock;
//import java.util.concurrent.locks.ReentrantLock;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.jws.WebService;
import javax.xml.ws.BindingType;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadMensajeBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaDiferidoBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.negocio.interfaces.ws.IMapUsers;

@Stateless
@WebService(portName = "MapUsersPort", serviceName = "MapUsersService", targetNamespace = "http://www.angelhelmet.com/wsdl", endpointInterface = "com.angelhelmet.server.negocio.interfaces.ws.IMapUsers")
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP12HTTP_BINDING)

public class MapUsers implements IMapUsers
{
	@EJB
	ILogBean _log_bean;
	@EJB
	IUnidadMensajeBean _um_bean;
	@EJB
	IUnidadMapaDiferidoBean _cliente_diferido;
	@EJB
	IUnidadMapaRealBean _cliente_real;

	private Logger _log;

	// private final Lock lock = new ReentrantLock();

	public MapUsers()
	{
		_log = LogManager.getLogger(MapUsers.class);
	}

	// public String sayHello(String name)
	// {
	// System.out.println("Hello: " + name);
	// return "Hello " + name + "!";
	// }

	public List<datosUnidadMapaWS> getUsuarios(int id_mapa)
	{
		List<datosUnidadMapaWS> ret = null;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesPorMapa(id_mapa);
		}
		catch (Exception ex)
		{
			_log.error("No se puede obtener la lista de operarios del mapa: {}", id_mapa, ex);
			// _log.error(Level.forName("ERROR_WS", 501),
			// "No se puede obtener la lista de operarios del mapa:");
		}
		finally
		{
			// lock.unlock();
		}
		if (ret == null)
		{
			return new ArrayList<datosUnidadMapaWS>();
		}
		else
		{
			return ret;
		}
	}

	public List<datosUnidadMapaWS> getUsuariosMina()
	{
		List<datosUnidadMapaWS> ret = null;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesMina();
		}
		catch (Exception ex)
		{
			_log.error("No se puede obtener la lista de operarios de la mina", ex);
		}
		finally
		{
			// lock.unlock();
		}
		if (ret == null)
		{
			return new ArrayList<datosUnidadMapaWS>();
		}
		else
		{
			return ret;
		}
	}

	public List<Integer> getUnidadesCambioMapa(int id_mapa)
	{
		List<Integer> ret = null;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesCambioMapa(id_mapa);
		}
		catch (Exception ex)
		{
			_log.error("No se puede obtener la lista de operarios de la mina", ex);
		}
		finally
		{
			// lock.unlock();
		}
		if (ret == null)
		{
			return new ArrayList<Integer>();
		}
		else
		{
			return ret;
		}
	}

	public boolean BorraUnidadCambioMapa(int id_unidad)
	{
		boolean ret = false;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().delUnidadCambiadaMapa(id_unidad);
		}
		catch (Exception ex)
		{
			_log.error("No se puede eliminar la unidad unidad cambiada de mapa: {}", id_unidad, ex);

		}
		finally
		{
			// lock.unlock();
		}
		return ret;
	}

	public List<datosUnidadMapaWS> getUsuariosMinaPorEmpresa(List<Integer> empresas)
	{
		List<datosUnidadMapaWS> ret = null;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getUnidadesMina(empresas);
		}
		catch (Exception ex)
		{
			StringBuilder sb = new StringBuilder();
			String s = "";
			if (empresas != null && empresas.size() > 0)
			{
				for (Integer item : empresas)
				{
					sb.append(item.intValue() + ", ");
				}
				s = sb.toString().substring(0, sb.toString().length() - 1);
			}
			_log.error("No se puede obtener la lista de operarios de la mina por empresas: {}", s, ex);
		}
		finally
		{
			// lock.unlock();
		}
		if (ret == null)
		{
			return new ArrayList<datosUnidadMapaWS>();
		}
		else
		{
			return ret;
		}
	}

	public List<Long> getLogs(Date inicio, Date fin, List<Integer> unidades, int id_mapa)
	{
		List<Long> ret = null;
		List<datosComunicacion> logs = null;

		try
		{
			// lock.lockInterruptibly();
			logs = _log_bean.getLogsByFecha(inicio, fin, unidades, id_mapa);
		}
		catch (Exception ex)
		{
			StringBuilder sb = new StringBuilder();
			SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS Z");
			String s = "";
			if (unidades != null && unidades.size() > 0)
			{
				for (Integer item : unidades)
				{
					sb.append(item.intValue() + ", ");
				}
				s = sb.toString().substring(0, sb.toString().length() - 1);
			}
			_log.error("No se puede obtener la lista de logs, inicio: {}, final: {}, unidades {} ", df.format(inicio), df.format(fin), s, ex);
		}
		finally
		{
			// lock.unlock();
		}

		if (logs != null)
		{
			ret = new ArrayList<Long>();
			for (datosComunicacion item_log : logs)
			{
				ret.add(item_log.getIdLog());
			}
		}

		return ret;
	}

	public List<datosUnidadMapaWS> getLog(long id_log, int id_mapa)
	{
		List<datosUnidadMapaWS> ret = null;
		try
		{
			// lock.lockInterruptibly();
			datosLog datos_log = new datosLog();
			datos_log = _cliente_diferido.creaDatosLog(id_log);
			if (datos_log != null)
			{
				_cliente_diferido.setUnidadMapa(datos_log);
				if (_cliente_diferido.creaUnidadMapa(_cliente_diferido.getPool()))
				{
					_cliente_diferido.gestionaPool(_cliente_diferido.getPool());
					ret = _cliente_diferido.getPool().getUnidadesPorMapa(id_mapa);
				}
			}
		}
		catch (Exception ex)
		{
			_log.error("No se puede obtener la lista de operarios, log: {}, mapa: {}", id_log, id_mapa, ex);
		}
		finally
		{
			// lock.unlock();
		}
		if (ret == null)
		{
			return new ArrayList<datosUnidadMapaWS>();
		}
		else
		{
			return ret;
		}
	}

	public boolean borraUnidadesDiferido(List<Integer> unidades)
	{
		boolean ret = false;
		try
		{
			// lock.lockInterruptibly();
			for (int id_unidad : unidades)
			{
				_cliente_diferido.getPool().delUnidad(id_unidad);
			}
			ret = true;
		}
		catch (Exception ex)
		{
			StringBuilder sb = new StringBuilder();
			String s = "";
			if (unidades != null && unidades.size() > 0)
			{
				for (Integer item : unidades)
				{
					sb.append(item.intValue() + ", ");
				}
				s = sb.toString().substring(0, sb.toString().length() - 1);
			}
			_log.error("No se puede borrar la lista de unidades en diferido unidades: {}", s, ex);
		}
		finally
		{
			// lock.unlock();
		}
		return ret;
	}

	public boolean apagaUnidad(int id_unidad)
	{
		boolean ret = false;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().delUnidad(id_unidad);
		}
		catch (Exception ex)
		{
			_log.error("No se puede apagar la unidad unidad: {}", id_unidad, ex);

		}
		finally
		{
			// lock.unlock();
		}
		return ret;
	}

	public int getTotalOperariosMina()
	{
		int ret = 0;
		try
		{
			// lock.lockInterruptibly();
			ret = _cliente_real.getPool().getTotalOperariosMina();
		}
		catch (Exception ex)
		{
			_log.error("No se puede obtener el total de operarios de la mina", ex);
		}
		finally
		{
			// lock.unlock();
		}
		return ret;
	}

	public boolean alarmaInformada(int id_unidad, long firma)
	{
		boolean ret = true;
		try
		{
			datosUnidadMapa datos = _cliente_real.getPool().getUnidad(id_unidad);
			if (datos.getAlarmas().remove(firma) == null)
			{
				ret = false;
			}
		}
		catch (Exception ex)
		{
			_log.error("No se puede informar la alarma de la unidad {}, con firma {}", id_unidad, firma, ex);
		}

		return ret;
	}
		

	@Override
	public String sayHello(String name) {
		// TODO Auto-generated method stub
		return null;
	}
}
