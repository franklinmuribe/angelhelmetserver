/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.math.BigInteger;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "umbrales_medicion_salud")
public class UmbralesMedicionSaludEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_umbrales_medicion_salud", unique = true)
	private Integer idUmbralesMedicionSalud;
	@Column(name = "max")
	private BigInteger max;
	@Column(name = "min")
	private BigInteger min;
	@JoinColumn(name = "id_tipo_medicion", referencedColumnName = "id_tipo_medicion")
	@ManyToOne
	private TipoMedicionEB idTipoMedicion;
	@JoinColumn(name = "id_unidad_salud", referencedColumnName = "id_unidad_salud")
	@ManyToOne
	private UnidadSaludEB idUnidadSalud;

	public UmbralesMedicionSaludEB() {
	}

	public UmbralesMedicionSaludEB(Integer idUmbralesMedicionSalud) {
		this.idUmbralesMedicionSalud = idUmbralesMedicionSalud;
	}

	public Integer getIdUmbralesMedicionSalud() {
		return idUmbralesMedicionSalud;
	}

	public void setIdUmbralesMedicionSalud(Integer idUmbralesMedicionSalud) {
		this.idUmbralesMedicionSalud = idUmbralesMedicionSalud;
	}

	public BigInteger getMax() {
		return max;
	}

	public void setMax(BigInteger max) {
		this.max = max;
	}

	public BigInteger getMin() {
		return min;
	}

	public void setMin(BigInteger min) {
		this.min = min;
	}

	public TipoMedicionEB getIdTipoMedicion() {
		return idTipoMedicion;
	}

	public void setIdTipoMedicion(TipoMedicionEB idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	public UnidadSaludEB getIdUnidadSalud() {
		return idUnidadSalud;
	}

	public void setIdUnidadSalud(UnidadSaludEB idUnidadSalud) {
		this.idUnidadSalud = idUnidadSalud;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idUmbralesMedicionSalud != null ? idUmbralesMedicionSalud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof UmbralesMedicionSaludEB)) {
			return false;
		}
		UmbralesMedicionSaludEB other = (UmbralesMedicionSaludEB) object;
		if ((this.idUmbralesMedicionSalud == null && other.idUmbralesMedicionSalud != null)
				|| (this.idUmbralesMedicionSalud != null
						&& !this.idUmbralesMedicionSalud.equals(other.idUmbralesMedicionSalud))) {
			return false;
		}
		return true;
	}

	

}
