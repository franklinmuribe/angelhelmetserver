package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the apsadyacentes database table.
 * 
 */
@Entity
@Table(name = "apsadyacentes")
@NamedQuery(name = "ApAdyacenteEB.findAll", query = "SELECT a FROM ApAdyacenteEB a")
public class ApAdyacenteEB implements Serializable
{
	private static final long serialVersionUID = -6286321794915115539L;

	@Id
	@GeneratedValue(generator = "id_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "id_seq", sequenceName = "aps_adyacentes_id_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer id;

	@Column(name = "id_adyacente", nullable = false)
	private Integer idAdyacente;

	// bi-directional many-to-one association to ApEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_ap", nullable = false)
	private ApEB ap;

	public ApAdyacenteEB()
	{
	}

	public Integer getId()
	{
		return this.id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Integer getIdAdyacente()
	{
		return this.idAdyacente;
	}

	public void setIdAdyacente(Integer idAdyacente)
	{
		this.idAdyacente = idAdyacente;
	}

	public ApEB getAp()
	{
		return this.ap;
	}

	public void setAp(ApEB ap)
	{
		this.ap = ap;
	}

}