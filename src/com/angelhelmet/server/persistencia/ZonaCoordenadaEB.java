package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the zonascoordenadas database table.
 * 
 */
@Entity
@Table(name = "zonascoordenadas")
@NamedQuery(name = "ZonaCoordenadaEB.findAll", query = "SELECT z FROM ZonaCoordenadaEB z")
@NamedQueries({
		@NamedQuery(name = "ZonaCoordenadaEB.getZonaCoordenadaByIdZona", query = "SELECT zc FROM ZonaCoordenadaEB zc WHERE zc.zona.idZona = :id_zona")
})
public class ZonaCoordenadaEB implements Serializable
{
	private static final long serialVersionUID = 2521708890389356273L;

	@Id
	@GeneratedValue(generator = "zonacoordenada_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "zonacoordenada_seq", sequenceName = "zonascoordenadas_id_zonacoordenada_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_zonacoordenada", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idZonacoordenada;

	@Column(nullable = false)
	private Double x;

	@Column(nullable = false)
	private Double y;

	// bi-directional many-to-one association to ZonaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_zona", nullable = false)
	private ZonaEB zona;

	public ZonaCoordenadaEB()
	{
	}

	public Long getIdZonacoordenada()
	{
		return this.idZonacoordenada;
	}

	public void setIdZonacoordenada(Long idZonacoordenada)
	{
		this.idZonacoordenada = idZonacoordenada;
	}

	public Double getX()
	{
		return this.x;
	}

	public void setX(Double x)
	{
		this.x = x;
	}

	public Double getY()
	{
		return this.y;
	}

	public void setY(Double y)
	{
		this.y = y;
	}

	public ZonaEB getZona()
	{
		return this.zona;
	}

	public void setZona(ZonaEB zona)
	{
		this.zona = zona;
	}

}