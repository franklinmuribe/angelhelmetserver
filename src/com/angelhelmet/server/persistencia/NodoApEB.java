package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the nodosaps database table.
 * 
 */
@Entity
@Table(name = "nodosaps")
@NamedQuery(name = "NodoApEB.findAll", query = "SELECT n FROM NodoApEB n")
@NamedQueries({
		@NamedQuery(name = "NodoApEB.getNodosAsociados", query = "SELECT n FROM NodoApEB n WHERE n.ap.idAp = :id_ap1 OR n.ap.idAp = :id_ap2")
})
public class NodoApEB implements Serializable
{
	private static final long serialVersionUID = -3882461586843486330L;

	@Id
	@GeneratedValue(generator = "nodoap_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "nodoap_seq", sequenceName = "nodosaps_id_nodoap_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_nodoap", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idNodoap;

	@Column(name = "id_nodo", nullable = false)
	private Integer idNodo;

	// bi-directional many-to-one association to ApEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_ap", nullable = false)
	private ApEB ap;

	public NodoApEB()
	{
	}

	public Long getIdNodoap()
	{
		return this.idNodoap;
	}

	public void setIdNodoap(Long idNodoap)
	{
		this.idNodoap = idNodoap;
	}

	public Integer getIdNodo()
	{
		return this.idNodo;
	}

	public void setIdNodo(Integer idNodo)
	{
		this.idNodo = idNodo;
	}

	public ApEB getAp()
	{
		return this.ap;
	}

	public void setAp(ApEB ap)
	{
		this.ap = ap;
	}

}