/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "tipo_medicion")
@JsonInclude(Include.NON_NULL)
public class TipoMedicionEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_tipo_medicion", unique = true)
	private Integer idTipoMedicion;
	@Column(name = "nb_tipo_medicion")
	private String nbTipoMedicion;
	@Column(name = "st_activo")
	private Boolean stActivo;
	@Column(name = "valor_trama")
    private String valorTrama;
	@OneToMany(mappedBy = "idTipoMedicion")
	private Set<UmbralesMedicionSaludEB> umbralesMedicionSaludSet;
	@OneToMany(mappedBy = "idTipoMedicion")
	private Set<AlarmaSaludEB> alarmaSaludSet;

	public TipoMedicionEB() {
	}

	public TipoMedicionEB(Integer idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	public Integer getIdTipoMedicion() {
		return idTipoMedicion;
	}

	public void setIdTipoMedicion(Integer idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	public String getNbTipoMedicion() {
		return nbTipoMedicion;
	}

	public void setNbTipoMedicion(String nbTipoMedicion) {
		this.nbTipoMedicion = nbTipoMedicion;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public Set<UmbralesMedicionSaludEB> getUmbralesMedicionSaludSet() {
		return umbralesMedicionSaludSet;
	}

	public void setUmbralesMedicionSaludSet(Set<UmbralesMedicionSaludEB> umbralesMedicionSaludSet) {
		this.umbralesMedicionSaludSet = umbralesMedicionSaludSet;
	}
	
	

	public String getValorTrama() {
		return valorTrama;
	}

	public void setValorTrama(String valorTrama) {
		this.valorTrama = valorTrama;
	}

	public Set<AlarmaSaludEB> getAlarmaSaludSet() {
		return alarmaSaludSet;
	}

	public void setAlarmaSaludSet(Set<AlarmaSaludEB> alarmaSaludSet) {
		this.alarmaSaludSet = alarmaSaludSet;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idTipoMedicion != null ? idTipoMedicion.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof TipoMedicionEB)) {
			return false;
		}
		TipoMedicionEB other = (TipoMedicionEB) object;
		if ((this.idTipoMedicion == null && other.idTipoMedicion != null)
				|| (this.idTipoMedicion != null && !this.idTipoMedicion.equals(other.idTipoMedicion))) {
			return false;
		}
		return true;
	}

	

}
