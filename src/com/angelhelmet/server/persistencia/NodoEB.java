package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the nodos database table.
 * 
 */
@Entity
@Table(name = "nodos")
@NamedQuery(name = "NodoEB.findAll", query = "SELECT n FROM NodoEB n")
public class NodoEB implements Serializable
{
	private static final long serialVersionUID = -80158014125551204L;

	@Id
	@GeneratedValue(generator = "nodo_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "nodo_seq", sequenceName = "nodos_id_nodo_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_nodo", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idNodo;

	@Column(length = 40)
	private String descripcion;

	// bi-directional many-to-one association to NodoPuntoEB
	@OneToMany(mappedBy = "nodo")
	private List<NodoPuntoEB> nodospuntos;

	public NodoEB()
	{
	}

	public Integer getIdNodo()
	{
		return this.idNodo;
	}

	public void setIdNodo(Integer idNodo)
	{
		this.idNodo = idNodo;
	}

	public String getDescripcion()
	{
		return this.descripcion;
	}

	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	public List<NodoPuntoEB> getNodosPuntos()
	{
		return this.nodospuntos;
	}

	public void setNodosPuntos(List<NodoPuntoEB> nodospuntos)
	{
		this.nodospuntos = nodospuntos;
	}

	public NodoPuntoEB addNodosPunto(NodoPuntoEB nodospunto)
	{
		getNodosPuntos().add(nodospunto);
		nodospunto.setNodo(this);

		return nodospunto;
	}

	public NodoPuntoEB removeNodosPunto(NodoPuntoEB nodospunto)
	{
		getNodosPuntos().remove(nodospunto);
		nodospunto.setNodo(null);

		return nodospunto;
	}

}