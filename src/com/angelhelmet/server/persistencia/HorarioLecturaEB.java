/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Type;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "horario_lectura")
public class HorarioLecturaEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_horario_lectura", unique = true)
	private Integer idHorarioLectura;
	@Column(name = "hora")
	@Temporal(TemporalType.TIME)
	private Date hora;
	@Column(name = "intervalo")
	private Double intervalo;
	@JoinColumn(name = "id_unidad_salud", referencedColumnName = "id_unidad_salud")
	@ManyToOne
	private UnidadSaludEB idUnidadSalud;

	public HorarioLecturaEB() {
	}

	public HorarioLecturaEB(Integer idHorarioLectura) {
		this.idHorarioLectura = idHorarioLectura;
	}

	public Integer getIdHorarioLectura() {
		return idHorarioLectura;
	}

	public void setIdHorarioLectura(Integer idHorarioLectura) {
		this.idHorarioLectura = idHorarioLectura;
	}

	public Date getHora() {
		return hora;
	}

	public void setHora(Date hora) {
		this.hora = hora;
	}

	public Double getIntervalo() {
		return intervalo;
	}

	public void setIntervalo(Double intervalo) {
		this.intervalo = intervalo;
	}

	public UnidadSaludEB getIdUnidadSalud() {
		return idUnidadSalud;
	}

	public void setIdUnidadSalud(UnidadSaludEB idUnidadSalud) {
		this.idUnidadSalud = idUnidadSalud;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idHorarioLectura != null ? idHorarioLectura.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof HorarioLecturaEB)) {
			return false;
		}
		HorarioLecturaEB other = (HorarioLecturaEB) object;
		if ((this.idHorarioLectura == null && other.idHorarioLectura != null)
				|| (this.idHorarioLectura != null && !this.idHorarioLectura.equals(other.idHorarioLectura))) {
			return false;
		}
		return true;
	}

	

}
