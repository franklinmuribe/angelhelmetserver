package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the operariosempresas database table.
 * 
 */
@Entity
@Table(name = "operariosempresas")
@NamedQuery(name = "OperarioEmpresaEB.findAll", query = "SELECT o FROM OperarioEmpresaEB o")
@NamedQueries({
		@NamedQuery(name = "OperarioEmpresa.getEmpresaByIdOperario", query = "SELECT e FROM OperarioEmpresaEB oe JOIN oe.empresa e WHERE oe.operario.idOperario = :id AND oe.activo = TRUE")
})
public class OperarioEmpresaEB implements Serializable
{
	private static final long serialVersionUID = -8117059938691994208L;

	@Id
	@GeneratedValue(generator = "operarioempresa_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "operarioempresa_seq", sequenceName = "operariosempresas_id_operarioempresa_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_operarioempresa", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idOperarioempresa;

	@Column(nullable = false)
	private Boolean activo;

	@Column(name = "fecha_alta", nullable = false)
	private Timestamp fechaAlta;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	// bi-directional many-to-one association to EmpresaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_empresa", nullable = false)
	private EmpresaEB empresa;

	// bi-directional many-to-one association to OperarioEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_operario", nullable = false)
	private OperarioEB operario;

	public OperarioEmpresaEB()
	{
	}

	public Integer getIdOperarioempresa()
	{
		return this.idOperarioempresa;
	}

	public void setIdOperarioempresa(Integer idOperarioempresa)
	{
		this.idOperarioempresa = idOperarioempresa;
	}

	public Boolean getActivo()
	{
		return this.activo;
	}

	public void setActivo(Boolean activo)
	{
		this.activo = activo;
	}

	public Timestamp getFechaAlta()
	{
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta)
	{
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja()
	{
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja)
	{
		this.fechaBaja = fechaBaja;
	}

	public EmpresaEB getEmpresa()
	{
		return this.empresa;
	}

	public void setEmpresa(EmpresaEB empresa)
	{
		this.empresa = empresa;
	}

	public OperarioEB getOperario()
	{
		return this.operario;
	}

	public void setOperario(OperarioEB operario)
	{
		this.operario = operario;
	}

}