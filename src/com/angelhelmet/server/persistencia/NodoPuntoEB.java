package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the nodospuntos database table.
 * 
 */
@Entity
@Table(name="nodospuntos")
@NamedQuery(name = "NodoPuntoEB.findAll", query = "SELECT n FROM NodoPuntoEB n")
@NamedQueries({
		@NamedQuery(name = "NodoPuntoEB.getNodosPuntosExtremos", query = "SELECT np FROM NodoPuntoEB np WHERE np.nodo.idNodo = :id_nodo AND (np.idTipopunto = 3 OR np.idTipopunto = 2)")
})
public class NodoPuntoEB implements Serializable
{
	private static final long serialVersionUID = 6362312637078600337L;

	@Id
	@GeneratedValue(generator = "nodopunto_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "nodopunto_seq", sequenceName = "nodospuntos_id_nodopunto_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_nodopunto", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idNodopunto;

	@Column(name="id_tipopunto", nullable=false)
	private Integer idTipopunto;

	@Column(nullable=false)
	private Integer x;

	@Column(nullable=false)
	private Integer y;

	//bi-directional many-to-one association to NodoEB
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_nodo", nullable=false)
	private NodoEB nodo;

	public NodoPuntoEB() {
	}

	public Long getIdNodopunto() {
		return this.idNodopunto;
	}

	public void setIdNodopunto(Long idNodopunto) {
		this.idNodopunto = idNodopunto;
	}

	public Integer getIdTipopunto() {
		return this.idTipopunto;
	}

	public void setIdTipopunto(Integer idTipopunto) {
		this.idTipopunto = idTipopunto;
	}

	public Integer getX() {
		return this.x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return this.y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public NodoEB getNodo() {
		return this.nodo;
	}

	public void setNodo(NodoEB nodo) {
		this.nodo = nodo;
	}

}