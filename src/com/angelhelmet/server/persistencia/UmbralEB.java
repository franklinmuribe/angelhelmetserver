package com.angelhelmet.server.persistencia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "umbrales")
public class UmbralEB implements Serializable {

	private static final long serialVersionUID = -3422759467700449354L;

	@Id
	@GeneratedValue(generator = "umbrale_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "umbrale_seq", sequenceName = "umbrales_id_umbrale_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_umbral", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idUmbral;

	@Column(name = "impacto_x")
	private Integer x_impacto;

	@Column(name = "impacto_y")
	private Integer y_impacto;

	@Column(name = "impacto_z")
	private Integer z_impacto;

	@Column(name = "gas")
	private Integer gas;

	@Column(name = "prealarma")
	private Integer prealarma;

	@Column(name = "estatico")
	private Integer estatico;

	@Column(name = "casco_caido")
	private boolean casco_caido;

	@Column(name = "tracking")
	private Integer tracking;

	@Column(name = "apn")
	private String apn;

	@Column(name = "apn_user")
	private String apn_user;

	@Column(name = "apn_pwd")
	private String apn_pwd;

	@Column(name = "host_gprs")
	private String host_gprs;

	@Column(name = "puerto")
	private Integer puerto;

	@Column(name = "ssid")
	private String ssid;

	@Column(name = "wifi_pwd")
	private String wifi_pwd;

	@Column(name = "id_encriptacion")
	private Integer id_encriptacion;

	@Column(name = "host_wifi")
	private String host_wifi;

	@Column(name = "canales")
	private Integer canales;

	@Column(name = "comunicacion_casset")
	private boolean comunicacion_casset;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "telefono_sos")
	private String telefono_sos;

	@Column(name = "id_tipoumbral")
	private Integer id_tipoumbral;

	@Column(name = "wt_unit")
	private boolean wt_unit;

	@Column(name = "wt_channel")
	private Integer wt_channel;

	@Column(name = "wt_blocked")
	private boolean wt_blocked;

	@Column(name = "coronavirus_proximity")
	private boolean coronavirus_proximity;

	@Column(name = "ble_health")
	private boolean ble_health;

	@Column(name = "hw_version")
	private boolean hw_version;

	public Integer getIdUmbrale() {
		return idUmbral;
	}

	public void setIdUmbrale(Integer idUmbral) {
		this.idUmbral = idUmbral;
	}

	public Integer getX_impacto() {
		return x_impacto;
	}

	public void setX_impacto(Integer x_impacto) {
		this.x_impacto = x_impacto;
	}

	public Integer getY_impacto() {
		return y_impacto;
	}

	public void setY_impacto(Integer y_impacto) {
		this.y_impacto = y_impacto;
	}

	public Integer getZ_impacto() {
		return z_impacto;
	}

	public void setZ_impacto(Integer z_impacto) {
		this.z_impacto = z_impacto;
	}

	public Integer getGas() {
		return gas;
	}

	public void setGas(Integer gas) {
		this.gas = gas;
	}

	public Integer getPrealarma() {
		return prealarma;
	}

	public void setPrealarma(Integer prealarma) {
		this.prealarma = prealarma;
	}

	public Integer getEstatico() {
		return estatico;
	}

	public void setEstatico(Integer estatico) {
		this.estatico = estatico;
	}

	public boolean isCasco_caido() {
		return casco_caido;
	}

	public void setCasco_caido(boolean casco_caido) {
		this.casco_caido = casco_caido;
	}

	public Integer getTracking() {
		return tracking;
	}

	public void setTracking(Integer tracking) {
		this.tracking = tracking;
	}

	public String getApn() {
		return apn;
	}

	public void setApn(String apn) {
		this.apn = apn;
	}

	public String getApn_user() {
		return apn_user;
	}

	public void setApn_user(String apn_user) {
		this.apn_user = apn_user;
	}

	public String getApn_pwd() {
		return apn_pwd;
	}

	public void setApn_pwd(String apn_pwd) {
		this.apn_pwd = apn_pwd;
	}

	public String getHost_gprs() {
		return host_gprs;
	}

	public void setHost_gprs(String host_gprs) {
		this.host_gprs = host_gprs;
	}

	public Integer getPuerto() {
		return puerto;
	}

	public void setPuerto(Integer puerto) {
		this.puerto = puerto;
	}

	public String getSsid() {
		return ssid;
	}

	public void setSsid(String ssid) {
		this.ssid = ssid;
	}

	public String getWifi_pwd() {
		return wifi_pwd;
	}

	public void setWifi_pwd(String wifi_pwd) {
		this.wifi_pwd = wifi_pwd;
	}

	public Integer getId_encriptacion() {
		return id_encriptacion;
	}

	public void setId_encriptacion(Integer id_encriptacion) {
		this.id_encriptacion = id_encriptacion;
	}

	public String getHost_wifi() {
		return host_wifi;
	}

	public void setHost_wifi(String host_wifi) {
		this.host_wifi = host_wifi;
	}

	public Integer getCanales() {
		return canales;
	}

	public void setCanales(Integer canales) {
		this.canales = canales;
	}

	public boolean isComunicacion_casset() {
		return comunicacion_casset;
	}

	public void setComunicacion_casset(boolean comunicacion_casset) {
		this.comunicacion_casset = comunicacion_casset;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getTelefono_sos() {
		return telefono_sos;
	}

	public void setTelefono_sos(String telefono_sos) {
		this.telefono_sos = telefono_sos;
	}

	public Integer getId_tipoumbral() {
		return id_tipoumbral;
	}

	public void setId_tipoumbral(Integer id_tipoumbral) {
		this.id_tipoumbral = id_tipoumbral;
	}

	public Integer getIdUmbral() {
		return idUmbral;
	}

	public void setIdUmbral(Integer idUmbral) {
		this.idUmbral = idUmbral;
	}

	public boolean isWt_unit() {
		return wt_unit;
	}

	public void setWt_unit(boolean wt_unit) {
		this.wt_unit = wt_unit;
	}

	public Integer getWt_channel() {
		return wt_channel;
	}

	public void setWt_channel(Integer wt_channel) {
		this.wt_channel = wt_channel;
	}

	public boolean isWt_blocked() {
		return wt_blocked;
	}

	public void setWt_blocked(boolean wt_blocked) {
		this.wt_blocked = wt_blocked;
	}

	public boolean isCoronavirus_proximity() {
		return coronavirus_proximity;
	}

	public void setCoronavirus_proximity(boolean coronavirus_proximity) {
		this.coronavirus_proximity = coronavirus_proximity;
	}

	public boolean isBle_health() {
		return ble_health;
	}

	public void setBle_health(boolean ble_health) {
		this.ble_health = ble_health;
	}

	public boolean isHw_version() {
		return hw_version;
	}

	public void setHw_version(boolean hw_version) {
		this.hw_version = hw_version;
	}

}
