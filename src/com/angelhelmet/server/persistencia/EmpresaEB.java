package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the empresas database table.
 * 
 */
@Entity
@Table(name = "empresas")
@NamedQuery(name = "EmpresaEB.findAll", query = "SELECT e FROM EmpresaEB e")
@NamedQueries({
		@NamedQuery(name = "Empresa.getEmpresaById", query = "SELECT e FROM EmpresaEB e WHERE e.idEmpresa = :id"),
		@NamedQuery(name = "Empresa.getEmpresaByUnidad", query = "SELECT e FROM EmpresaEB e "
				+ "JOIN e.operariosempresas oe JOIN oe.operario o JOIN o.unidadesoperarios uo "
				+ "WHERE oe.activo=TRUE and uo.activo=TRUE AND uo.unidad.idUnidad = :id_unidad")

})
public class EmpresaEB implements Serializable
{
	private static final long serialVersionUID = -8052659919830527615L;

	@Id
	@GeneratedValue(generator = "empresa_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "empresa_seq", sequenceName = "empresas_id_empresa_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_empresa", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idEmpresa;

	@Column(name = "codigo_postal", length = 15)
	private String codigoPostal;

	@Column(length = 200)
	private String direccion1;

	@Column(length = 200)
	private String direccion2;

	@Column(length = 150)
	private String email;

	@Column(name = "id_pais")
	private Integer idPais;

	@Column(length = 20)
	private String movil;

	// @Column(length = 2147483647)
	@Column(columnDefinition = "TEXT")
	private String notas;

	@Column(name = "persona_contacto", length = 250)
	private String personaContacto;

	@Column(length = 150)
	private String poblacion;

	@Column(length = 200)
	private String provincia;

	@Column(name = "razon_social", nullable = false, length = 200)
	private String razonSocial;

	@Column(length = 20)
	private String telefono;

	// bi-directional many-to-one association to OperarioEmpresaEB
	@OneToMany(mappedBy = "empresa")
	private List<OperarioEmpresaEB> operariosempresas;

	public EmpresaEB()
	{
	}

	public Integer getIdEmpresa()
	{
		return this.idEmpresa;
	}

	public void setIdEmpresa(Integer idEmpresa)
	{
		this.idEmpresa = idEmpresa;
	}

	public String getCodigoPostal()
	{
		return this.codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal)
	{
		this.codigoPostal = codigoPostal;
	}

	public String getDireccion1()
	{
		return this.direccion1;
	}

	public void setDireccion1(String direccion1)
	{
		this.direccion1 = direccion1;
	}

	public String getDireccion2()
	{
		return this.direccion2;
	}

	public void setDireccion2(String direccion2)
	{
		this.direccion2 = direccion2;
	}

	public String getEmail()
	{
		return this.email;
	}

	public void setEmail(String email)
	{
		this.email = email;
	}

	public Integer getIdPais()
	{
		return this.idPais;
	}

	public void setIdPais(Integer idPais)
	{
		this.idPais = idPais;
	}

	public String getMovil()
	{
		return this.movil;
	}

	public void setMovil(String movil)
	{
		this.movil = movil;
	}

	public String getNotas()
	{
		return this.notas;
	}

	public void setNotas(String notas)
	{
		this.notas = notas;
	}

	public String getPersonaContacto()
	{
		return this.personaContacto;
	}

	public void setPersonaContacto(String personaContacto)
	{
		this.personaContacto = personaContacto;
	}

	public String getPoblacion()
	{
		return this.poblacion;
	}

	public void setPoblacion(String poblacion)
	{
		this.poblacion = poblacion;
	}

	public String getProvincia()
	{
		return this.provincia;
	}

	public void setProvincia(String provincia)
	{
		this.provincia = provincia;
	}

	public String getRazonSocial()
	{
		return this.razonSocial;
	}

	public void setRazonSocial(String razonSocial)
	{
		this.razonSocial = razonSocial;
	}

	public String getTelefono()
	{
		return this.telefono;
	}

	public void setTelefono(String telefono)
	{
		this.telefono = telefono;
	}

	public List<OperarioEmpresaEB> getOperariosEmpresas()
	{
		return this.operariosempresas;
	}

	public void setOperariosEmpresas(List<OperarioEmpresaEB> operariosempresas)
	{
		this.operariosempresas = operariosempresas;
	}

	public OperarioEmpresaEB addOperariosEmpresa(OperarioEmpresaEB operariosempresa)
	{
		getOperariosEmpresas().add(operariosempresa);
		operariosempresa.setEmpresa(this);

		return operariosempresa;
	}

	public OperarioEmpresaEB removeOperariosEmpresa(OperarioEmpresaEB operariosempresa)
	{
		getOperariosEmpresas().remove(operariosempresa);
		operariosempresa.setEmpresa(null);

		return operariosempresa;
	}

}