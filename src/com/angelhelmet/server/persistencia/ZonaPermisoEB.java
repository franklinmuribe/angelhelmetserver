package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the zonaspermisos database table.
 * 
 */
@Entity
@Table(name = "zonaspermisos")
@NamedQuery(name = "ZonaPermisoEB.findAll", query = "SELECT z FROM ZonaPermisoEB z")
public class ZonaPermisoEB implements Serializable
{
	private static final long serialVersionUID = -7110672633385370273L;

	@Id
	@GeneratedValue(generator = "zonapermiso_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "zonapermiso_seq", sequenceName = "zonaspermisos_id_zonapermiso_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_zonapermiso", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idZonapermiso;

	// bi-directional many-to-one association to UnidadEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_unidad", nullable = false)
	private UnidadEB unidad;

	// bi-directional many-to-one association to ZonaAlarmaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_zonaalarma", nullable = false)
	private ZonaAlarmaEB zonasalarma;

	public ZonaPermisoEB()
	{
	}

	public Integer getIdZonapermiso()
	{
		return this.idZonapermiso;
	}

	public void setIdZonapermiso(Integer idZonapermiso)
	{
		this.idZonapermiso = idZonapermiso;
	}

	public UnidadEB getUnidad()
	{
		return this.unidad;
	}

	public void setUnidad(UnidadEB unidad)
	{
		this.unidad = unidad;
	}

	public ZonaAlarmaEB getZonasAlarma()
	{
		return this.zonasalarma;
	}

	public void setZonasAlarma(ZonaAlarmaEB zonasalarma)
	{
		this.zonasalarma = zonasalarma;
	}

}
