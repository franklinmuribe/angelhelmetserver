package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the operariosempresas database table.
 * 
 */
@Entity
@Table(name = "operarioscargos")
//@NamedQuery(name = "OperarioCargosEB.findAll", query = "SELECT o FROM OperarioCargosEB o")
//@NamedQueries({
//		@NamedQuery(name = "OperarioCargos.getCargosByIdOperario", query = "SELECT e FROM OperarioCargoEB oe JOIN oe.cargo e WHERE oe.operario.idOperario = :id AND oe.activo = TRUE")
//})
public class OperarioCargoEB implements Serializable
{
	private static final long serialVersionUID = -8117059938691994208L;

	@Id
	@GeneratedValue(generator = "operariocargo_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "operariocargo_seq", sequenceName = "operariocargo_id_operariocargo_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_operariocargo", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idOperariocargo;

	@Column(nullable = false)
	private Boolean activo;

	@Column(name = "fecha_alta", nullable = false)
	private Timestamp fechaAlta;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	// bi-directional many-to-one association to EmpresaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_cargo", nullable = false)
	private CargoEB cargo;

	// bi-directional many-to-one association to OperarioEB
	@JoinColumn(name = "id_operario", referencedColumnName = "id_operario")
	@ManyToOne(fetch = FetchType.LAZY)	
	private OperarioEB operario;

	public OperarioCargoEB()
	{
	}

	public Boolean getActivo()
	{
		return this.activo;
	}

	public void setActivo(Boolean activo)
	{
		this.activo = activo;
	}

	public Timestamp getFechaAlta()
	{
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta)
	{
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja()
	{
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja)
	{
		this.fechaBaja = fechaBaja;
	}

	public OperarioEB getOperario()
	{
		return this.operario;
	}

	public void setOperario(OperarioEB operario)
	{
		this.operario = operario;
	}

	public Integer getIdOperariocargo() {
		return idOperariocargo;
	}

	public void setIdOperariocargo(Integer idOperariocargo) {
		this.idOperariocargo = idOperariocargo;
	}

	public CargoEB getCargo() {
		return cargo;
	}

	public void setCargo(CargoEB cargo) {
		this.cargo = cargo;
	}

}