/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "tipo_horario")
@JsonInclude(Include.NON_NULL)
public class TipoHorarioEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_tipo_horario", unique = true)
	private Integer idTipoHorario;
	@Column(name = "descripcion")
	private String descripcion;
	@Column(name = "st_activo")
	private Boolean stActivo;
	@Column(name = "tipo")
	private String tipo;
	@OneToMany(mappedBy = "idTipoHorario")
	private Set<UnidadSaludEB> unidadSaludSet;

	public TipoHorarioEB() {
	}

	public TipoHorarioEB(Integer idTipoHorario) {
		this.idTipoHorario = idTipoHorario;
	}

	public Integer getIdTipoHorario() {
		return idTipoHorario;
	}

	public void setIdTipoHorario(Integer idTipoHorario) {
		this.idTipoHorario = idTipoHorario;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Set<UnidadSaludEB> getUnidadSaludSet() {
		return unidadSaludSet;
	}

	public void setUnidadSaludSet(Set<UnidadSaludEB> unidadSaludSet) {
		this.unidadSaludSet = unidadSaludSet;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idTipoHorario != null ? idTipoHorario.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof TipoHorarioEB)) {
			return false;
		}
		TipoHorarioEB other = (TipoHorarioEB) object;
		if ((this.idTipoHorario == null && other.idTipoHorario != null)
				|| (this.idTipoHorario != null && !this.idTipoHorario.equals(other.idTipoHorario))) {
			return false;
		}
		return true;
	}

	

}
