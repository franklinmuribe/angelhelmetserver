package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the zonas database table.
 * 
 */
@Entity
@Table(name="zonas")
@NamedQuery(name = "ZonaEB.findAll", query = "SELECT z FROM ZonaEB z")
@NamedQueries({
		@NamedQuery(name = "ZonaEB.getZonasByIdMapa", query = "SELECT z FROM ZonaEB z JOIN z.mapa zm WHERE z.mapa.idMapa = :id_mapa AND z.activa = TRUE ORDER BY z.idZona"),
		@NamedQuery(name = "ZonaEB.getZonaByIdZona", query = "SELECT z FROM ZonaEB z WHERE z.idZona = :id_zona AND z.activa = TRUE")
})
public class ZonaEB implements Serializable
{
	private static final long serialVersionUID = 7183962104203127939L;

	@Id
	@GeneratedValue(generator = "zona_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "zona_seq", sequenceName = "zonas_id_zona_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_zona", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idZona;

	@Column(nullable=false)
	private Boolean activa;

	@Column(length=150)
	private String descripcion;

	@Column(name="fecha_alta", nullable=false)
	private Timestamp fechaAlta;

	@Column(name="fecha_baja")
	private Timestamp fechaBaja;

	private Integer sustituye;

	@Column(name="tiempo_permanencia", nullable=false)
	private Integer tiempoPermanencia;

	//bi-directional many-to-one association to MapaEB
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="id_mapa", nullable=false)
	private MapaEB mapa;

	//bi-directional many-to-one association to ZonaAlarmaEB
	@OneToMany(mappedBy="zona")
	private List<ZonaAlarmaEB> zonasalarmas;

	//bi-directional many-to-one association to ZonaCoordenadaEB
	@OneToMany(mappedBy="zona")
	private List<ZonaCoordenadaEB> zonascoordenadas;

	public ZonaEB() {
	}

	public Integer getIdZona() {
		return this.idZona;
	}

	public void setIdZona(Integer idZona) {
		this.idZona = idZona;
	}

	public Boolean getActiva() {
		return this.activa;
	}

	public void setActiva(Boolean activa) {
		this.activa = activa;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Integer getSustituye() {
		return this.sustituye;
	}

	public void setSustituye(Integer sustituye) {
		this.sustituye = sustituye;
	}

	public Integer getTiempoPermanencia() {
		return this.tiempoPermanencia;
	}

	public void setTiempoPermanencia(Integer tiempoPermanencia) {
		this.tiempoPermanencia = tiempoPermanencia;
	}

	public MapaEB getMapa() {
		return this.mapa;
	}

	public void setMapa(MapaEB mapa) {
		this.mapa = mapa;
	}

	public List<ZonaAlarmaEB> getZonasAlarmas() {
		return this.zonasalarmas;
	}

	public void setZonasAlarmas(List<ZonaAlarmaEB> zonasalarmas) {
		this.zonasalarmas = zonasalarmas;
	}

	public ZonaAlarmaEB addZonasAlarma(ZonaAlarmaEB zonasalarma) {
		getZonasAlarmas().add(zonasalarma);
		zonasalarma.setZona(this);

		return zonasalarma;
	}

	public ZonaAlarmaEB removeZonasAlarma(ZonaAlarmaEB zonasalarma) {
		getZonasAlarmas().remove(zonasalarma);
		zonasalarma.setZona(null);

		return zonasalarma;
	}

	public List<ZonaCoordenadaEB> getZonasCoordenadas() {
		return this.zonascoordenadas;
	}

	public void setZonasCoordenadas(List<ZonaCoordenadaEB> zonascoordenadas) {
		this.zonascoordenadas = zonascoordenadas;
	}

	public ZonaCoordenadaEB addZonasCoordenada(ZonaCoordenadaEB zonascoordenada) {
		getZonasCoordenadas().add(zonascoordenada);
		zonascoordenada.setZona(this);

		return zonascoordenada;
	}

	public ZonaCoordenadaEB removeZonasCoordenada(ZonaCoordenadaEB zonascoordenada) {
		getZonasCoordenadas().remove(zonascoordenada);
		zonascoordenada.setZona(null);

		return zonascoordenada;
	}

}