package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the aps database table.
 * 
 */
@Entity
@Table(name = "aps")
@NamedQuery(name = "ApEB.findAll", query = "SELECT a FROM ApEB a")
@NamedQueries({
		@NamedQuery(name = "Ap.getApByMac", query = "SELECT a FROM ApEB a WHERE a.mac = :mac"),
		@NamedQuery(name = "Ap.getApByIdAP", query = "SELECT a FROM ApEB a WHERE a.idAp = :id_ap"),
		@NamedQuery(name = "Mapa.getApsMapa", query = "SELECT a FROM ApEB a JOIN a.mapa m WHERE m.idMapa = :id_mapa"),
		@NamedQuery(name = "Ap.getApsAdyacentes", query = "SELECT a FROM ApEB a WHERE a.idAp IN (SELECT aa.idAdyacente FROM ApAdyacenteEB aa WHERE aa.ap.idAp = :id_ap)")
		//@NamedQuery(name = "Ap.getApsAdyacentes", query = "SELECT a FROM ApEB a WHERE a.idAp IN (SELECT aa.idAdyacente FROM ApAdyacenteEB aa) and a.idAp  = :id_ap")
})
public class ApEB implements Serializable
{
	private static final long serialVersionUID = -7192457554235799485L;

	@Id
	@GeneratedValue(generator = "ap_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "ap_seq", sequenceName = "aps_id_ap_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_ap", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idAp;

	@Column(nullable = false)
	private Boolean activo;

	@Column(length = 16)
	private String clave;

	@Column(length = 100)
	private String descripcion;

	@Column(name = "id_apextremo")
	private Integer idApextremo;

	@Column(name = "id_canal_cp", nullable = false)
	private Integer idCanal;

	@Column(name = "id_encriptacion", nullable = false)
	private Integer idEncriptacion;

	@Column(nullable = false, length = 15)
	private String ip;

	@Column(nullable = false, length = 16)
	private String mac;

	@Column(nullable = false, length = 15)
	private String mascara;

	private Integer x;

	private Integer y;

	// bi-directional many-to-one association to MapaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mapa", nullable = false)
	private MapaEB mapa;

	// bi-directional many-to-one association to ApAdyacenteEB
	@OneToMany(mappedBy = "ap")
	private List<ApAdyacenteEB> apsAdyacentes;

	// bi-directional many-to-one association to NodoApEB
	@OneToMany(mappedBy = "ap")
	private List<NodoApEB> nodosAps;

	public ApEB()
	{
	}

	public Integer getIdAp()
	{
		return this.idAp;
	}

	public void setIdAp(Integer idAp)
	{
		this.idAp = idAp;
	}

	public Boolean getActivo()
	{
		return this.activo;
	}

	public void setActivo(Boolean activo)
	{
		this.activo = activo;
	}

	public String getClave()
	{
		return this.clave;
	}

	public void setClave(String clave)
	{
		this.clave = clave;
	}

	public String getDescripcion()
	{
		return this.descripcion;
	}

	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	public Integer getIdApextremo()
	{
		return this.idApextremo;
	}

	public void setIdApextremo(Integer idApextremo)
	{
		this.idApextremo = idApextremo;
	}

	public Integer getIdCanal()
	{
		return this.idCanal;
	}

	public void setIdCanal(Integer idCanal)
	{
		this.idCanal = idCanal;
	}

	public Integer getIdEncriptacion()
	{
		return this.idEncriptacion;
	}

	public void setIdEncriptacion(Integer idEncriptacion)
	{
		this.idEncriptacion = idEncriptacion;
	}

	public String getIp()
	{
		return this.ip;
	}

	public void setIp(String ip)
	{
		this.ip = ip;
	}

	public String getMac()
	{
		return this.mac;
	}

	public void setMac(String mac)
	{
		this.mac = mac;
	}

	public String getMascara()
	{
		return this.mascara;
	}

	public void setMascara(String mascara)
	{
		this.mascara = mascara;
	}

	public Integer getX()
	{
		return this.x;
	}

	public void setX(Integer x)
	{
		this.x = x;
	}

	public Integer getY()
	{
		return this.y;
	}

	public void setY(Integer y)
	{
		this.y = y;
	}

	public MapaEB getMapa()
	{
		return this.mapa;
	}

	public void setMapa(MapaEB mapa)
	{
		this.mapa = mapa;
	}

	public List<ApAdyacenteEB> getApsAdyacentes()
	{
		return this.apsAdyacentes;
	}

	public void setApsAdyacentes(List<ApAdyacenteEB> apsadyacentes)
	{
		this.apsAdyacentes = apsadyacentes;
	}

	public ApAdyacenteEB addApsAdyacente(ApAdyacenteEB apsadyacente)
	{
		getApsAdyacentes().add(apsadyacente);
		apsadyacente.setAp(this);

		return apsadyacente;
	}

	public ApAdyacenteEB removeApsAdyacente(ApAdyacenteEB apsadyacente)
	{
		getApsAdyacentes().remove(apsadyacente);
		apsadyacente.setAp(null);

		return apsadyacente;
	}

	public List<NodoApEB> getNodosAps()
	{
		return this.nodosAps;
	}

	public void setNodosAps(List<NodoApEB> nodosaps)
	{
		this.nodosAps = nodosaps;
	}

	public NodoApEB addNodosap(NodoApEB nodosap)
	{
		getNodosAps().add(nodosap);
		nodosap.setAp(this);

		return nodosap;
	}

	public NodoApEB removeNodosap(NodoApEB nodosap)
	{
		getNodosAps().remove(nodosap);
		nodosap.setAp(null);

		return nodosap;
	}

}
