package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the mapas database table.
 * 
 */
@Entity
@Table(name="mapas")
@NamedQuery(name = "MapaEB.findAll", query = "SELECT m FROM MapaEB m")
@NamedQueries({
		@NamedQuery(name = "Mapa.getMapaById", query = "SELECT m FROM MapaEB m WHERE m.idMapa = :id AND m.activo = TRUE "),
		@NamedQuery(name = "Mapa.getMapaExterior", query = "SELECT m FROM MapaEB m WHERE m.activo = TRUE AND (ABS(:lon)>0 AND ABS(:lat)>0) AND (m.longitudMax >= :lon AND m.longitudMin <= :lon) AND (m.latitudMax >= :lat AND m.latitudMin <= :lat)")
})

//TODO HCL SANJAY
//@NamedQuery(name = "Mapa.getMapaExterior", query = "SELECT m FROM MapaEB m WHERE m.activo = TRUE AND (ABS(:lon)>0 AND ABS(:lat)>0) AND (m.longitudMax <= :lon AND m.longitudMin >= :lon) AND (m.latitudMax >= :lat AND m.latitudMin <= :lat)")

//@NamedQuery(name = "Mapa.getMapaExterior", query = "SELECT m FROM MapaEB m WHERE m.activo = TRUE AND m.idTipomapa = 2")
public class MapaEB implements Serializable
{
	private static final long serialVersionUID = -6852267735977630598L;

	@Id
	@GeneratedValue(generator = "mapa_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "mapa_seq", sequenceName = "mapas_id_mapa_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_mapa", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idMapa;

	@Column(nullable=false)
	private Boolean activo;

	private Integer alto;

	private Integer ancho;

	@Column(name="correccion_latitud_max")
	private double correccionLatitudMax;

	@Column(name="correccion_latitud_min")
	private double correccionLatitudMin;

	@Column(name="correccion_longitud_max")
	private double correccionLongitudMax;

	@Column(name="correccion_longitud_min")
	private double correccionLongitudMin;

	@Column(length=200)
	private String descripcion;

	@Column(name="fecha_alta", nullable=false)
	private Timestamp fechaAlta;

	@Column(name="fecha_baja")
	private Timestamp fechaBaja;

	@Column(name="id_tipomapa", nullable=false)
	private Integer idTipomapa;

	@Column(name="latitud_max")
	private double latitudMax;

	@Column(name="latitud_min")
	private double latitudMin;

	@Column(name="longitud_max")
	private double longitudMax;

	@Column(name="longitud_min")
	private double longitudMin;

	private Integer metros;

	@Column(name="nombre_mapa", nullable=false, length=200)
	private String nombreMapa;

	private Integer pix;

	@Column(name="url_mapa", length=500)
	private String urlMapa;

	//bi-directional many-to-one association to ApEB
	@OneToMany(mappedBy="mapa")
	private List<ApEB> aps;

	//bi-directional many-to-one association to ZonaEB
	@OneToMany(mappedBy="mapa")
	private List<ZonaEB> zonas;
	
	// bi-directional many-to-one association to TipoMapaEB
//	@ManyToOne
//	@JoinColumn(name = "id_tipomapa")
//	private TipoMapaEB tipomapa;

	public MapaEB() {
	}

	public Integer getIdMapa() {
		return this.idMapa;
	}

	public void setIdMapa(Integer idMapa) {
		this.idMapa = idMapa;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Integer getAlto() {
		return this.alto;
	}

	public void setAlto(Integer alto) {
		this.alto = alto;
	}

	public Integer getAncho() {
		return this.ancho;
	}

	public void setAncho(Integer ancho) {
		this.ancho = ancho;
	}

	public double getCorreccionLatitudMax() {
		return this.correccionLatitudMax;
	}

	public void setCorreccionLatitudMax(double correccionLatitudMax) {
		this.correccionLatitudMax = correccionLatitudMax;
	}

	public double getCorreccionLatitudMin() {
		return this.correccionLatitudMin;
	}

	public void setCorreccionLatitudMin(double correccionLatitudMin) {
		this.correccionLatitudMin = correccionLatitudMin;
	}

	public double getCorreccionLongitudMax() {
		return this.correccionLongitudMax;
	}

	public void setCorreccionLongitudMax(double correccionLongitudMax) {
		this.correccionLongitudMax = correccionLongitudMax;
	}

	public double getCorreccionLongitudMin() {
		return this.correccionLongitudMin;
	}

	public void setCorreccionLongitudMin(double correccionLongitudMin) {
		this.correccionLongitudMin = correccionLongitudMin;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Integer getIdTipoMapa() {
		return this.idTipomapa;
	}

	public void setIdTipoMapa(Integer idTipomapa) {
		this.idTipomapa = idTipomapa;
	}

	public double getLatitudMax() {
		return this.latitudMax;
	}

	public void setLatitudMax(double latitudMax) {
		this.latitudMax = latitudMax;
	}

	public double getLatitudMin() {
		return this.latitudMin;
	}

	public void setLatitudMin(double latitudMin) {
		this.latitudMin = latitudMin;
	}

	public double getLongitudMax() {
		return this.longitudMax;
	}

	public void setLongitudMax(double longitudMax) {
		this.longitudMax = longitudMax;
	}

	public double getLongitudMin() {
		return this.longitudMin;
	}

	public void setLongitudMin(double longitudMin) {
		this.longitudMin = longitudMin;
	}

	public Integer getMetros() {
		return this.metros;
	}

	public void setMetros(Integer metros) {
		this.metros = metros;
	}

	public String getNombreMapa() {
		return this.nombreMapa;
	}

	public void setNombreMapa(String nombreMapa) {
		this.nombreMapa = nombreMapa;
	}

	public Integer getPix() {
		return this.pix;
	}

	public void setPix(Integer pix) {
		this.pix = pix;
	}

	public String getUrlMapa() {
		return this.urlMapa;
	}

	public void setUrlMapa(String urlMapa) {
		this.urlMapa = urlMapa;
	}

	public List<ApEB> getAps() {
		return this.aps;
	}

	public void setAps(List<ApEB> aps) {
		this.aps = aps;
	}

	public ApEB addAp(ApEB ap) {
		getAps().add(ap);
		ap.setMapa(this);

		return ap;
	}

	public ApEB removeAp(ApEB ap) {
		getAps().remove(ap);
		ap.setMapa(null);

		return ap;
	}

	public List<ZonaEB> getZonas() {
		return this.zonas;
	}

	public void setZonas(List<ZonaEB> zonas) {
		this.zonas = zonas;
	}

	public ZonaEB addZona(ZonaEB zona) {
		getZonas().add(zona);
		zona.setMapa(this);

		return zona;
	}

	public ZonaEB removeZona(ZonaEB zona) {
		getZonas().remove(zona);
		zona.setMapa(null);

		return zona;
	}

}