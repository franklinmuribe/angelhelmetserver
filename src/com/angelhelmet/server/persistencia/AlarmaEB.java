package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 * The persistent class for the alarmas database table.
 * 
 */
@Entity
@Table(name = "alarmas")
@NamedQuery(name = "Alarma.findAll", query = "SELECT a FROM AlarmaEB a")
@NamedQueries({ @NamedQuery(name = "Alarma.getAlarmaById", query = "SELECT a FROM AlarmaEB a WHERE a.idAlarma = :id") })
public class AlarmaEB implements Serializable {
	private static final long serialVersionUID = 8842390110881628516L;

	@Id
	@GeneratedValue(generator = "alarma_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "alarma_seq", sequenceName = "alarmas_id_alarma_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_alarma", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idAlarma;

	@Column(length = 100)
	private String descripcion;

	@Column(nullable = false)
	private Boolean visible;

	@Column(name = "tipo_alarma", nullable = true, length = 30)
	private String tipoAlarma;

	// bi-directional many-to-one association to ZonaAlarmaEB
	@OneToMany(mappedBy = "alarma")
	private List<ZonaAlarmaEB> zonasalarmas;

	public AlarmaEB() {
	}

	public Integer getIdAlarma() {
		return this.idAlarma;
	}

	public void setIdAlarma(Integer idAlarma) {
		this.idAlarma = idAlarma;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getVisible() {
		return this.visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public String getTipoAlarma() {
		return tipoAlarma;
	}

	public void setTipoAlarma(String tipoAlarma) {
		this.tipoAlarma = tipoAlarma;
	}

	public List<ZonaAlarmaEB> getZonasAlarmas() {
		return this.zonasalarmas;
	}

	public void setZonasAlarmas(List<ZonaAlarmaEB> zonasalarmas) {
		this.zonasalarmas = zonasalarmas;
	}

	public ZonaAlarmaEB addZonasAlarma(ZonaAlarmaEB zonasalarma) {
		getZonasAlarmas().add(zonasalarma);
		zonasalarma.setAlarma(this);

		return zonasalarma;
	}

	public ZonaAlarmaEB removeZonasAlarma(ZonaAlarmaEB zonasalarma) {
		getZonasAlarmas().remove(zonasalarma);
		zonasalarma.setAlarma(null);

		return zonasalarma;
	}

}