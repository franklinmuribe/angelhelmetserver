package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the unidadesoperarios database table.
 * 
 */
@Entity
@Table(name = "unidadesoperarios")
@NamedQuery(name = "UnidadOperarioEB.findAll", query = "SELECT u FROM UnidadOperarioEB u WHERE u.activo = TRUE")
public class UnidadOperarioEB implements Serializable
{
	private static final long serialVersionUID = -6349289712080227670L;

	@Id
	@GeneratedValue(generator = "unidadoperario_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "unidadoperario_seq", sequenceName = "unidadesoperarios_id_unidadoperario_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_unidadoperario", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idUnidadoperario;

	@Column(nullable = false)
	private Boolean activo;

	@Column(name = "fecha_alta", nullable = false)
	private Timestamp fechaAlta;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	// bi-directional many-to-one association to OperarioEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_operario", nullable = false)
	private OperarioEB operario;

	// bi-directional many-to-one association to UnidadEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_unidad", nullable = false)
	private UnidadEB unidad;

	public UnidadOperarioEB()
	{
	}

	public Integer getIdUnidadoperario()
	{
		return this.idUnidadoperario;
	}

	public void setIdUnidadoperario(Integer idUnidadoperario)
	{
		this.idUnidadoperario = idUnidadoperario;
	}

	public Boolean getActivo()
	{
		return this.activo;
	}

	public void setActivo(Boolean activo)
	{
		this.activo = activo;
	}

	public Timestamp getFechaAlta()
	{
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta)
	{
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja()
	{
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja)
	{
		this.fechaBaja = fechaBaja;
	}

	public OperarioEB getOperario()
	{
		return this.operario;
	}

	public void setOperario(OperarioEB operario)
	{
		this.operario = operario;
	}

	public UnidadEB getUnidade()
	{
		return this.unidad;
	}

	public void setUnidade(UnidadEB unidad)
	{
		this.unidad = unidad;
	}

}
