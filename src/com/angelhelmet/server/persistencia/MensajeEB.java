package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the mensajes database table.
 * 
 */
@Entity
@Table(name = "mensajes")
@NamedQuery(name = "MensajeEB.findAll", query = "SELECT m FROM MensajeEB m")
@NamedQueries({
		@NamedQuery(name = "Mensaje.getMensajeByID", query = "SELECT m FROM MensajeEB m WHERE m.idMensaje = :id") })
public class MensajeEB implements Serializable {
	private static final long serialVersionUID = -3912759467700449354L;

	@Id
	@GeneratedValue(generator = "mensaje_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "mensaje_seq", sequenceName = "mensajes_id_mensaje_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_mensaje", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idMensaje;

	@Column(name = "fecha_creacion")
	private Timestamp fechaCreacion;

	@Column(name = "id_evento")
	private Long idEvento;

	@Column(name = "id_sesion")
	private Long idSesion;

	@Column(name = "id_tipomensaje", nullable = false)
	private Integer idTipomensaje;

	@Column(name = "id_unidadumbral")
	private Long idUnidadumbral;

	@Column(length = 255)
	private String texto;

	@Column(name = "id_firmacasco")
	private Long idFirmaCasco;

	// bi-directional many-to-one association to UnidadMensajeEB
	@OneToMany(mappedBy = "mensaje")
	private List<UnidadMensajeEB> unidadesmensajes;

	public MensajeEB() {
	}

	public Long getIdMensaje() {
		return this.idMensaje;
	}

	public void setIdMensaje(Long idMensaje) {
		this.idMensaje = idMensaje;
	}

	public Timestamp getFechaCreacion() {
		return this.fechaCreacion;
	}

	public void setFechaCreacion(Timestamp fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public Long getIdEvento() {
		return this.idEvento;
	}

	public void setIdEvento(Long idEvento) {
		this.idEvento = idEvento;
	}

	public Long getIdSesion() {
		return this.idSesion;
	}

	public void setIdSesion(Long idSesion) {
		this.idSesion = idSesion;
	}

	public Integer getIdTipoMensaje() {
		return this.idTipomensaje;
	}

	public void setIdTipoMensaje(Integer idTipomensaje) {
		this.idTipomensaje = idTipomensaje;
	}

	public Long getIdUnidadUmbral() {
		return this.idUnidadumbral;
	}

	public void setIdUnidadUmbral(Long idUnidadumbral) {
		this.idUnidadumbral = idUnidadumbral;
	}

	public String getTexto() {
		return this.texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	public Long getIdFirmaCasco() {
		return idFirmaCasco;
	}

	public void setIdFirmaCasco(Long idFirmaCasco) {
		this.idFirmaCasco = idFirmaCasco;
	}

	public List<UnidadMensajeEB> getUnidadesMensajes() {
		return this.unidadesmensajes;
	}

	public void setUnidadesMensajes(List<UnidadMensajeEB> unidadesmensajes) {
		this.unidadesmensajes = unidadesmensajes;
	}

	public UnidadMensajeEB addUnidadesMensaje(UnidadMensajeEB unidadesmensaje) {
		getUnidadesMensajes().add(unidadesmensaje);
		unidadesmensaje.setMensaje(this);

		return unidadesmensaje;
	}

	public UnidadMensajeEB removeUnidadesMensaje(UnidadMensajeEB unidadesmensaje) {
		getUnidadesMensajes().remove(unidadesmensaje);
		unidadesmensaje.setMensaje(null);

		return unidadesmensaje;
	}

}