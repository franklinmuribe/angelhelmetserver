package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the empresas database table.
 * 
 */
@Entity
@Table(name = "cargos")

//@NamedQuery(name = "CargoEB.findAll", query = "SELECT e FROM CargoEB e")
//@NamedQueries({
//		@NamedQuery(name = "Cargo.getCargoById", query = "SELECT e FROM CargoEB e WHERE e.idCargo = :id"),
//		@NamedQuery(name = "Cargo.getCargoByUnidad", query = "SELECT e FROM CargoEB e "
//				+ "JOIN e.operarioscargos oe JOIN oe.operario o JOIN o.unidadescargos uo "
//				+ "WHERE oe.activo=TRUE and uo.activo=TRUE AND uo.unidad.idUnidad = :id_unidad")
//
//})
public class CargoEB implements Serializable
{
	private static final long serialVersionUID = -8052659919830527615L;

	@Id
	@GeneratedValue(generator = "cargo_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "cargo_seq", sequenceName = "cargo_id_cargo_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_cargo", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idCargo;

	@Column(name = "descripcion", length = 100)
	private String descripcion;

	// bi-directional many-to-one association to OperarioEmpresaEB
	@OneToMany(mappedBy = "cargo")
	private List<OperarioCargoEB> operarioscargos;

	public CargoEB()
	{
	}

	public Integer getIdCargo() {
		return idCargo;
	}

	public void setIdCargo(Integer idCargo) {
		this.idCargo = idCargo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<OperarioCargoEB> getOperarioscargos() {
		return operarioscargos;
	}

	public void setOperarioscargos(List<OperarioCargoEB> operarioscargos) {
		this.operarioscargos = operarioscargos;
	}

//	public OperarioEmpresaEB addOperariosEmpresa(OperarioEmpresaEB operariosempresa)
//	{
//		getOperariosEmpresas().add(operariosempresa);
//		operariosempresa.setEmpresa(this);
//
//		return operariosempresa;
//	}
//
//	public OperarioEmpresaEB removeOperariosEmpresa(OperarioEmpresaEB operariosempresa)
//	{
//		getOperariosEmpresas().remove(operariosempresa);
//		operariosempresa.setEmpresa(null);
//
//		return operariosempresa;
//	}

}