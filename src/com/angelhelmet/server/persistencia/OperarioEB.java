package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the operarios database table.
 * 
 */
@Entity
@Table(name = "operarios")
@NamedQuery(name = "OperarioEB.findAll", query = "SELECT o FROM OperarioEB o")
@NamedQueries({
		// @NamedQuery(name="Operario.getOperarioByUnidad",
		// query="SELECT o FROM OperarioEB o JOIN o.unidadesoperarios uo JOIN
		// o.operarioscargos oc JOIN o.operariosempresas oe WHERE uo.unidad.idUnidad =
		// :id AND oc.activo = TRUE AND oe.activo=TRUE")
		@NamedQuery(name = "Operario.getOperarioByUnidad", query = "SELECT o FROM OperarioEB o JOIN o.unidadesoperarios uo WHERE uo.unidad.idUnidad = :id AND o.activo = TRUE AND uo.activo = TRUE")
// @NamedQuery(name="Operario.getOperarioByUnidad",
// query="SELECT o FROM OperarioEB o "
// + "JOIN FETCH o.unidadesoperarios, "
// + "JOIN FETCH o.operarioscargos, "
// + "JOIN FETCH o.operariosempresas "
// +
// "WHERE o.unidadesoperarios.unidad.idUnidad = :id AND o.operarioscargos.activo = TRUE AND o.operariosempresas.activo=TRUE")
})
public class OperarioEB implements Serializable {
	private static final long serialVersionUID = -1779781193917588673L;

	@Id
	@GeneratedValue(generator = "operario_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "operario_seq", sequenceName = "operarios_id_operario_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_operario", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idOperario;

	@Column(nullable = false)
	private Boolean activo;

	@Column(nullable = false, length = 200)
	private String apellidos;

	private Integer color;

	@Column(name = "es_root", nullable = false)
	private Boolean esRoot;

	@Column(name = "fecha_activacion")
	private Timestamp fechaActivacion;

	@Column(name = "fecha_alta", nullable = false)
	private Timestamp fechaAlta;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_nacimiento")
	private Date fechaNacimiento;

	@Column(nullable = false, length = 50)
	private String identificacion;

	@Column(nullable = false, length = 100)
	private String nombre;

	// @Column(length = 2147483647)
	@Column(columnDefinition = "TEXT")
	private String notas;

	// Added by Gunjan Start
	private String supervisor;

	private String blood;

	private String emergency;

	private String pais;

	private String rol;

	private Boolean deleted;

	@Column(name = "sms_number")
	private String smsNumber;
	// Added by Gunjan Ends

	// bi-directional many-to-one association to OperarioEmpresaEB
	@OneToMany(mappedBy = "operario")
	private List<OperarioEmpresaEB> operariosempresas;

	// bi-directional many-to-one association to UnidadOperarioEB
	@OneToMany(mappedBy = "operario")
	private List<UnidadOperarioEB> unidadesoperarios;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "operario", fetch = FetchType.EAGER)
	private List<OperarioCargoEB> operarioscargos;

	public OperarioEB() {
	}

	public Integer getIdOperario() {
		return this.idOperario;
	}

	public void setIdOperario(Integer idOperario) {
		this.idOperario = idOperario;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public String getApellidos() {
		return this.apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public Integer getColor() {
		return this.color;
	}

	public void setColor(Integer color) {
		this.color = color;
	}

	public Boolean getEsRoot() {
		return this.esRoot;
	}

	public void setEsRoot(Boolean esRoot) {
		this.esRoot = esRoot;
	}

	public Timestamp getFechaActivacion() {
		return this.fechaActivacion;
	}

	public void setFechaActivacion(Timestamp fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getIdentificacion() {
		return this.identificacion;
	}

	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getNotas() {
		return this.notas;
	}

	public void setNotas(String notas) {
		this.notas = notas;
	}

	public List<OperarioEmpresaEB> getOperariosEmpresas() {
		return this.operariosempresas;
	}

	public void setOperariosEmpresas(List<OperarioEmpresaEB> operariosempresas) {
		this.operariosempresas = operariosempresas;
	}

	public OperarioEmpresaEB addOperariosEmpresa(OperarioEmpresaEB operariosempresa) {
		getOperariosEmpresas().add(operariosempresa);
		operariosempresa.setOperario(this);

		return operariosempresa;
	}

	public OperarioEmpresaEB removeOperariosEmpresa(OperarioEmpresaEB operariosempresa) {
		getOperariosEmpresas().remove(operariosempresa);
		operariosempresa.setOperario(null);

		return operariosempresa;
	}

	public List<UnidadOperarioEB> getUnidadesOperarios() {
		return this.unidadesoperarios;
	}

	public void setUnidadesOperarios(List<UnidadOperarioEB> unidadesoperarios) {
		this.unidadesoperarios = unidadesoperarios;
	}

	public UnidadOperarioEB addUnidadesOperario(UnidadOperarioEB unidadesoperario) {
		getUnidadesOperarios().add(unidadesoperario);
		unidadesoperario.setOperario(this);

		return unidadesoperario;
	}

	public UnidadOperarioEB removeUnidadesOperario(UnidadOperarioEB unidadesoperario) {
		getUnidadesOperarios().remove(unidadesoperario);
		unidadesoperario.setOperario(null);

		return unidadesoperario;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}

	public String getEmergency() {
		return emergency;
	}

	public void setEmergency(String emergency) {
		this.emergency = emergency;
	}

	public String getSmsNumber() {
		return smsNumber;
	}

	public void setSmsNumber(String smsNumber) {
		this.smsNumber = smsNumber;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getRol() {
		return rol;
	}

	public void setRol(String rol) {
		this.rol = rol;
	}

	public List<OperarioEmpresaEB> getOperariosempresas() {
		return operariosempresas;
	}

	public void setOperariosempresas(List<OperarioEmpresaEB> operariosempresas) {
		this.operariosempresas = operariosempresas;
	}

	public List<UnidadOperarioEB> getUnidadesoperarios() {
		return unidadesoperarios;
	}

	public void setUnidadesoperarios(List<UnidadOperarioEB> unidadesoperarios) {
		this.unidadesoperarios = unidadesoperarios;
	}

	public List<OperarioCargoEB> getOperarioscargos() {
		return operarioscargos;
	}

	public void setOperarioscargos(List<OperarioCargoEB> operarioscargos) {
		this.operarioscargos = operarioscargos;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

}