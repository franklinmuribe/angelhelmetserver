package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;

/**
 * The persistent class for the zonasalarmas database table.
 * 
 */
@Entity
@Table(name = "zonasalarmas")
@NamedQuery(name = "ZonaAlarmaEB.findAll", query = "SELECT z FROM ZonaAlarmaEB z")
@NamedQueries({
		@NamedQuery(name = "ZonaAlarmaEB.findPermisosUnidad", query = "SELECT z FROM ZonaAlarmaEB z JOIN z.zonaspermisos zp WHERE z.zona.idZona = :id_zona AND zp.unidad.idUnidad = :id_unidad AND z.activa = TRUE")
})
public class ZonaAlarmaEB implements Serializable
{
	private static final long serialVersionUID = 1399243877716761345L;

	@Id
	@GeneratedValue(generator = "zonaalarma_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "zonaalarma_seq", sequenceName = "zonasalarmas_id_zonaalarma_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_zonaalarma", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idZonaalarma;

	@Column(nullable = false)
	private Boolean activa;

	// bi-directional many-to-one association to AlarmaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_alarma", nullable = false)
	private AlarmaEB alarma;

	// bi-directional many-to-one association to ZonaEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_zona", nullable = false)
	private ZonaEB zona;

	// bi-directional many-to-one association to ZonaPermisoEB
	@OneToMany(mappedBy = "zonasalarma")
	private List<ZonaPermisoEB> zonaspermisos;

	public ZonaAlarmaEB()
	{
	}

	public Integer getIdZonaAlarma()
	{
		return this.idZonaalarma;
	}

	public void setIdZonaAlarma(Integer idZonaalarma)
	{
		this.idZonaalarma = idZonaalarma;
	}

	public Boolean getActiva()
	{
		return this.activa;
	}

	public void setActiva(Boolean activa)
	{
		this.activa = activa;
	}

	public AlarmaEB getAlarma()
	{
		return this.alarma;
	}

	public void setAlarma(AlarmaEB alarma)
	{
		this.alarma = alarma;
	}

	public ZonaEB getZona()
	{
		return this.zona;
	}

	public void setZona(ZonaEB zona)
	{
		this.zona = zona;
	}

	public List<ZonaPermisoEB> getZonasPermisos()
	{
		return this.zonaspermisos;
	}

	public void setZonasPermisos(List<ZonaPermisoEB> zonaspermisos)
	{
		this.zonaspermisos = zonaspermisos;
	}

	public ZonaPermisoEB addZonasPermiso(ZonaPermisoEB zonaspermiso)
	{
		getZonasPermisos().add(zonaspermiso);
		zonaspermiso.setZonasAlarma(this);

		return zonaspermiso;
	}

	public ZonaPermisoEB removeZonasPermiso(ZonaPermisoEB zonaspermiso)
	{
		getZonasPermisos().remove(zonaspermiso);
		zonaspermiso.setZonasAlarma(null);

		return zonaspermiso;
	}

}