/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "dias_lectura")
public class DiasLecturaEB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "integer")
    @Column(name = "id_dias_lectura", unique = true)
    private Integer idDiasLectura;
    @Column(name = "lunes")
    private Boolean lunes;
    @Column(name = "martes")
    private Boolean martes;
    @Column(name = "miercoles")
    private Boolean miercoles;
    @Column(name = "jueves")
    private Boolean jueves;
    @Column(name = "viernes")
    private Boolean viernes;
    @Column(name = "sabado")
    private Boolean sabado;
    @Column(name = "domingo")
    private Boolean domingo;
    @JoinColumn(name = "id_unidad_salud", referencedColumnName = "id_unidad_salud")
    @ManyToOne
    private UnidadSaludEB idUnidadSalud;

    public DiasLecturaEB() {
    }

    public DiasLecturaEB(Integer idDiasLectura) {
        this.idDiasLectura = idDiasLectura;
    }

    public Integer getIdDiasLectura() {
        return idDiasLectura;
    }

    public void setIdDiasLectura(Integer idDiasLectura) {
        this.idDiasLectura = idDiasLectura;
    }

    public Boolean getLunes() {
        return lunes;
    }

    public void setLunes(Boolean lunes) {
        this.lunes = lunes;
    }

    public Boolean getMartes() {
        return martes;
    }

    public void setMartes(Boolean martes) {
        this.martes = martes;
    }

    public Boolean getMiercoles() {
        return miercoles;
    }

    public void setMiercoles(Boolean miercoles) {
        this.miercoles = miercoles;
    }

    public Boolean getJueves() {
        return jueves;
    }

    public void setJueves(Boolean jueves) {
        this.jueves = jueves;
    }

    public Boolean getViernes() {
        return viernes;
    }

    public void setViernes(Boolean viernes) {
        this.viernes = viernes;
    }

    public Boolean getSabado() {
        return sabado;
    }

    public void setSabado(Boolean sabado) {
        this.sabado = sabado;
    }

    public Boolean getDomingo() {
        return domingo;
    }

    public void setDomingo(Boolean domingo) {
        this.domingo = domingo;
    }

   

    public UnidadSaludEB getIdUnidadSalud() {
		return idUnidadSalud;
	}

	public void setIdUnidadSalud(UnidadSaludEB idUnidadSalud) {
		this.idUnidadSalud = idUnidadSalud;
	}

	@Override
    public int hashCode() {
        int hash = 0;
        hash += (idDiasLectura != null ? idDiasLectura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof DiasLecturaEB)) {
            return false;
        }
        DiasLecturaEB other = (DiasLecturaEB) object;
        if ((this.idDiasLectura == null && other.idDiasLectura != null) || (this.idDiasLectura != null && !this.idDiasLectura.equals(other.idDiasLectura))) {
            return false;
        }
        return true;
    }

	

    
}
