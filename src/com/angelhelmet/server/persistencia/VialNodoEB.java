package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the vialesnodos database table.
 * 
 */
@Entity
@Table(name = "vialesnodos")
@NamedQuery(name = "VialNodoEB.findAll", query = "SELECT v FROM VialNodoEB v")
public class VialNodoEB implements Serializable
{
	private static final long serialVersionUID = -1036387984286025306L;

	@Id
	@GeneratedValue(generator = "vialnodo_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "vialnodo_seq", sequenceName = "viales_nodos_id_vialnodo_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_vialnodo", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idVialnodo;

	@Column(name = "id_nodo", nullable = false)
	private Integer idNodo;

	@Column(name = "id_vial", nullable = false)
	private Integer idVial;

	public VialNodoEB()
	{
	}

	public Integer getIdVialnodo()
	{
		return this.idVialnodo;
	}

	public void setIdVialnodo(Integer idVialnodo)
	{
		this.idVialnodo = idVialnodo;
	}

	public Integer getIdNodo()
	{
		return this.idNodo;
	}

	public void setIdNodo(Integer idNodo)
	{
		this.idNodo = idNodo;
	}

	public Integer getIdVial()
	{
		return this.idVial;
	}

	public void setIdVial(Integer idVial)
	{
		this.idVial = idVial;
	}

}