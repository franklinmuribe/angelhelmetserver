package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;

/**
 * The persistent class for the unidadesmensajes database table.
 * 
 */
@Entity
@Table(name = "unidadesmensajes")
@NamedQuery(name = "UnidadMensajeEB.findAll", query = "SELECT u FROM UnidadMensajeEB u")
@NamedQueries({
		@NamedQuery(name = "UnidadMensaje.purgarMensajes", query = "SELECT um FROM UnidadMensajeEB um JOIN um.unidad u JOIN um.mensaje m WHERE um.idMensajeestado = 1 AND u.idUnidad = :id_unidad AND m.idTipomensaje <> 2 ORDER BY m.idMensaje"),
		@NamedQuery(name = "UnidadMensaje.getMensajesByIdUnidad", query = "SELECT um FROM UnidadMensajeEB um JOIN um.unidad u JOIN um.mensaje m WHERE um.idMensajeestado = 1 AND u.idUnidad = :id_unidad ORDER BY m.idMensaje desc"),
		@NamedQuery(name = "UnidadMensaje.getUnidadMensajeByIdUnidad", query = "SELECT um FROM UnidadMensajeEB um WHERE um.idUnidadmensaje = :id_unidadmensaje AND um.idMensajeestado = 1")
})
public class UnidadMensajeEB implements Serializable
{
	private static final long serialVersionUID = 1591511424677471568L;

	@Id
	@GeneratedValue(generator = "unidadmensage_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "unidadmensage_seq", sequenceName = "unidadesmensajes_id_unidadmensage_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_unidadmensaje", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idUnidadmensaje;

	@Column(nullable = false)
	private Boolean confirmacion;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	@Column(name = "fecha_lectura")
	private Timestamp fechaLectura;

	@Column(name = "id_mensajeestado", nullable = false)
	private Integer idMensajeestado;

	@Column(name = "id_sesion")
	private Long idSesion;

	// bi-directional many-to-one association to MensajeEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_mensaje", nullable = false)
	private MensajeEB mensaje;

	// bi-directional many-to-one association to UnidadEB
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id_unidad", nullable = false)
	private UnidadEB unidad;

	public UnidadMensajeEB()
	{
	}

	public Long getIdUnidadMensaje()
	{
		return this.idUnidadmensaje;
	}

	public void setIdUnidadMensaje(Long idUnidadmensaje)
	{
		this.idUnidadmensaje = idUnidadmensaje;
	}

	public Boolean getConfirmacion()
	{
		return this.confirmacion;
	}

	public void setConfirmacion(Boolean confirmacion)
	{
		this.confirmacion = confirmacion;
	}

	public Timestamp getFechaBaja()
	{
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja)
	{
		this.fechaBaja = fechaBaja;
	}

	public Timestamp getFechaLectura()
	{
		return this.fechaLectura;
	}

	public void setFechaLectura(Timestamp fechaLectura)
	{
		this.fechaLectura = fechaLectura;
	}

	public Integer getIdMensajeEstado()
	{
		return this.idMensajeestado;
	}

	public void setIdMensajeEstado(Integer idMensajeestado)
	{
		this.idMensajeestado = idMensajeestado;
	}

	public Long getIdSesion()
	{
		return this.idSesion;
	}

	public void setIdSesion(Long idSesion)
	{
		this.idSesion = idSesion;
	}

	public MensajeEB getMensaje()
	{
		return this.mensaje;
	}

	public void setMensaje(MensajeEB mensaje)
	{
		this.mensaje = mensaje;
	}

	public UnidadEB getUnidad()
	{
		return this.unidad;
	}

	public void setUnidad(UnidadEB unidad)
	{
		this.unidad = unidad;
	}
	

}
