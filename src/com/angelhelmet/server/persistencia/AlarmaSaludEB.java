/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "alarma_salud")

public class AlarmaSaludEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_alarma_salud", unique = true)
	private Integer idAlarmaSalud;
	@Column(name = "st_activo")
	private Boolean stActivo;
	@JoinColumn(name = "id_tipo_medicion", referencedColumnName = "id_tipo_medicion")
	@ManyToOne
	private TipoMedicionEB idTipoMedicion;
	@JoinColumn(name = "id_unidad_salud", referencedColumnName = "id_unidad_salud")
	@ManyToOne
	private UnidadSaludEB idUnidadSalud;

	public AlarmaSaludEB() {
	}

	public AlarmaSaludEB(Integer idAlarmaSalud) {
		this.idAlarmaSalud = idAlarmaSalud;
	}

	public Integer getIdAlarmaSalud() {
		return idAlarmaSalud;
	}

	public void setIdAlarmaSalud(Integer idAlarmaSalud) {
		this.idAlarmaSalud = idAlarmaSalud;
	}

	public Boolean getStActivo() {
		return stActivo;
	}

	public void setStActivo(Boolean stActivo) {
		this.stActivo = stActivo;
	}

	public TipoMedicionEB getIdTipoMedicion() {
		return idTipoMedicion;
	}

	public void setIdTipoMedicion(TipoMedicionEB idTipoMedicion) {
		this.idTipoMedicion = idTipoMedicion;
	}

	public UnidadSaludEB getIdUnidadSalud() {
		return idUnidadSalud;
	}

	public void setIdUnidadSalud(UnidadSaludEB idUnidadSalud) {
		this.idUnidadSalud = idUnidadSalud;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (idAlarmaSalud != null ? idAlarmaSalud.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (!(object instanceof AlarmaSaludEB)) {
			return false;
		}
		AlarmaSaludEB other = (AlarmaSaludEB) object;
		if ((this.idAlarmaSalud == null && other.idAlarmaSalud != null)
				|| (this.idAlarmaSalud != null && !this.idAlarmaSalud.equals(other.idAlarmaSalud))) {
			return false;
		}
		return true;
	}

	

}
