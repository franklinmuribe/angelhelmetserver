package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

import org.hibernate.annotations.Where;
import org.hibernate.annotations.WhereJoinTable;

import com.hcl.iot.smartworker.model.UnidadSaludModel;

import java.sql.Timestamp;
import java.util.List;

/**
 * The persistent class for the unidades database table.
 * 
 */
@Entity
@Table(name = "unidades")
@NamedQuery(name = "UnidadEB.findAll", query = "SELECT u FROM UnidadEB u")
@NamedQueries({
		@NamedQuery(name = "Unidad.getBySerie", query = "SELECT u FROM UnidadEB u WHERE u.numeroSerie = :serie"),
		@NamedQuery(name = "Unidad.getById", query = "SELECT u FROM UnidadEB u WHERE u.idUnidad = :id  AND u.activo = TRUE") })
public class UnidadEB implements Serializable {

	private static final long serialVersionUID = 5418803610518978546L;

	@Id
	@GeneratedValue(generator = "id_unidad_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "id_unidad_seq", sequenceName = "unidades_id_unidad_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_unidad", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idUnidad;

	@Column(name = "numero_serie", nullable = false, length = 16)
	private String numeroSerie;

	@Column(name = "fecha_alta", nullable = false)
	private Timestamp fechaAlta;

	@Column(name = "fecha_baja")
	private Timestamp fechaBaja;

	@Column(nullable = false)
	private Boolean activo;

	@Column(nullable = false)
	private Boolean asignada;

	@Column(name = "fecha_activacion")
	private Timestamp fechaActivacion;

	@Column(name = "api_key")
	private String APIKey;

	@Column(name = "authentication_method")
	private String AuthenticationMethod;

	@Column(name = "authentication_token")
	private String AuthenticationToken;

	@Column(name = "clean_session")
	private boolean CleanSession;

	@Column(name = "device_id")
	private String DeviceID;

	@Column(name = "device_type")
	private String DeviceType;

	@Column(name = "organization_id")
	private String OrganizationID;

	@Column(name = "shared_subscription")
	private boolean SharedSubscription;

	private String authtoken;

	@Column(name = "id_mina")
	private String idMina;

	private String ip;

	private String mascara;

	@Column(name = "puerta_enlace")
	private String puertEnlace;

	private String telefono;

	private String wsbroker;

	private Boolean deleted;
	
	@Column(name = "mask_address", nullable = true)
	private String maskAddress;

	// bi-directional many-to-one association to UnidadMensajeEB
	@OneToMany(mappedBy = "unidad")
	private List<UnidadMensajeEB> unidadesMensajes;

	// bi-directional many-to-one association to UnidadOperarioEB
	@OneToMany(mappedBy = "unidad" )
	@Where(clause = "activo = 'TRUE'")
	private List<UnidadOperarioEB> unidadesOperarios;

	// bi-directional many-to-one association to ZonaPermisoEB
	@OneToMany(mappedBy = "unidad")
	private List<ZonaPermisoEB> zonasPermisos;
	
	@OneToMany(mappedBy = "idUnidad", cascade = CascadeType.ALL)
    private List<UnidadSaludEB> unidadSaludSet;

	public UnidadEB() {
	}

	public Integer getIdUnidad() {
		return this.idUnidad;
	}

	public void setIdUnidad(Integer idUnidad) {
		this.idUnidad = idUnidad;
	}

	public Boolean getActivo() {
		return this.activo;
	}

	public void setActivo(Boolean activo) {
		this.activo = activo;
	}

	public Boolean getAsignada() {
		return this.asignada;
	}

	public void setAsignada(Boolean asignada) {
		this.asignada = asignada;
	}

	public Timestamp getFechaActivacion() {
		return this.fechaActivacion;
	}

	public void setFechaActivacion(Timestamp fechaActivacion) {
		this.fechaActivacion = fechaActivacion;
	}

	public Timestamp getFechaAlta() {
		return this.fechaAlta;
	}

	public void setFechaAlta(Timestamp fechaAlta) {
		this.fechaAlta = fechaAlta;
	}

	public Timestamp getFechaBaja() {
		return this.fechaBaja;
	}

	public void setFechaBaja(Timestamp fechaBaja) {
		this.fechaBaja = fechaBaja;
	}

	public String getNumeroSerie() {
		return this.numeroSerie;
	}

	public void setNumeroSerie(String numeroSerie) {
		this.numeroSerie = numeroSerie;
	}

	public List<UnidadMensajeEB> getUnidadesMensajes() {
		return this.unidadesMensajes;
	}

	public void setUnidadesMensajes(List<UnidadMensajeEB> unidadesmensajes) {
		this.unidadesMensajes = unidadesmensajes;
	}

	public UnidadMensajeEB addUnidadesMensaje(UnidadMensajeEB unidadesmensaje) {
		getUnidadesMensajes().add(unidadesmensaje);
		unidadesmensaje.setUnidad(this);

		return unidadesmensaje;
	}

	public UnidadMensajeEB removeUnidadesMensaje(UnidadMensajeEB unidadesmensaje) {
		getUnidadesMensajes().remove(unidadesmensaje);
		unidadesmensaje.setUnidad(null);

		return unidadesmensaje;
	}

	public List<UnidadOperarioEB> getUnidadesOperarios() {
		return this.unidadesOperarios;
	}

	public void setUnidadesOperarios(List<UnidadOperarioEB> unidadesoperarios) {
		this.unidadesOperarios = unidadesoperarios;
	}

	public UnidadOperarioEB addUnidadesOperario(UnidadOperarioEB unidadesoperario) {
		getUnidadesOperarios().add(unidadesoperario);
		unidadesoperario.setUnidade(this);

		return unidadesoperario;
	}

	public UnidadOperarioEB removeUnidadesOperario(UnidadOperarioEB unidadesoperario) {
		getUnidadesOperarios().remove(unidadesoperario);
		unidadesoperario.setUnidade(null);

		return unidadesoperario;
	}

	public List<ZonaPermisoEB> getZonasPermiso() {
		return this.zonasPermisos;
	}

	public void setZonasPermiso(List<ZonaPermisoEB> zonaspermisos) {
		this.zonasPermisos = zonaspermisos;
	}

	public ZonaPermisoEB addZonasPermiso(ZonaPermisoEB zonaspermiso) {
		getZonasPermiso().add(zonaspermiso);
		zonaspermiso.setUnidad(this);

		return zonaspermiso;
	}

	public ZonaPermisoEB removeZonasPermiso(ZonaPermisoEB zonaspermiso) {
		getZonasPermiso().remove(zonaspermiso);
		zonaspermiso.setUnidad(null);

		return zonaspermiso;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public String getPuertEnlace() {
		return puertEnlace;
	}

	public void setPuertEnlace(String puertEnlace) {
		this.puertEnlace = puertEnlace;
	}

	public String getIdMina() {
		return idMina;
	}

	public void setIdMina(String idMina) {
		this.idMina = idMina;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getOrganizationID() {
		return OrganizationID;
	}

	public void setOrganizationID(String organizationID) {
		OrganizationID = organizationID;
	}

	public String getAuthenticationMethod() {
		return AuthenticationMethod;
	}

	public void setAuthenticationMethod(String authenticationMethod) {
		AuthenticationMethod = authenticationMethod;
	}

	public String getAPIKey() {
		return APIKey;
	}

	public void setAPIKey(String aPIKey) {
		APIKey = aPIKey;
	}

	public String getAuthenticationToken() {
		return AuthenticationToken;
	}

	public void setAuthenticationToken(String authenticationToken) {
		AuthenticationToken = authenticationToken;
	}

	public String getDeviceType() {
		return DeviceType;
	}

	public void setDeviceType(String deviceType) {
		DeviceType = deviceType;
	}

	public String getDeviceID() {
		return DeviceID;
	}

	public void setDeviceID(String deviceID) {
		DeviceID = deviceID;
	}

	public boolean isSharedSubscription() {
		return SharedSubscription;
	}

	public void setSharedSubscription(boolean sharedSubscription) {
		SharedSubscription = sharedSubscription;
	}

	public boolean isCleanSession() {
		return CleanSession;
	}

	public void setCleanSession(boolean cleanSession) {
		CleanSession = cleanSession;
	}

	public String getWsbroker() {
		return wsbroker;
	}

	public void setWsbroker(String wsbroker) {
		this.wsbroker = wsbroker;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public List<UnidadSaludEB> getUnidadSaludSet() {
		return unidadSaludSet;
	}

	public void setUnidadSaludSet(List<UnidadSaludEB> unidadSaludSet) {
		this.unidadSaludSet = unidadSaludSet;
	}

	public String getMaskAddress() {
		return maskAddress;
	}

	public void setMaskAddress(String maskAddress) {
		this.maskAddress = maskAddress;
	}
	
	
	
	

}
