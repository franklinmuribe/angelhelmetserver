package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.sql.Timestamp;

/**
 * The persistent class for the logs database table.
 * 
 */
@Entity
@Table(name = "logs")
@NamedQuery(name = "LogEB.findAll", query = "SELECT l FROM LogEB l")
@NamedQueries({
		// @NamedQuery(name = "Log.getLastLogByIdUnidad", query =
		// "SELECT l FROM LogEB l JOIN l.unidad u WHERE u.idUnidad = :id_unidad ORDER BY
		// l.idLog DESC"),
		@NamedQuery(name = "Log.getLogsByFechas", query = "SELECT l FROM LogEB l WHERE l.fechaLog BETWEEN :inicio AND :final AND l.idUnidad in :unidades AND l.idMapa = :id_mapa ORDER BY l.idLog ASC"),
		@NamedQuery(name = "Log.getLogByIdLog", query = "SELECT l FROM LogEB l WHERE l.idLog = :id"),
		@NamedQuery(name = "Log.getLogByFirma", query = "SELECT l FROM LogEB l WHERE l.firma = :firma ORDER BY l.idLog DESC"), })
public class LogEB implements Serializable {
	private static final long serialVersionUID = 6615682068976599445L;

	@Id
	@GeneratedValue(generator = "log_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "log_seq", sequenceName = "logs_id_log_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_log", unique = true, nullable = false, insertable = false, updatable = false)
	private Long idLog;

	private Integer bateria;

	private float exactitud;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	@Column(name = "fecha_log")
	private Timestamp fechaLog;

	private Integer gas;

	@Column(name = "id_alarma", nullable = false)
	private Integer idAlarma;

	@Column(name = "id_mapa", nullable = false)
	private Integer idMapa;

	@Column(name = "id_unidad", nullable = false)
	private Integer idUnidad;

	private double latitud;

	@Column(name = "latitud_c", length = 1)
	private String latitudC;

	@Column(nullable = false)
	private Boolean leido;

	private double longitud;

	@Column(name = "longitud_c", length = 1)
	private String longitudC;

	@Column(name = "mac_1", length = 17)
	private String mac1;

	@Column(name = "mac_2", length = 17)
	private String mac2;

	@Column(name = "mac_3", length = 17)
	private String mac3;

	@Column(name = "mac_4", length = 17)
	private String mac4;

	@Column(nullable = false)
	private Integer mensaje;

	@Column(name = "rssi_1")
	private Integer rssi1;

	@Column(name = "rssi_2")
	private Integer rssi2;

	@Column(name = "rssi_3")
	private Integer rssi3;

	@Column(name = "rssi_4")
	private Integer rssi4;

	private Integer satelites;

	@Column(name = "sensor_x")
	private Integer sensorX;

	@Column(name = "sensor_y")
	private Integer sensorY;

	@Column(name = "sensor_z")
	private Integer sensorZ;

	private Integer tiempo;

	private Integer x;

	private Integer y;

	private Integer temperatura;

	private Boolean volcado;

	private String debug;

	@Column(name = "firma")
	private Long firma;

	private int respuesta;

	private String respuesta_sms;

	@Column(name = "canal_wt")
	private Integer canalWt;

	@Column(name = "modo_wt")
	private Integer modoWt;

	@Column(name = "health_temperature")
	private Integer healthTemperature;

	@Column(name = "health_pressure_sistole")
	private Integer healthPressureSistole;

	@Column(name = "health_pressure_diastole")
	private Integer healthPressureDiastole;

	@Column(name = "health_oxygen_sp02")
	private Integer healthOxygenSp02;

	@Column(name = "health_heartbeat")
	private Integer healthHeartbeat;

	public LogEB() {
	}

	public Long getIdLog() {
		return this.idLog;
	}

	public void setIdLog(Long idLog) {
		this.idLog = idLog;
	}

	public Integer getBateria() {
		return this.bateria;
	}

	public void setBateria(Integer bateria) {
		this.bateria = bateria;
	}

	public float getExactitud() {
		return this.exactitud;
	}

	public void setExactitud(float exactitud) {
		this.exactitud = exactitud;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Timestamp getFechaLog() {
		return this.fechaLog;
	}

	public void setFechaLog(Timestamp fechaLog) {
		this.fechaLog = fechaLog;
	}

	public Integer getGas() {
		return this.gas;
	}

	public void setGas(Integer gas) {
		this.gas = gas;
	}

	public Integer getIdAlarma() {
		return this.idAlarma;
	}

	public void setIdAlarma(Integer idAlarma) {
		this.idAlarma = idAlarma;
	}

	public Integer getIdMapa() {
		return this.idMapa;
	}

	public void setIdMapa(Integer idMapa) {
		this.idMapa = idMapa;
	}

	public Integer getIdUnidad() {
		return this.idUnidad;
	}

	public void setIdUnidad(Integer idUnidad) {
		this.idUnidad = idUnidad;
	}

	public double getLatitud() {
		return this.latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public String getLatitudC() {
		return this.latitudC;
	}

	public void setLatitudC(String latitudC) {
		this.latitudC = latitudC;
	}

	public Boolean getLeido() {
		return this.leido;
	}

	public void setLeido(Boolean leido) {
		this.leido = leido;
	}

	public double getLongitud() {
		return this.longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public String getLongitudC() {
		return this.longitudC;
	}

	public void setLongitudC(String longitudC) {
		this.longitudC = longitudC;
	}

	public String getMac1() {
		return this.mac1;
	}

	public void setMac1(String mac1) {
		this.mac1 = mac1;
	}

	public String getMac2() {
		return this.mac2;
	}

	public void setMac2(String mac2) {
		this.mac2 = mac2;
	}

	public String getMac3() {
		return this.mac3;
	}

	public void setMac3(String mac3) {
		this.mac3 = mac3;
	}

	public String getMac4() {
		return this.mac4;
	}

	public void setMac4(String mac4) {
		this.mac4 = mac4;
	}

	public Integer getMensaje() {
		return this.mensaje;
	}

	public void setMensaje(Integer mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getRssi1() {
		return this.rssi1;
	}

	public void setRssi1(Integer rssi1) {
		this.rssi1 = rssi1;
	}

	public Integer getRssi2() {
		return this.rssi2;
	}

	public void setRssi2(Integer rssi2) {
		this.rssi2 = rssi2;
	}

	public Integer getRssi3() {
		return this.rssi3;
	}

	public void setRssi3(Integer rssi3) {
		this.rssi3 = rssi3;
	}

	public Integer getRssi4() {
		return this.rssi4;
	}

	public void setRssi4(Integer rssi4) {
		this.rssi4 = rssi4;
	}

	public Integer getSatelites() {
		return this.satelites;
	}

	public void setSatelites(Integer satelites) {
		this.satelites = satelites;
	}

	public Integer getSensorX() {
		return this.sensorX;
	}

	public void setSensorX(Integer sensorX) {
		this.sensorX = sensorX;
	}

	public Integer getSensorY() {
		return this.sensorY;
	}

	public void setSensorY(Integer sensorY) {
		this.sensorY = sensorY;
	}

	public Integer getSensorZ() {
		return this.sensorZ;
	}

	public void setSensorZ(Integer sensorZ) {
		this.sensorZ = sensorZ;
	}

	public Integer getTiempo() {
		return this.tiempo;
	}

	public void setTiempo(Integer tiempo) {
		this.tiempo = tiempo;
	}

	public Integer getX() {
		return x;
	}

	public void setX(Integer x) {
		this.x = x;
	}

	public Integer getY() {
		return y;
	}

	public void setY(Integer y) {
		this.y = y;
	}

	public Integer getTemperatura() {
		return temperatura;
	}

	public void setTemperatura(Integer temperatura) {
		this.temperatura = temperatura;
	}

	public Boolean getVolcado() {
		return volcado;
	}

	public void setVolcado(Boolean volcado) {
		this.volcado = volcado;
	}

	public String getDebug() {
		return debug;
	}

	public void setDebug(String debug) {
		this.debug = debug;
	}

	public Long getFirma() {
		return firma;
	}

	public void setFirma(Long firma) {
		this.firma = firma;
	}

	public int getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(int respuesta) {
		this.respuesta = respuesta;
	}

	public String getRespuesta_sms() {
		return respuesta_sms;
	}

	public void setRespuesta_sms(String respuesta_sms) {
		this.respuesta_sms = respuesta_sms;
	}

	public Integer getCanalWt() {
		return canalWt;
	}

	public void setCanalWt(Integer canalWt) {
		this.canalWt = canalWt;
	}

	public Integer getModoWt() {
		return modoWt;
	}

	public void setModoWt(Integer modoWt) {
		this.modoWt = modoWt;
	}

	public Integer getHealthTemperature() {
		return healthTemperature;
	}

	public void setHealthTemperature(Integer healthTemperature) {
		this.healthTemperature = healthTemperature;
	}

	public Integer getHealthPressureSistole() {
		return healthPressureSistole;
	}

	public void setHealthPressureSistole(Integer healthPressureSistole) {
		this.healthPressureSistole = healthPressureSistole;
	}

	public Integer getHealthPressureDiastole() {
		return healthPressureDiastole;
	}

	public void setHealthPressureDiastole(Integer healthPressureDiastole) {
		this.healthPressureDiastole = healthPressureDiastole;
	}

	public Integer getHealthOxygenSp02() {
		return healthOxygenSp02;
	}

	public void setHealthOxygenSp02(Integer healthOxygenSp02) {
		this.healthOxygenSp02 = healthOxygenSp02;
	}

	public Integer getHealthHeartbeat() {
		return healthHeartbeat;
	}

	public void setHealthHeartbeat(Integer healthHeartbeat) {
		this.healthHeartbeat = healthHeartbeat;
	}

}
