package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import javax.persistence.*;

/**
 * The persistent class for the viales database table.
 * 
 */
@Entity
@Table(name = "viales")
@NamedQuery(name = "VialEB.findAll", query = "SELECT v FROM VialEB v")
public class VialEB implements Serializable
{
	private static final long serialVersionUID = -4478233013315149395L;

	@Id
	@GeneratedValue(generator = "vial_seq", strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "vial_seq", sequenceName = "viales_id_vial_seq", initialValue = 1, allocationSize = 1)
	@Column(name = "id_vial", unique = true, nullable = false, insertable = false, updatable = false)
	private Integer idVial;

	@Column(length = 20)
	private String descripcion;

	@Column(name = "id_mapa", nullable = false)
	private Integer idMapa;

	public VialEB()
	{
	}

	public Integer getIdVial()
	{
		return this.idVial;
	}

	public void setIdVial(Integer idVial)
	{
		this.idVial = idVial;
	}

	public String getDescripcion()
	{
		return this.descripcion;
	}

	public void setDescripcion(String descripcion)
	{
		this.descripcion = descripcion;
	}

	public Integer getIdMapa()
	{
		return this.idMapa;
	}

	public void setIdMapa(Integer idMapa)
	{
		this.idMapa = idMapa;
	}

}