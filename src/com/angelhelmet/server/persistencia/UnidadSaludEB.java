/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

/**
 *
 * @author frank
 */
@Entity
@Table(name = "unidad_salud")
public class UnidadSaludEB implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Type(type = "integer")
	@Column(name = "id_unidad_salud", unique = true)
	private Integer idUnidadSalud;
	@Column(name = "control_medico")
	private Boolean controlMedico;
	@OneToMany(mappedBy = "idUnidadSalud", cascade = CascadeType.ALL)
	private List<HorarioLecturaEB> horarioLecturaList;
	@OneToMany(mappedBy = "idUnidadSalud", cascade = CascadeType.ALL)
	private List<DiasLecturaEB> diasLecturaList;
	@OneToMany(mappedBy = "idUnidadSalud", cascade = CascadeType.ALL)
	private List<ActivaMedicionEB> activaMedicionList;
	@OneToMany(mappedBy = "idUnidadSalud", cascade = CascadeType.ALL)
	private List<UmbralesMedicionSaludEB> umbralesMedicionSaludList;
	@JoinColumn(name = "id_tipo_horario", referencedColumnName = "id_tipo_horario")
	@ManyToOne
	private TipoHorarioEB idTipoHorario;
	@JoinColumn(name = "id_unidad", referencedColumnName = "id_unidad")
	@ManyToOne
	private UnidadEB idUnidad;
	@OneToMany(mappedBy = "idUnidadSalud", cascade = CascadeType.ALL)
	private List<AlarmaSaludEB> alarmaSaludList;

	public UnidadSaludEB() {
	}

	public Integer getIdUnidadSalud() {
		return idUnidadSalud;
	}

	public void setIdUnidadSalud(Integer idUnidadSalud) {
		this.idUnidadSalud = idUnidadSalud;
	}

	public Boolean getControlMedico() {
		return controlMedico;
	}

	public void setControlMedico(Boolean controlMedico) {
		this.controlMedico = controlMedico;
	}

	public List<HorarioLecturaEB> getHorarioLecturaList() {
		return horarioLecturaList;
	}

	public void setHorarioLecturaList(List<HorarioLecturaEB> horarioLecturaList) {
		this.horarioLecturaList = horarioLecturaList;
	}

	public List<DiasLecturaEB> getDiasLecturaList() {
		return diasLecturaList;
	}

	public void setDiasLecturaList(List<DiasLecturaEB> diasLecturaList) {
		this.diasLecturaList = diasLecturaList;
	}

	public List<ActivaMedicionEB> getActivaMedicionList() {
		return activaMedicionList;
	}

	public void setActivaMedicionList(List<ActivaMedicionEB> activaMedicionList) {
		this.activaMedicionList = activaMedicionList;
	}

	public List<UmbralesMedicionSaludEB> getUmbralesMedicionSaludList() {
		return umbralesMedicionSaludList;
	}

	public void setUmbralesMedicionSaludList(List<UmbralesMedicionSaludEB> umbralesMedicionSaludList) {
		this.umbralesMedicionSaludList = umbralesMedicionSaludList;
	}

	public TipoHorarioEB getIdTipoHorario() {
		return idTipoHorario;
	}

	public void setIdTipoHorario(TipoHorarioEB idTipoHorario) {
		this.idTipoHorario = idTipoHorario;
	}

	public UnidadEB getIdUnidad() {
		return idUnidad;
	}

	public void setIdUnidad(UnidadEB idUnidad) {
		this.idUnidad = idUnidad;
	}

	public List<AlarmaSaludEB> getAlarmaSaludList() {
		return alarmaSaludList;
	}

	public void setAlarmaSaludList(List<AlarmaSaludEB> alarmaSaludList) {
		this.alarmaSaludList = alarmaSaludList;
	}

	
}
