/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.angelhelmet.server.persistencia;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;





/**
 *
 * @author frank
 */
@Entity
@Table(name = "activa_medicion")
public class ActivaMedicionEB implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Type(type = "integer")
    @Column(name = "id_activa_medicion", unique = true)
    private Integer idActivaMedicion;
    @Column(name = "activo")
    private Boolean activo;
    @JoinColumn(name = "id_unidad_salud", referencedColumnName = "id_unidad_salud")
    @ManyToOne
    private UnidadSaludEB idUnidadSalud;
    
    @JoinColumn(name = "id_tipo_medicion", referencedColumnName = "id_tipo_medicion")
    @ManyToOne(fetch = FetchType.EAGER)
    private TipoMedicionEB idTipoMedicion;

    public ActivaMedicionEB() {
    }

    public ActivaMedicionEB(Integer idActivaMedicion) {
        this.idActivaMedicion = idActivaMedicion;
    }

    public Integer getIdActivaMedicion() {
        return idActivaMedicion;
    }

    public void setIdActivaMedicion(Integer idActivaMedicion) {
        this.idActivaMedicion = idActivaMedicion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public UnidadSaludEB getIdUnidadSalud() {
        return idUnidadSalud;
    }

    public void setIdUnidadSalud(UnidadSaludEB idUnidadSalud) {
        this.idUnidadSalud = idUnidadSalud;
    }
    
    public TipoMedicionEB getIdTipoMedicion() {
        return idTipoMedicion;
    }

    public void setIdTipoMedicion(TipoMedicionEB idTipoMedicion) {
        this.idTipoMedicion = idTipoMedicion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idActivaMedicion != null ? idActivaMedicion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ActivaMedicionEB)) {
            return false;
        }
        ActivaMedicionEB other = (ActivaMedicionEB) object;
        if ((this.idActivaMedicion == null && other.idActivaMedicion != null) || (this.idActivaMedicion != null && !this.idActivaMedicion.equals(other.idActivaMedicion))) {
            return false;
        }
        return true;
    }

	

   	
    
}
