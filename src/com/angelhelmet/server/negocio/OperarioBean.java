package com.angelhelmet.server.negocio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IOperarioDAO;
import com.angelhelmet.server.negocio.interfaces.IOperarioBean;
import com.angelhelmet.server.persistencia.OperarioEB;

@Stateless
//@Deprecated
public class OperarioBean implements IOperarioBean
{
	@EJB
	IOperarioDAO _operario_dao;

	public OperarioEB getOperarioPorUnidad(Integer id_unidad)
	{
		OperarioEB ret = null;
		try
		{
			ret = _operario_dao.getOperarioPorUnidad(id_unidad);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}
	
	@Override
	public List<OperarioEB> getOperarios() throws Exception
	{
		List<OperarioEB> ret = new ArrayList<OperarioEB>();
		ret = _operario_dao.getOperarios();
		return ret;
	}

	@Override
	public List<OperarioEB> getOperarios(String supervisor) throws Exception
	{
		List<OperarioEB> ret = new ArrayList<OperarioEB>();
		ret = _operario_dao.getOperarios(supervisor);
		return ret;
	}

}
