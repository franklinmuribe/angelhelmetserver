package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.AlarmaDAO;
import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.interfaces.IAlarmaDAO;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.util.TipoAlarma;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;

@Stateless
public class AlarmaBean implements IAlarmaBean {
	@EJB
	private IAlarmaDAO _alarma_dao;

	public AlarmaBean() {
		_alarma_dao = new AlarmaDAO();
	}

	@Override
	public AlarmaEB getAlarma(TipoAlarma alarma) throws AlarmaNoEncontradaException, Exception {
		AlarmaEB ret = null;
		ret = _alarma_dao.getAlarma(alarma);
		return ret;
	}

	@Override
	public AlarmSource getAlarma(Long firmaId) throws AlarmaNoEncontradaException, Exception {
		AlarmSource ret = null;
		ret = _alarma_dao.getAlarma(firmaId);
		return ret;
	}

	@Override
	public AlarmEvent getAlarmaEvento(Long firmaId) throws AlarmaNoEncontradaException, Exception {
		AlarmEvent ret = null;
		ret = _alarma_dao.getAlarmaEvento(firmaId);
		return ret;

	}
	
	@Override
	public WorkerReadMessage getReadMessage(Long firma) throws AlarmaNoEncontradaException, Exception {
		
		WorkerReadMessage ret = null;
		ret =  _alarma_dao.getReadMessage(firma);
		return ret;
	}
	
	

}
