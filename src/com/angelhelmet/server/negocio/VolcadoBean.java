package com.angelhelmet.server.negocio;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Calendar;
import java.util.GregorianCalendar;
//import java.util.Timer;
//import java.util.TimerTask;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import com.angelhelmet.server.negocio.interfaces.IVolcado;
import com.angelhelmet.server.util.ServidorUtil;

@Singleton
@Startup
@Deprecated
public class VolcadoBean implements IVolcado
{
	private FileWriter fichero;
	private PrintWriter pw;

	// private Timer temporizador;

	@PostConstruct
	private void startup()
	{
		System.out.println("Volcado: Singleton inited!");
		try
		{
			// String nombre_fichero = "C:\\tmp\\volcado\\log_" + getHoy(true) +
			// ".log";
			String nombre_fichero = File.separator + "ah\\logs\\jboss_volcado" + File.separator + "log_" + getHoy(true) + ".log";
			
			System.out.println(nombre_fichero);
			
			File f = new File(nombre_fichero);
			
			fichero = new FileWriter(f.getAbsolutePath());
			pw = new PrintWriter(fichero);
			
			// temporizador = new Timer();

			// TimerTask tarea = new TimerTask() {
			// @Override
			// public void run()
			// {
			// pw.flush();
			// System.out.println("*************************** ACTUALIZO VOLCADO ***************************");
			// }
			// };

			// temporizador.schedule(tarea, 60000, 60000);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void escribePeticion(String peticion)
	{
		pw.println(getHoy(false) + "::" + peticion);

	}

	public void escribeLog(String log)
	{
		pw.println(log);
	}

	public void cierraFichero()
	{
		try
		{
			if (fichero != null) fichero.close();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public void escribeCabeceraConsola(String data)
	{
		System.out.println("================================================================================");
		System.out.println("DATA = " + data);
	}

	public void escribeConsola(String texto)
	{
		System.out.println(texto);
	}

	public void escribeSinDatosConsola()
	{
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
		System.out.println("        DATOS PERDIDOS        ");
		System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	}

	public void escribePieConsola()
	{
		System.out.println("----------------------------------------------------------------------------------------");
	}

	private String getHoy(boolean esFichero)
	{
		Calendar hoy = new GregorianCalendar();
		int ano = hoy.get(Calendar.YEAR);
		int mes = hoy.get(Calendar.MONTH) + 1;
		int dia = hoy.get(Calendar.DAY_OF_MONTH);

		int hora = hoy.get(Calendar.HOUR_OF_DAY);
		int minutos = hoy.get(Calendar.MINUTE);
		int segundos = hoy.get(Calendar.SECOND);
		int milisegundos = hoy.get(Calendar.MILLISECOND);

		char cf;
		char ch;
		String ret = "";

		if (esFichero)
		{
			cf = ch = '_';
		}
		else
		{
			cf = '/';
			ch = ':';
		}

		ret = ServidorUtil.completaString(Integer.toString(ano), '0', 4) + cf;
		ret += ServidorUtil.completaString(Integer.toString(mes), '0', 2) + cf;
		ret += ServidorUtil.completaString(Integer.toString(dia), '0', 2) + " ";
		ret += ServidorUtil.completaString(Integer.toString(hora), '0', 2) + ch;
		ret += ServidorUtil.completaString(Integer.toString(minutos), '0', 2) + ch;
		ret += ServidorUtil.completaString(Integer.toString(segundos), '0', 2) + "_" + milisegundos;

		return ret;
	}
}
