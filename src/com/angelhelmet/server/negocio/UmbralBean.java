package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IUmbralDAO;
import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.negocio.interfaces.IUmbralBean;
import com.angelhelmet.server.rest.service.api.Threshold;

@Stateless
public class UmbralBean implements IUmbralBean {

	@EJB
	IUmbralDAO _umbral_dao;

	@Override
	public long addUmbral(datosUmbral datos) throws Exception {
		String umbralId = _umbral_dao.addUmbral(datos);
		long id = Long.parseLong(umbralId);
		return id;
	}

	@Override
	public Long addUnidadUmbral(Integer id_unidad, Integer sessionId, Integer id_umbral) throws Exception {
		return _umbral_dao.addUnidadUmbral(id_unidad, sessionId, id_umbral);
	}

	@Override
	public Threshold getThresholdConfiguration(Integer configId) throws Exception {
		return _umbral_dao.getThresholdConfiguration(configId);
	}

	@Override
	public datosUmbral getUmbralPorUnidad(Integer id_unidad) throws Exception {
		
		return _umbral_dao.getUmbralPorUnidad(id_unidad);
	}
}
