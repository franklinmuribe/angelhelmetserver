package com.angelhelmet.server.negocio;

import java.sql.Timestamp;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IUnidadOperarioDAO;
import com.angelhelmet.server.negocio.interfaces.IUnidadOperarioBean;
import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadOperarioEB;

@Stateless
public class UnidadOperarioBean implements IUnidadOperarioBean
{

	@EJB
	IUnidadOperarioDAO _uo_dao;

	@Override
	public UnidadOperarioEB addUnidadOperario(UnidadEB unidad, OperarioEB operario, Timestamp fecha_alta) throws Exception
	{
		UnidadOperarioEB ret = null;
		ret = _uo_dao.addUnidadOperario(unidad, operario, fecha_alta);
		return ret;
	}

}
