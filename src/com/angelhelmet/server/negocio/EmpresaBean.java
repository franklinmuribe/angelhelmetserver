package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.GetEmpresaByIdException;
import com.angelhelmet.server.dao.excepciones.GetEmpresaByUnidadException;
import com.angelhelmet.server.dao.interfaces.IEmpresaDAO;
import com.angelhelmet.server.negocio.interfaces.IEmpresaBean;
import com.angelhelmet.server.persistencia.EmpresaEB;

@Stateless
public class EmpresaBean implements IEmpresaBean
{
	@EJB
	private IEmpresaDAO _empresa_dao;

	@Override
	@Deprecated
	public EmpresaEB getEmpresa(int id_empresa) throws GetEmpresaByIdException, Exception
	{
		EmpresaEB ret = null;
		ret = _empresa_dao.getEmpresa(id_empresa);
		return ret;
	}

	@Override
	public EmpresaEB getEmpresaPorUnidad(int id_unidad) throws GetEmpresaByUnidadException, Exception
	{
		EmpresaEB ret = null;
		ret = _empresa_dao.getEmpresaPorUnidad(id_unidad);
		return ret;
	}

}
