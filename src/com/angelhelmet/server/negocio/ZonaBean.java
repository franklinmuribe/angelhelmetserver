package com.angelhelmet.server.negocio;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.GetZonaException;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.dao.interfaces.INodoPuntoDAO;
import com.angelhelmet.server.dao.interfaces.IZonaDAO;
import com.angelhelmet.server.dao.interfaces.IZonaCoordenadaDAO;
import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosPermisoZona;
import com.angelhelmet.server.negocio.interfaces.IZonaBean;
import com.angelhelmet.server.persistencia.ZonaEB;
import com.angelhelmet.server.persistencia.ZonaCoordenadaEB;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.TipoAlarma;
import com.angelhelmet.server.util.excepciones.DentroDelPoligonoException;

@Stateless
public class ZonaBean implements IZonaBean
{
	@EJB
	private IZonaDAO _zona;
	@EJB
	private IZonaCoordenadaDAO _zona_coordenada;
	@EJB
	private INodoPuntoDAO _nodo_punto;

	@Override
	public List<ZonaEB> getZonasMapa(int id_mapa) throws GetZonasMapaException, Exception
	{
		List<ZonaEB> ret = null;
		ret = _zona.getZonasMapa(id_mapa);
		return ret;
	}

	@Override
	public ZonaEB getZona(int id_zona) throws GetZonaException, Exception
	{
		ZonaEB ret = null;
		ret = _zona.getZona(id_zona);
		return ret;
	}

	@Override
	public ZonaEB estaEnZona(int id_mapa, datosCoordenada punto) throws GetZonasMapaException, GetZonaCoordenadas,
			DentroDelPoligonoException, ZonaSinCoordenadasException, Exception
	{
		ZonaEB ret = null;
		List<ZonaEB> zonas_mapa = this.getZonasMapa(id_mapa);
		for (ZonaEB item_zona : zonas_mapa)
		{
			List<ZonaCoordenadaEB> zona_coordenadas = new ArrayList<ZonaCoordenadaEB>();
			zona_coordenadas = _zona_coordenada.getZonaCoordenadas(item_zona.getIdZona());
			List<datosCoordenada> lista_coordenadas = new ArrayList<datosCoordenada>();
			for (ZonaCoordenadaEB item_coordenadas : zona_coordenadas)
			{
				datosCoordenada dc = new datosCoordenada();
				dc.setX(item_coordenadas.getX());
				dc.setY(item_coordenadas.getY());
				lista_coordenadas.add(dc);
			}
			if (ServidorUtil.dentroDelPoligono(lista_coordenadas, punto))
			{
				ret = item_zona;
			}
			if (ret != null) break;
		}
		return ret;
	}

	@Override
	public Date getTiempoEntrada(Timestamp tiempo_entrada)
	{
		return ServidorUtil.getFecha(tiempo_entrada.getTime());
	}

	@Override
	public boolean zonaConPermanencia(ZonaEB zona)
	{
		return zona.getTiempoPermanencia() > 0;
	}

	@Override
	public boolean tienePermisoEntrada(List<datosPermisoZona> lista)
	{
		return tienePermiso(lista, TipoAlarma.ENTRADAZONA);
	}

	@Override
	public boolean tienePermisoSalida(List<datosPermisoZona> lista)
	{
		return tienePermiso(lista, TipoAlarma.SALIDAZONA);
	}

	@Override
	public boolean tienePermisoPermanencia(List<datosPermisoZona> lista)
	{
		return tienePermiso(lista, TipoAlarma.PERMANENCIAZONA);
	}

	@Override
	public boolean calculaPermanencia(Timestamp fecha_log, Date fecha_entrada, int permanencia)
	{
		Date fl = ServidorUtil.getFecha(fecha_log.getTime());
		long dif = ServidorUtil.sumaSegundos(fecha_entrada, permanencia * 60).getTime() - fl.getTime();
		long segundos = dif ;
		return segundos <= 0;
	}

	private boolean tienePermiso(List<datosPermisoZona> lista, TipoAlarma tipo)
	{
		boolean ret = false;
		for (datosPermisoZona item : lista)
		{
			if (item.getAlarma() == tipo)
			{
				ret = true;
				break;
			}
		}
		return ret;
	}

}
