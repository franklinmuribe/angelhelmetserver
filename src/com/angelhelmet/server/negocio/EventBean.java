package com.angelhelmet.server.negocio;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.dao.interfaces.IEventDAO;
import com.angelhelmet.server.dao.interfaces.ILogDAO;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IApBean;
import com.angelhelmet.server.negocio.interfaces.IEventBean;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IMapaBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.persistencia.ApEB;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.MapaEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.util.ModoFuncionamiento;
import com.angelhelmet.server.util.ServidorUtil;

@Stateless
public class EventBean implements IEventBean {

	@EJB
	private IEventDAO _event_dao;
	@EJB
	private IUnidadBean _unidad_bean;
	@EJB
	private IAlarmaBean _alarma_bean;
	@EJB
	private IApBean _ap_bean;
	@EJB
	private IMapaBean _mapa_bean;

	public EventBean() {
	}

	@Override
	public long addAlarmaEvento(datosComunicacion datos, datosLog datos_log,
			com.angelhelmet.server.datos.datosUnidadMapa _unidad_mapa, datosAlarma currAlarm) throws Exception {
		return _event_dao.addAlarmaEvento(datos, datos_log, _unidad_mapa, currAlarm);
	}

}
