package com.angelhelmet.server.negocio.excepciones;

public class IndiceFueraDelStringException extends Exception
{
	private static final long serialVersionUID = -3150829157431677092L;
	
	public IndiceFueraDelStringException() {
	}
	
	public IndiceFueraDelStringException(String s) {
		super(s);
	}
	
	public IndiceFueraDelStringException(String s, Exception e) {
		super(s, e);
	}

}
