package com.angelhelmet.server.negocio.excepciones;

public class PeticionSinDatosException extends Exception
{
	private static final long serialVersionUID = 3648479915615842488L;
	
	public PeticionSinDatosException(String s) {
		super(s);
	}
	
	public PeticionSinDatosException(String s, Exception e) {
		super(s, e);
	}

}
