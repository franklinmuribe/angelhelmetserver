package com.angelhelmet.server.negocio.posicionamiento;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.datos.datosAp;
import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosProyeccion;
import com.angelhelmet.server.datos.datosPunto;
import com.angelhelmet.server.negocio.interfaces.INodoPuntoBean;
import com.angelhelmet.server.negocio.interfaces.posicionamiento.IPosicionamientoInterior;
import com.angelhelmet.server.negocio.kalman.Kalman;
import com.angelhelmet.server.negocio.kalman.KalmanIn;
import com.angelhelmet.server.negocio.kalman.KalmanPunto;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PonderaPuntosException;
import com.angelhelmet.server.persistencia.NodoPuntoEB;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.TipoLinea;

@Stateless
public class PosicionamientoInterior implements IPosicionamientoInterior
{
	@EJB
	INodoPuntoBean _nodo_punto_bean;

	private List<datosAp> _aps_mapa;
	private datosPunto _datos_posicion;

	public PosicionamientoInterior()
	{
	}

	@Override
	public void setApsMapa(List<datosAp> aps_mapa)
	{
		this._aps_mapa = aps_mapa;
	}

	@Override
	public void setDatosPosicion(datosPunto datos_posicion)
	{
		this._datos_posicion = datos_posicion;
	}

	@Override
	public datosPunto getPosicion() throws PonderaPuntosException, CalculaKalmanException, GetNodosProyeccionException, Exception
	{
		datosCoordenada R = new datosCoordenada();
		datosCoordenada extremo1 = new datosCoordenada();
		datosCoordenada extremo2 = new datosCoordenada();
		datosCoordenada punto_proyectado = new datosCoordenada();

		int cont_aps_adyacentes = 0;
		int id_ap_b = 0;

		ordenaApsPorPotencia();

		List<datosCoordenada> puntos = new ArrayList<datosCoordenada>();
		datosAp ap_potencia_max = _aps_mapa.get(0);

		for (datosAp item : _aps_mapa)
		{
			item.setPonderadoAdyacente(null);
			if (!item.equals(ap_potencia_max))
			{
				if (item.getApsAdyacentes().contains(ap_potencia_max) && item.getValorInicial().getValorFinal() >= 0.5)
				{
					datosCoordenada punto_ponderado = new datosCoordenada();
					punto_ponderado = ponderaPuntos(item, ap_potencia_max);

					item.setPonderadoAdyacente(new datosCoordenada());
					item.getPonderadoAdyacente().setX(punto_ponderado.getX());
					item.getPonderadoAdyacente().setY(punto_ponderado.getY());
					puntos.add(punto_ponderado);
					cont_aps_adyacentes++;
				}
			}
		}

		List<datosAp> lista_ponderados = new ArrayList<datosAp>();

		switch (cont_aps_adyacentes)
		{
		case 0: // solucion es el ap de max. potencia
			R.setX(ap_potencia_max.getCoordenadaX());
			R.setY(ap_potencia_max.getCoordenadaY());

			break;
		case 1: // solucion es el ponderado
		case 3: // el ortocentro del triangulo pon1-pon2-pon3			
		case 4: // el corte de la diagonales del cuadrilatero
				// pon1-pon2-pon3-pon4
		default: // echale imaginacion	
		case 2: // solucion media entre los ponderados o pond1+pon2-apmax

			int i = 0;
			for (datosAp item : _aps_mapa)
			{
				if (!item.equals(ap_potencia_max))
				{
					if (item.getPonderadoAdyacente() != null)
					{
						lista_ponderados.add(item);
						i++;
					}
				}
				if (i == 2) break;
			}

			if (lista_ponderados.size() == 1)
			{
				R.setX(lista_ponderados.get(0).getPonderadoAdyacente().getX());
				R.setY(lista_ponderados.get(0).getPonderadoAdyacente().getY());
				id_ap_b = lista_ponderados.get(0).getId();
			}
			else
			{
				datosCoordenada punto = new datosCoordenada();
				datosCoordenada oblicuo1 = new datosCoordenada();
				datosCoordenada oblicuo2 = new datosCoordenada();
				datosCoordenada oblicuo3 = new datosCoordenada();

				punto.setX(lista_ponderados.get(0).getPonderadoAdyacente().getX() + lista_ponderados.get(1).getPonderadoAdyacente().getX() - ap_potencia_max.getCoordenadaX());
				punto.setY(lista_ponderados.get(0).getPonderadoAdyacente().getY() + lista_ponderados.get(1).getPonderadoAdyacente().getY() - ap_potencia_max.getCoordenadaY());

				extremo1.setX(lista_ponderados.get(0).getPonderadoAdyacente().getX());
				extremo1.setY(lista_ponderados.get(0).getPonderadoAdyacente().getY());

				extremo2.setX(lista_ponderados.get(1).getPonderadoAdyacente().getX());
				extremo2.setY(lista_ponderados.get(1).getPonderadoAdyacente().getY());

				oblicuo1.setX(ap_potencia_max.getCoordenadaX());
				oblicuo1.setY(ap_potencia_max.getCoordenadaY());

				oblicuo2.setX(lista_ponderados.get(0).getCoordenadaX());
				oblicuo2.setY(lista_ponderados.get(0).getCoordenadaY());

				oblicuo3.setX(lista_ponderados.get(1).getCoordenadaX());
				oblicuo3.setY(lista_ponderados.get(1).getCoordenadaY());

				datosCoordenada R1 = proyectaOblicuamente(punto, extremo1, extremo2, oblicuo1, oblicuo2);
				datosCoordenada R2 = proyectaOblicuamente(punto, extremo1, extremo2, oblicuo1, oblicuo3);

				double dPR1ToMedia = calculaDistancia(punto, R1);
				double dPR2ToMedia = calculaDistancia(punto, R2);

				if (dPR1ToMedia < dPR2ToMedia)
				{
					R.setX(R1.getX());
					R.setY(R1.getY());
				}
				else
				{
					R.setX(R2.getX());
					R.setY(R2.getY());
				}

				int comparacion1 = R.compare(R, oblicuo1) * R.compare(R, oblicuo2);
				if (comparacion1 <= 0)
				{
					id_ap_b = lista_ponderados.get(0).getId();
				}
				else
				{
					id_ap_b = lista_ponderados.get(1).getId();
				}
			}

			break;
//		case 3: // el ortocentro del triangulo pon1-pon2-pon3
//			break;
//		case 4: // el corte de la diagonales del cuadrilatero
//				// pon1-pon2-pon3-pon4
//			break;
//		default: // echale imaginacion
//			break;
		}

		aplicaKalman(R);

		datosCoordenada punto_kalman = new datosCoordenada();
		punto_kalman.setX(this._datos_posicion.getValorInicialX().getValorFinal());
		punto_kalman.setY(this._datos_posicion.getValorInicialY().getValorFinal());

		switch (cont_aps_adyacentes)
		{
		case 0:
			punto_proyectado.setX(punto_kalman.getX());
			punto_proyectado.setY(punto_kalman.getY());
			break;
		case 1:
			punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), id_ap_b);
			break;		
		case 2:
		case 3: // el ortocentro del triangulo pon1-pon2-pon3	//added by Sanjay as discussed with Marcelo to fix 0,0 issue.		
		case 4: // el corte de la diagonales del cuadrilatero
				// pon1-pon2-pon3-pon4
		default: // echale imaginacion				
			datosCoordenada ap_max_w = new datosCoordenada();
			List<datosCoordenada> poligono = new ArrayList<datosCoordenada>();

			ap_max_w.setX(ap_potencia_max.getCoordenadaX());
			ap_max_w.setY(ap_potencia_max.getCoordenadaY());

			extremo1.setX(lista_ponderados.get(0).getCoordenadaX());
			extremo1.setY(lista_ponderados.get(0).getCoordenadaY());

			extremo2.setX(lista_ponderados.get(1).getCoordenadaX());
			extremo2.setY(lista_ponderados.get(1).getCoordenadaY());

			datosCoordenada p_kalman_sobre_extremos = proyectaOrtogonalmente(punto_kalman, extremo1, extremo2);
			datosCoordenada ap_max_w_sobre_extremos = proyectaOrtogonalmente(ap_max_w, extremo1, extremo2);

			if (coincideConAp(extremo1, p_kalman_sobre_extremos) || coincideConAp(extremo2, p_kalman_sobre_extremos) || coincideConAp(ap_max_w, p_kalman_sobre_extremos))
			{
				punto_proyectado = p_kalman_sobre_extremos;
			}
			else
			{
				double px, py;

				px = p_kalman_sobre_extremos.getX();
				py = p_kalman_sobre_extremos.getY();

				switch (enLinea(extremo1, extremo2, ap_max_w))
				{
				case HORIZONTAL:
					double x1 = extremo1.getX();
					double x2 = ap_max_w.getX();

					if (x2 < x1)
					{
						x1 = x2;
						x2 = extremo1.getX();
					}

					if (x1 <= px && px <= x2)
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(0).getId());
					}
					else
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(1).getId());
					}

					break;

				case VERTICAL:
					double y1 = extremo1.getY();
					double y2 = ap_max_w.getY();

					if (y2 < y1)
					{
						y1 = y2;
						y2 = extremo1.getY();
					}

					if (y1 <= py && py <= y2)
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(0).getId());
					}
					else
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(1).getId());
					}

					break;

				case OBLICUA:
					datosCoordenada v1 = new datosCoordenada();
					v1.setX(ap_max_w.getX());
					v1.setY(extremo1.getY());

					poligono.add(extremo1);
					poligono.add(ap_max_w);
					poligono.add(v1);
					poligono.add(construyePoligono(extremo1, ap_max_w, ap_max_w_sobre_extremos));

					if (ServidorUtil.dentroDelPoligono(poligono, p_kalman_sobre_extremos))
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(0).getId());
					}
					else
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(1).getId());
					}

					break;

				default:
					poligono.add(extremo1);
					poligono.add(ap_max_w);
					poligono.add(ap_max_w_sobre_extremos);
					poligono.add(construyePoligono(extremo1, ap_max_w, ap_max_w_sobre_extremos));

					if (ServidorUtil.dentroDelPoligono(poligono, p_kalman_sobre_extremos))
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(0).getId());
					}
					else
					{
						punto_proyectado = proyectaSobreVial(punto_kalman, ap_potencia_max.getId(), lista_ponderados.get(1).getId());
					}
					break;
				}
			}

			break;
		}

		this._datos_posicion.setCoordenadas(punto_proyectado);
		this._datos_posicion.toInt();
		return this._datos_posicion;
	}

	private boolean coincideConAp(datosCoordenada ap, datosCoordenada punto)
	{
		boolean ret = false;
		datosCoordenada p = new datosCoordenada();
		p.setX(punto.getX());
		p.setY(punto.getY());
		p = p.toInt();
		ret = (ap.getX() == p.getX()) && (ap.getY() == p.getY());
		return ret;
	}

	private TipoLinea enLinea(datosCoordenada extremo1, datosCoordenada extremo2, datosCoordenada punto)
	{
		TipoLinea ret;

		if ((extremo2.getX() - extremo1.getX()) == 0)
		{
			ret = TipoLinea.VERTICAL;
		}
		else if ((extremo2.getY() - extremo1.getY()) == 0)
		{
			ret = TipoLinea.HORIZONTAL;
		}
		else
		{
			if ((punto.getX() - extremo1.getX()) / (extremo2.getX() - extremo1.getX()) == (punto.getY() - extremo1.getY()) / (extremo2.getY() - extremo1.getY()))
			{
				ret = TipoLinea.OBLICUA;
			}
			else
			{
				ret = TipoLinea.NOALINEADO;
			}
		}
		return ret;
	}

	private datosCoordenada construyePoligono(datosCoordenada extremo, datosCoordenada ap_max, datosCoordenada ap_max_pro)
	{
		datosCoordenada ret = new datosCoordenada();
		ret.setX(ap_max_pro.getX() - (ap_max.getX() - extremo.getX()));
		ret.setY(ap_max_pro.getY() - (ap_max.getY() - extremo.getY()));
		return ret;
	}

	private datosCoordenada proyectaSobreVial(datosCoordenada punto, int id_ap_max_w, int id_ap) throws GetNodosProyeccionException, Exception
	{
		datosCoordenada ret = new datosCoordenada();
		datosCoordenada extremo1 = new datosCoordenada();
		datosCoordenada extremo2 = new datosCoordenada();

		List<NodoPuntoEB> nodos = _nodo_punto_bean.getNodosProyeccion(id_ap_max_w, id_ap);
		if (nodos != null)
		{
			if (nodos.size() == 2)
			{
				extremo1.setX(nodos.get(0).getX());
				extremo1.setY(nodos.get(0).getY());

				extremo2.setX(nodos.get(1).getX());
				extremo2.setY(nodos.get(1).getY());

				ret = proyectaOrtogonalmente(punto, extremo1, extremo2);
			}
			else
			{
				Map<Double, datosProyeccion> proyeccion_nodos_order_by_distancia_asc = new TreeMap<Double, datosProyeccion>(new Comparator<Double>()
				{
					@Override
					public int compare(Double o1, Double o2)
					{
						return o1.compareTo(o2);
					}
				});

				datosCoordenada Rp = new datosCoordenada();
				extremo1.setX(nodos.get(0).getX());
				extremo1.setY(nodos.get(0).getY());

				extremo2.setX(nodos.get(nodos.size() - 1).getX());
				extremo2.setY(nodos.get(nodos.size() - 1).getY());

				Rp = proyectaOrtogonalmente(punto, extremo1, extremo2);

				for (int i = 0; i < nodos.size(); i++)
				{
					datosProyeccion pry = new datosProyeccion();
					datosCoordenada pn = new datosCoordenada();

					pn.setX(nodos.get(i).getX());
					pn.setY(nodos.get(i).getY());

					pry.setIndiceNodo(i);
					pry.setPunto(proyectaOrtogonalmente(pn, extremo1, extremo2));

					double distancia = calculaDistancia(Rp, pry.getPunto());
					proyeccion_nodos_order_by_distancia_asc.put(distancia, pry);
				}

				datosCoordenada oblicuo1 = new datosCoordenada();
				datosCoordenada oblicuo2 = new datosCoordenada();

				int i = 0;
				for (Entry<Double, datosProyeccion> item : proyeccion_nodos_order_by_distancia_asc.entrySet())
				{
					if (i == 0)
					{
						oblicuo1.setX(nodos.get(item.getValue().getIndiceNodo()).getX());
						oblicuo1.setY(nodos.get(item.getValue().getIndiceNodo()).getY());
					}
					else
					{
						oblicuo2.setX(nodos.get(item.getValue().getIndiceNodo()).getX());
						oblicuo2.setY(nodos.get(item.getValue().getIndiceNodo()).getY());
						break;
					}
					i++;
				}
				ret = proyectaOblicuamente(Rp, extremo1, extremo2, oblicuo1, oblicuo2);
			}
		}
		return ret;
	}

	private datosCoordenada proyectaOrtogonalmente(datosCoordenada punto, datosCoordenada extremo1, datosCoordenada extremo2)
	{
		datosCoordenada ret = new datosCoordenada();
		double yp = 0;
		double xp = 0;

		double a1 = extremo1.getX();
		double a2 = extremo2.getX();
		double b1 = extremo1.getY();
		double b2 = extremo2.getY();
		double v1 = a2 - a1;
		double v2 = b2 - b1;
		double c = v1 * punto.getX() + v2 * punto.getY();

		if (a1 == a2)
		{
			xp = a1;
			yp = punto.getY();
		}
		else if (b1 == b2)
		{
			yp = b1;
			xp = punto.getX();
		}
		else
		{
			yp = (-a1 * b2 + a2 * b1 + v2 * c / v1) / ((Math.pow(v1, 2) + Math.pow(v2, 2)) / v1);
			xp = (c - v2 * yp) / v1;
		}

		ret.setX(xp);
		ret.setY(yp);
		return ret;
	}

	private datosCoordenada proyectaOblicuamente(datosCoordenada punto, datosCoordenada extremo1, datosCoordenada extremo2, datosCoordenada oblicuo1, datosCoordenada oblicuo2)
	{
		datosCoordenada ret = new datosCoordenada();
		double a1, b1, c, c1, d1, v1, v2, f1, f2, f11, f12, f21, f22, xp, yp;

		f1 = punto.getX();
		f2 = punto.getY();

		f11 = extremo1.getX();
		f12 = extremo1.getY();

		f21 = extremo2.getX();
		f22 = extremo2.getY();

		c = (f21 - f11) * f1 + (f22 - f12) * f2;
		c1 = oblicuo1.getX() - oblicuo2.getX();
		d1 = oblicuo1.getY() - oblicuo2.getY();
		a1 = oblicuo1.getX();
		b1 = oblicuo1.getY();

		v1 = (f11 - f21);
		v2 = (f12 - f22);

		if (d1 == 0)
		{
			yp = b1;
			xp = ((f22 - f12) * yp - c) / (f11 - f21);
		}
		else
		{
			if (c1 == 0)
			{
				xp = a1;
				yp = ((f21 - f11) * xp - c) / (f12 - f22);
			}
			else
			{
				yp = (((c1 * v1 * b1) / d1 - a1 * v1 - c) / ((v1 * c1) / d1 + v2));
				xp = ((c1 * yp) / d1 - c1 * b1 / d1 + a1);
			}
		}

		ret.setX(xp);
		ret.setY(yp);

		return ret;
	}

	private double calculaDistancia(datosCoordenada p1, datosCoordenada p2)
	{
		return Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2) + Math.pow(p1.getY() - p2.getY(), 2));
	}

	private void ordenaApsPorPotencia()
	{
		Collections.sort(_aps_mapa, new Comparator<datosAp>()
		{
			@Override
			public int compare(datosAp ap1, datosAp ap2)
			{
				return Double.compare(ap2.getValorInicial().getValorFinal(), ap1.getValorInicial().getValorFinal());
			}
		});
	}

	private void aplicaKalman(datosCoordenada coordenadas) throws CalculaKalmanException, Exception
	{
		calculaX(coordenadas.getX());
		calculaY(coordenadas.getY());
	}

	private double calculaX(double x) throws CalculaKalmanException, Exception
	{
		KalmanIn entrada = new KalmanIn();

		entrada.setEstimacionInicial(this._datos_posicion.getValorInicialX().getEstimacionFinal());
		if (this._datos_posicion.esPrimerPuntoX())
		{
			entrada.setValorInicial(x);
			this._datos_posicion.setPrimerPuntoX(false);
		}
		else
		{
			entrada.setValorInicial(this._datos_posicion.getValorInicialX().getValorFinal());
		}

		entrada.setPotencia(x);
		this._datos_posicion.setValorInicialX(Kalman.Calcula(entrada, new KalmanPunto()));
		return this._datos_posicion.getValorInicialX().getValorFinal();
	}

	private double calculaY(double y) throws CalculaKalmanException, Exception
	{
		KalmanIn entrada = new KalmanIn();

		entrada.setEstimacionInicial(this._datos_posicion.getValorInicialY().getEstimacionFinal());
		if (this._datos_posicion.esPrimerPuntoY())
		{
			entrada.setValorInicial(y);
			this._datos_posicion.setPrimerPuntoY(false);
		}
		else
		{
			entrada.setValorInicial(this._datos_posicion.getValorInicialY().getValorFinal());
		}

		entrada.setPotencia(y);
		this._datos_posicion.setValorInicialY(Kalman.Calcula(entrada, new KalmanPunto()));
		return this._datos_posicion.getValorInicialY().getValorFinal();
	}

	private datosCoordenada ponderaPuntos(datosAp ap1, datosAp ap2) throws PonderaPuntosException
	{
		datosCoordenada ret = new datosCoordenada();

		double x;
		double y;

		try
		{
			x = ((ap1.getCoordenadaX() * ap1.getValorInicial().getValorFinal()) + (ap2.getCoordenadaX() * ap2.getValorInicial().getValorFinal())) / (ap1.getValorInicial().getValorFinal() + ap2.getValorInicial().getValorFinal());
			y = ((ap1.getCoordenadaY() * ap1.getValorInicial().getValorFinal()) + (ap2.getCoordenadaY() * ap2.getValorInicial().getValorFinal())) / (ap1.getValorInicial().getValorFinal() + ap2.getValorInicial().getValorFinal());
			ret.setX(x);
			ret.setY(y);
		}
		catch (Exception ex)
		{
			StringBuilder sb = new StringBuilder();
			sb.append("AP1 " + ap1.getValorInicial().getValorFinal() + " AP2 " + ap2.getValorInicial().getValorFinal());
			throw new PonderaPuntosException("No se puede ponderar\n" + sb.toString(), ex);
		}
		return ret;
	}
}
