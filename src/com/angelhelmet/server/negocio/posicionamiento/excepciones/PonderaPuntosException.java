package com.angelhelmet.server.negocio.posicionamiento.excepciones;

public class PonderaPuntosException extends Exception
{
	private static final long serialVersionUID = 6463312176651763462L;

	public PonderaPuntosException() {
	}
	
	public PonderaPuntosException(String s) {
		super(s);
	}
	
	public PonderaPuntosException(String s, Exception e) {
		super(s, e);
	}
}
