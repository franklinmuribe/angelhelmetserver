package com.angelhelmet.server.negocio.posicionamiento.excepciones;

public class PosicionExteriorException extends Exception
{
	private static final long serialVersionUID = -1947383719393807976L;
	
	public PosicionExteriorException() {
	}
	
	public PosicionExteriorException(String s) {
		super(s);
	}
	
	public PosicionExteriorException(String s, Exception e) {
		super(s, e);
	}

}
