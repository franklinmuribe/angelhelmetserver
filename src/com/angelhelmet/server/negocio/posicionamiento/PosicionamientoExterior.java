package com.angelhelmet.server.negocio.posicionamiento;

import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosPunto;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PosicionExteriorException;
import com.angelhelmet.server.persistencia.MapaEB;
import com.angelhelmet.server.util.ServidorUtil;

public class PosicionamientoExterior
{
	private double latitud_mapa_max;
	private double longitud_mapa_max;
	private double latitud_mapa_min;
	private double longitud_mapa_min;
	private double dif_x_mapa;
	private double dif_y_mapa;
	private int x_mapa;
	private int y_mapa;

	public PosicionamientoExterior()
	{
	}

	public datosCoordenada getPosicion(MapaEB mapa, double longitud, double latitud) throws PosicionExteriorException, Exception
	{
		datosPunto ret = new datosPunto();
		try
		{

			latitud_mapa_max = mapa.getLatitudMax() * (mapa.getCorreccionLatitudMax() == 0 ? 1 : mapa.getCorreccionLatitudMax());
			longitud_mapa_max = mapa.getLongitudMax() * (mapa.getCorreccionLongitudMax() == 0 ? 1 : mapa.getCorreccionLongitudMax());

			latitud_mapa_min = mapa.getLatitudMin() * (mapa.getCorreccionLatitudMin() == 0 ? 1 : mapa.getCorreccionLatitudMin());
			longitud_mapa_min = mapa.getLongitudMin() * (mapa.getCorreccionLongitudMin() == 0 ? 1 : mapa.getCorreccionLongitudMin());

			dif_x_mapa = Math.abs(ServidorUtil.redondear(Math.abs(longitud_mapa_max - longitud_mapa_min), 6));
			dif_y_mapa = Math.abs(ServidorUtil.redondear(Math.abs(latitud_mapa_max - latitud_mapa_min), 6));

			x_mapa = mapa.getAncho();
			y_mapa = mapa.getAlto();

			ret.getCoordenadas().setX(calcularX(longitud));
			ret.getCoordenadas().setY(calcularY(latitud));
		}
		catch (Exception ex)
		{
			throw new PosicionExteriorException("No se puede calcular la posicion exterior del mapa " + mapa.getIdMapa() + " longitud " + longitud + " latitud " + latitud, ex);
		}
		return ret.getCoordenadas().toInt();
	}

	private int calcularX(double longitud)
	{
		int ret = 0;

		double dif = Math.abs(longitud_mapa_max - (double) longitud);
		double x = (x_mapa * dif) / dif_x_mapa;
		ret = (int) Math.ceil(ServidorUtil.redondear(x, 6));

		return ret;
	}

	private int calcularY(double latitud)
	{
		int ret = 0;
		
		double dif = Math.abs(latitud_mapa_max - (double) latitud);
		double y = (y_mapa * dif) / dif_y_mapa;
		ret = (int) Math.ceil(ServidorUtil.redondear(y, 6));

		return ret;
	}
}
