package com.angelhelmet.server.negocio.interfaces;

import java.sql.Timestamp;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadOperarioEB;

@Local
public interface IUnidadOperarioBean
{
	public UnidadOperarioEB addUnidadOperario(UnidadEB unidad, OperarioEB operario, Timestamp fecha_alta) throws Exception;
}
