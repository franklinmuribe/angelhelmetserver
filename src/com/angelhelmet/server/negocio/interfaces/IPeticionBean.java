package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosGestion;

@Local
public interface IPeticionBean
{
	public datosGestion gestionaPeticion(datosComunicacion datos_comunicacion) throws Exception;
}
