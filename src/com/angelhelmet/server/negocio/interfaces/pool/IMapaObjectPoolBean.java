package com.angelhelmet.server.negocio.interfaces.pool;

import java.util.List;
import java.util.Map;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.negocio.pool.excepciones.CambioDeMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.DelUnidadCambiadaMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesCambioMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.AddUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.DelUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.EditUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ExisteUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetListaUnidadesMinaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetTotalOperariosMinaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesMinaPorEmpresasException;
import com.angelhelmet.server.negocio.pool.excepciones.LimpiaPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ReemplazaUnidadPoolException;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolDto;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolData;

@Local
public interface IMapaObjectPoolBean
{
	public boolean existeUnidad(int id_unidad) throws ExisteUnidadPoolException;
	public boolean addUnidad(datosUnidadMapa datos, int id_unidad) throws AddUnidadPoolException;
	public boolean delUnidad(int id_unidad) throws DelUnidadPoolException;
	public datosUnidadMapa getUnidad(int id_unidad) throws GetUnidadPoolException;
	public boolean reemplazaUnidad(int unidad_anterior, datosUnidadMapa unidad_nueva) throws ReemplazaUnidadPoolException;
	public boolean editUnidad(datosUnidadMapa datos, int id_unidad) throws EditUnidadPoolException;
	public List<datosUnidadMapaWS> getUnidadesPorMapa(int id_mapa) throws GetUnidadesMapaException;
	public List<datosUnidadMapaWS> getUnidadesMina() throws GetListaUnidadesMinaException;
	public List<datosUnidadMapaWS> getUnidadesMina(List<Integer> empresas) throws GetUnidadesMinaPorEmpresasException;
	public int getTotalOperariosMina() throws GetTotalOperariosMinaException;
	public List<Integer> getUnidadesCambioMapa(int id_mapa) throws GetUnidadesCambioMapaException;
	public boolean delUnidadCambiadaMapa(int id_unidad) throws DelUnidadCambiadaMapaException;
	public boolean cambioDeMapa(int id_unidad, int id_mapa) throws CambioDeMapaException;	
	public String imprimePool();
	public boolean limpiaPool() throws LimpiaPoolException;
	public	Map<Integer, Integer> getUnidadesTotalWithMapa(List<Integer> lstMapId) throws GetUnidadesMapaException;
	public List<HelmetPoolData> getUnidadesByMapa(int id_mapa) throws GetUnidadesMapaException;
	public List<HelmetPoolDto> getAlarmasByMapa(int id_mapa) throws GetUnidadesMapaException;
	public List<Integer> getUsuariosForLockedMap(List<Integer> lstMaps) throws GetListaUnidadesMinaException;
	List<Integer> getUsuariosByCompanyForLockedMap(List<Integer> empresas, List<Integer> lstMaps)
			throws GetListaUnidadesMinaException;
	Map<Integer, String> getCompaniesForLockedMap(List<Integer> lstMaps) throws GetListaUnidadesMinaException;
	Map<String, Integer> getAlarmWorkerTotal(List<Integer> lstMapIds) throws GetUnidadesMapaException;
	
}
