package com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;
import com.angelhelmet.server.negocio.pool.excepciones.AddUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.EditUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ExisteUnidadPoolException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PonderaPuntosException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PosicionExteriorException;
import com.angelhelmet.server.util.excepciones.DentroDelPoligonoException;

@Local
public interface IUnidadMapa
{
	public datosUnidadMapa getUnidadMapa();

	public void setUnidadMapa(datosLog datos_log);
	public void setDatos(datosComunicacion datos) ;
	
	public boolean creaUnidadMapa(IMapaObjectPoolBean pool) throws ApsMapaNoEncontradosException, ApNoEncontradoException, PosicionExteriorException,
			PonderaPuntosException, CalculaKalmanException, GetNodosProyeccionException, GetZonasMapaException, GetZonaCoordenadas, DentroDelPoligonoException,
			ZonaSinCoordenadasException, GetPermisosZonaException, UnidadMensajeNoEncontradoException, GetUnidadMensajeByIdException,
			GetMensajesPurgarException, PurgarMensajeException, UnidadMensajeSetLeidoException, Exception;

	public void gestionaPool(IMapaObjectPoolBean pool) throws ExisteUnidadPoolException, EditUnidadPoolException, AddUnidadPoolException, Exception;
}
