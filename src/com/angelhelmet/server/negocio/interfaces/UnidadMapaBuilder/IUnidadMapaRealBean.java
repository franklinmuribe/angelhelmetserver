package com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder;

import javax.ejb.Local;

import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;

@Local
public interface IUnidadMapaRealBean extends IUnidadMapa
{
	public IMapaObjectPoolBean getPool();
}
