package com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder;

import javax.ejb.Local;
//import javax.ejb.Stateless;

import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;

@Local
//@Stateless
public interface IUnidadMapaDiferidoBean extends IUnidadMapa
{
	public datosLog creaDatosLog(long id_log);
	public IMapaObjectPoolBean getPool();
}
