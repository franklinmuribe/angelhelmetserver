package com.angelhelmet.server.negocio.interfaces;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.UnidadEB;

@Local
public interface IEventBean {

	long addAlarmaEvento(datosComunicacion datos, datosLog datos_log, datosUnidadMapa _unidad_mapa,
			datosAlarma currAlarm) throws Exception;
}
