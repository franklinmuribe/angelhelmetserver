package com.angelhelmet.server.negocio.interfaces;

import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.UnidadEB;

@Local
public interface ILogBean {
	public datosLog grabaLog(datosComunicacion datos, UnidadEB unidad) throws AlarmaNoEncontradaException,
			ApNoEncontradoException, MapaNoEncontradoException, ParseLogException, GrabaLogException, Exception;

	@Deprecated
	public LogEB getUltimoLog(int id_unidad);

	public List<datosComunicacion> getLogsByFecha(Date inicio, Date fin, List<Integer> unidades, int id_mapa);

	public LogEB getLogById(long id_log);

	public LogEB getLogByFirma(Long firma);

	public datosComunicacion parseLogEB2DatosComunicacion(LogEB log_eb, String serie) throws Exception;
}
