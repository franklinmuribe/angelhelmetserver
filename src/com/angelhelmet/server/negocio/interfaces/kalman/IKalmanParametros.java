package com.angelhelmet.server.negocio.interfaces.kalman;

import javax.ejb.Local;

@Local
public interface IKalmanParametros
{
	public double getRuidoR();
	public double getRuidoQ();
	public double getEstimacionInicial();
}
