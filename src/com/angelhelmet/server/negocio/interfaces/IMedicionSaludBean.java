package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.UmbralesMedicionSaludEB;
import com.hcl.iot.smartworker.dto.MediconesDTO;

@Local
public interface IMedicionSaludBean {
	
	public MediconesDTO getMedicion(Integer unidadId) throws Exception;
	
	public UmbralesMedicionSaludEB getUmbralByUnidadSaludAndTipoMedicion( Integer tipoMedicionId, Integer unidadSaludId) throws  Exception;

}
