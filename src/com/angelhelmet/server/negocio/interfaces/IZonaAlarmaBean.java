package com.angelhelmet.server.negocio.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.datos.datosPermisoZona;

@Local
public interface IZonaAlarmaBean
{
	public List<datosPermisoZona> getPermisosZona(int id_zona, int id_unidad) throws GetPermisosZonaException, Exception;
}
