package com.angelhelmet.server.negocio.interfaces.ws;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebService;

import com.angelhelmet.server.datos.datosUnidadMapaWS;

@WebService(targetNamespace = "http://www.angelhelmet.com/wsdl")
public interface IMapUsers
{
	@WebMethod()
	public String sayHello(String name);
	
	@WebMethod()
	public List<datosUnidadMapaWS> getUsuarios(int id_mapa);
	
	@WebMethod()
	public List<datosUnidadMapaWS> getUsuariosMina();
	
	@WebMethod()
	public List<datosUnidadMapaWS> getUsuariosMinaPorEmpresa(List<Integer> empresas);
	
	@WebMethod()
	public List<Long> getLogs(Date inicio, Date fin, List<Integer> unidades, int id_mapa);
	
	@WebMethod()
	public List<datosUnidadMapaWS> getLog(long id_log, int id_mapa);
	
	@WebMethod()
	public boolean borraUnidadesDiferido(List<Integer> unidades);
	
	@WebMethod()
	public boolean apagaUnidad(int id_unidad);
	
	@WebMethod()
	public int getTotalOperariosMina();
	
	@WebMethod()
	boolean alarmaInformada(int id_unidad, long firma);
	
	@WebMethod()
	public List<Integer> getUnidadesCambioMapa(int id_mapa);
	
	@WebMethod()
	public boolean BorraUnidadCambioMapa(int id_unidad);

}
