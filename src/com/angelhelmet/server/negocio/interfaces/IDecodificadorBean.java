package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;
import javax.servlet.http.HttpServletRequest;

import com.angelhelmet.server.datos.datosComunicacion;

@Local
public interface IDecodificadorBean
{
	public datosComunicacion DecodificaGet(HttpServletRequest req) throws Exception;
	public String toString();
}
