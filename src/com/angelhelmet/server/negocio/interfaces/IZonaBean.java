package com.angelhelmet.server.negocio.interfaces;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.GetZonaException;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosPermisoZona;
import com.angelhelmet.server.persistencia.ZonaEB;

@Local
public interface IZonaBean
{
	public List<ZonaEB> getZonasMapa(int id_mapa) throws GetZonasMapaException, Exception;

	public ZonaEB getZona(int id_zona) throws GetZonaException, Exception;

	public ZonaEB estaEnZona(int id_mapa, datosCoordenada punto) throws GetZonasMapaException, GetZonaCoordenadas, ZonaSinCoordenadasException, Exception;

	public Date getTiempoEntrada(Timestamp tiempo_entrada);

	public boolean zonaConPermanencia(ZonaEB zona);

	public boolean tienePermisoEntrada(List<datosPermisoZona> lista);

	public boolean tienePermisoSalida(List<datosPermisoZona> lista);

	public boolean tienePermisoPermanencia(List<datosPermisoZona> lista);

	public boolean calculaPermanencia(Timestamp fecha_log, Date fecha_entrada, int permanencia);
}
