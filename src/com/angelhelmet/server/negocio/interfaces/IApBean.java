package com.angelhelmet.server.negocio.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsAdyacentesNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.persistencia.ApEB;

@Local
public interface IApBean
{
	public ApEB getAp(String mac) throws ApNoEncontradoException, Exception;
	public ApEB getAp(int id_ap) throws ApNoEncontradoException, Exception;
	public List<ApEB> getAdyacentes(int id_ap) throws ApsAdyacentesNoEncontradosException, Exception;
	public List<ApEB> getApsMapa(int id_mapa) throws ApsMapaNoEncontradosException, Exception;
}
