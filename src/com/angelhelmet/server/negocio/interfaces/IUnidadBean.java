package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.UnidadNoEncontradaException;
import com.angelhelmet.server.persistencia.UnidadEB;

@Local
public interface IUnidadBean
{
	public UnidadEB getUnidad(String serie) throws UnidadNoEncontradaException, Exception;
	public UnidadEB getUnidad(int id_unidad) throws UnidadNoEncontradaException, Exception;
}
