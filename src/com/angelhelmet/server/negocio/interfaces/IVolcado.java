package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

@Local
@Deprecated
public interface IVolcado
{
	public void escribePeticion(String peticion);
	public void escribeLog(String log);
	public void cierraFichero();
	public void escribeCabeceraConsola(String data);
	public void escribeConsola(String texto);
	public void escribeSinDatosConsola();
	public void escribePieConsola();
}
