package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosGlobales;

@Local
public interface IUsaGlobalesBean
{
	public datosGlobales getGlobales();
}
