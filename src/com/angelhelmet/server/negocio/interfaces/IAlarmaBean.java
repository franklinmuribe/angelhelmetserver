package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.util.TipoAlarma;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;
import com.hcl.iot.smartworker.exception.SmartWorkerException;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;

@Local
public interface IAlarmaBean {
	public AlarmaEB getAlarma(TipoAlarma alarma) throws AlarmaNoEncontradaException, Exception;

	public AlarmSource getAlarma(Long firmaId) throws AlarmaNoEncontradaException, Exception;
	
	public AlarmEvent getAlarmaEvento(Long firmaId) throws AlarmaNoEncontradaException, Exception;
	
	public WorkerReadMessage getReadMessage(Long firma) throws AlarmaNoEncontradaException, Exception;

}
