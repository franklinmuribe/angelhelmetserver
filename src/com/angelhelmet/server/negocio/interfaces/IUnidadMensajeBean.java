package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AddMensajeApagadoException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadMensajeEB;

@Local
public interface IUnidadMensajeBean
{
	public UnidadMensajeEB getMensajeUnidad(int id_unidad) throws UnidadMensajeNoEncontradoException, Exception;

	public boolean setLeido(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception;

	public boolean purgarMensajesSMS(int id_unidad) throws GetMensajesPurgarException, PurgarMensajeException, Exception;

	@Deprecated
	public boolean addMensajeApagadoUnidad(UnidadEB unidad, MensajeEB mensaje) throws AddMensajeApagadoException, Exception;
	
	public boolean setEntregdoSalud(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception;
	
	public boolean setConfirmacionSalud(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception;
}
