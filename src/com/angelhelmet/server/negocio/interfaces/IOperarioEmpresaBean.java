package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.EmpresaEB;

@Local
@Deprecated
public interface IOperarioEmpresaBean
{
	public EmpresaEB getEmpresaPorOperario(int id_operario);
}
