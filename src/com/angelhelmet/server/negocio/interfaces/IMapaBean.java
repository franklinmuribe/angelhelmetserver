package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.persistencia.MapaEB;

@Local
public interface IMapaBean
{
	public MapaEB getMapa(int id) throws MapaNoEncontradoException, Exception;
	public MapaEB getMapa(double latitud, double longitud) throws MapaNoEncontradoException, Exception;
}
