package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetEmpresaByIdException;
import com.angelhelmet.server.dao.excepciones.GetEmpresaByUnidadException;
import com.angelhelmet.server.persistencia.EmpresaEB;

@Local
public interface IEmpresaBean
{
	@Deprecated
	public EmpresaEB getEmpresa(int id_empresa) throws GetEmpresaByIdException, Exception;
	
	public EmpresaEB getEmpresaPorUnidad(int id_unidad) throws GetEmpresaByUnidadException, Exception;;
}
