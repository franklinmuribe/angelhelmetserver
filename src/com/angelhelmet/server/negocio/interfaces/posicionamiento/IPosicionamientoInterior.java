package com.angelhelmet.server.negocio.interfaces.posicionamiento;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.datos.datosAp;
import com.angelhelmet.server.datos.datosPunto;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PonderaPuntosException;

@Local
public interface IPosicionamientoInterior
{
	public void setApsMapa(List<datosAp> aps_mapa);
	public void setDatosPosicion(datosPunto datos_posicion);
	public datosPunto getPosicion() throws PonderaPuntosException, GetNodosProyeccionException, CalculaKalmanException, Exception;
}
