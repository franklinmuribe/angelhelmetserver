package com.angelhelmet.server.negocio.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.util.TipoMensaje;

@Local
@Deprecated
public interface IMensajeBean {
	public MensajeEB getMensaje(int id_mensaje);

	public long addMensaje(String texto, TipoMensaje tipo, Integer id_unidad, String serie, Integer sessionId,
			Long unidadUmbrale, Long idFirmaCasco) throws Exception;

	public void addMensajesMasivos(String texto, TipoMensaje tipo, List<Integer> lista, MensajeMasivo mensajes,
			Long idFirmaCasco) throws Exception;

	public MensajeEB getMensajeConfiguracion(String serie);

	long addMensaje(String texto, TipoMensaje tipo, Integer id_unidad, String serie, Long idFirmaCasco)
			throws Exception;

	public Long getIdFirmaCasco() throws Exception;

	public String getMensajeByCasco(Long firma) throws Exception;

	public Long getMensajeIdLogByCasco(Long firma) throws Exception;

}
