package com.angelhelmet.server.negocio.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.persistencia.NodoPuntoEB;

@Local
public interface INodoPuntoBean
{
	public List<NodoPuntoEB> getNodosProyeccion(int id_ap1, int id_ap2) throws GetNodosProyeccionException, Exception;
}
