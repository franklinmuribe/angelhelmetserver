package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.rest.service.api.Threshold;

@Local
public interface IUmbralBean {
	public long addUmbral(datosUmbral datos) throws Exception;

	public Threshold getThresholdConfiguration(Integer configId) throws Exception;

	public Long addUnidadUmbral(Integer id_unidad, Integer sessionId, Integer id_umbral) throws Exception;
	
	public datosUmbral getUmbralPorUnidad(Integer id_unidad ) throws Exception;

}
