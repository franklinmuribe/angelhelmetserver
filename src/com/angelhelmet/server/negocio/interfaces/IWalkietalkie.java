package com.angelhelmet.server.negocio.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosWalkietalkieAudio;
import com.angelhelmet.server.datos.datosWalkietalkieImei;

@Local
public interface IWalkietalkie {

	public datosWalkietalkieImei obtenerImeiWT(String imei) throws Exception;

	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje) throws Exception;
	
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje, Long firmaCasco) throws Exception;

	public Long actualizarEstadoWT(Long idMensajeHelmet, Long idEstadoMensaje, Long firma) throws Exception;
}
