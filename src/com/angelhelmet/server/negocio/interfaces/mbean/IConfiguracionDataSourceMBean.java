package com.angelhelmet.server.negocio.interfaces.mbean;

import java.util.Properties;

import javax.ejb.Local;
import javax.management.MalformedObjectNameException;

@Local
public interface IConfiguracionDataSourceMBean
{
	public Properties getConfiguracionDataSource() throws MalformedObjectNameException, Exception;
}
