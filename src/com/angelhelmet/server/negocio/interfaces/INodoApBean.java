package com.angelhelmet.server.negocio.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.NodoApEB;

@Local
@Deprecated
public interface INodoApBean
{
	public List<NodoApEB> getNodosAsociados(int id_ap1, int id_ap2);
}
