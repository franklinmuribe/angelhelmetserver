package com.angelhelmet.server.negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.INodoApDAO;
import com.angelhelmet.server.negocio.interfaces.INodoApBean;
import com.angelhelmet.server.persistencia.NodoApEB;

@Stateless
@Deprecated
public class NodoApBean implements INodoApBean
{
	@EJB
	private INodoApDAO _nodo_ap_dao;

	public List<NodoApEB> getNodosAsociados(int id_ap1, int id_ap2)
	{
		List<NodoApEB> ret = null;
		try
		{
			ret = _nodo_ap_dao.getNodosAsociados(id_ap1, id_ap2);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}
}
