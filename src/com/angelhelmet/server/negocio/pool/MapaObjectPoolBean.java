package com.angelhelmet.server.negocio.pool;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IEmpresaBean;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;
import com.angelhelmet.server.negocio.pool.excepciones.AddUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.CambioDeMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.DelUnidadCambiadaMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.DelUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.EditUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ExisteUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetListaUnidadesMinaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetTotalOperariosMinaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesCambioMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesMapaException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadesMinaPorEmpresasException;
import com.angelhelmet.server.negocio.pool.excepciones.LimpiaPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ReemplazaUnidadPoolException;
import com.angelhelmet.server.persistencia.EmpresaEB;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolDto;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolDto.HelmetData.LifeTime;
import com.angelhelmet.server.rest.service.dtos.DatosOperario;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolData;
import com.angelhelmet.server.rest.service.dtos.datosAlarmas;
import com.angelhelmet.server.util.TipoAlarma;
import com.google.gson.JsonObject;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;

@Stateless
public class MapaObjectPoolBean implements IMapaObjectPoolBean {
	@EJB
	private IEmpresaBean _empresa_bean;

	@EJB
	private IAlarmaBean _alarm_bean;

	private final static ConcurrentHashMap<Integer, datosUnidadMapa> _lista_unidades = new ConcurrentHashMap<Integer, datosUnidadMapa>();

	// <id_unidad, id_mapa>
	private final static ConcurrentHashMap<Integer, Integer> _lista_cambio_plano = new ConcurrentHashMap<Integer, Integer>();
	private Logger _log;

	public MapaObjectPoolBean() {
		_log = LogManager.getLogger(MapaObjectPoolBean.class);
	}

	@Override
	public boolean existeUnidad(int id_unidad) throws ExisteUnidadPoolException {
		boolean ret = false;
		synchronized (MapaObjectPoolBean.class) {
			try {
				
				
				
				ret = _lista_unidades.containsKey(id_unidad);
				if (!ret) {
					//_log.info("NO !!! existeUnidad->Hilo({}): {}", Thread.currentThread().getId(), id_unidad);
					_log.debug("NO !!! existeUnidad->Hilo({}): {}", Thread.currentThread().getId(), id_unidad);
					//
					// _log.info(imprimePool());
					// _log.debug(imprimePool());
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - existeUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new ExisteUnidadPoolException(
						"No se puede comprobar la existencia de la unidad " + id_unidad + " en el pool", e);
			}
		}
		return ret;
	}

	@Override
	public boolean addUnidad(datosUnidadMapa datos, int id_unidad) throws AddUnidadPoolException {
		boolean ret = false;
		synchronized (MapaObjectPoolBean.class) {
			try {
				datosUnidadMapa aux = _lista_unidades.putIfAbsent(id_unidad, datos);
				if (aux == null) {
					ret = true;
				} else {
					_log.debug("No se puede a�adir la unidad: {} Hilo: {}", id_unidad, Thread.currentThread().getId());
					//_log.debug("No se puede a�adir la unidad: {} Hilo: {}", id_unidad, Thread.currentThread().getId());
				}

				// if (!ret)
				// {
				// _log.info(imprimePool());
				// _log.debug(imprimePool());
				// }
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - addUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new AddUnidadPoolException("No se puede a�adir la unidad " + id_unidad + " en el pool", e);
			}
		}
		return ret;
	}

	@Override
	public boolean delUnidad(int id_unidad) throws DelUnidadPoolException {
		boolean ret = true;
		synchronized (MapaObjectPoolBean.class) {
			try {
				datosUnidadMapa unidad = _lista_unidades.remove(id_unidad);
				if (unidad == null) {
					ret = false;

					/*_log.info("No se puede eliminar la unidad: {} Hilo: {}", id_unidad,
							Thread.currentThread().getId());*/
					_log.debug("No se puede eliminar la unidad: {} Hilo: {}", id_unidad,
							Thread.currentThread().getId());

					// _log.info(imprimePool());
					// _log.debug(imprimePool());
				} else {
					if (_lista_cambio_plano.containsKey(id_unidad)) {
						_lista_cambio_plano.remove(id_unidad);
					}
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - delUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new DelUnidadPoolException("No se puede eliminar la unidad " + id_unidad + " del pool", e);
			}
		}
		return ret;
	}

	@Override
	public datosUnidadMapa getUnidad(int id_unidad) throws GetUnidadPoolException {
		// _log.info(this.getClass().getName() + " - getUnidad");
		datosUnidadMapa ret = null;
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					_log.debug("unidad existente" + item.getKey());
				}
				
				_log.debug("MapaObjectPoolBean - getUnidad - size : " + _lista_unidades.size());
				ret = _lista_unidades.get(id_unidad);

				if (ret == null) {
					/*_log.info("getUnidad:: No se puede recuperar la unidad: {} Hilo: {}", id_unidad,
							Thread.currentThread().getId());*/
					_log.debug("No se puede recuperar la unidad: {} Hilo: {}", id_unidad,
							Thread.currentThread().getId());

					// _log.info(imprimePool());
					// _log.debug(imprimePool());
				}
			} catch (Exception e) {
				_log.debug("MapaObjectPoolBean - getUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new GetUnidadPoolException("No se puede recuperar la unidad " + id_unidad + " del pool", e);
			}
		}
		return ret;
	}

	@Override
	public boolean reemplazaUnidad(int unidad_anterior, datosUnidadMapa unidad_nueva)
			throws ReemplazaUnidadPoolException {
		//System.out.println("MapaObjectPoolBean - reemplazaUnidad - inicio ");
		boolean ret = false;
		synchronized (MapaObjectPoolBean.class) {
			int nueva = unidad_nueva.getUnidad().getIdUnidad();
			try {
				if (delUnidad(unidad_anterior)) {
					if (addUnidad(unidad_nueva, nueva)) {
						ret = true;
					} else {
						_log.info("No se puede reemplazar la unidad (add): {}", unidad_nueva.toString());
					}
				} else {
					_log.info("No se puede reemplazar la unidad (del): {}", unidad_nueva.toString());
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - reemplazaUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new ReemplazaUnidadPoolException(
						"No se puede reemplazar la unidad " + unidad_anterior + " por la unidad " + nueva + " del pool",
						e);
			}
		}
		return ret;
	}

	@Override
	public boolean editUnidad(datosUnidadMapa datos, int id_unidad) throws EditUnidadPoolException {
		boolean ret = false;
		synchronized (MapaObjectPoolBean.class) {
			try {
				datosUnidadMapa aux = getUnidad(id_unidad);
				if (aux != null) {
					aux = datos;
					ret = true;
				} else {
				/*	_log.info("No se puede editar la unidad: {} Hilo: {} ({})", id_unidad,
							Thread.currentThread().getId(), Thread.currentThread().getName());*/
					_log.debug("No se puede editar la unidad: {} Hilo: {} ({})", id_unidad,
							Thread.currentThread().getId(), Thread.currentThread().getName());

					// _log.info(imprimePool());
					// _log.debug(imprimePool());
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - editUnidad - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new EditUnidadPoolException("No se puede editar la unidad " + id_unidad + " del pool", e);
			}
		}
		return ret;
	}

	@Override
	public boolean cambioDeMapa(int id_unidad, int id_mapa_anterior) throws CambioDeMapaException {
		boolean ret = false;
		synchronized (MapaObjectPoolBean.class) {
			try {
				datosUnidadMapa datos = getUnidad(id_unidad);
				if (datos != null) {
					if (datos.getIdMapa() != id_mapa_anterior) {
						if (!_lista_cambio_plano.containsKey(id_unidad)) {
							_lista_cambio_plano.putIfAbsent(id_unidad, id_mapa_anterior);
						} else {
							_lista_cambio_plano.remove(id_unidad);
							_lista_cambio_plano.putIfAbsent(id_unidad, id_mapa_anterior);
						}
					} else {
						if (_lista_cambio_plano.containsKey(id_unidad)) {
							_lista_cambio_plano.remove(id_unidad);
						}
					}
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - cambioDeMapa - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new CambioDeMapaException("No se puede realizar el cambio de mapa de la unidad " + id_unidad
						+ " al mapa " + id_mapa_anterior, e);
			}
		}
		return ret;
	}

	@Override
	public boolean delUnidadCambiadaMapa(int id_unidad) throws DelUnidadCambiadaMapaException {
		boolean ret = false;
		int id_mapa = 0;
		synchronized (MapaObjectPoolBean.class) {
			try {
				if (_lista_cambio_plano.containsKey(id_unidad)) {
					id_mapa = _lista_cambio_plano.remove(id_unidad);
					ret = true;
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - delUnidadCambiadaMapa - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new DelUnidadCambiadaMapaException(
						"No se puede eliminar la unidad cambiada de mapa id_unidad: " + id_unidad + " mapa " + id_mapa,
						e);
			}
		}
		return ret;
	}

	@Override
	public List<datosUnidadMapaWS> getUnidadesPorMapa(int id_mapa) throws GetUnidadesMapaException {
		List<datosUnidadMapaWS> ret = new ArrayList<datosUnidadMapaWS>();

		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					if (item.getValue().getIdMapa() == id_mapa) {
						ret.add(item.getValue());
					}
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - getUnidadesPorMapa - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new GetUnidadesMapaException("No se pruede recuperar la lista de unidades del mapa " + id_mapa,
						e);
			}
		}
		return ret;
	}

	@Override
	public List<datosUnidadMapaWS> getUnidadesMina() throws GetListaUnidadesMinaException {
		List<datosUnidadMapaWS> ret = new ArrayList<datosUnidadMapaWS>();
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					ret.add(item.getValue());
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - getUnidadesMina - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new GetListaUnidadesMinaException("No se pruede recuperar la lista de unidades de la mina", e);
			}
		}
		return ret;
	}

	@Override
	public List<datosUnidadMapaWS> getUnidadesMina(List<Integer> empresas) throws GetUnidadesMinaPorEmpresasException {
		List<datosUnidadMapaWS> ret = new ArrayList<datosUnidadMapaWS>();

		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					int id_unidad = item.getValue().getUnidad().getIdUnidad();
					EmpresaEB empresa = _empresa_bean.getEmpresaPorUnidad(id_unidad);
					if (empresa != null && empresas.contains(empresa.getIdEmpresa())) {
						ret.add(item.getValue());
					}
				}
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - getUnidadesMina - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new GetUnidadesMinaPorEmpresasException(
						"No se pruede recuperar la lista de unidades (por empresas) de la mina", e);
			}
		}
		return ret;
	}

	@Override
	public int getTotalOperariosMina() throws GetTotalOperariosMinaException {
		int ret = 0;
		synchronized (MapaObjectPoolBean.class) {
			try {
				ret = _lista_unidades.size();
			} catch (Exception e) {
				System.out.println("MapaObjectPoolBean - getTotalOperariosMina - getLocalizedMessage: " + e.getLocalizedMessage());
				throw new GetTotalOperariosMinaException("No se pruede recuperar el total de unidades de la mina", e);
			}
		}
		return ret;
	}

	@Override
	public List<Integer> getUnidadesCambioMapa(int id_mapa) throws GetUnidadesCambioMapaException {
		List<Integer> ret = new ArrayList<Integer>();

		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, Integer> item : _lista_cambio_plano.entrySet()) {
					if (item.getValue() == id_mapa) {
						ret.add(item.getKey());
					}
				}
			} catch (Exception e) {
				throw new GetUnidadesCambioMapaException(
						"No se pruede recuperar la lista de unidades que han cambiado de mapa " + id_mapa, e);
			}
		}
		return ret;
	}

	@Override
	public boolean limpiaPool() throws LimpiaPoolException {
		synchronized (MapaObjectPoolBean.class) {
			// _log.info(this.getClass().getName() + " - limpiaPool");
			
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					delUnidad(item.getValue().getUnidad().getIdUnidad());
				}

				for (Map.Entry<Integer, Integer> item : _lista_cambio_plano.entrySet()) {
					_lista_cambio_plano.remove(item.getKey());
				}
			} catch (Exception e) {
				throw new LimpiaPoolException("No se puede limpiar el pool", e);
			}
		}
		return true;
	}

	@Override
	public String imprimePool() {
		StringBuilder sb;
		synchronized (MapaObjectPoolBean.class) {
			// _log.info(this.getClass().getName() + " - imprimePool");
			sb = new StringBuilder();
			sb.append("\n******************************** POOL ********************************\n");
			for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
				sb.append(
						"---------------------------------------------------------------------------------------------\n");
				sb.append("Key: " + item.getKey() + "\n");
				sb.append(item.getValue().toString() + "\n");
			}
			sb.append("******************************** FIN POOL ****************************\n");
		}
		return sb.toString();
	}

	/* HCL SANJAY: added to implement new functionalities */
	@Override
	public Map<Integer, Integer> getUnidadesTotalWithMapa(List<Integer> lstMapId) throws GetUnidadesMapaException {
		Map<Integer, List<datosUnidadMapa>> lstMapUnidades = new HashMap();
		Map<Integer, Integer> ret = new HashMap();
		/* map Id worker List */
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {

					if (!lstMapUnidades.containsKey(item.getValue().getIdMapa())) {
						List<datosUnidadMapa> lst = new ArrayList<datosUnidadMapa>();
						lst.add(item.getValue());
						lstMapUnidades.put(item.getValue().getIdMapa(), lst);
					} else {
						List<datosUnidadMapa> lst = lstMapUnidades.get(item.getValue().getIdMapa());
						lst.add(item.getValue());
						lstMapUnidades.put(item.getValue().getIdMapa(), lst);
					}
				}

				for (Map.Entry<Integer, List<datosUnidadMapa>> map : lstMapUnidades.entrySet()) {
					ret.put(map.getKey(), map.getValue().size());
				}
			} catch (Exception e) {
				throw new GetUnidadesMapaException(
						"No se pruede recuperar la lista de unidades del mapa " + "getUnidadesTotalWithMapa", e);
			}
		}

		return ret;
	}

	@Override
	public List<HelmetPoolData> getUnidadesByMapa(int id_mapa) throws GetUnidadesMapaException {
		List<HelmetPoolData> ret = new ArrayList<HelmetPoolData>();

		synchronized (MapaObjectPoolBean.class) {
			try {
				HelmetPoolData helmetPoolData = null;
				DatosOperario operario = null;
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					_log.info("idUnidad: ",item.getKey());
					_log.info("idUnidad ** : ",item.getValue().toString());
					helmetPoolData = new HelmetPoolData();
					if (item.getValue().getIdMapa() == id_mapa) {
						helmetPoolData.setId_mapa(item.getValue().getIdMapa());
						helmetPoolData.setBateria(item.getValue().getUnidad().getBateria());
						helmetPoolData.setGas(item.getValue().getUnidad().getGas());
						helmetPoolData.setId_unidad(item.getValue().getUnidad().getIdUnidad());
						helmetPoolData.setSerie(item.getValue().getUnidad().getSerie());
						helmetPoolData.setTiempo_vida(item.getValue().getUnidad().getTiempoVida());
						/* worker data */
						operario = new DatosOperario();

						operario.setIdentificacion(item.getValue().getOperario().getIdentificacion());
						operario.setNombre(item.getValue().getOperario().getNombre());
						operario.setApellidos(item.getValue().getOperario().getApellidos());
						operario.setCargo(item.getValue().getOperario().getCargo());
						operario.setEmpresa(item.getValue().getOperario().getEmpresa());
						helmetPoolData.setOperario(operario);

						ret.add(helmetPoolData);
					}
				}
			} catch (Exception e) {
				throw new GetUnidadesMapaException("No se pruede recuperar la lista de unidades del mapa " + id_mapa,
						e);
			}
		}
		return ret;
	}

	@Override
	public List<HelmetPoolDto> getAlarmasByMapa(int id_mapa) throws GetUnidadesMapaException {
		List<HelmetPoolDto> ret = new ArrayList<HelmetPoolDto>();
		_log.info("getAlarmasByMapa ************* " + _lista_unidades.size());
		synchronized (MapaObjectPoolBean.class) {
			try {
				HelmetPoolDto helmetPool = null;
				_log.info("getAlarmasByMapa ************* " + _lista_unidades.size());
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					helmetPool = new HelmetPoolDto();
					if (item.getValue().getIdMapa() == id_mapa) {
						List<datosAlarmas> lst = new ArrayList();
						// alarmasPool.setSerial(String.valueOf(item.getValue().getUnidad().getSerie()));
						// TODO alarmasPool.setAlarmId(
						// Long.valueOf(String.valueOf(item.getValue().getUnidad().getSerie())));
						_log.info("getAlarmasByMapa ************* " + item.getValue().getAlarmas().size());
						for (Entry<Long, datosAlarma> al : item.getValue().getAlarmas().entrySet()) {
							datosAlarmas alarm = new datosAlarmas();
							alarm.setAlarm(al.getValue().getAlarma().name());
							alarm.setAlarmId(al.getValue().getFirma());
							alarm.setCancelTo(String.valueOf(al.getValue().getCancelaAlarma()));
							alarm.setCount(al.getValue().getContador());
							alarm.setQuestion(al.getValue().getQuestion());
							alarm.setAnswerNumber(al.getValue().getAnswerNumber());
							alarm.setAnswerDesc(al.getValue().getAnswerDesc());
							alarm.setAnswerText(al.getValue().getAnswerText());

							try {
								System.out.println("event firma: " + al.getValue().getFirma());
								AlarmEvent alarmEvent = _alarm_bean.getAlarmaEvento(al.getValue().getFirma());

								alarm.setEventId(alarmEvent.getEventId());
								alarm.setEventDate(alarmEvent.getEventDate().getTime());
								alarm.setEventResponseDate(alarmEvent.getEventResponseDate().getTime());

							} catch (AlarmaNoEncontradaException e1) {
								System.out.println("error event: " + e1.getLocalizedMessage());
							} catch (Exception e1) {
								System.out.println("error event: " + e1.getLocalizedMessage());
							}

							lst.add(alarm);
						}

						helmetPool.setAlarmsList(lst); // set alarm list

						HelmetPoolDto.IndoorPositions indoor = helmetPool.new IndoorPositions();
						indoor.X = item.getValue().getPosicion().getCoordenadas().getX();
						indoor.Y = item.getValue().getPosicion().getCoordenadas().getY();
						helmetPool.IndoorPosition = indoor;

						HelmetPoolDto.OutdoorPositions outdoor = helmetPool.new OutdoorPositions();
						outdoor.setLatitude(item.getValue().getPosicion().getCoordenadas().getX());
						outdoor.setLongitude(item.getValue().getPosicion().getCoordenadas().getY());
						helmetPool.OutdoorPosition = outdoor;
						helmetPool.setMode(item.getValue().getPosicion().get_mode());
						/* set map id */
						helmetPool.setMapId(item.getValue().getIdMapa());
						Map<String, Map<String, Integer>> map = new HashMap<>();
						Map<String, Integer> mapId = new HashMap<>();
						mapId.put("mapId", item.getValue().getIdMapa());
						map.put("map", mapId);
						/* set helmet details */
						HelmetPoolDto.HelmetData helmet = helmetPool.new HelmetData();
						helmet.setBattery(item.getValue().getUnidad().getBateria());
						helmet.setGas(item.getValue().getUnidad().getGas());
						helmet.setHelmetId(item.getValue().getUnidad().getIdUnidad());
						helmet.setxSensor(item.getValue().getUnidad().getSensorX());
						helmet.setySensor(item.getValue().getUnidad().getSensorY());
						helmet.setzSensor(item.getValue().getUnidad().getSensorZ());
						helmet.setSerial(item.getValue().getUnidad().getSerie());
						// TODO telephone is not present in pool
						// helmet.setTelephone(item.getValue().getUnidad());
						HelmetPoolDto.HelmetData.LifeTime life = helmet.new LifeTime();
						life.setH(item.getValue().getUnidad().getTiempoVida().getHoras());
						life.setM(item.getValue().getUnidad().getTiempoVida().getMinutos());
						life.setS(item.getValue().getUnidad().getTiempoVida().getSegundos());
						helmet.setLifeTime(life);
						helmet.setTimeLastReport(item.getValue().getUnidad().getTimeLastReport());
						helmetPool.Helmet = helmet;
						helmetPool.MenssageStatus = item.getValue().getDatosMensajeWS().getEstadoMensaje().name();
						/* set worker details */
						HelmetPoolDto.WorkerData worker = helmetPool.new WorkerData();
						worker.setBussiness(item.getValue().getOperario().getEmpresa());
						worker.setIdentification(item.getValue().getOperario().getIdentificacion());
						worker.setWorkerId(item.getValue().getOperario().getIdOperario());
						worker.setName(item.getValue().getOperario().getNombre());
						worker.setEmergency(item.getValue().getOperario().getEmergency());
						worker.setBlood(item.getValue().getOperario().getBlood());
						worker.setSupervisor(item.getValue().getOperario().getSupervisor());
						worker.setWorkerposition(item.getValue().getOperario().getCargo());
						worker.setChargeId(item.getValue().getOperario().getCargoId());
						helmetPool.setWorker(worker);
						helmetPool.setHelmetId(item.getValue().getUnidad().getIdUnidad());
						
						//Walkie Talkie
						// Definir campos

						ret.add(helmetPool);
					}
				}
			} catch (Exception e) {
				throw new GetUnidadesMapaException("No se pruede recuperar la lista de unidades del mapa " + id_mapa,
						e);
			}
		}
		return ret;
	}

	@Override
	public Map<String, Integer> getAlarmWorkerTotal(List<Integer> lstMapIds) throws GetUnidadesMapaException {
		List<HelmetPoolDto> ret = new ArrayList<HelmetPoolDto>();
		Map<String, Integer> map = new HashMap<>();
		Integer alertTotal = 0;
		synchronized (MapaObjectPoolBean.class) {
			try {
				HelmetPoolDto helmetPool = null;

				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					helmetPool = new HelmetPoolDto();
					if (lstMapIds.contains(item.getValue().getIdMapa())) {
						List<datosAlarmas> lst = new ArrayList();
						// alarmasPool.setSerial(String.valueOf(item.getValue().getUnidad().getSerie()));
						// TODO alarmasPool.setAlarmId(
						// Long.valueOf(String.valueOf(item.getValue().getUnidad().getSerie())));
						for (Entry<Long, datosAlarma> al : item.getValue().getAlarmas().entrySet()) {
							datosAlarmas alarm = new datosAlarmas();
							alarm.setAlarm(al.getValue().getAlarma().name());
							alarm.setAlarmId(al.getValue().getFirma());
							alarm.setCancelTo(String.valueOf(al.getValue().getCancelaAlarma()));
							alarm.setCount(al.getValue().getContador());
							alarm.setQuestion("Cantidad de Agua?");
							alarm.setAnswerNumber(1);
							alarm.setAnswerDesc("100%");
							alarm.setAnswerText("Capacidad Completa");
							lst.add(alarm);
						}
						alertTotal += lst.size();
						helmetPool.setAlarmsList(lst); // set alarm list

						// HelmetPoolDto.IndoorPositions indoor = helmetPool.new
						// IndoorPositions();
						// indoor.X=
						// item.getValue().getPosicion().getCoordenadas().getX();
						// indoor.Y =
						// item.getValue().getPosicion().getCoordenadas().getY();
						// helmetPool.IndoorPosition=indoor;
						//
						// HelmetPoolDto.OutdoorPositions outdoor =
						// helmetPool.new OutdoorPositions();
						// outdoor.setLatitude(item.getValue().getPosicion().getCoordenadas().getX());
						// outdoor.setLongitude(item.getValue().getPosicion().getCoordenadas().getY());
						// helmetPool.OutdoorPosition = outdoor;
						// helmetPool.setMode(item.getValue().getPosicion().get_mode());
						/* set map id */
						// helmetPool.setMapId(item.getValue().getIdMapa());
						// Map<String, Map<String, Integer>> map = new
						// HashMap<>();
						// Map<String, Integer> mapId = new HashMap<>();
						// mapId.put("mapId", item.getValue().getIdMapa());
						// map.put("map", mapId);

						/* set helmet details */
						// HelmetPoolDto.HelmetData helmet = helmetPool.new
						// HelmetData();
						// helmet.setBattery(item.getValue().getUnidad().getBateria());
						// helmet.setGas(item.getValue().getUnidad().getGas());
						// helmet.setHelmetId(item.getValue().getUnidad().getIdUnidad());
						// helmet.setxSensor(item.getValue().getUnidad().getSensorX());
						// helmet.setySensor(item.getValue().getUnidad().getSensorY());
						// helmet.setzSensor(item.getValue().getUnidad().getSensorZ());
						// helmet.setSerial(item.getValue().getUnidad().getSerie());
						// HelmetPoolDto.HelmetData.LifeTime life = helmet.new
						// LifeTime();
						// life.setH(item.getValue().getUnidad().getTiempoVida().getHoras());
						// life.setM(item.getValue().getUnidad().getTiempoVida().getMinutos());
						// life.setS(item.getValue().getUnidad().getTiempoVida().getSegundos());
						// helmet.setLifeTime(life);
						// helmetPool.Helmet = helmet;
						// helmetPool.MenssageStatus =
						// item.getValue().getDatosMensajeWS().getEstadoMensaje().name();
						/* set worker details */
						HelmetPoolDto.WorkerData worker = helmetPool.new WorkerData();
						// worker.setBussiness(item.getValue().getOperario().getEmpresa());
						// worker.setIdentification(item.getValue().getOperario().getIdentificacion());
						worker.setWorkerId(item.getValue().getOperario().getIdOperario());
						// worker.setName(item.getValue().getOperario().getNombre());
						// worker.setEmergency(item.getValue().getOperario().getEmergency());
						// worker.setBlood(item.getValue().getOperario().getBlood());
						// worker.setSupervisor(item.getValue().getOperario().getSupervisor());
						// worker.setWorkerposition(item.getValue().getOperario().getCargo());
						// worker.setChargeId(item.getValue().getOperario().getCargoId());
						helmetPool.setWorker(worker);
						helmetPool.setHelmetId(item.getValue().getUnidad().getIdUnidad());

						ret.add(helmetPool);
					}
				}

				map.put("workerTotal", ret.size());
				map.put("alertTotal", alertTotal);
			} catch (Exception e) {
				throw new GetUnidadesMapaException("error while getting alarm and worker total ", e);
			}
		}
		return map;
	}

	@Override
	public List<Integer> getUsuariosForLockedMap(List<Integer> lstMaps) throws GetListaUnidadesMinaException {
		List<Integer> ret = new ArrayList<Integer>();
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					if (lstMaps.contains(item.getValue().getIdMapa())) {
						ret.add(item.getValue().getUnidad().getIdUnidad());
					}
				}
			} catch (Exception e) {
				throw new GetListaUnidadesMinaException("No se pruede recuperar la lista de unidades de la mina", e);
			}
		}
		return ret;
	}

	@Override
	public Map<Integer, String> getCompaniesForLockedMap(List<Integer> lstMaps) throws GetListaUnidadesMinaException {
		Map<Integer, String> ret = new HashMap();
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					if (lstMaps.contains(item.getValue().getIdMapa())) {

						if (!ret.containsKey(item.getValue().getOperario().getEmpresaId())) {
							ret.put(item.getValue().getOperario().getEmpresaId(),
									item.getValue().getOperario().getEmpresa());
						}
					}
				}
			} catch (Exception e) {
				throw new GetListaUnidadesMinaException("exception in getCompaniesForLockedMap : ", e);
			}
		}

		return ret;
	}

	@Override
	public List<Integer> getUsuariosByCompanyForLockedMap(List<Integer> empresas, List<Integer> lstMaps)
			throws GetListaUnidadesMinaException {
		List<Integer> ret = new ArrayList<Integer>();
		synchronized (MapaObjectPoolBean.class) {
			try {
				for (Map.Entry<Integer, datosUnidadMapa> item : _lista_unidades.entrySet()) {
					if (lstMaps.contains(item.getValue().getIdMapa())) {

						if (empresas.contains(item.getValue().getOperario().getEmpresaId())) {
							ret.add(item.getValue().getUnidad().getIdUnidad());
						}
					}
				}
			} catch (Exception e) {
				throw new GetListaUnidadesMinaException("No se pruede recuperar la lista de unidades de la mina", e);
			}
		}
		return ret;
	}

	// @Override
	// public List<datosUnidadMapaWS> getUnidadesPorMapa(int id_mapa) throws
	// GetUnidadesMapaException
	// {
	// List<datosUnidadMapaWS> ret = new ArrayList<datosUnidadMapaWS>();
	//
	// synchronized (MapaObjectPoolBean.class)
	// {
	// try
	// {
	// for (Map.Entry<Integer, datosUnidadMapa> item :
	// _lista_unidades.entrySet())
	// {
	// if (item.getValue().getIdMapa() == id_mapa)
	// {
	// ret.add(item.getValue());
	// }
	// }
	// }
	// catch (Exception e)
	// {
	// throw new GetUnidadesMapaException("No se pruede recuperar la lista de
	// unidades del mapa " + id_mapa, e);
	// }
	// }
	// return ret;
	// }
}
