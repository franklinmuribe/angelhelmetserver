package com.angelhelmet.server.negocio.pool.excepciones;

public class AddUnidadPoolException extends Exception
{
	private static final long serialVersionUID = 5814899885690506394L;
	
	public AddUnidadPoolException() {
	}
	
	public AddUnidadPoolException(String s) {
		super(s);
	}
	
	public AddUnidadPoolException(String s, Exception e) {
		super(s, e);
	}

}
