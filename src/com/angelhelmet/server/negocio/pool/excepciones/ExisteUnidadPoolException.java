package com.angelhelmet.server.negocio.pool.excepciones;

public class ExisteUnidadPoolException extends Exception
{
	private static final long serialVersionUID = 8825442384063944558L;

	public ExisteUnidadPoolException() {
	}
	
	public ExisteUnidadPoolException(String s) {
		super(s);
	}
	
	public ExisteUnidadPoolException(String s, Exception e) {
		super(s, e);
	}
}
