package com.angelhelmet.server.negocio.pool.excepciones;

public class GetUnidadesMapaException extends Exception
{
	private static final long serialVersionUID = -9102604519701675973L;
	
	public GetUnidadesMapaException() {
	}
	
	public GetUnidadesMapaException(String s) {
		super(s);
	}
	
	public GetUnidadesMapaException(String s, Exception e) {
		super(s, e);
	}

}
