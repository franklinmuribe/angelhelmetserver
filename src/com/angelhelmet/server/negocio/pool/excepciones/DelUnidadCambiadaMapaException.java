package com.angelhelmet.server.negocio.pool.excepciones;

public class DelUnidadCambiadaMapaException extends Exception
{
	private static final long serialVersionUID = -1990190888501010476L;
	
	public DelUnidadCambiadaMapaException() {
	}
	
	public DelUnidadCambiadaMapaException(String s) {
		super(s);
	}
	
	public DelUnidadCambiadaMapaException(String s, Exception e) {
		super(s, e);
	}

}
