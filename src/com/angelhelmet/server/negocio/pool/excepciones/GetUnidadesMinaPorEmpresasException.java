package com.angelhelmet.server.negocio.pool.excepciones;

public class GetUnidadesMinaPorEmpresasException extends Exception
{
	private static final long serialVersionUID = 504746901500221009L;
	
	public GetUnidadesMinaPorEmpresasException() {
	}
	
	public GetUnidadesMinaPorEmpresasException(String s) {
		super(s);
	}
	
	public GetUnidadesMinaPorEmpresasException(String s, Exception e) {
		super(s, e);
	}

}
