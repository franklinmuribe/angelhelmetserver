package com.angelhelmet.server.negocio.pool.excepciones;

public class EditUnidadPoolException extends Exception
{
	private static final long serialVersionUID = -7849338395779532196L;
	
	public EditUnidadPoolException() {
	}
	
	public EditUnidadPoolException(String s) {
		super(s);
	}
	
	public EditUnidadPoolException(String s, Exception e) {
		super(s, e);
	}

}
