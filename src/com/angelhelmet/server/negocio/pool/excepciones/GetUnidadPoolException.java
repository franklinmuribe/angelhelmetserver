package com.angelhelmet.server.negocio.pool.excepciones;

public class GetUnidadPoolException extends Exception
{
	private static final long serialVersionUID = -5454577565277092543L;

	public GetUnidadPoolException() {
	}
	
	public GetUnidadPoolException(String s) {
		super(s);
	}
	
	public GetUnidadPoolException(String s, Exception e) {
		super(s, e);
	}
}
