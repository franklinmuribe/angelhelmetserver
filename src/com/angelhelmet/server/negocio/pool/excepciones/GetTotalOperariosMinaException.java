package com.angelhelmet.server.negocio.pool.excepciones;

public class GetTotalOperariosMinaException extends Exception
{
	private static final long serialVersionUID = -3872475404781351585L;
	
	public GetTotalOperariosMinaException() {
	}
	
	public GetTotalOperariosMinaException(String s) {
		super(s);
	}
	
	public GetTotalOperariosMinaException(String s, Exception e) {
		super(s, e);
	}

}
