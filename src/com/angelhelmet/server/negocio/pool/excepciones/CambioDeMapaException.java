package com.angelhelmet.server.negocio.pool.excepciones;

public class CambioDeMapaException extends Exception
{
	private static final long serialVersionUID = 1101097287580517372L;
	
	public CambioDeMapaException() {
	}
	
	public CambioDeMapaException(String s) {
		super(s);
	}
	
	public CambioDeMapaException(String s, Exception e) {
		super(s, e);
	}

}
