package com.angelhelmet.server.negocio.pool.excepciones;

public class LimpiaPoolException extends Exception
{
	private static final long serialVersionUID = 4077122147831639568L;
	
	public LimpiaPoolException() {
	}
	
	public LimpiaPoolException(String s) {
		super(s);
	}
	
	public LimpiaPoolException(String s, Exception e) {
		super(s, e);
	}

}
