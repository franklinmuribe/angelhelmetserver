package com.angelhelmet.server.negocio.pool.excepciones;

public class ReemplazaUnidadPoolException extends Exception
{
	private static final long serialVersionUID = 5240257168641257124L;

	public ReemplazaUnidadPoolException() {
	}
	
	public ReemplazaUnidadPoolException(String s) {
		super(s);
	}
	
	public ReemplazaUnidadPoolException(String s, Exception e) {
		super(s, e);
	}
}
