package com.angelhelmet.server.negocio.pool.excepciones;

public class DelUnidadPoolException extends Exception
{
	private static final long serialVersionUID = 1505007600217887296L;
	
	public DelUnidadPoolException() {
	}
	
	public DelUnidadPoolException(String s) {
		super(s);
	}
	
	public DelUnidadPoolException(String s, Exception e) {
		super(s, e);
	}

}
