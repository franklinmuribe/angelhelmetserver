package com.angelhelmet.server.negocio.pool.excepciones;

public class GetListaUnidadesMinaException extends Exception
{
	private static final long serialVersionUID = 2095155020183559277L;
	
	public GetListaUnidadesMinaException() {
	}
	
	public GetListaUnidadesMinaException(String s) {
		super(s);
	}
	
	public GetListaUnidadesMinaException(String s, Exception e) {
		super(s, e);
	}

}
