package com.angelhelmet.server.negocio.pool.excepciones;

public class GetUnidadesCambioMapaException extends Exception
{
	private static final long serialVersionUID = -5205411502250466403L;
	
	public GetUnidadesCambioMapaException() {
	}
	
	public GetUnidadesCambioMapaException(String s) {
		super(s);
	}
	
	public GetUnidadesCambioMapaException(String s, Exception e) {
		super(s, e);
	}

}
