package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.MedicionDAO;
import com.angelhelmet.server.dao.interfaces.IMedicionSaludDAO;
import com.angelhelmet.server.negocio.interfaces.IMedicionSaludBean;
import com.angelhelmet.server.persistencia.UmbralesMedicionSaludEB;
import com.hcl.iot.smartworker.dto.MediconesDTO;

@Stateless
public class MedicionSaludBean implements IMedicionSaludBean {
	@EJB
	private IMedicionSaludDAO _medicion_salud_dao;
	private Logger _log;

	public MedicionSaludBean() {
		_log = LogManager.getLogger(MedicionSaludBean.class);
		_medicion_salud_dao = new MedicionDAO();
	}

	@Override
	public MediconesDTO getMedicion(Integer unidadId) throws Exception {
		MediconesDTO ret = null;
		_log.error(" MedicionSaludBean getMedicion  implement ************ " + unidadId);
		ret = _medicion_salud_dao.getMedicion(unidadId);
		return ret;
	}

	@Override
	public UmbralesMedicionSaludEB getUmbralByUnidadSaludAndTipoMedicion(Integer tipoMedicionId, Integer unidadSaludId)
			throws Exception {
		UmbralesMedicionSaludEB ret = null;
		_log.error(" MedicionSaludBean getUmbralByUnidadSaludAndTipoMedicion  implement ************ tipoMedicionId "
				+ tipoMedicionId + " unidadSaludId " + unidadSaludId);
		ret = _medicion_salud_dao.getUmbralByUnidadSaludAndTipoMedicion(tipoMedicionId, unidadSaludId);
		return ret;
	}

}
