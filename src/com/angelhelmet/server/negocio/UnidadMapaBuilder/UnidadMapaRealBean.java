package com.angelhelmet.server.negocio.UnidadMapaBuilder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;

@Singleton
@Startup
public class UnidadMapaRealBean extends UnidadMapa implements IUnidadMapaRealBean
{
	@EJB
	private IMapaObjectPoolBean _pool;
	private Logger _log;

	@PostConstruct
	private void startup()
	{
		_log = LogManager.getLogger(UnidadMapaRealBean.class);
		//_log.info("UnidadMapaRealBean: Singleton inited!");
	}

	public IMapaObjectPoolBean getPool()
	{
		return _pool;
	}

}
