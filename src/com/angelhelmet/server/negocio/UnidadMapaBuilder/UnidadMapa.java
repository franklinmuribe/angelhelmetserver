package com.angelhelmet.server.negocio.UnidadMapaBuilder;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
//import java.io.FileWriter;
//import java.io.PrintWriter;
//import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
//import java.util.Calendar;
//import java.util.GregorianCalendar;
import java.util.List;
//import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosAp;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosMapa;
import com.angelhelmet.server.datos.datosOperarioWS;
import com.angelhelmet.server.datos.datosPermisoZona;
import com.angelhelmet.server.datos.datosPunto;
import com.angelhelmet.server.datos.datosTiempoVida;
import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.datos.datosZona;
import com.angelhelmet.server.negocio.interfaces.IApBean;
import com.angelhelmet.server.negocio.interfaces.IEventBean;
import com.angelhelmet.server.negocio.interfaces.IMedicionSaludBean;
import com.angelhelmet.server.negocio.interfaces.IUmbralBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadMensajeBean;
import com.angelhelmet.server.negocio.interfaces.IUsaGlobalesBean;
import com.angelhelmet.server.negocio.interfaces.IZonaAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IZonaBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapa;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;
import com.angelhelmet.server.negocio.interfaces.posicionamiento.IPosicionamientoInterior;
import com.angelhelmet.server.negocio.kalman.Kalman;
import com.angelhelmet.server.negocio.kalman.KalmanIn;
import com.angelhelmet.server.negocio.kalman.KalmanPotencia;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;
import com.angelhelmet.server.negocio.pool.excepciones.AddUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.EditUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.ExisteUnidadPoolException;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadPoolException;
import com.angelhelmet.server.negocio.posicionamiento.PosicionamientoExterior;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PonderaPuntosException;
import com.angelhelmet.server.negocio.posicionamiento.excepciones.PosicionExteriorException;
import com.angelhelmet.server.persistencia.ActivaMedicionEB;
import com.angelhelmet.server.persistencia.AlarmaSaludEB;
import com.angelhelmet.server.persistencia.ApAdyacenteEB;
import com.angelhelmet.server.persistencia.ApEB;
import com.angelhelmet.server.persistencia.DiasLecturaEB;
import com.angelhelmet.server.persistencia.HorarioLecturaEB;
import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.persistencia.UmbralesMedicionSaludEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadMensajeEB;
import com.angelhelmet.server.persistencia.UnidadSaludEB;
import com.angelhelmet.server.persistencia.ZonaEB;
import com.angelhelmet.server.rest.service.api.MensajeUnico;
import com.angelhelmet.server.rest.service.bean.EnvioMensaje;
import com.angelhelmet.server.servlet.Server;
import com.angelhelmet.server.util.EstadoAlarma;
import com.angelhelmet.server.util.EstadoMensajeEntrada;
import com.angelhelmet.server.util.EstadoMensajeSalida;
import com.angelhelmet.server.util.ModoFuncionamiento;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.TipoAlarma;
import com.angelhelmet.server.util.TipoPotencia;
import com.angelhelmet.server.util.excepciones.DentroDelPoligonoException;
import com.angelhelmet.server.util.excepciones.GeneraFirmaException;
import com.hcl.iot.smartworker.dto.MediconesDTO;

@Stateless
public class UnidadMapa implements IUnidadMapa {
	private datosLog _datos_log;
	private datosUnidadMapa _unidad_mapa;
	private int _id_mapa_anterior;
	private datosComunicacion datos;

	private Logger _log;

	@Override
	public void setDatos(datosComunicacion datos) {
		this.datos = datos;
	}

	@EJB
	private IApBean _ap_bean;
	@EJB
	private IUnidadMensajeBean _um_bean;
	@EJB
	private IPosicionamientoInterior _posicionamiento_interior;
	@EJB
	private IZonaBean _zona_bean;
	@EJB
	private IZonaAlarmaBean _zona_alarma_bean;
	@EJB
	private IUsaGlobalesBean _globales;
	@EJB
	IUnidadBean _unidad_bean;
	@EJB
	EnvioMensaje _envio_mensaje;

	@EJB
	IMedicionSaludBean _medicion_salud_bean;

	@EJB
	IUmbralBean _umbral_bean;

	IMapaObjectPoolBean poolBean;

	private Map<Integer, Boolean> mensajeEnviado = new HashMap<>();

	// private final Lock lock = new ReentrantLock();
	private final static Object block = new Object();

	// FileWriter fichero;
	// PrintWriter pw;

	public UnidadMapa() {
		_log = LogManager.getLogger(Server.class);

		try {
			_datos_log = new datosLog();
			_unidad_mapa = new datosUnidadMapa();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public void setUnidadMapa(datosLog datos_log) {
		// _log.info(this.getClass().getName() + " - setUnidadMapa");

		_datos_log = datos_log;
	}

	@Override
	public datosUnidadMapa getUnidadMapa() {
		// _log.info(this.getClass().getName() + " - getUnidadMapa");
		return _unidad_mapa;
	}

	@Override
	public boolean creaUnidadMapa(IMapaObjectPoolBean pool)
			throws ApsMapaNoEncontradosException, ApNoEncontradoException, PosicionExteriorException,
			PonderaPuntosException, CalculaKalmanException, GetNodosProyeccionException, GetZonasMapaException,
			GetZonaCoordenadas, DentroDelPoligonoException, ZonaSinCoordenadasException, GetPermisosZonaException,
			UnidadMensajeNoEncontradoException, GetUnidadMensajeByIdException, GetMensajesPurgarException,
			PurgarMensajeException, UnidadMensajeSetLeidoException, Exception

	{
		// _log.info(this.getClass().getName() + " - creaUnidadMapa");
		boolean ret = false;

		if (_datos_log != null) {
			int id_unidad = 0;
			id_unidad = _datos_log.getUnidad().getIdUnidad();

			poolBean = pool;

			compruebaEstadoUnidad(pool, id_unidad);

			_id_mapa_anterior = _unidad_mapa.getIdMapa();
			_unidad_mapa.setIdMapa(_datos_log.getMapa().getIdMapa());

			_unidad_mapa.setIdLog(_datos_log.getLog().getIdLog());

			_unidad_mapa.getUnidad().setIdUnidad(id_unidad);
			_unidad_mapa.getUnidad().setSerie(_datos_log.getUnidad().getNumeroSerie());
			_unidad_mapa.getUnidad().setSensorX(_datos_log.getLog().getSensorX());
			_unidad_mapa.getUnidad().setSensorY(_datos_log.getLog().getSensorY());
			_unidad_mapa.getUnidad().setSensorZ(_datos_log.getLog().getSensorZ());
			_unidad_mapa.getUnidad().setBateria(_datos_log.getLog().getBateria());
			_unidad_mapa.getUnidad().setGas(_datos_log.getLog().getGas());
			_unidad_mapa.getUnidad().setTiempoVida(new datosTiempoVida(_datos_log.getLog().getTiempo()));
			_unidad_mapa.getUnidad()
					.setTimeLastReport(_datos_log.getLog().getFechaLog().toInstant().getEpochSecond() * 1000);
			_unidad_mapa.setFechaLog(_datos_log.getLog().getFechaLog().getTime());

			// Added extra by Gunjan
			defineOperario();
			defineMapa();
			// TODO added sanjay try catch

			gestionaMensaje();

			creaAlarma();
			calculaPosicion();
			calculaZona();
			creaAlarmaUnidadNoActiva();
			creaAlarmaUnidadNoAsignada();
			crearAlarmaCoronaVirus();
			crearAlarmaSalud();
			crearMensajeSalud();

			_datos_log.getLog().setX((int) Math.round(_unidad_mapa.getPosicion().getCoordenadas().getX()));
			_datos_log.getLog().setY((int) Math.round(_unidad_mapa.getPosicion().getCoordenadas().getY()));

			// TODO HCL
			// _datos_log.getLog().setX(_unidad_mapa.getPosicion().getCoordenadas().getX());
			// _datos_log.getLog().setY(_unidad_mapa.getPosicion().getCoordenadas().getY());

			// imprimePosicion();
			ret = true;
		}
		return ret;
	}

	// Added extra by Gunjan
	private void defineOperario() {
		// _log.info(this.getClass().getName() + " - defineOperario");

		datosOperarioWS operario = new datosOperarioWS();

		// if (_datos_log.getUnidad().getUnidadesOperarios().stream().filter(uo->
		// uo.getActivo().equals(true)).count() == 1)
		// TODO SANJAY
		// if (_datos_log.getUnidad().getUnidadesOperarios().size() ==1 ) need to revert
		// it latter.
		if (_datos_log.getUnidad().getUnidadesOperarios().size() > 0) {
			System.out.println("datos_log.getUnidad().getUnidadesOperarios().size(): "
					+ _datos_log.getUnidad().getUnidadesOperarios().size());
			OperarioEB o_eb = _datos_log.getUnidad().getUnidadesOperarios().get(0).getOperario();
			operario.setApellidos(o_eb.getApellidos());
			/*
			 * if (o_eb.getOperariosCargos().size() == 1) {
			 * operario.setCargo(o_eb.getOperarioscargos().get(0).getCargo().getDescripcion(
			 * )); }
			 */

			if (o_eb.getOperariosEmpresas().size() == 1) {
				operario.setEmpresa(o_eb.getOperariosEmpresas().get(0).getEmpresa().getRazonSocial());
				operario.setEmpresaId(o_eb.getOperariosEmpresas().get(0).getEmpresa().getIdEmpresa());
			}

			if (o_eb.getOperarioscargos().size() == 1) {
				operario.setCargo(o_eb.getOperarioscargos().get(0).getCargo().getDescripcion());
				operario.setCargoId(o_eb.getOperarioscargos().get(0).getCargo().getIdCargo());
			}

			operario.setIdentificacion(o_eb.getIdentificacion());
			operario.setIdOperario(o_eb.getIdOperario());
			operario.setNombre(o_eb.getNombre());
			System.out.println("o_eb.getNombre(): " + o_eb.getNombre());
			operario.setBlood(o_eb.getBlood());
			operario.setEmergency(o_eb.getEmergency());
			operario.setSmsNumber(o_eb.getSmsNumber());
			operario.setSupervisor(o_eb.getSupervisor());
		}

		_unidad_mapa.setOperario(operario);

	}

	@Override
	public void gestionaPool(IMapaObjectPoolBean pool)
			throws ExisteUnidadPoolException, EditUnidadPoolException, AddUnidadPoolException, Exception {
		// _log.info(this.getClass().getName() + " - gestionaPool");

		int id_unidad = 0;
		id_unidad = _unidad_mapa.getUnidad().getIdUnidad();

		if (pool.existeUnidad(id_unidad)) {
			// pool.editUnidad(_unidad_mapa, id_unidad);
			pool.cambioDeMapa(id_unidad, _id_mapa_anterior);
		} else {
			pool.addUnidad(_unidad_mapa, id_unidad);
		}
	}

	private void compruebaEstadoUnidad(IMapaObjectPoolBean pool, int id_unidad)
			throws ExisteUnidadPoolException, GetUnidadPoolException, Exception {

		// _log.info(this.getClass().getName() + " - compruebaEstadoUnidad");
		if (pool.existeUnidad(id_unidad)) {
			_unidad_mapa = pool.getUnidad(id_unidad);
		} else {
			_unidad_mapa = new datosUnidadMapa();
		}
	}

	private void defineMapa() throws ApsMapaNoEncontradosException, ApNoEncontradoException, Exception {
		// _log.info(this.getClass().getName() + " - defineMapa");

		if (_unidad_mapa.getIdMapa() != _id_mapa_anterior) {
			_unidad_mapa.setZona(new datosZona());
			_unidad_mapa.setPosicion(new datosPunto());
		}

		if (_datos_log.getMapa().getIdTipoMapa() == 1) {
			if (_unidad_mapa.getMapa() == null || _unidad_mapa.getIdMapa() != _id_mapa_anterior) {
				List<ApEB> lista_ap = _ap_bean.getApsMapa(_datos_log.getMapa().getIdMapa());
				if (lista_ap != null) {
					datosMapa datos_mapa = new datosMapa();

					for (ApEB item_ap : lista_ap) {
						datosAp datos_ap = new datosAp();
						List<datosAp> lista_adyacentes = new ArrayList<datosAp>();

						datos_ap.setCoordenadaX(item_ap.getX());
						datos_ap.setCoordenadaY(item_ap.getY());
						datos_ap.setId(item_ap.getIdAp());
						datos_ap.setMac(item_ap.getMac());

						for (ApAdyacenteEB item_adyacente : item_ap.getApsAdyacentes()) {
							datosAp datos_adyacentes = new datosAp();
							ApEB adyacente = _ap_bean.getAp(item_adyacente.getIdAdyacente());

							datos_adyacentes.setCoordenadaX(adyacente.getX());
							datos_adyacentes.setCoordenadaY(adyacente.getY());
							datos_adyacentes.setId(adyacente.getIdAp());

							lista_adyacentes.add(datos_adyacentes);

						}
						datos_ap.setApsAdyacentes(lista_adyacentes);
						datos_mapa.addApMapa(datos_ap);
					}
					_unidad_mapa.setMapa(datos_mapa);
				}
			}
		}
	}

	private void creaAlarma() throws GeneraFirmaException, Exception {
		// _log.info(this.getClass().getName() + " - creaAlarma");

		// _unidad_mapa.setCurrAlarm(null);
		datosAlarma alarma_actual = new datosAlarma();
		datosAlarma ultima_alarma = _unidad_mapa.getAlarmaAnterior();

		TipoAlarma tipo_alarma = _datos_log.getDatos().getAlarma();
		boolean cancelada = false;
		boolean hay_gas = _unidad_mapa.HayGas();
		boolean gas_otros = _unidad_mapa.GasOtros();

		/*
		 * if (hay_gas) { _log.info("hay_gas"); } else { _log.info("no hay_gas"); }
		 * 
		 * if (gas_otros) { _log.info("gas_otros"); } else { _log.info("no gas_otros");
		 * }
		 */

		alarma_actual.setAlarma(tipo_alarma);

		/*
		 * _log.info("alarma_actual.getValorAlarma: " + alarma_actual.getValorAlarma());
		 * _log.info("alarma actual: " + alarma_actual.getAlarma());
		 * 
		 * _log.info("ultima_alarma.getValorAlarma: " + ultima_alarma.getValorAlarma());
		 * _log.info("ultima alarma : " + ultima_alarma.getAlarma());
		 */

		if (alarma_actual.getAlarma() != TipoAlarma.NINGUNA || ultima_alarma.getAlarma() != TipoAlarma.NINGUNA) {
			if (alarma_actual.getAlarma() != ultima_alarma.getAlarma()) {
				if (alarma_actual.getAlarma() == TipoAlarma.NINGUNA) {
					// _log.info("creaAlarma --> 1");

					if (!gas_otros && hay_gas) {
						// _log.info("creaAlarma --> 1-1");
						hay_gas = false;
						tipo_alarma = TipoAlarma.GASOK;
						// 19-07-2020 JFP - Si hay gas, cancela alarma previa.
						cancelada = true;
					} else if (gas_otros && hay_gas) {
						// _log.info("creaAlarma --> 1-2");
						hay_gas = false;
						addAlarma(TipoAlarma.GASOK, _unidad_mapa.getFirmaGas());
						cancelada = true;
					} else {
						if (ultima_alarma.getAlarma() != TipoAlarma.NINGUNA && !hay_gas) {
							// _log.info("creaAlarma --> 1-3");
							if (gas_otros) {
								cancelada = true;
								// _log.info("creaAlarma --> 1-4");
							} else {
								tipo_alarma = TipoAlarma.ALARMAOK;
								// _log.info("creaAlarma --> 1-5");
							}
							gas_otros = false;
						}
					}
				} else {
					tipo_alarma = alarma_actual.getAlarma();
					// sos, impacto, inmovil
					if (alarma_actual.getValorAlarma() > TipoAlarma.NINGUNA.getAlarma()
							&& alarma_actual.getValorAlarma() < (int) TipoAlarma.ENTRADAZONA.getAlarma()) {
						if (ultima_alarma.getAlarma() != TipoAlarma.NINGUNA) {

							// _log.info("creaAlarma --> 2");

							if (hay_gas && gas_otros) {
								hay_gas = false;
								addAlarma(TipoAlarma.CANCELADASISTEMA, _unidad_mapa.getFirmaGas());
								cancelada = true;
							} else if (hay_gas) {
								hay_gas = false;
								addAlarma(TipoAlarma.CANCELADASISTEMA, _unidad_mapa.getFirmaGas());
							} else if (gas_otros) {
								gas_otros = false;
								addAlarma(TipoAlarma.CANCELADASISTEMA, ultima_alarma.getFirma());
							} else if ((ultima_alarma.getValorAlarma() > TipoAlarma.NINGUNA.getAlarma()
									&& ultima_alarma.getValorAlarma() < (int) TipoAlarma.ENTRADAZONA.getAlarma())) {
								cancelada = true;
							}

						}
					}
					// zona de gases
					else if (alarma_actual.getValorAlarma() > TipoAlarma.PERMANENCIAZONA.getAlarma()
							&& alarma_actual.getValorAlarma() < TipoAlarma.MENSAJELEIDO.getAlarma()) {

						// _log.info("creaAlarma --> 3");

						// acabo de entrar el gas
						if (alarma_actual.getAlarma() == TipoAlarma.GAS) {
							// _log.info("creaAlarma --> 3-1");
							if (!hay_gas && ultima_alarma.getValorAlarma() >= TipoAlarma.NINGUNA.getAlarma()
									&& ultima_alarma.getValorAlarma() < (int) TipoAlarma.ENTRADAZONA.getAlarma()) {
								hay_gas = true;
								// _log.info("creaAlarma --> 3-2");
								if (ultima_alarma.getValorAlarma() > TipoAlarma.NINGUNA.getAlarma()
										&& ultima_alarma.getValorAlarma() < (int) TipoAlarma.ENTRADAZONA.getAlarma()) {
									cancelada = true;
									// _log.info("creaAlarma --> 3-3");
								}
							}

							// estoy en zona de gas y se cancela una alarma
							// en
							// esta zona
							if (ultima_alarma.getValorAlarma() > TipoAlarma.GAS.getAlarma()
									&& ultima_alarma.getValorAlarma() < TipoAlarma.MENSAJELEIDO.getAlarma()) {
								tipo_alarma = TipoAlarma.ALARMAOK;
								gas_otros = false;
								// _log.info("creaAlarma --> 3-4");
							}
							// hay_gas = true;
						} else {
							if (ultima_alarma.getAlarma() != TipoAlarma.GASOK) {
								// _log.info("creaAlarma --> 3-5");
								if ( // ultima_alarma.getAlarma() != TipoAlarma.GAS // 19-07-2020 JFP
								ultima_alarma.getAlarma() != TipoAlarma.ALARMAOK
										&& ultima_alarma.getAlarma() != TipoAlarma.NINGUNA) {
									// _log.info("creaAlarma --> 3-6");
									cancelada = true;
								}

							}
							// }
							gas_otros = true;
						}
						// hay_gas = true;
					}
				}
			}
		}

		if (cancelada) {
			// _log.info("creaAlarma --> cancelada - " + ultima_alarma.getAlarma());
			long ultima_firma = 0;
			if (ultima_alarma.getAlarma() == TipoAlarma.GAS) {

				ultima_firma = _unidad_mapa.getFirmaGas();
			} else {
				if (gas_otros) {
					gas_otros = false;
				}
				ultima_firma = ultima_alarma.getFirma();
			}
			addAlarma(TipoAlarma.CANCELADASISTEMA, ultima_firma);
		}

		if (alarma_actual.getAlarma() != TipoAlarma.NINGUNA || ultima_alarma.getAlarma() != TipoAlarma.NINGUNA) {
			if (alarma_actual.getAlarma() != ultima_alarma.getAlarma()) {
				// _log.info("creaAlarma --> 4");
				if (tipo_alarma == TipoAlarma.GASOK) {
					ultima_alarma = addAlarma(tipo_alarma, _unidad_mapa.getFirmaGas());
				} else {
					if (!(ultima_alarma.getAlarma() == TipoAlarma.ALARMAOK && tipo_alarma == TipoAlarma.GAS)) {
						ultima_alarma = addAlarma(tipo_alarma, ultima_alarma.getFirma());
						if (tipo_alarma == TipoAlarma.GAS) {
							_unidad_mapa.setFirmaGas(ultima_alarma.getFirma());
						}
					}
				}
				_unidad_mapa.setAlarmaAnterior(ultima_alarma);
				_unidad_mapa.setHayGas(hay_gas);
				_unidad_mapa.setGasOtros(gas_otros);
			}
		}

		if (tipo_alarma == TipoAlarma.ALARMAOK || tipo_alarma == TipoAlarma.GASOK) {
			// _log.info("creaAlarma --> 5");
			if (!hay_gas) {
				_unidad_mapa.setAlarmaAnterior(new datosAlarma());
				if (tipo_alarma == TipoAlarma.GASOK) {
					_unidad_mapa.setFirmaGas(0);
				}
			}
		}

	}

	private void calculaPosicion() throws PosicionExteriorException, PonderaPuntosException, CalculaKalmanException,
			GetNodosProyeccionException, Exception {
		// _log.info(this.getClass().getName() + " - calculaPosicion");

		if (_datos_log.getDatos().getModo() == ModoFuncionamiento.INTERIOR) {
			posicionInterior();
		} else {
			posicionExterior();
		}
	}

	private void posicionInterior()
			throws PonderaPuntosException, CalculaKalmanException, GetNodosProyeccionException, Exception {

		// _log.info(this.getClass().getName() + " - posicionInterior");

		if (_datos_log.getDatos().getRssi1() < 80) {
			datosAp ap_max_potencia = new datosAp();

			int id1 = _unidad_mapa.getMapa().getIdApByMac(_datos_log.getDatos().getMac1());
			int id2 = _unidad_mapa.getMapa().getIdApByMac(_datos_log.getDatos().getMac2());
			int id3 = _unidad_mapa.getMapa().getIdApByMac(_datos_log.getDatos().getMac3());
			int id4 = _unidad_mapa.getMapa().getIdApByMac(_datos_log.getDatos().getMac4());

			ap_max_potencia = _unidad_mapa.getMapa().getAp(id1);

			for (datosAp item_ap : _unidad_mapa.getMapa().getApsMapa()) {
				int potencia = 0;
				boolean aplico_kalman = false;
				datosAp ap_actual = new datosAp();

				item_ap.setPotencia(0);

				if (id1 == item_ap.getId()) {
					potencia = _datos_log.getDatos().getRssi1();
					ap_actual = ap_max_potencia;
				}

				if (id2 == item_ap.getId()) {
					potencia = _datos_log.getDatos().getRssi2();
					ap_actual = item_ap;
				}

				if (id3 == item_ap.getId()) {
					potencia = _datos_log.getDatos().getRssi3();
					ap_actual = item_ap;
				}

				if (id4 == item_ap.getId()) {
					potencia = _datos_log.getDatos().getRssi4();
					ap_actual = item_ap;
				}

				if (ap_actual.getApsAdyacentes().contains(ap_max_potencia)
						|| ap_actual.getId() == ap_max_potencia.getId()) {
					item_ap.setPotencia(potencia, TipoPotencia.Real);
					aplico_kalman = true;
				} else {
					double potencia_reducida = item_ap.getValorInicial().getValorFinal() * 0.5;
					item_ap.setPotencia(potencia_reducida, TipoPotencia.Kalman);
					aplico_kalman = true;
				}

				if (aplico_kalman) {
					KalmanIn entrada = new KalmanIn();
					entrada.setEstimacionInicial(item_ap.getValorInicial().getEstimacionFinal());
					entrada.setValorInicial(item_ap.getValorInicial().getValorFinal());

					entrada.setPotencia(item_ap.getPotencia());
					item_ap.setValorInicial(Kalman.Calcula(entrada, new KalmanPotencia()));
				}
			}

			_posicionamiento_interior.setApsMapa(_unidad_mapa.getMapa().getApsMapa());
			// TODO ADDDED BY HCL SANJAY
			_unidad_mapa.getPosicion().set_mode("INTERIOR");
			_posicionamiento_interior.setDatosPosicion(_unidad_mapa.getPosicion());
			_unidad_mapa.setPosicion(_posicionamiento_interior.getPosicion());
		}
	}

	private void posicionExterior() throws PosicionExteriorException, Exception {
		PosicionamientoExterior posicionamiento = new PosicionamientoExterior();

		// _log.info(this.getClass().getName() + " - posicionExterior");

		// TODO HCL
		// datosCoordenada punto = posicionamiento.getPosicion(_datos_log.getMapa(),
		// _datos_log.getLog().getLongitud(), _datos_log.getLog().getLatitud());
		datosCoordenada punto = new datosCoordenada();
		punto.setX(_datos_log.getLog().getLatitud());
		punto.setY(_datos_log.getLog().getLongitud());

		datosPunto p = _unidad_mapa.getPosicion();
		// TODO ADDDED BY HCL SANJAY
		_unidad_mapa.getPosicion().set_mode("EXTERIOR");
		p.setCoordenadas(punto);
		_unidad_mapa.setPosicion(p);
		// TODO HCL SANJAY
	}

	private void calculaZona() throws GetZonasMapaException, GetZonaCoordenadas, DentroDelPoligonoException,
			ZonaSinCoordenadasException, GetPermisosZonaException, Exception {

		// _log.info(this.getClass().getName() + " - calculaZona");

		ZonaEB zona = new ZonaEB();
		List<datosPermisoZona> permisos = new ArrayList<datosPermisoZona>();

		_unidad_mapa.getZona().setEstaEnZona(false);
		zona = _zona_bean.estaEnZona(_unidad_mapa.getIdMapa(), _unidad_mapa.getPosicion().getCoordenadas());
		if (zona != null) {
			// SI esta en zona
			_unidad_mapa.getZona().setNombreZona(zona.getDescripcion());
			_unidad_mapa.getZona().setIdZona(zona.getIdZona());
			_unidad_mapa.getZona().setEstaEnZona(true);
			permisos = _zona_alarma_bean.getPermisosZona(zona.getIdZona(), _unidad_mapa.getUnidad().getIdUnidad());
			if (permisos != null) {
				// No hay error al obtener los permisos para la unidad que esta
				// en la zona
				if (!_zona_bean.tienePermisoEntrada(permisos)) {
					// **NO** tiene permiso de entrada

					if (_unidad_mapa.getZona().getAlarmaEntrada() == EstadoAlarma.NINGUNA) {
						_unidad_mapa.getZona()
								.setTiempoEntrada(_zona_bean.getTiempoEntrada(_datos_log.getLog().getFechaLog()));
						addAlarma(TipoAlarma.ENTRADAZONA);
					}
					_unidad_mapa.getZona().setAlarmaEntrada(EstadoAlarma.LANZA);
				}
				_unidad_mapa.getZona().setUltimaZona(zona.getIdZona());
				if (_zona_bean.zonaConPermanencia(zona)) {
					if (!_zona_bean.tienePermisoPermanencia(permisos)) {
						// ****
						// El operario tiene permiso de entrada, por lo tanto se
						// registra el tiempo de entrada
						// el calculo de la permanencia
						if (_unidad_mapa.getZona().getTiempoEntrada() == null) {
							_unidad_mapa.getZona()
									.setTiempoEntrada(_zona_bean.getTiempoEntrada(_datos_log.getLog().getFechaLog()));
						}
						// ****
						if (_zona_bean.calculaPermanencia(_datos_log.getLog().getFechaLog(),
								_unidad_mapa.getZona().getTiempoEntrada(), zona.getTiempoPermanencia())) {
							if (_unidad_mapa.getZona().getAlarmaPermanencia() == EstadoAlarma.NINGUNA)
								addAlarma(TipoAlarma.PERMANENCIAZONA);
							_unidad_mapa.getZona().setAlarmaPermanencia(EstadoAlarma.LANZA);
						}
					}
				}
			}
		} else {
			// NO esta en zona
			if (_unidad_mapa.getZona().getUltimaZona() == 0) {
				_unidad_mapa.setZona(new datosZona());
			} else {
				permisos = _zona_alarma_bean.getPermisosZona(_unidad_mapa.getZona().getIdZona(),
						_unidad_mapa.getUnidad().getIdUnidad());
				if (permisos != null) {
					if (!_zona_bean.tienePermisoSalida(permisos)) {
						// **NO** tiene permiso de salida
						if (_unidad_mapa.getZona().getAlarmaSalida() == EstadoAlarma.NINGUNA)
							addAlarma(TipoAlarma.SALIDAZONA);
						_unidad_mapa.getZona().setAlarmaSalida(EstadoAlarma.LANZA);
					}
				}
				_unidad_mapa.getZona().setUltimaZona(0);
			}
		}
	}

	private void crearAlarmaCoronaVirus() throws Exception {

		_log.info("crearAlarmaCoronaVirus : ************* ");

		datosUmbral datosUmbral = _umbral_bean.getUmbralPorUnidad(_unidad_mapa.getUnidad().getIdUnidad());
		datosUnidadMapa datosUnidadMapa = poolBean.getUnidad(_unidad_mapa.getUnidad().getIdUnidad());
		if (datosUnidadMapa != null) {
			datosCoordenada datosCoordenada = datosUnidadMapa.getPosicion().getCoordenadas();

			if (datosUmbral != null && datosUmbral.isCoronavirus_proximity()) {
				_log.info("coronavirus : ************* " + datosUmbral.isCoronavirus_proximity());
				List<datosUnidadMapaWS> datosUnidadMapaWSs = poolBean.getUnidadesPorMapa(_unidad_mapa.getIdMapa());

				for (datosUnidadMapaWS datosUnidadMapaWS : datosUnidadMapaWSs) {
					_log.info("getIdUnidad ---> :  " + datosUnidadMapaWS.getUnidad().getIdUnidad());

					datosUnidadMapa datosUnidadMapa1 = poolBean.getUnidad(datosUnidadMapaWS.getUnidad().getIdUnidad());

					datosCoordenada datosCoordenada1 = datosUnidadMapa1.getPosicion().getCoordenadas();
					_log.info("datosCoordenada1.getX() ---> :  " + datosCoordenada1.getX());
					_log.info("datosCoordenada1.gety() ---> :  " + datosCoordenada1.getY());
					_log.info("datosCoordenada.getX() ---> :  " + datosCoordenada.getX());
					_log.info("datosCoordenada.gety() ---> :  " + datosCoordenada.getY());
					if (_unidad_mapa.getUnidad().getIdUnidad() != datosUnidadMapaWS.getUnidad().getIdUnidad()) {
						double distancia = distFrom(datosCoordenada.getX(), datosCoordenada.getY(),
								datosCoordenada1.getX(), datosCoordenada1.getY());

						_log.info("distancia  antes ---> :  " + distancia);
						if (distancia < 1) {
							_log.info("distancia despues---> :  " + distancia);
							addAlarma(TipoAlarma.COVID);

						}
					}

				}

			}
		}

	}

	private void crearAlarmaSalud() throws Exception {

		_log.info("crearAlarmaSalud : ************* ");

		// se busca si la unidad tiene alarmas de salud configuradas
		UnidadEB unidad = _unidad_bean.getUnidad(_unidad_mapa.getUnidad().getIdUnidad());

		// obtengo ultima medicion de salud
		_log.info("obtengo ultima medicion de salud unidad ***************  " + _unidad_mapa.getUnidad().getIdUnidad());
		MediconesDTO mediconesDTO = _medicion_salud_bean.getMedicion(_unidad_mapa.getUnidad().getIdUnidad());
		_log.info("resultado ultima medicion de salud " + mediconesDTO);
		if (mediconesDTO != null) {
			_log.info("mediconesDTO getMaxTemp " + mediconesDTO.getMaxTemp());
			_log.info("mediconesDTO getMinTemp " + mediconesDTO.getMinTemp());
			_log.info("mediconesDTO getMaxSistole " + mediconesDTO.getMaxSistole());
			_log.info("mediconesDTO getMinSistole " + mediconesDTO.getMinSistole());
			_log.info("mediconesDTO getMaxSp02 " + mediconesDTO.getMaxSp02());
		}

		List<UnidadSaludEB> unidadSaludEBs = unidad.getUnidadSaludSet();
		for (UnidadSaludEB unidadSaludEB : unidadSaludEBs) {
			List<DiasLecturaEB> diasLecturaEBs = unidadSaludEB.getDiasLecturaList();
			List<AlarmaSaludEB> alarmaSaludEBs = unidadSaludEB.getAlarmaSaludList();
			List<UmbralesMedicionSaludEB> umbralesMedicionSaludEBs = unidadSaludEB.getUmbralesMedicionSaludList();

			for (AlarmaSaludEB alarmaSaludEB : alarmaSaludEBs) {
				_log.info("alarmaSaludEB getIdTipoMedicion " + alarmaSaludEB.getIdTipoMedicion().getIdTipoMedicion());
				_log.info("alarmaSaludEB getStActivo " + alarmaSaludEB.getStActivo());
				_log.info("alarmaSaludEB getStActivo " + alarmaSaludEB.getIdUnidadSalud().getIdUnidadSalud());

				if (alarmaSaludEB.getStActivo()) {

					UmbralesMedicionSaludEB umbralesMedicionSaludEB = _medicion_salud_bean
							.getUmbralByUnidadSaludAndTipoMedicion(
									alarmaSaludEB.getIdTipoMedicion().getIdTipoMedicion(),
									alarmaSaludEB.getIdUnidadSalud().getIdUnidadSalud());
					if (mediconesDTO.getMinTemp() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 1) {
						if (mediconesDTO.getMinTemp() < umbralesMedicionSaludEB.getMin().doubleValue()) {
							addAlarma(TipoAlarma.TEMPMIN);
						}

					}
					if (mediconesDTO.getMaxTemp() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 1) {
						if (mediconesDTO.getMaxTemp() > umbralesMedicionSaludEB.getMax().doubleValue()) {
							addAlarma(TipoAlarma.TEMPMAX);
						}

					}

					if (mediconesDTO.getMinDiastole() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 2) {
						if (mediconesDTO.getMinDiastole() < umbralesMedicionSaludEB.getMin().doubleValue()) {

							addAlarma(TipoAlarma.DIASTOLEMIN);

						}

					}
					if (mediconesDTO.getMaxDiastole() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 2) {
						if (mediconesDTO.getMaxDiastole() > umbralesMedicionSaludEB.getMax().doubleValue()) {

							addAlarma(TipoAlarma.DIASTOLEMAX);

						}

					}

					if (mediconesDTO.getMaxSp02() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 3) {
						if (mediconesDTO.getMaxSp02() > umbralesMedicionSaludEB.getMax().doubleValue()) {

							addAlarma(TipoAlarma.SPO2MAX);

						}

					}
					if (mediconesDTO.getMinSp02() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 3) {
						if (mediconesDTO.getMinSp02() < umbralesMedicionSaludEB.getMin().doubleValue()) {

							addAlarma(TipoAlarma.SPO2MIN);

						}

					}

					if (mediconesDTO.getMaxHealthHeartbeat() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 3) {
						if (mediconesDTO.getMaxHealthHeartbeat() > umbralesMedicionSaludEB.getMax().doubleValue()) {

							addAlarma(TipoAlarma.HEARTBEATMAX);

						}

					}
					if (mediconesDTO.getMinHealthHeartbeat() != null
							&& umbralesMedicionSaludEB.getIdTipoMedicion().getIdTipoMedicion() == 3) {
						if (mediconesDTO.getMinHealthHeartbeat() < umbralesMedicionSaludEB.getMin().doubleValue()) {

							addAlarma(TipoAlarma.HEARTBEATMIN);

						}

					}

				}

			}

		}

	}

	private double distFrom(double lat1, double lng1, double lat2, double lng2) {
		double earthRadius = 6371;
		// kilometers
		double dLat = Math.toRadians(lat2 - lat1);
		double dLng = Math.toRadians(lng2 - lng1);
		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(Math.toRadians(lat1))
				* Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2) * Math.sin(dLng / 2);
		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		float dist = (float) (earthRadius * c);
		return dist;
	}

	private void creaAlarmaUnidadNoAsignada() throws GeneraFirmaException {

		// _log.info(this.getClass().getName() + " - creaAlarmaUnidadNoAsignada");

		if (!_datos_log.getUnidad().getAsignada()) {
			if (_unidad_mapa.getAlarmaUnidadAsignada().getAlarma() == TipoAlarma.NINGUNA) {
				datosAlarma alarma = addAlarma(TipoAlarma.UNIDADNOASIGNADA, 0);
				_unidad_mapa.setAlarmaUnidadAsignada(alarma);
			}
		} else {
			if (_unidad_mapa.getAlarmaUnidadAsignada().getAlarma() == TipoAlarma.UNIDADNOASIGNADA) {
				addAlarma(TipoAlarma.ALARMAOK, _unidad_mapa.getAlarmaUnidadAsignada().getFirma());
				_unidad_mapa.setAlarmaUnidadAsignada(new datosAlarma());
			}
		}
	}

	private void creaAlarmaUnidadNoActiva() throws GeneraFirmaException {

		// _log.info(this.getClass().getName() + " - creaAlarmaUnidadNoActiva");

		if (!_datos_log.getUnidad().getActivo()) {
			if (_unidad_mapa.getAlarmaUnidadActiva().getAlarma() == TipoAlarma.NINGUNA) {
				datosAlarma alarma = addAlarma(TipoAlarma.UNIDADDEBAJA, 0);
				_unidad_mapa.setAlarmaUnidadActiva(alarma);
			}
		} else {
			if (_unidad_mapa.getAlarmaUnidadActiva().getAlarma() == TipoAlarma.UNIDADDEBAJA) {
				addAlarma(TipoAlarma.ALARMAOK, _unidad_mapa.getAlarmaUnidadActiva().getFirma());
				_unidad_mapa.setAlarmaUnidadActiva(new datosAlarma());
			}
		}
	}

	private void crearMensajeSalud() throws Exception {
		ZoneId zoneIdMad = ZoneId.of("Europe/Madrid");

		LocalDateTime localDate = LocalDateTime.now(zoneIdMad);

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");

		Integer diaActual = localDate.getDayOfWeek().getValue();
		String horaActual = formatter.format(localDate);

		_log.info("horaActual ************** " + horaActual);
		_log.info("diaActual ************** " + diaActual);

		// se busca si la unidad tiene alarmas de salud configuradas
		UnidadEB unidad = _unidad_bean.getUnidad(_unidad_mapa.getUnidad().getIdUnidad());

		List<UnidadSaludEB> unidadSaludEBs = unidad.getUnidadSaludSet();
		Calendar objCalendarAnterior = Calendar.getInstance();

		ZonedDateTime now = ZonedDateTime.now(ZoneId.of("Europe/Madrid"));
		ZonedDateTime horaTranmision = null;
		StringBuilder trama = new StringBuilder();

		for (UnidadSaludEB unidadSaludEB : unidadSaludEBs) {
			List<ActivaMedicionEB> activaMedicionList = unidadSaludEB.getActivaMedicionList();
			Integer total = 0;
			for (ActivaMedicionEB activaMedicionEB : activaMedicionList) {
				if (activaMedicionEB.getActivo()) {
					total += Integer.valueOf(activaMedicionEB.getIdTipoMedicion().getValorTrama());

				}

			}
			trama.append("A0").append(Integer.toHexString(total).toUpperCase()).append(unidad.getMaskAddress());
			if (unidadSaludEB.getControlMedico()) {

				if (unidadSaludEB.getIdTipoHorario().getTipo().equalsIgnoreCase("intervalo")) {

					List<HorarioLecturaEB> horarioLecturaEBs = unidadSaludEB.getHorarioLecturaList();
					Double intervalo = 0.0;
					for (HorarioLecturaEB horarioLecturaEB : horarioLecturaEBs) {
						intervalo = horarioLecturaEB.getIntervalo();
					}
					if (horaTranmision == null) {
						horaTranmision = now.plusHours(intervalo.longValue());
					}
					if (horaTranmision != null && horaTranmision.isAfter(now)) {
						if (addMensaje(unidad, trama.toString()) != null) {
							_log.info("inserto el mensaje en unidadesmensajes ************** ");
							horaTranmision = null;
						}
					}

				}
				if (unidadSaludEB.getIdTipoHorario().getTipo().equalsIgnoreCase("semanal")) {

					_log.info("trama a guardar  ************** " + trama.toString());

					List<DiasLecturaEB> diasLecturaEBs = unidadSaludEB.getDiasLecturaList();
					List<HorarioLecturaEB> horarioLecturaEBs = unidadSaludEB.getHorarioLecturaList();
					Integer horarioEnvio = 0;
					for (DiasLecturaEB diasLecturaEB : diasLecturaEBs) {
						if (diasLecturaEB.getLunes() && diaActual == 1) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getMartes() && diaActual == 2) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getMiercoles() && diaActual == 3) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getJueves() && diaActual == 4) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getViernes() && diaActual == 5) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getSabado() && diaActual == 6) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes
								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
						if (diasLecturaEB.getDomingo() && diaActual == 7) {

							if (isHoraEnvio(horarioLecturaEBs)) {
								// inserto el mensaje en unidadesmensajes

								_log.info("va insertar  el mensaje en unidadesmensajes ************** ");

								if (addMensaje(unidad, trama.toString()) != null) {
									_log.info("inserto el mensaje en unidadesmensajes ************** ");
									horarioEnvio++;
								}
								_log.info("horarioEnvio ************** " + horarioEnvio);
								if (horarioEnvio >= mensajeEnviado.size()) {
									mensajeEnviado.clear();
								}

							}
						}
					}

				}

			}

		}

	}

	private MensajeUnico addMensaje(UnidadEB unidad, String mensaje) throws Exception {
		MensajeUnico mensajeUnico = new MensajeUnico();
		mensajeUnico.setHelmetId(unidad.getIdUnidad());
		mensajeUnico.setHelmetSerial(unidad.getNumeroSerie());
		mensajeUnico.setMessageType("salud");
		mensajeUnico.setStatus(true);
		mensajeUnico.setText(mensaje);

		return _envio_mensaje.addMensaje(mensajeUnico);
	}

	private Boolean isHoraEnvio(List<HorarioLecturaEB> horarioLecturaEBs) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
		Boolean result = Boolean.FALSE;
		ZoneId zoneIdMad = ZoneId.of("Europe/Madrid");

		LocalDateTime localDate = LocalDateTime.now(zoneIdMad);

		LocalTime now = LocalTime.now(zoneIdMad);
//		LocalTime limit = LocalTime.parse( "15:30" );
//		Boolean isLate = now.isAfter( limit );

		List<HorarioLecturaEB> sortedHorarioLecturaEBs = horarioLecturaEBs.stream()
				.sorted(Comparator.comparing(HorarioLecturaEB::getHora)).collect(Collectors.toList());

		Integer diaActual = localDate.getDayOfWeek().getValue();
		String horaActual = formatter.format(localDate);

		for (HorarioLecturaEB horarioLecturaEB : sortedHorarioLecturaEBs) {
			_log.info("horarioLecturaEB id ************** " + horarioLecturaEB.getIdHorarioLectura());

//			LocalDateTime ac = LocalDate.parse(horaActual, formatter).atStartOfDay();	
			LocalTime limit = LocalTime.parse(dateFormat.format(horarioLecturaEB.getHora()));
//			LocalDateTime limit = LocalDateTime.parse(dateFormat.format(horarioLecturaEB.getHora()), formatter);
			_log.info("ac ************** " + now);
			_log.info("limit ************** " + limit);
			_log.info("limit.isAfter(now) ************** " + limit.isAfter(now));
			_log.info("horaActual ************** " + mensajeEnviado.isEmpty());
			if (mensajeEnviado.isEmpty()) {
				if (limit.isAfter(now)) {
					mensajeEnviado.put(horarioLecturaEB.getIdHorarioLectura(), Boolean.TRUE);
					result = Boolean.TRUE;
					_log.info("va a retornar " + result);
					return result;
				}
			} else {

				if (limit.isAfter(now) && mensajeEnviado.get(horarioLecturaEB.getIdHorarioLectura())) {
					mensajeEnviado.put(horarioLecturaEB.getIdHorarioLectura(), Boolean.TRUE);
					result = Boolean.TRUE;
					return result;
				}

			}

		}

		return result;

	}

	private void gestionaMensaje() throws UnidadMensajeNoEncontradoException, GetUnidadMensajeByIdException,
			GetMensajesPurgarException, PurgarMensajeException, UnidadMensajeSetLeidoException, Exception {

		// _log.info(this.getClass().getName() + " - gestionaMensaje");

		UnidadMensajeEB um_eb = null;

		EstadoMensajeEntrada estado_actual = _datos_log.getDatos().getMensaje();

		/*
		 * _log.info("firma: " + _datos_log.getLog().getFirma() + "\n" + "leido: " +
		 * _datos_log.getLog().getLeido() + "\n" + "mensaje: " +
		 * _datos_log.getLog().getMensaje() + "\n" + "respuesta: " +
		 * _datos_log.getLog().getRespuesta() + "\n" + "respuesta_sms: " +
		 * _datos_log.getLog().getRespuesta_sms() + "\n" + "estado actual: " +
		 * estado_actual);
		 */

		_log.info(estado_actual);

		_unidad_mapa.setTextoMensaje("");
		_unidad_mapa.getDatosMensajeWS().setEstadoMensaje(estado_actual);
		um_eb = _um_bean.getMensajeUnidad(_unidad_mapa.getUnidad().getIdUnidad());

		// comprobar si exiten mesajes configurado
		UnidadEB unidad = _unidad_bean.getUnidad(_unidad_mapa.getUnidad().getIdUnidad());
		_log.info("getUnidadSaludSet tama;o *-*-*-*-*-*-*-*-* " + unidad.getUnidadSaludSet().size());

		List<UnidadSaludEB> unidadSaludEBs = unidad.getUnidadSaludSet();
		for (UnidadSaludEB unidadSaludEB : unidadSaludEBs) {
			List<DiasLecturaEB> diasLecturaEBs = unidadSaludEB.getDiasLecturaList();
			for (DiasLecturaEB diasLecturaEB : diasLecturaEBs) {
				_log.info("diasLecturaEB " + diasLecturaEB.getLunes());
			}
		}
		try {
			switch (estado_actual) {
			case Libre:

				_unidad_mapa.getDatosMensajeWS().setIdUnidadMensaje(0);
				if (um_eb != null) {
					creaMensaje(um_eb);
				}
				break;
			case Ocupado:
				break;
			case Leido: // Lanza Alarma MENSAJELEIDO

				if (_unidad_mapa.getDatosMensajeWS().getEstadoMensajeAnterior() == EstadoMensajeEntrada.Ocupado
						&& estado_actual == EstadoMensajeEntrada.Leido && um_eb != null) {
					_um_bean.setLeido(_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje());
					_unidad_mapa.getDatosMensajeWS()
							.setIdUnidadMensaje(_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje());

					addAlarma(TipoAlarma.MENSAJELEIDO);

					// pw.println(_unidad_mapa.toString());
					// fichero.flush();
				}
				break;
			case Configuracion:

				if (_unidad_mapa.getDatosMensajeWS().getEstadoMensajeAnterior() != EstadoMensajeEntrada.Configuracion
						&& um_eb != null) {
					_um_bean.setLeido(_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje());
					_unidad_mapa.getDatosMensajeWS()
							.setIdUnidadMensaje(_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje());
				}
				break;
			case Apagado: // Lanza Alarma CASCOAPAGADO

				_unidad_mapa.setTextoMensaje(creaTextoMensaje(_globales.getGlobales().APAGADO,
						EstadoMensajeSalida.Apagado.getEstadoMensage()));
				// _unidad_mapa.getUnidad().setCascoApagado(TipoAlarma.CASCOAPAGADO);
				addAlarma(TipoAlarma.CASCOAPAGADO);
				break;
			case Backdoor:
			case Lampisteria:
				_um_bean.purgarMensajesSMS(_unidad_mapa.getUnidad().getIdUnidad());

				if (_datos_log.getDatos().getMensaje() == EstadoMensajeEntrada.Backdoor) {
					um_eb = _um_bean.getMensajeUnidad(_unidad_mapa.getUnidad().getIdUnidad());
					if (um_eb != null) {
						creaMensaje(um_eb);
					}
					addAlarma(TipoAlarma.BACKDOOR);
				} else {
					_unidad_mapa.setTextoMensaje(creaTextoMensaje(_globales.getGlobales().APAGADO,
							EstadoMensajeSalida.Apagado.getEstadoMensage()));
					// _unidad_mapa.getUnidad().setCascoApagado(TipoAlarma.LAMPISTERIA);
					addAlarma(TipoAlarma.LAMPISTERIA);
				}
				break;
			case ApagadoSeguro:
				if (_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje() > 0) {
					_um_bean.setLeido(_unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje());
					_unidad_mapa.getDatosMensajeWS().setIdUnidadMensaje(0);
				}
				_unidad_mapa.setTextoMensaje(creaTextoMensaje(_globales.getGlobales().APAGADO_SEGURO,
						EstadoMensajeSalida.Apagado.getEstadoMensage()));
				// _unidad_mapa.getUnidad().setCascoApagado(TipoAlarma.BACKDOOR);
				addAlarma(TipoAlarma.APAGADOSEGURO);
				break;
			case MensajeOperario:
				_unidad_mapa.setTextoMensaje(creaTextoMensaje(_globales.getGlobales().APAGADO,
						EstadoMensajeSalida.Apagado.getEstadoMensage()));
				// _unidad_mapa.getUnidad().setCascoApagado(TipoAlarma.CASCOAPAGADO);
				addAlarma(TipoAlarma.MENSAJEOPERARIO);
				break;
			case EntregaSalud:
				// se debe actulizar la tabla unidad mensajes campo esdomensajeid = 2 leido
				_um_bean.setEntregdoSalud(um_eb.getIdUnidadMensaje());
			case ConfirmacionSalud:
				// Se debe actualizar el ultimo mensaje con estomensajeid = 2 el campo
				// confirmacion = true
				
				_um_bean.setConfirmacionSalud(um_eb.getIdUnidadMensaje());
				
			}
			_unidad_mapa.getDatosMensajeWS().setEstadoMensajeAnterior(estado_actual);
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void creaMensaje(UnidadMensajeEB um_eb) {

		// _log.info(this.getClass().getName() + " - creaMensaje");

		_unidad_mapa.setTextoMensaje(
				creaTextoMensaje(um_eb.getMensaje().getTexto(), um_eb.getMensaje().getIdTipoMensaje().intValue()));
		_unidad_mapa.getDatosMensajeWS().setIdUnidadMensaje(um_eb.getIdUnidadMensaje());
	}

	private String creaTextoMensaje(String texto, int tipo_mensaje) {

		// _log.info(this.getClass().getName() + " - creaTextoMensaje");

		String ret = texto;

		switch (tipo_mensaje) {
		case 2:
			ret = "#" + ret;
			break;
		case 1:
		case 4:
			int longitud = ret.length();
			String longitud_hex = Integer.toHexString(longitud);

			if (longitud_hex.length() == 1)
				longitud_hex = "0" + longitud_hex;
			ret = "#" + longitud_hex.toUpperCase() + "#" + ret;
			break;
		case 5:
			ret = "#$" + ret;
			break;
		}
		return ret;
	}

	private datosAlarma addAlarma(TipoAlarma alarma, long firma_anterior) throws GeneraFirmaException {

		_log.info(this.getClass().getName() + " - addAlarma - " + alarma);

		datosAlarma ret = null;
		synchronized (block) {
			ret = new datosAlarma();
			ret.setAlarma(alarma);
			if (alarma != TipoAlarma.NINGUNA) {
				long firma = ServidorUtil.generaFirmaLong();
				ret.setFirma(firma);
				ret.setCancelaAlarma(firma_anterior);
				ret.setIdLog(_datos_log.getLog().getIdLog());
				_unidad_mapa.getAlarmas().put(firma, ret);

				// TODO ADDED BY SANJAY HCL TO SAVE alarms in DB
				try {
					_event_bean.addAlarmaEvento(datos, _datos_log, _unidad_mapa, ret);
				} catch (Exception ex) {
					// TODO Auto-generated catch block
					ex.printStackTrace();
					// throw ex;
				}
				try {
					Thread.sleep(2);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}
		// _unidad_mapa.setCurrAlarm(ret);
		return ret;
	}

	@EJB
	private IEventBean _event_bean;

	private void addAlarma(TipoAlarma alarma) throws GeneraFirmaException {
		// _log.info(this.getClass().getName() + " - addAlarma");

		addAlarma(alarma, 0);
	}

}
