package com.angelhelmet.server.negocio.UnidadMapaBuilder;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IApBean;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IMapaBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaDiferidoBean;
import com.angelhelmet.server.negocio.interfaces.pool.IMapaObjectPoolBean;
import com.angelhelmet.server.persistencia.ApEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.util.ModoFuncionamiento;

@Singleton
@Startup
public class UnidadMapaDiferidoBean extends UnidadMapa implements IUnidadMapaDiferidoBean
{
	private datosLog _datos_log;
	private Logger _log;

	@EJB
	private IUnidadBean _unidad_bean;
	@EJB
	private IAlarmaBean _alarma_bean;
	@EJB
	private IApBean _ap_bean;
	@EJB
	private IMapaBean _mapa_bean;
	@EJB
	private ILogBean _log_bean;
	@EJB
	private IMapaObjectPoolBean _pool;

	@PostConstruct
	private void startup()
	{
		_log = LogManager.getLogger(UnidadMapaDiferidoBean.class);
		//_log.info("UnidadMapaDiferido: Singleton inited!");
	}

	public IMapaObjectPoolBean getPool()
	{
		return _pool;
	}

	public datosLog creaDatosLog(long id_log)
	{
		_datos_log = new datosLog();
		try
		{
			_datos_log.setLog(_log_bean.getLogById(id_log));
			UnidadEB unidad_eb = new UnidadEB();
			unidad_eb = _unidad_bean.getUnidad(_datos_log.getLog().getIdUnidad());
			_datos_log.setDatos(_log_bean.parseLogEB2DatosComunicacion(_datos_log.getLog(), unidad_eb.getNumeroSerie()));
			defineDatosLog();
		}
		catch (Exception ex)
		{
			_datos_log = null;
			ex.printStackTrace();
		}
		return _datos_log;
	}

	private void defineDatosLog() throws Exception
	{
		defineUnidad();
		defineMapa();
		defineAlarma();
	}

	private void defineUnidad() throws Exception
	{
		_datos_log.setUnidad(_unidad_bean.getUnidad(_datos_log.getDatos().getSerie()));
	}

	private void defineAlarma() throws Exception
	{
		_datos_log.setAlarma(_alarma_bean.getAlarma(_datos_log.getDatos().getAlarma()));
	}

	private void defineMapa() throws Exception
	{
		ApEB ap_eb = new ApEB();
		
		if (_datos_log.getDatos().getModo() == ModoFuncionamiento.INTERIOR)
		{
			ap_eb = _ap_bean.getAp(_datos_log.getDatos().getMac1());
			_datos_log.setMapa(ap_eb.getMapa());
		}
		else
		{
			_datos_log.setMapa(_mapa_bean.getMapa(_datos_log.getDatos().getLatitud(), _datos_log.getDatos().getLongitud()));
		}
	}

}
