package com.angelhelmet.server.negocio.kalman;

import javax.ejb.Stateless;

import com.angelhelmet.server.negocio.interfaces.kalman.IKalmanParametros;

@Stateless
public class KalmanPotencia implements IKalmanParametros
{

	private double _ruido_r;
	private double _ruido_q;
	private double _estimacion_inicial;

	public KalmanPotencia()
	{
		configura();
	}

	@Override
	public double getRuidoR()
	{
		return this._ruido_r;
	}

	@Override
	public double getRuidoQ()
	{
		return this._ruido_q;
	}

	@Override
	public double getEstimacionInicial()
	{
		return this._estimacion_inicial;
	}

	private void configura()
	{
		this._ruido_r = 2.95;
		this._ruido_q = 0.65;
		this._estimacion_inicial = 2;
	}

}
