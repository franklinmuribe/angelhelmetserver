package com.angelhelmet.server.negocio.kalman.excepciones;

public class CalculaKalmanException extends Exception
{
	private static final long serialVersionUID = -1381533282823961350L;
	
	public CalculaKalmanException() {
	}
	
	public CalculaKalmanException(String s) {
		super(s);
	}
	
	public CalculaKalmanException(String s, Exception e) {
		super(s, e);
	}

}
