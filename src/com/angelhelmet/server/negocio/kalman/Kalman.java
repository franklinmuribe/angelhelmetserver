package com.angelhelmet.server.negocio.kalman;

import com.angelhelmet.server.negocio.interfaces.kalman.IKalmanParametros;
import com.angelhelmet.server.negocio.kalman.excepciones.CalculaKalmanException;

public class Kalman
{
	public static KalmanOut Calcula(KalmanIn entrada, IKalmanParametros parametros) throws CalculaKalmanException
	{
		KalmanOut ret = new KalmanOut();

		double x = entrada.getValorInicial();
		double k = 0;
		double p = 0;

		try
		{
			if (entrada.getEstimacionInicial() == 0)
			{
				p = parametros.getEstimacionInicial() + parametros.getRuidoQ();
				k = parametros.getEstimacionInicial() / (parametros.getEstimacionInicial() + parametros.getRuidoR());
			}
			else
			{
				p = entrada.getEstimacionInicial() + parametros.getRuidoQ();
				k = p / (p + parametros.getRuidoR());
			}

			x = x + k * (entrada.getPotencia() - x);
			p = (1 - k) * p;

			ret.setValorFinal(x);
			ret.setEstimacionFinal(p);
		}
		catch (Exception ex)
		{			
			throw new CalculaKalmanException("No se puede calcular Kalman", ex);
		}
		
		return ret;
	}
}
