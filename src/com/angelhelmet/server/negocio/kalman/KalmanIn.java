package com.angelhelmet.server.negocio.kalman;

public class KalmanIn
{
	private double _estimacion_inicial;
	private double _valor_inicial;
	private double _potencia;

	public KalmanIn()
	{
	}

	public double getEstimacionInicial()
	{
		return _estimacion_inicial;
	}

	public void setEstimacionInicial(double _estimacion_inicial)
	{
		this._estimacion_inicial = _estimacion_inicial;
	}

	public double getValorInicial()
	{
		return _valor_inicial;
	}

	public void setValorInicial(double _valor_inicial)
	{
		this._valor_inicial = _valor_inicial;
	}

	public double getPotencia()
	{
		return _potencia;
	}

	public void setPotencia(double _potencia)
	{
		this._potencia = _potencia;
	}

}
