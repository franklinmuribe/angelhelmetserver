package com.angelhelmet.server.negocio.kalman;

public class KalmanOut
{
	private double _estimacion_final;
	private double _valor_final;

	public KalmanOut()
	{
	}

	public double getEstimacionFinal()
	{
		return _estimacion_final;
	}

	public void setEstimacionFinal(double _estimacion_final)
	{
		this._estimacion_final = _estimacion_final;
	}

	public double getValorFinal()
	{
		return _valor_final;
	}

	public void setValorFinal(double _valor_final)
	{
		this._valor_final = _valor_final;
	}

	@Override
	public String toString()
	{
		return "KalmanOut (EstimacionFinal: " + this._estimacion_final + ", ValorFinal: " + this._valor_final + ")";
	}
}
