package com.angelhelmet.server.negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.dao.interfaces.IZonaAlarmaDAO;
import com.angelhelmet.server.datos.datosPermisoZona;
import com.angelhelmet.server.negocio.interfaces.IZonaAlarmaBean;

@Stateless
public class ZonaAlarmaBean implements IZonaAlarmaBean
{
	@EJB
	IZonaAlarmaDAO _zona_alarma_dao;

	@Override
	public List<datosPermisoZona> getPermisosZona(int id_zona, int id_unidad) throws GetPermisosZonaException, Exception
	{
		List<datosPermisoZona> ret = null;
		ret = _zona_alarma_dao.getPermisosZona(id_zona, id_unidad);
		return ret;
	}

}
