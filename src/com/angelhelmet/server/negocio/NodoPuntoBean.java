package com.angelhelmet.server.negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.dao.interfaces.INodoPuntoDAO;
import com.angelhelmet.server.negocio.interfaces.INodoPuntoBean;
import com.angelhelmet.server.persistencia.NodoPuntoEB;

@Stateless
public class NodoPuntoBean implements INodoPuntoBean
{

	@EJB
	private INodoPuntoDAO _nodo_punto_dao;

	public NodoPuntoBean()
	{
	}

	@Override
	public List<NodoPuntoEB> getNodosProyeccion(int id_ap1, int id_ap2) throws GetNodosProyeccionException, Exception
	{
		List<NodoPuntoEB> ret = null;
		ret = _nodo_punto_dao.getNodosProyeccion(id_ap1, id_ap2);
		return ret;
	}

}
