package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IWalkietalkieDAO;
import com.angelhelmet.server.datos.datosWalkietalkieAudio;
import com.angelhelmet.server.datos.datosWalkietalkieImei;
import com.angelhelmet.server.negocio.interfaces.IWalkietalkie;

@Stateless
public class WalkietalkieBean implements IWalkietalkie {

	@EJB
	private IWalkietalkieDAO _walkietalkie_dao;

	@Override
	public datosWalkietalkieImei obtenerImeiWT(String imei) throws Exception {

		datosWalkietalkieImei ret = null;
		ret = _walkietalkie_dao.obtenerImeiWT(imei);
		return ret;

	}

	@Override
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje) throws Exception {
		datosWalkietalkieAudio ret = null;
		ret = _walkietalkie_dao.obtenerAudioWT(imei, idEstadoMensaje);
		return ret;
	}

	@Override
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje, Long firmaCasco) throws Exception {
		datosWalkietalkieAudio ret = null;
		ret = _walkietalkie_dao.obtenerAudioWT(imei, idEstadoMensaje, firmaCasco);
		return ret;
	}

	@Override
	public Long actualizarEstadoWT(Long idMensajeHelmet, Long idEstadoMensaje, Long firma) throws Exception {

		return _walkietalkie_dao.actualizarEstadoWT(idMensajeHelmet, idEstadoMensaje, firma);

	}

}
