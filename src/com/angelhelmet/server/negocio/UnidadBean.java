package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.UnidadNoEncontradaException;
import com.angelhelmet.server.dao.interfaces.IUnidadDAO;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.persistencia.UnidadEB;

@Stateless
public class UnidadBean implements IUnidadBean
{
	@EJB
	private IUnidadDAO _unidad;

	@Override
	public UnidadEB getUnidad(String serie) throws UnidadNoEncontradaException, Exception
	{
		UnidadEB ret = null;
		ret = _unidad.getUnidad(serie);
		return ret;
	}

	@Override
	public UnidadEB getUnidad(int id_unidad) throws UnidadNoEncontradaException, Exception
	{
		UnidadEB ret = null;
		ret = _unidad.getUnidad(id_unidad);
		return ret;
	}
}
