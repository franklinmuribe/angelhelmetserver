package com.angelhelmet.server.negocio.mbean;

// https://fascynacja.wordpress.com/2013/08/27/jboss-7-x-retrieve-datasource-properties-username-database-name-password-at-runtime-using-jmx/

import java.util.Properties;
import java.util.Set;

import javax.ejb.Stateless;
import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServer;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import com.angelhelmet.server.negocio.interfaces.mbean.IConfiguracionDataSourceMBean;

@Stateless
public class ConfiguracionDataSourceMBean implements IConfiguracionDataSourceMBean
{
	private final String _ds_name = "\"java:jboss/datasources/rtls_pas_bolivia_pool\"";

	@Override
	public Properties getConfiguracionDataSource() throws MalformedObjectNameException, Exception
	{
		Properties ret = new Properties();

		MBeanServer server = java.lang.management.ManagementFactory.getPlatformMBeanServer();

		// create jmx name for local datasources name eg. (1a)
		final ObjectName localDSJMXName = new ObjectName("jboss.as:subsystem=datasources,data-source=" + _ds_name);

		// create jmx name for XA datasources name eg. (1b)
		// "jboss.as:subsystem=datasources,xa-data-source=PostgresXADS"
		final ObjectName xaDSJMXName = new ObjectName("jboss.as:subsystem=datasources,xa-data-source=" + _ds_name);

		// check wheter given datasource is LOCAL or XA transaction type.
		// (2)

		// is this datasource registered under local mbean jmx name (3a)
		boolean isLocalDS = server.isRegistered(localDSJMXName);

		// is this datasource registered under XA mbean jmx name (3b)
		boolean isXADDS = server.isRegistered(xaDSJMXName);

		if (isLocalDS)
		{
			// read and put into properties the "user-name" attribute (4a)
			String userNamePropertyName = "userName";
			readAndSaveAttribute(ret, server, localDSJMXName, userNamePropertyName);

			// read and put into properties "the password" attribute (4b)
			String passwordPropertyName = "password";
			readAndSaveAttribute(ret, server, localDSJMXName, passwordPropertyName);

			String connectionUrlPropertyName = "connectionUrl";
			readAndSaveAttribute(ret, server, localDSJMXName, connectionUrlPropertyName);

		}
		else if (isXADDS)
		{

			// read and put into properties the "user-name" attribute
			String userNamePropertyName = "userName";
			readAndSaveAttribute(ret, server, xaDSJMXName, userNamePropertyName);

			// read and put into properties the "password" attribute
			String passwordPropertyName = "password";
			readAndSaveAttribute(ret, server, xaDSJMXName, passwordPropertyName);

			// additionally we read out the xa-properties (5)
			String canonicalName = xaDSJMXName.getCanonicalName();
			// create jmx query for xa-properties (6)
			ObjectName jmxQuery = new ObjectName(canonicalName + ",xa-datasource-properties=*");

			// get the results matching given filter.
			final Set<ObjectInstance> xaPropertiesMBeans = server.queryMBeans(jmxQuery, null);

			// iterate over mbeans and retrieve information about
			// xa-properties
			for (final ObjectInstance mBean : xaPropertiesMBeans)
			{

				ObjectName objectName = mBean.getObjectName();

				// get the name of given xa-property e.g.: ServerName (7)
				String attributeKey = objectName.getKeyProperty("xa-datasource-properties");

				// get the value of given xa-property e.g.: localhost
				Object attributeValue = server.getAttribute(objectName, "value");

				// save the attribute value under correct key
				ret.put(attributeKey, attributeValue);
			}

		}
		else
		{
			// datasource is not registered in jmx, or is registered under
			// another name
		}

		return ret;
	}

	private void readAndSaveAttribute(Properties dsProperties, MBeanServer server, final ObjectName dsJMXName,
			String propertyName) throws AttributeNotFoundException, MBeanException, InstanceNotFoundException,
			ReflectionException
	{
		// read the property
		String userName = (String) server.getAttribute(dsJMXName, propertyName);
		// save the property into properties
		dsProperties.put(propertyName, userName);
	}

}
