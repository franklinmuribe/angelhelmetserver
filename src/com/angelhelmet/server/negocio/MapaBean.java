package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.interfaces.IMapaDAO;
import com.angelhelmet.server.negocio.interfaces.IMapaBean;
import com.angelhelmet.server.persistencia.MapaEB;

@Stateless
public class MapaBean implements IMapaBean
{
	@EJB
	private IMapaDAO _mapa_dao;

	@Override
	public MapaEB getMapa(int id) throws MapaNoEncontradoException, Exception
	{
		MapaEB ret = null;
		ret = _mapa_dao.getMapa(id);
		return ret;
	}

	@Override
	public MapaEB getMapa(double latitud, double longitud) throws MapaNoEncontradoException, Exception
	{
		MapaEB ret = null;
		ret = _mapa_dao.getMapa(latitud, longitud);
		return ret;
	}

}
