package com.angelhelmet.server.negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IMensajeDAO;
import com.angelhelmet.server.dao.interfaces.IUnidadDAO;
import com.angelhelmet.server.dao.interfaces.IUnidadMensajeDAO;
import com.angelhelmet.server.negocio.interfaces.IMensajeBean;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.util.TipoMensaje;

@Stateless
//@Deprecated
public class MensajeBean implements IMensajeBean {
	@EJB
	private IMensajeDAO _mensaje_dao;

	@EJB
	private IUnidadMensajeDAO _unidad_mensaje_dao;

	@EJB
	private IUnidadDAO _unidad_dao;

	@Override
	public Long getIdFirmaCasco() throws Exception {
		return _mensaje_dao.getIdFirmaCasco();
	}

	@Override
	public String getMensajeByCasco(Long firma) throws Exception {
		return _mensaje_dao.getMensajeByCasco(firma);
	}
	@Override
	public Long getMensajeIdLogByCasco(Long firma) throws Exception {
		return _mensaje_dao.getMensajeIdLogByCasco(firma);
	}

	@Override
	public long addMensaje(String texto, TipoMensaje tipo, Integer id_unidad, String serie, Integer sessionId,
			Long unidadUmbrale, Long idFirmaCasco) throws Exception {
		long ret = 0;
		MensajeEB mensaje = _mensaje_dao.addMensaje(texto, tipo, sessionId, unidadUmbrale, idFirmaCasco);
		if (mensaje != null) {
			ret = mensaje.getIdMensaje();
			if (id_unidad != null) {
				_unidad_mensaje_dao.addUnidadMensaje(mensaje, id_unidad);
			} else {
				UnidadEB unidad = _unidad_dao.getUnidad(serie);
				if (unidad != null) {
					_unidad_mensaje_dao.addUnidadMensaje(mensaje, unidad.getIdUnidad());
				}
			}

		}
		return ret;
	}

	@Override
	public long addMensaje(String texto, TipoMensaje tipo, Integer id_unidad, String serie, Long idFirmaCasco)
			throws Exception {
		long ret = 0;
		MensajeEB mensaje = _mensaje_dao.addMensajeMasivos(texto, tipo, null, idFirmaCasco);
		if (mensaje != null) {
			ret = mensaje.getIdMensaje();
			if (id_unidad != null) {
				_unidad_mensaje_dao.addUnidadMensaje(mensaje, id_unidad);
			} else {
				UnidadEB unidad = _unidad_dao.getUnidad(serie);
				if (unidad != null) {
					_unidad_mensaje_dao.addUnidadMensaje(mensaje, unidad.getIdUnidad());
				}
			}

		}
		return ret;
	}

	public void addMensajesMasivos(String texto, TipoMensaje tipo, List<Integer> lista, MensajeMasivo mensajes,
			Long idFirmaCasco) throws Exception {
		MensajeEB mensaje = _mensaje_dao.addMensajeMasivos(texto, tipo, mensajes, idFirmaCasco);
		if (mensaje != null) {
			for (Integer item : lista) {
				_unidad_mensaje_dao.addUnidadMensaje(mensaje, item);
			}
		}
	}

	public MensajeEB getMensaje(int id_mensaje) {
		MensajeEB ret = null;
		try {
			ret = _mensaje_dao.getMensaje(id_mensaje);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		return ret;
	}

	public MensajeEB getMensajeConfiguracion(String serie) {
		MensajeEB ret = null;
		try {
			ret = _mensaje_dao.getMensajeConfiguracion(serie);
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
			ex.printStackTrace();
		}
		return ret;
	}

}
