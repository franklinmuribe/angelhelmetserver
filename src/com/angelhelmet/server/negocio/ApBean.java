package com.angelhelmet.server.negocio;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsAdyacentesNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.dao.interfaces.IApDAO;
import com.angelhelmet.server.negocio.interfaces.IApBean;
import com.angelhelmet.server.persistencia.ApEB;

@Stateless
public class ApBean implements IApBean
{
	@EJB
	private IApDAO _ap;

	@Override
	public ApEB getAp(String mac) throws ApNoEncontradoException, Exception
	{
		ApEB ret = null;
		ret = _ap.getAp(mac);
		return ret;
	}
	
	@Override
	public ApEB getAp(int id_ap) throws ApNoEncontradoException, Exception
	{
		ApEB ret = null;
		ret = _ap.getAp(id_ap);
		return ret;
	}

	@Override
	public List<ApEB> getAdyacentes(int id_ap) throws ApsAdyacentesNoEncontradosException, Exception
	{
		List<ApEB> ret = null;
		ret = _ap.getAdyacentes(id_ap);
		return ret;
	}

	@Override
	public List<ApEB> getApsMapa(int id_mapa) throws ApsMapaNoEncontradosException, Exception
	{
		List<ApEB> ret = null;
		ret = _ap.getApsMapa(id_mapa);
		return ret;
	}
}
