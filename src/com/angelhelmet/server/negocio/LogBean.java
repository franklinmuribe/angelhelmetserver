package com.angelhelmet.server.negocio;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.dao.interfaces.ILogDAO;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IApBean;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IMapaBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.persistencia.ApEB;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.MapaEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.servlet.Server;
import com.angelhelmet.server.util.ModoFuncionamiento;
import com.angelhelmet.server.util.ServidorUtil;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Stateless
public class LogBean implements ILogBean {

	@EJB
	private ILogDAO _log_dao;
	@EJB
	private IUnidadBean _unidad_bean;
	@EJB
	private IAlarmaBean _alarma_bean;
	@EJB
	private IApBean _ap_bean;
	@EJB
	private IMapaBean _mapa_bean;

	private Logger _log;

	public LogBean() {
		_log = LogManager.getLogger(Server.class);
	}

	@Override
	public datosLog grabaLog(datosComunicacion datos, UnidadEB unidad) throws AlarmaNoEncontradaException,
			ApNoEncontradoException, MapaNoEncontradoException, ParseLogException, GrabaLogException, Exception {

		 _log.info(this.getClass().getName() + " - datosLog grabaLog");
		 
		 _log.info(" - datosLog grabaLog mensaje " + datos.getMensaje().getTipoMensage());

		datosLog ret = new datosLog();

		AlarmaEB alarma = _alarma_bean.getAlarma(datos.getAlarma());
		MapaEB mapa = null;
		if (datos.getModo() == ModoFuncionamiento.INTERIOR) {
			ApEB ap = _ap_bean.getAp(datos.getMac1());
			mapa = ap.getMapa();
			if (mapa == null)
				throw new MapaNoEncontradoException(
						"No se ha encontrado el mapa del ap: " + ap.getIdAp() + " mac: " + datos.getMac1());
		} else {
			mapa = _mapa_bean.getMapa(datos.getLatitud(), datos.getLongitud());
		}

		ret.setDatos(datos);
		ret.setUnidad(unidad);
		ret.setAlarma(alarma);
		ret.setMapa(mapa);

		ret.setLog(_log_dao.grabaLog(ret));
		return ret;
	}

	@Override
	@Deprecated
	public LogEB getUltimoLog(int id_unidad) {
		LogEB ret = null;
		try {
			ret = _log_dao.getUltimoLog(id_unidad);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	@Override
	public List<datosComunicacion> getLogsByFecha(Date inicio, Date fin, List<Integer> unidades, int id_mapa) {
		List<datosComunicacion> ret = new ArrayList<datosComunicacion>();
		boolean primero = true;
		String serie = "";

		try {
			long i = inicio.getTime();
			long f = fin.getTime();
			List<LogEB> lista = _log_dao.getLogsByFecha(new Timestamp(i), new Timestamp(f), unidades, id_mapa);

			for (LogEB item : lista) {
				if (primero) {
					UnidadEB unidad_eb = new UnidadEB();
					unidad_eb = _unidad_bean.getUnidad(item.getIdUnidad());
					serie = unidad_eb.getNumeroSerie();
					primero = false;
				}
				ret.add(parseLogEB2DatosComunicacion(item, serie));
			}
		} catch (Exception ex) {
			ret = null;
			ex.printStackTrace();
		}
		return ret;
	}

	@Override
	public LogEB getLogById(long id_log) {
		LogEB ret = null;
		try {
			ret = _log_dao.getLogById(id_log);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	@Override
	public LogEB getLogByFirma(Long firma) {
		LogEB ret = null;
		try {
			ret = _log_dao.getLogByFirma(firma);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	@Override
	public datosComunicacion parseLogEB2DatosComunicacion(LogEB log_eb, String serie) throws Exception {
		datosComunicacion ret = new datosComunicacion();

		ret.setAlarma(ServidorUtil.getTipoAlarma(log_eb.getIdAlarma() - 1));
		ret.setBateria(log_eb.getBateria());
		ret.setExactitud(log_eb.getExactitud());
		ret.setFecha(log_eb.getFecha());
		ret.setFechaLog(log_eb.getFechaLog());
		ret.setGas(log_eb.getGas());
		ret.setIdLog(log_eb.getIdLog());
		ret.setLatitud(log_eb.getLatitud());
		ret.setLatitudC(log_eb.getLatitudC());
		ret.setLongitud(log_eb.getLongitud());
		ret.setLongitudC(log_eb.getLongitudC());
		ret.setMac1(log_eb.getMac1());
		ret.setMac2(log_eb.getMac2());
		ret.setMac3(log_eb.getMac3());
		ret.setMac4(log_eb.getMac4());
		ret.setMensaje(ServidorUtil.getTipoMensaje(log_eb.getMensaje()));

		if (log_eb.getLatitud() == 0 && log_eb.getLongitud() == 0) {
			ret.setModo(ModoFuncionamiento.INTERIOR);
		} else {
			ret.setModo(ModoFuncionamiento.EXTERIOR);
		}

		ret.setRssi1(log_eb.getRssi1());
		ret.setRssi2(log_eb.getRssi2());
		ret.setRssi3(log_eb.getRssi3());
		ret.setRssi4(log_eb.getRssi4());
		ret.setSatelites(log_eb.getSatelites());
		ret.setSensorX(log_eb.getSensorX());
		ret.setSensorY(log_eb.getSensorY());
		ret.setSensorZ(log_eb.getSensorZ());
		ret.setTiempo(log_eb.getTiempo());

		ret.setSerie(serie);

		// Walkie Talkie
		ret.setCanalWt(log_eb.getCanalWt());
		ret.setModoWt(log_eb.getModoWt());
		// Salud
		ret.setHealthHeartbeat(log_eb.getHealthHeartbeat());
		ret.setHealthOxygenSp02(log_eb.getHealthOxygenSp02());
		ret.setHealthPressureDiastole(log_eb.getHealthPressureDiastole());
		ret.setHealthPressureSistole(log_eb.getHealthPressureSistole());
		ret.setHealthTemperature(log_eb.getHealthTemperature());

		return ret;
	}

}
