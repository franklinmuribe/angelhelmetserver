package com.angelhelmet.server.negocio;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.datos.datosGlobales;
import com.angelhelmet.server.negocio.UnidadMapaBuilder.UnidadMapaRealBean;
import com.angelhelmet.server.negocio.interfaces.IUsaGlobalesBean;

@Singleton
@Startup
public class UsaGlobalesBean implements IUsaGlobalesBean
{
	private datosGlobales globales;
	private Logger _log;

	@PostConstruct
	private void startup()
	{
		_log = LogManager.getLogger(UnidadMapaRealBean.class);
	//	_log.info("UsaGlobalesBean: Singleton inited!");

		this.globales = new datosGlobales();
	}

	public datosGlobales getGlobales()
	{
		return this.globales;
	}
}
