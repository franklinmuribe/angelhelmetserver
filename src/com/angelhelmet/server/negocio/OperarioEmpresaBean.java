package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.interfaces.IOperarioEmpresaDAO;
import com.angelhelmet.server.negocio.interfaces.IOperarioEmpresaBean;
import com.angelhelmet.server.persistencia.EmpresaEB;

@Stateless
@Deprecated
public class OperarioEmpresaBean implements IOperarioEmpresaBean
{
	@EJB
	private IOperarioEmpresaDAO _oe_dao;

	@Override
	public EmpresaEB getEmpresaPorOperario(int id_operario)
	{
		EmpresaEB ret = null;
		try
		{
			ret = _oe_dao.getEmpresaPorOperario(id_operario);
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

}
