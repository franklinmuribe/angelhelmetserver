package com.angelhelmet.server.negocio;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.angelhelmet.server.dao.excepciones.AddMensajeApagadoException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.dao.interfaces.IUnidadMensajeDAO;
import com.angelhelmet.server.negocio.interfaces.IUnidadMensajeBean;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadMensajeEB;

@Stateless
public class UnidadMensajeBean implements IUnidadMensajeBean
{

	@EJB
	private IUnidadMensajeDAO _um;

	@Override
	public UnidadMensajeEB getMensajeUnidad(int id_unidad) throws UnidadMensajeNoEncontradoException, Exception
	{
		UnidadMensajeEB ret = null;
		ret = _um.getMensajeUnidad(id_unidad);
		return ret;
	}

	@Override
	public boolean setLeido(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception
	{
		boolean ret = false;
		ret = _um.setLeido(id_unidadmensaje);
		return ret;
	}

	@Override
	public boolean purgarMensajesSMS(int id_unidad) throws GetMensajesPurgarException, PurgarMensajeException, Exception
	{
		boolean ret = false;
		ret = _um.purgarMensajesSMS(id_unidad);
		return ret;
	}
	
	@Override
	@Deprecated
	public boolean addMensajeApagadoUnidad(UnidadEB unidad, MensajeEB mensaje) throws AddMensajeApagadoException, Exception
	{
		boolean ret = false;
		ret = _um.addMensajeApagadoUnidad(unidad, mensaje);
		return ret;
	}

	@Override
	public boolean setEntregdoSalud(long id_unidadmensaje)
			throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception {
		Boolean ret = null;
		ret = _um.setEntregdoSalud(id_unidadmensaje);
		return ret;
	}
	@Override
	public boolean setConfirmacionSalud(long id_unidadmensaje)
			throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception {
		Boolean ret = null;
		ret = _um.setConfirmacionSalud(id_unidadmensaje);
		return ret;
	}
}
