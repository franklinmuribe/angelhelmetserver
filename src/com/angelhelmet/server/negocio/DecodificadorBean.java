package com.angelhelmet.server.negocio;

import java.math.BigInteger;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.servlet.http.HttpServletRequest;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.negocio.excepciones.IndiceFueraDelStringException;
import com.angelhelmet.server.negocio.excepciones.PeticionSinDatosException;
import com.angelhelmet.server.negocio.interfaces.IDecodificadorBean;
import com.angelhelmet.server.negocio.interfaces.IUsaGlobalesBean;
import com.angelhelmet.server.util.ModoFuncionamiento;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.excepciones.ConvertHexToStringException;
import com.angelhelmet.server.util.excepciones.GetFechaByStringException;
import com.angelhelmet.server.util.excepciones.ModoDeFuncionamientoException;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToDoubleException;
import com.angelhelmet.server.util.excepciones.ParseStringToFloatException;
import com.angelhelmet.server.util.excepciones.ParseStringToIntException;

@Stateless
public class DecodificadorBean implements IDecodificadorBean {
	@EJB
	private IUsaGlobalesBean _globales;
	private Logger _log;

	private String _id_unidad;
	private String _modo;
	private String _alarma;
	private String _tiempo;
	private String _gas;
	private String _sensor_x;
	private String _sensor_y;
	private String _sensor_z;
	private String _exactitud;
	private String _satelites;
	private String _latitud;
	private String _longitud;
	private String _mac_1;
	private String _rssi_1;
	private String _mac_2;
	private String _rssi_2;
	private String _mac_3;
	private String _rssi_3;
	private String _mac_4;
	private String _rssi_4;
	private String _fecha;
	private String _bateria;
	private String _mensaje;
	private String _temperatura;
	private String _volteo;
	private String _debug;
	private String _firma;
	private String _respuesta;
	private String _respuesta_sms;
	// Walkie Talkie
	private String _canal_wt;
	private String _modo_wt;
	// Salud
	private String _health_temperature;
	private String _health_pressure_sistole;
	private String _health_pressure_diastole;
	private String _health_oxygen_sp02;
	private String _health_heartbeat;

	private datosComunicacion datosComunicacionHelmet;

	public DecodificadorBean() {
		_log = LogManager.getLogger(DecodificadorBean.class);
	}

	@Override
	public datosComunicacion DecodificaGet(HttpServletRequest req)
			throws PeticionSinDatosException, ConvertHexToStringException, ParseStringToIntException,
			ParseStringToBigIntegerException, ParseStringToDoubleException, ParseStringToFloatException,
			IndiceFueraDelStringException, ModoDeFuncionamientoException, GetFechaByStringException, Exception {

		datosComunicacionHelmet = new datosComunicacion();
		String peticion = req.getParameter("DATA");

		try {
			if (!peticion.trim().equals("")) {
				_id_unidad = peticion.substring(0, 30);

				datosComunicacionHelmet.setSerie(ServidorUtil.convertHexToString(_id_unidad, "serie"));

				//_log.info("getSerie: " + datosComunicacionHelmet.getSerie());
				//_log.info("peticion#: " + peticion.length());

				if (!datosComunicacionHelmet.getSerie().equals(_globales.getGlobales().LIMPIA_POOL)
						&& !datosComunicacionHelmet.getSerie().equals(_globales.getGlobales().IMPRIME_POOL)) {
					asignaDatosPeticion(peticion);
					getDatosComunicacion();
				}

			} else {
				datosComunicacionHelmet = null;
				throw new PeticionSinDatosException(_globales.getGlobales().SIN_DATOS);
			}
		} catch (StringIndexOutOfBoundsException ex) {
			throw new IndiceFueraDelStringException(
					"La peticion no tiene la longitud adecuada: " + ex.getLocalizedMessage() + " / " + peticion);
		} catch (Exception ex) {
			throw ex;
		}
		return datosComunicacionHelmet;
	}

	@Override
	public String toString() {
		StringBuilder tramaHelmet = new StringBuilder();
		try {
			tramaHelmet.append("Unidad = " + _id_unidad + " => " + datosComunicacionHelmet.getSerie() + "\n");
			tramaHelmet.append("Modo = " + _modo + " => " + datosComunicacionHelmet.getModo()+ "\n");
			tramaHelmet.append("Alarma = " + _alarma + " => " + datosComunicacionHelmet.getAlarma() + "\n");
			tramaHelmet.append("Tiempo = " + _tiempo + " => " + datosComunicacionHelmet.getTiempo() + "\n");
			tramaHelmet.append("Gas = " + _gas + " => " + datosComunicacionHelmet.getGas() + "\n");
			tramaHelmet.append("SensorX = " + _sensor_x +" => " + datosComunicacionHelmet.getSensorX() +  "\n");
			tramaHelmet.append("SensorY = " + _sensor_y + " => " + datosComunicacionHelmet.getSensorY() + "\n");
			tramaHelmet.append("SensorZ = " + _sensor_z +" => " + datosComunicacionHelmet.getSensorZ() +  "\n");
			tramaHelmet.append("Precision = " + _exactitud +" => " + datosComunicacionHelmet.getExactitud()+  "\n");
			tramaHelmet.append("Satelites = " + _satelites + " => " + datosComunicacionHelmet.getSatelites() + "\n");
			tramaHelmet.append("Latitud = " + _latitud + " => " + datosComunicacionHelmet.getLatitud() + "\n");
			tramaHelmet.append("Longitud = " + _longitud + " => " + datosComunicacionHelmet.getLongitud() + "\n");
			
			tramaHelmet.append("Mac1 = " + _mac_1 + " => " + datosComunicacionHelmet.getMac1() + "\n");
			tramaHelmet.append("Rssi1 = " + _rssi_1 + " => " + datosComunicacionHelmet.getRssi1() + "\n");
			
			tramaHelmet.append("Mac2 = " + _mac_2 + " => " + datosComunicacionHelmet.getMac2() + "\n");
			tramaHelmet.append("Rssi2 = " + _rssi_2 + " => " + datosComunicacionHelmet.getRssi2() + "\n");
			
			tramaHelmet.append("Mac3 = " + _mac_3 + " => " + datosComunicacionHelmet.getMac3() + "\n");
			tramaHelmet.append("Rssi3 = " + _rssi_3 + " => " + datosComunicacionHelmet.getRssi3() + "\n");
			
			tramaHelmet.append("Mac4 = " + _mac_4 + " => " + datosComunicacionHelmet.getMac4() + "\n");
			tramaHelmet.append("Rssi4 = " + _rssi_4 + " => " + datosComunicacionHelmet.getRssi4() + "\n");
			
			tramaHelmet.append("Fecha = " + _fecha + " => " + datosComunicacionHelmet.getFecha() + "\n");
			tramaHelmet.append("Bateria = " + _bateria + " => " + datosComunicacionHelmet.getBateria() + "\n");
			tramaHelmet.append("Mensaje = " + _mensaje + " => " + datosComunicacionHelmet.getMensaje() + "\n");
			tramaHelmet.append("Debug = " + _debug +" => " + datosComunicacionHelmet.getDebug() +  "\n");
			tramaHelmet.append("Temperatura = " + _temperatura + " => " + datosComunicacionHelmet.getTemperatura() + "\n");
			tramaHelmet.append("Volteo = " + _volteo +" => " + datosComunicacionHelmet.isVolteo() +  "\n");
			tramaHelmet.append("Firma = " + _firma + " => " + datosComunicacionHelmet.getFirma() + "\n");
			tramaHelmet.append("Respuesta = " + _respuesta + " => " + datosComunicacionHelmet.getRespuesta() + "\n");
			
			tramaHelmet.append("Respuesta Sms = " + _respuesta_sms + " => " + datosComunicacionHelmet.getRespuestaSms() + "\n");
			
			tramaHelmet.append("Canal WT = " + _canal_wt + " => " + datosComunicacionHelmet.getCanalWt() + "\n");
			tramaHelmet.append("Modo WT = " + _modo_wt + " => " + datosComunicacionHelmet.getModoWt() + "\n");
			
			tramaHelmet.append("Health Temperature = " + _health_temperature + " => " + datosComunicacionHelmet.getHealthTemperature() + "\n");
			tramaHelmet.append("Health Pressure Sistole = " + _health_pressure_sistole +" => " + datosComunicacionHelmet.getHealthPressureSistole() +  "\n");
			tramaHelmet.append("Health Pressure Diastole = " + _health_pressure_diastole +" => " + datosComunicacionHelmet.getHealthPressureDiastole() + "\n");
			tramaHelmet.append("Health Oxygen Sp02 = " + _health_oxygen_sp02 +" => " + datosComunicacionHelmet.getHealthOxygenSp02() +  "\n");
			tramaHelmet.append("Health Heartbeat = " + _health_heartbeat + " => " + datosComunicacionHelmet.getHealthHeartbeat() + "\n");

		} catch (Exception ex) {
			_log.error("ERROR: en toString() del decodificador", ex);
			throw ex;
		}
		return tramaHelmet.toString();
	}

	private void asignaDatosPeticion(String peticion) {

		// _log.info(this.getClass().getName() + " - asignaDatosPeticion");

		_modo = peticion.substring(30, 32);
		_alarma = peticion.substring(32, 34);
		_tiempo = peticion.substring(34, 42);
		_gas = peticion.substring(42, 44);
		_sensor_x = peticion.substring(44, 48);
		_sensor_y = peticion.substring(48, 52);
		_sensor_z = peticion.substring(52, 56);
		_exactitud = peticion.substring(56, 68);
		_satelites = peticion.substring(68, 72);
		_latitud = peticion.substring(72, 104);
		_longitud = peticion.substring(104, 136);
		_mac_1 = peticion.substring(136, 148);
		_rssi_1 = peticion.substring(148, 150);
		_mac_2 = peticion.substring(150, 162);
		_rssi_2 = peticion.substring(162, 164);
		_mac_3 = peticion.substring(164, 176);
		_rssi_3 = peticion.substring(176, 178);
		_mac_4 = peticion.substring(178, 190);
		_rssi_4 = peticion.substring(190, 192);
		_fecha = peticion.substring(192, 204);
		_bateria = peticion.substring(204, 206);
		_mensaje = peticion.substring(206, 208);
		_temperatura = peticion.substring(208, 212);
		_volteo = peticion.substring(212, 214);
		if (peticion.length() <= 218) {
			_debug = peticion.substring(214);
			_firma = "";
			_respuesta = "00";
			_respuesta_sms = "";
		} else {
			_debug = "";
			_firma = peticion.substring(214, 222);
			_respuesta = peticion.substring(222, 224);
			_respuesta_sms = peticion.substring(224, 280);

			if (peticion.length() > 282) {
				_canal_wt = peticion.substring(280, 282);
				_modo_wt = peticion.substring(282, 284);
				_health_temperature = peticion.substring(284, 288);
				_health_pressure_sistole = peticion.substring(288, 290);
				_health_pressure_diastole = peticion.substring(290, 292);
				_health_oxygen_sp02 = peticion.substring(292, 294);
				_health_heartbeat = peticion.substring(294, 296);
				_log.info( " - asignaDatosPeticion _health_heartbeat " + _health_heartbeat);
			} else {
				_canal_wt = "00";
				_modo_wt = "00";
				_health_temperature = "0000";
				_health_pressure_sistole = "00";
				_health_pressure_diastole = "00";
				_health_oxygen_sp02 = "00";
				_health_heartbeat = "00";
			}

		}
	}

	private void getDatosComunicacion() throws ConvertHexToStringException, Exception {

		// _log.info(this.getClass().getName() + " - getDatosComunicacion");

		_modo = ServidorUtil.convertHexToString(_modo, "modo");

		int m = ServidorUtil.parseStringToInt16(_modo, "modo");
		if (m == 2) {
			datosComunicacionHelmet.setModo(ModoFuncionamiento.INTERIOR);
		} else {
			datosComunicacionHelmet.setModo(ModoFuncionamiento.EXTERIOR);
		}

		datosComunicacionHelmet
				.setAlarma(ServidorUtil.getTipoAlarma(ServidorUtil.parseStringToInt16(_alarma, "alarma")));

		BigInteger t = ServidorUtil.parseStringToBigInteger(_tiempo, "tiempo");
		datosComunicacionHelmet.setTiempo(t.intValue());
		datosComunicacionHelmet.setGas(ServidorUtil.parseStringToInt16(_gas, "gas"));

		int s_x = ServidorUtil.parseStringToInt16(_sensor_x, "sensor_x");
		int s_y = ServidorUtil.parseStringToInt16(_sensor_y, "sensor_Y");
		int s_z = ServidorUtil.parseStringToInt16(_sensor_z, "sensor_Z");

		if (s_x > 255)
			s_x = (s_x & 0x00FF) * -1;
		if (s_y > 255)
			s_y = (s_y & 0x00FF) * -1;
		if (s_z > 255)
			s_z = (s_z & 0x00FF) * -1;

		datosComunicacionHelmet.setSensorX(s_x);
		datosComunicacionHelmet.setSensorY(s_y);
		datosComunicacionHelmet.setSensorZ(s_z);

		_exactitud = ServidorUtil.convertHexToString(_exactitud, "exactitud").trim();
		if (_exactitud.equals(""))
			_exactitud = "0";
		datosComunicacionHelmet.setExactitud(ServidorUtil.parseStringToFloat(_exactitud, "exactitud"));

		_satelites = ServidorUtil.convertHexToString(_satelites, "satelites").trim();
		if (_satelites.equals(""))
			_satelites = "0";
		datosComunicacionHelmet.setSatelites(ServidorUtil.parseStringToInt16(_satelites, "satelites"));

		_latitud = ServidorUtil.convertHexToString(_latitud, "latitud").trim();
		if (_latitud.equals(""))
			_latitud = "0";

		_longitud = ServidorUtil.convertHexToString(_longitud, "longitud").trim();
		if (_longitud.equals(""))
			_longitud = "0";

		datosComunicacionHelmet.setLatitud(ServidorUtil.parseStringToDouble(_latitud, "latitud"));
		if (datosComunicacionHelmet.getLatitud() >= 0) {
			datosComunicacionHelmet.setLatitudC("N");
		} else {
			datosComunicacionHelmet.setLatitudC("S");
		}

		datosComunicacionHelmet.setLongitud(ServidorUtil.parseStringToDouble(_longitud, "longitud"));
		if (datosComunicacionHelmet.getLongitud() >= 0) {
			datosComunicacionHelmet.setLongitudC("E");
		} else {
			datosComunicacionHelmet.setLongitudC("W");
		}

		datosComunicacionHelmet.setMac1(_mac_1);
		datosComunicacionHelmet.setMac2(_mac_2);
		datosComunicacionHelmet.setMac3(_mac_3);
		datosComunicacionHelmet.setMac4(_mac_4);

		int r1 = ServidorUtil.parseStringToInt(_rssi_1, "rssi_1");
		if (r1 > 98)
			r1 = 80;
		datosComunicacionHelmet.setRssi1(r1);

		int r2 = ServidorUtil.parseStringToInt(_rssi_2, "rssi_2");
		if (r2 > 98)
			r2 = 80;
		datosComunicacionHelmet.setRssi2(r2);

		int r3 = ServidorUtil.parseStringToInt(_rssi_3, "rssi_3");
		if (r3 > 98)
			r3 = 80;
		datosComunicacionHelmet.setRssi3(r3);

		int r4 = ServidorUtil.parseStringToInt(_rssi_4, "rssi_4");
		if (r4 > 98)
			r4 = 80;
		datosComunicacionHelmet.setRssi4(r4);

		datosComunicacionHelmet.setFecha(ServidorUtil.getFecha("010101"));

		datosComunicacionHelmet.setBateria(ServidorUtil.parseStringToInt16(_bateria, "bateria"));
		_log.info(this.getClass().getName() + " - getDatosComunicacion");
		_log.info(" - _mensaje ***** " + _mensaje );
		
//		_mensaje = ServidorUtil.convertHexToString(_mensaje, "mensaje");
		
		_log.info(" convertHexToString _mensaje ***** " + _mensaje );
		
		_log.info(" parseStringToInt16 _mensaje ***** " + ServidorUtil.parseStringToInt16(_mensaje, "mensaje") );
		
		
		datosComunicacionHelmet
				.setMensaje(ServidorUtil.getTipoMensaje(ServidorUtil.parseStringToInt16(_mensaje, "mensaje")));

		datosComunicacionHelmet.setDebug(_debug);

		datosComunicacionHelmet.setFechaLog(ServidorUtil.getTimestamp());

		// Version 3.0

		datosComunicacionHelmet.setTemperatura(ServidorUtil.convertHexToInt(_temperatura, "temperatura"));

		boolean volteo = (_volteo.equals(0)) ? false : true;
		datosComunicacionHelmet.setVolteo(volteo);

		//_log.info("_firma: " + _firma);
		//_log.info("hexReverse -> _firma: " + ServidorUtil.hexReverse(_firma));
		//_log.info("parseToLong -> hexReverse -> _firma: "
				//+ ServidorUtil.parseStringToLong16(ServidorUtil.hexReverse(_firma), "firma"));

		datosComunicacionHelmet.setFirma(ServidorUtil.parseStringToLong16(ServidorUtil.hexReverse(_firma), "firma"));

		datosComunicacionHelmet.setRespuesta(ServidorUtil.convertHexToInt(_respuesta, "respuesta"));

		datosComunicacionHelmet.setRespuestaSms(ServidorUtil.convertHexToString(_respuesta_sms, "respuesta_sms"));

		// Walkie Talkie
		datosComunicacionHelmet.setCanalWt(ServidorUtil.parseStringToInt16(_canal_wt, "Canal WT"));
		datosComunicacionHelmet.setModoWt(ServidorUtil.parseStringToInt16(_modo_wt, "Mode WT"));

		// Salud
		datosComunicacionHelmet
				.setHealthTemperature(ServidorUtil.parseStringToInt16(_health_temperature, "Health Temperature"));
		datosComunicacionHelmet.setHealthPressureSistole(
				ServidorUtil.parseStringToInt16(_health_pressure_sistole, "Health Pressure Sistole"));
		datosComunicacionHelmet.setHealthPressureDiastole(
				ServidorUtil.parseStringToInt16(_health_pressure_diastole, "Health Pressure Diastole"));
		datosComunicacionHelmet
				.setHealthOxygenSp02(ServidorUtil.parseStringToInt16(_health_oxygen_sp02, "Health Oxygen Sp02"));
		datosComunicacionHelmet
				.setHealthHeartbeat(ServidorUtil.parseStringToInt16(_health_heartbeat, "Health Heartbeat"));

	}

}
