package com.angelhelmet.server.negocio;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.UUID;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttPersistenceException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.datos.datosGestion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosOperarioWS;
import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.datos.datosUnidadMapaWS;
import com.angelhelmet.server.datos.datosUnidadWS;
import com.angelhelmet.server.negocio.interfaces.IAlarmaBean;
import com.angelhelmet.server.negocio.interfaces.IEventBean;
import com.angelhelmet.server.negocio.interfaces.ILogBean;
import com.angelhelmet.server.negocio.interfaces.IMensajeBean;
import com.angelhelmet.server.negocio.interfaces.IPeticionBean;
import com.angelhelmet.server.negocio.interfaces.IUmbralBean;
import com.angelhelmet.server.negocio.interfaces.IUnidadBean;
import com.angelhelmet.server.negocio.interfaces.IUsaGlobalesBean;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.negocio.pool.excepciones.GetUnidadPoolException;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.rest.service.AppProperties;
import com.angelhelmet.server.rest.service.dtos.HelmetPoolData;
import com.angelhelmet.server.util.EstadoMensajeEntrada;
import com.angelhelmet.server.util.ModoFuncionamiento;
import com.angelhelmet.server.util.TipoAlarma;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;
import com.ibm.iotf.client.app.ApplicationClient;

@Stateless
public class PeticionBean implements IPeticionBean {

	@EJB
	private IMensajeBean _mensaje_bean;
	@EJB
	private IUnidadBean _unidad_bean;
	@EJB
	private ILogBean _log_bean;
	@EJB
	private IEventBean _event_bean;
	@EJB
	private IUnidadMapaRealBean _cliente_real;
	@EJB
	private IUsaGlobalesBean _globales;
	// @EJB
	// AppProperties appProp1;
	@EJB
	AppProperties appProp;

	

	Boolean mqttStandalone = null;
	private Logger _log;

	private List<Long> _lista_purgas;

	private datosComunicacion datosComm;

	private Integer mapIdPrev = null;

	@EJB
	private IAlarmaBean _alarm_bean;

	public PeticionBean() {
		_log = LogManager.getLogger(PeticionBean.class);
	}

	

	@Override
	public datosGestion gestionaPeticion(datosComunicacion datos_comunicacion) throws Exception {

		// _log.info(this.getClass().getName() + " - gestionaPeticion");

		this.datosComm = datos_comunicacion;
		datosGestion ret = new datosGestion();

		ApplicationClient myClient = null;

		try {

			_log.info("Serie: ************* " + datos_comunicacion.getSerie());
			
			_log.info("datos_comunicacion mensaje: ************* " + datos_comunicacion.getMensaje().getTipoMensage());

			_log.info("Serie: ************* " + _globales.getGlobales().IMPRIME_POOL);

			if (datos_comunicacion.getSerie().equals(_globales.getGlobales().IMPRIME_POOL)) {

				ret.setTextoMensaje(_cliente_real.getPool().imprimePool().toString());

			} else if (datos_comunicacion.getSerie().equals(_globales.getGlobales().LIMPIA_POOL)) {

				_cliente_real.getPool().limpiaPool();
				_cliente_real.getPool().imprimePool();
			} else {
				// TODO
				// _cliente_real.getUnidadMapa()

				UnidadEB unidad_eb = null;
				datosLog datos_log = null;

				unidad_eb = _unidad_bean.getUnidad(datos_comunicacion.getSerie());

				
				

				if (unidad_eb.getIdUnidad() > 0) {
					_log.info("pool :  " + _cliente_real.getPool().imprimePool());
					_log.info("unidad_eb :  " + unidad_eb.getIdUnidad());

					datosUnidadMapa datosUnidadMapa = _cliente_real.getPool().getUnidad(unidad_eb.getIdUnidad());
					if (datosUnidadMapa != null) {
						_log.info("mapaID ---> :  " + datosUnidadMapa.getIdMapa());
						List<datosUnidadMapaWS> datosUnidadMapaWSs = _cliente_real.getPool()
								.getUnidadesPorMapa(datosUnidadMapa.getIdMapa());
						datosCoordenada datosCoordenada = _cliente_real.getUnidadMapa().getPosicion().getCoordenadas();
						if (datos_comunicacion.getModo() == ModoFuncionamiento.EXTERIOR) {
							_log.info("EXTERIOR ---> :  ");
							_log.info("datosCoordenada.getX() ---> :  " + datosCoordenada.getX());
							_log.info("datosCoordenada.gety() ---> :  " + datosCoordenada.getY());

						} else {
							_log.info("INTERIOR ---> :  ");
							_log.info("datosCoordenada.getX() ---> :  " + datosCoordenada.getX());
							_log.info("datosCoordenada.gety() ---> :  " + datosCoordenada.getY());

						}
						Map<Integer, List<datosUnidadMapa>> lstMapUnidades = new HashMap();
						
					}

					datosUnidadMapa unidadMapPrev = _cliente_real.getPool().getUnidad(unidad_eb.getIdUnidad());
					if (unidadMapPrev != null) {
						mapIdPrev = unidadMapPrev.getIdMapa();
					}
					
					

					datos_log = _log_bean.grabaLog(datos_comunicacion, unidad_eb);

					_log.info("idLog: ", datos_log.getLog().getIdLog());
					_log.info("iMapa: ", datos_log.getMapa().getIdMapa());

					_cliente_real.setDatos(datos_comunicacion);

					if (datos_log.getLog().getIdLog() > 0) {
						_cliente_real.setUnidadMapa(datos_log);
						List<HelmetPoolData> datas = _cliente_real.getPool()
								.getUnidadesByMapa(datos_log.getMapa().getIdMapa());
						_log.info("datas :  " + datas);
						for (HelmetPoolData helmetPoolData : datas) {

							_log.info("unidad mapas :  " + helmetPoolData.getId_unidad());

						}

						if (_cliente_real.creaUnidadMapa(_cliente_real.getPool())) {
							_cliente_real.gestionaPool(_cliente_real.getPool());

							ret.setIdUnidad(_cliente_real.getUnidadMapa().getUnidad().getIdUnidad());
							ret.setTextoMensaje(_cliente_real.getUnidadMapa().getTextoMensaje());
							ret.setIdLog(datos_log.getLog().getIdLog());
							// send JSON dada to HCL Cloud
							// 1.- We are creating the JSON to send it to
							JsonObject json = creaJsonHCL(datos_comunicacion.getModo(), unidadMapPrev, unidad_eb);
							if (json != null) {
								// 2.- Send JSON to HCL cloud
								Gson g = new Gson();

								// HCL�s cloud
								/*
								 * -- Code Added By Amit for HCL Cloud connection --- Start ---
								 */
								if (mqttStandalone == null) {
									mqttStandalone = Boolean
											.valueOf(appProp.getProperties().getProperty("mqtt.standalone"));
								}
								if (mqttStandalone) {
									publishToLocalMQTT(unidad_eb,
											String.valueOf(_cliente_real.getUnidadMapa().getIdMapa()), json);
								} else {
									Properties prop = new Properties();

									_log.info("Organization-ID: " + unidad_eb.getOrganizationID());
									_log.info("Authentication-Method: " + unidad_eb.getAuthenticationMethod());
									_log.info("API-Key: " + unidad_eb.getAPIKey());
									_log.info("Authentication-Token: " + unidad_eb.getAuthtoken());
									_log.info("Device-Type: " + unidad_eb.getDeviceType());
									_log.info("Device-ID: " + unidad_eb.getDeviceID());
									_log.info("Shared-Subscription: " + unidad_eb.isSharedSubscription());
									_log.info("Clean-Session: " + unidad_eb.isCleanSession());

									prop.put("Organization-ID", unidad_eb.getOrganizationID());
									prop.put("id", "app" + (Math.random() * 10000));
									prop.put("Authentication-Method", unidad_eb.getAuthenticationMethod());
									prop.put("API-Key", unidad_eb.getAPIKey());
									prop.put("Authentication-Token", unidad_eb.getAuthtoken());
									prop.put("Device-Type", unidad_eb.getDeviceType());
									prop.put("Device-ID", unidad_eb.getDeviceID());
									prop.put("Shared-Subscription", unidad_eb.isSharedSubscription());
									prop.put("Clean-Session", unidad_eb.isCleanSession());

									myClient = new ApplicationClient(prop);
									myClient.setKeepAliveInterval(60);
									myClient.connect(10);

									myClient.publishEvent(unidad_eb.getDeviceType(), unidad_eb.getDeviceID(),
											String.valueOf(_cliente_real.getUnidadMapa().getIdMapa()), json);
									myClient.disconnect();

								}
								_log.info("sending data To HCL :" + g.toJson(json));

							}
							// purga mensajes
							_lista_purgas = new ArrayList<Long>();
							getMensajesPurgar(_cliente_real.getUnidadMapa().getAlarmas());
							purga(_cliente_real.getUnidadMapa().getUnidad().getIdUnidad());
							_log.info("Count of Alarms in Cache: " + _cliente_real.getUnidadMapa().getAlarmas().size());
						} else {
							throw new Exception("Error: IUnidadMapaRealBean.creaUnidadMapa");
						}
					}
				}
			}
		} catch (Exception ex) {
			if (ex.getClass() == MapaNoEncontradoException.class) {
				if (datos_comunicacion.getMensaje() == EstadoMensajeEntrada.Lampisteria) {
					ret.setTextoMensaje("#$" + _globales.getGlobales().APAGADO);
				} else {
					throw ex;
				}
			} else {
				throw ex;
			}
		}
		return ret;
	}

	private void publishToLocalMQTT(UnidadEB unidad_eb, String mapId, JsonObject json)
			throws MqttPersistenceException, MqttException {

		// _log.info(this.getClass().getName() + " - publishToLocalMQTT");

		String clientId = UUID.randomUUID().toString();
		String topic = "";
		// topic = "iot-2/type/" + deviceType + "/id/" + deviceId + "/evt/" +
		// event + "/fmt/json";
		topic = appProp.getProperties().getProperty("mqtt.topic");
		topic = String.format(topic, unidad_eb.getDeviceType(), unidad_eb.getDeviceID(), mapId);

		// "iot-2/type/" + unidad_eb.getDeviceType() + "/id/" +
		// unidad_eb.getDeviceID() + "/evt/" + mapId
		// + "/fmt/json";
		int qos = 0;
		JsonObject payload = new JsonObject();
		payload.add("d", json);

		// String content = "Message from MqttPublishSample";

		String broker = appProp.getProperties().getProperty("mqtt.server");
		String uname = appProp.getProperties().getProperty("mqtt.username");
		String pwd = appProp.getProperties().getProperty("mqtt.password");

		MemoryPersistence persistence = new MemoryPersistence();
		MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
		MqttConnectOptions connOpts = new MqttConnectOptions();
		connOpts.setCleanSession(true);
		connOpts.setUserName(uname);
		char[] pass = pwd.toCharArray();
		connOpts.setPassword(pass);
		sampleClient.connect(connOpts);
		// MqttMessage message = new MqttMessage(content.getBytes());
		// message.setQos(qos);
		sampleClient.publish(topic, payload.toString().getBytes(), qos, false);
		sampleClient.disconnect();
	}

	private JsonObject creaJsonHCL(ModoFuncionamiento modo, datosUnidadMapa unidadMapPrev, UnidadEB unidad_eb) {

		// _log.info(this.getClass().getName() + " - creaJsonHCL");

		JsonObject json_to_hcl = new JsonObject();
		JsonObject jsonAlarmSource = new JsonObject();

		if (_cliente_real.getUnidadMapa().getAlarmas().size() > 0) {
			json_to_hcl.add("alarmsList", parseAlarmas(_cliente_real.getUnidadMapa().getAlarmas()));
		} else {
			json_to_hcl.addProperty("alarmsList", "no alarms");
		}

		if (modo == ModoFuncionamiento.EXTERIOR) {
			json_to_hcl.add("IndoorPosition", parsePosicionInterior(new datosCoordenada(0, 0)));

			datosUnidadMapa datos_log = _cliente_real.getUnidadMapa();

			json_to_hcl.add("OutdoorPosition", parsePosicionExterior(datos_log.getPosicion().getCoordenadas()));
			json_to_hcl.addProperty("mode", modo.name());
		} else {
			json_to_hcl.add("IndoorPosition",
					parsePosicionInterior(_cliente_real.getUnidadMapa().getPosicion().getCoordenadas()));

			json_to_hcl.add("OutdoorPosition", parsePosicionExterior(new datosCoordenada(0, 0)));

			json_to_hcl.addProperty("mode", modo.name());
			json_to_hcl.add("map", parseMap(_cliente_real.getUnidadMapa().getIdMapa()));
		}

		json_to_hcl.add("Helmet", parseHelmet(_cliente_real.getUnidadMapa().getUnidad(), unidad_eb));
		json_to_hcl.addProperty("MenssageStatus",
				_cliente_real.getUnidadMapa().getDatosMensajeWS().getEstadoMensaje().name());

		// json_to_hcl.add("mine", parseMina());
		json_to_hcl.add("worker", parseOperario(_cliente_real.getUnidadMapa().getOperario()));
		json_to_hcl.add("map", parseMap(_cliente_real.getUnidadMapa().getIdMapa()));

		json_to_hcl.addProperty("helmetId", _cliente_real.getUnidadMapa().getUnidad().getIdUnidad());
		json_to_hcl.addProperty("mapId", _cliente_real.getUnidadMapa().getIdMapa());

		/* to remove helmet from previous map */

		if (mapIdPrev != null && mapIdPrev != _cliente_real.getUnidadMapa().getIdMapa()) {
			json_to_hcl.addProperty("mapIdPrev", mapIdPrev);
		}

		return json_to_hcl;
	}

	// added by Arun
	private JsonElement parseMap(int idMapa) {
		// _log.info(this.getClass().getName() + " - parseMap");

		JsonObject ret = new JsonObject();
		ret.addProperty("mapId", idMapa);
		return ret;
	}

	// added by Arun
	private JsonElement parseMode(String mode) {
		// _log.info(this.getClass().getName() + " - parseMode");

		JsonObject ret = new JsonObject();
		ret.addProperty("mode", mode);
		return ret;
	}

	private JsonArray parseAlarmas(Map<Long, datosAlarma> lista) {

		// _log.info(this.getClass().getName() + " - parseAlarmas");

		JsonArray arr = new JsonArray();

		for (Entry<Long, datosAlarma> item : lista.entrySet()) {
			JsonObject object = new JsonObject();

			JsonObject objectAlarmSource = new JsonObject();

			object.addProperty("alarm", item.getValue().getAlarma().name());
			object.addProperty("cancelTo", String.valueOf(item.getValue().getCancelaAlarma()));

			object.addProperty("answerNumber", 10);
			object.addProperty("answerText", "");
			object.addProperty("answerDesc", "");

			/*
			 * System.out.println(item.getValue().getAlarma().getAlarma());
			 * System.out.println(item.getValue().getCancelaAlarma());
			 * System.out.println(item.getValue().getFirma());
			 */

			if (item.getValue().getCancelaAlarma() > 0 && (item.getValue().getAlarma().getAlarma() == 13
					|| item.getValue().getAlarma().getAlarma() == 18)) {

				object.addProperty("question", "");
				try {

					AlarmSource aSource = _alarm_bean.getAlarma(item.getValue().getCancelaAlarma());

					objectAlarmSource.addProperty("eventId", aSource.getEventId());
					objectAlarmSource.addProperty("alarmId", aSource.getAlarmId());
					objectAlarmSource.addProperty("alarmDescription", aSource.getAlarmDescription());
					objectAlarmSource.addProperty("alarmType", aSource.getAlarmType());
					objectAlarmSource.addProperty("logId", aSource.getLogId());
					objectAlarmSource.addProperty("eventDate", aSource.getEventDate().toString());
					objectAlarmSource.addProperty("responseDate", aSource.getResponseDate().toString());
					objectAlarmSource.addProperty("latitude", aSource.getLatitude());
					objectAlarmSource.addProperty("longitude", aSource.getLongitude());
					objectAlarmSource.addProperty("latitudC", aSource.getLatitudeC());
					objectAlarmSource.addProperty("longitudeC", aSource.getLongitudeC());
					objectAlarmSource.addProperty("x", aSource.getX());
					objectAlarmSource.addProperty("y", aSource.getY());
					objectAlarmSource.addProperty("mapTypeId", aSource.getMapTypeId());

				} catch (AlarmaNoEncontradaException e1) {
					System.out.println("alarmSource error1: " + e1.getLocalizedMessage());
					// TODO Auto-generated catch block
					// e1.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					System.out.println("alarmSource error2: " + e1.getLocalizedMessage());
					// e1.printStackTrace();
				}

			} else if (item.getValue().getAlarma().getAlarma() == 12) {

				try {

					WorkerReadMessage workerReadMessage = _alarm_bean.getReadMessage(item.getValue().getFirma());

					object.addProperty("question", workerReadMessage.getWorkerQuestionMessage());

				} catch (AlarmaNoEncontradaException e1) {
					object.addProperty("question", "");
					// e1.printStackTrace();
				} catch (Exception e1) {
					object.addProperty("question", "");
				}

			}
			object.add("alarmSource", objectAlarmSource);

			try {
				// _log.info("firma: " + item.getValue().getFirma());

				AlarmEvent alarmEvent = _alarm_bean.getAlarmaEvento(item.getValue().getFirma());

				object.addProperty("eventId", alarmEvent.getEventId());
				object.addProperty("eventDate", alarmEvent.getEventDate().getTime());
				object.addProperty("eventResponseDate", alarmEvent.getEventResponseDate().getTime());

			} catch (AlarmaNoEncontradaException e1) {
				_log.info("error event: " + e1.getLocalizedMessage());

			} catch (Exception e1) {
				_log.info("error event: " + e1.getLocalizedMessage());

			}

			object.addProperty("alarmId", String.valueOf(item.getValue().getFirma()));
			object.addProperty("count", String.valueOf(item.getValue().getContador()));

			// _log.info("firma: " + item.getValue().getFirma());
			// _log.info("name: " + item.getValue().getAlarma().name());
			try {

				Long idLog = _mensaje_bean.getMensajeIdLogByCasco(item.getValue().getFirma());

				// _log.info("idLog: " + idLog);

				if (idLog > 0L) {

					LogEB log = _log_bean.getLogById(idLog);

					Integer answerNumber = log.getRespuesta();

					object.addProperty("answerNumber", answerNumber);

					_log.info(
							"answerNumber: " + answerNumber + "\n" + "respuesta_sms: " + log.getRespuesta_sms().trim());

					object.addProperty("answerText", StringEscapeUtils.escapeJson(log.getRespuesta_sms().trim()));

					if (answerNumber > 0 && answerNumber < 9) {

						String mensajeCasco = _mensaje_bean.getMensajeByCasco(item.getValue().getFirma());

						_log.info("mensajeCasco: " + mensajeCasco);

						if (mensajeCasco != "") {

							String[] mensajCascoLista = mensajeCasco.split("~");

							if (mensajCascoLista.length > 0) {
								object.addProperty("question",
										StringEscapeUtils.escapeJson(mensajCascoLista[0].substring(9)));

								if (mensajCascoLista.length > answerNumber) {

									object.addProperty("answerDesc",
											StringEscapeUtils.escapeJson(mensajCascoLista[answerNumber]));

								}
							}

						}
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			arr.add(object);

		}

		return arr;
	}

	private JsonObject parsePosicionInterior(datosCoordenada datos) {

		// _log.info(this.getClass().getName() + " - parsePosicionInterior");

		JsonObject ret = new JsonObject();

		ret.addProperty("X", String.valueOf(datos.getX()));
		ret.addProperty("Y", String.valueOf(datos.getY()));

		return ret;
	}

	private JsonObject parsePosicionExterior(datosCoordenada datos) {

		// _log.info(this.getClass().getName() + " - parsePosicionExterior");

		JsonObject ret = new JsonObject();

		ret.addProperty("latitude", String.valueOf(datos.getX()));
		ret.addProperty("longitude", String.valueOf(datos.getY()));

		return ret;
	}
	/* done by HCL Sanjay */
	// private JsonObject parsePosicionExterior(datosGps datos)
	// {
	// JsonObject ret = new JsonObject();
	//
	// ret.addProperty("latitude", String.valueOf(datos.getLatitud()));
	// ret.addProperty("longitude", String.valueOf(datos.getLongitud()));
	//
	// return ret;
	// }

	private JsonObject parseHelmet(datosUnidadWS datos, UnidadEB unidad_eb) {

		// _log.info(this.getClass().getName() + " - parseHelmet");

		JsonObject ret = new JsonObject();

		ret.addProperty("battery", String.valueOf(datos.getBateria()));
		ret.addProperty("gas", String.valueOf(datos.getGas()));
		ret.addProperty("helmetId", String.valueOf(datos.getIdUnidad()));
		ret.addProperty("xSensor", String.valueOf(datosComm.getSensorX()));
		ret.addProperty("ySensor", String.valueOf(datosComm.getSensorY()));
		ret.addProperty("zSensor", String.valueOf(datosComm.getSensorZ()));

		ret.addProperty("serial", datos.getSerie());
		ret.addProperty("telephone", unidad_eb.getTelefono());
		ret.addProperty("timeLastReport", datos.getTimeLastReport());
		JsonObject life = new JsonObject();
		life.addProperty("h", datos.getTiempoVida().getHoras());
		life.addProperty("m", datos.getTiempoVida().getMinutos());
		life.addProperty("s", datos.getTiempoVida().getSegundos());

		ret.add("lifeTime", life);

		return ret;
	}

	private JsonObject parseMina() {

		// _log.info(this.getClass().getName() + " - parseMina");

		JsonObject ret = new JsonObject();

		// ret.addProperty("mineId", String.valueOf(1));
		// ret.addProperty("nameMine", "Isibonelo Coal Mine");

		ret.addProperty("mineId", String.valueOf(2));
		ret.addProperty("nameMine", "Consulmet Mine");

		return ret;
	}

	private JsonObject parseOperario(datosOperarioWS operario) {

		// _log.info(this.getClass().getName() + " - parseOperario");

		JsonObject ret = new JsonObject();

		// ret.addProperty("suname", operario.getApellidos());

		ret.addProperty("bussiness", operario.getEmpresa());
		ret.addProperty("identification", operario.getIdentificacion());
		ret.addProperty("workerId", String.valueOf(operario.getIdOperario()));
		ret.addProperty("name", operario.getNombre());
		ret.addProperty("emergency", operario.getEmergency());
		ret.addProperty("smsNumber", operario.getSmsNumber());
		ret.addProperty("blood", operario.getBlood());
		ret.addProperty("supervisor", operario.getSupervisor());
		ret.addProperty("workerposition", operario.getCargo());
		// TODO need to add worker charge
		ret.addProperty("chargeId", operario.getCargoId());

		return ret;
	}

	private void getMensajesPurgar(Map<Long, datosAlarma> mapas) {

		// _log.info(this.getClass().getName() + " - getMensajesPurgar");

		Iterator<Entry<Long, datosAlarma>> it = mapas.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<Long, datosAlarma> item = (Map.Entry<Long, datosAlarma>) it.next();

			long firma = item.getKey();
			datosAlarma alarma = item.getValue();

			// Changes done by Gunjan
			// 'BACKDOOR', 'LAMPISTERIA', 'APAGADO SEGURO' : when UI reads data
			// from pool it removes but does not show on client UI, just ignores

			if (alarma.getAlarma() == TipoAlarma.ALARMAOK || alarma.getAlarma() == TipoAlarma.GASOK
					|| alarma.getAlarma() == TipoAlarma.MENSAJELEIDO || alarma.getAlarma() == TipoAlarma.CASCOAPAGADO
					|| alarma.getAlarma() == TipoAlarma.BACKDOOR || alarma.getAlarma() == TipoAlarma.LAMPISTERIA
					|| alarma.getAlarma() == TipoAlarma.APAGADOSEGURO
					|| alarma.getAlarma() == TipoAlarma.CANCELADASISTEMA) {
				_lista_purgas.add(firma);
				_lista_purgas.add(alarma.getCancelaAlarma());
			}
		}
	}

	private void purga(int id_unidad) throws GetUnidadPoolException {
		// _log.info(this.getClass().getName() + " - purga");

		for (Long item : _lista_purgas) {
			datosUnidadMapa datos = _cliente_real.getPool().getUnidad(id_unidad);
			datos.getAlarmas().remove(item);
		}
	}
}
