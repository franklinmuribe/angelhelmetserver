package com.angelhelmet.server.listener;

import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebListener;

/**
 * Application Lifecycle Listener implementation class PersistenciaListener
 * 
 */
@WebListener
public class PersistenciaListener implements ServletRequestListener
{
//	private static EntityManagerFactory emf;

	public PersistenciaListener()
	{
		// System.out.println("PersistenciaListener....................");
	}

	@Override
	public void requestDestroyed(ServletRequestEvent arg0)
	{
		// System.out.println("PersistenciaListener: requestDestroyed....................");
//		if (emf != null)
//		{
//			emf.close();
//		}
	}

	@Override
	public void requestInitialized(ServletRequestEvent arg0)
	{
		// System.out.println("PersistenciaListener: requestInitialized....................");
//		emf = Persistence.createEntityManagerFactory("AngelHelmetServerContext");
		
//		Properties p = _conf_data_source.getConfiguracionDataSource();
//		for (Entry<Object, Object> entry: p.entrySet()) {
//		    String key = (String) entry.getKey();
//		    String value = (String) entry.getValue();
//		    
//		    System.out.println("key: " + key + " value: " + value);
//		    
//		}
		
	}

//	public static EntityManager createEntityManager()
//	{
//		if (emf == null) { throw new IllegalStateException("IllegalStateException en createEntityManager (emf = null)"); }
//		return emf.createEntityManager();
//	}
}
