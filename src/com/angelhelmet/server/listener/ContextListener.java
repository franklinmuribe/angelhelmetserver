package com.angelhelmet.server.listener;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Application Lifecycle Listener implementation class ContextListener
 * 
 */
@WebListener
public class ContextListener implements ServletContextListener
{
	private static EntityManagerFactory _emf;
	private Logger _log;
	
	public ContextListener()
	{
		_log = LogManager.getLogger(ContextListener.class);
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0)
	{
		try
		{
			_emf.close();
		}
		catch (IllegalStateException ex)
		{
			_log.error("ERROR al cerrar el EntityManagerFactory (IllegalStateException)", ex);
		}
		catch (Exception e)
		{
			_log.error("Error al cerrar el EntityManagerFactory (Exception)", e);
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent)
	{
		
//		ServletContext ctx = servletContextEvent.getServletContext();
//		Context ct = null;		
//		try
//		{
////			ct = new InitialContext();
////			DataSource ds = (DataSource) ct.lookup("java:jboss/datasources/AngelHelmetServerDS");			
////			Connection con = ds.getConnection();
////			String url = con.getMetaData().getURL();
////			String usuario = con.getMetaData().getUserName();
////			String pwd;
//		}
//		catch (NamingException | SQLException e)
//		{
//			e.printStackTrace();
//		}
		
		try
		{			
			_emf = Persistence.createEntityManagerFactory("AngelHelmetServerContext");
		}
		catch (Exception ex)
		{
			_log.error("ERROR al crear el EntityManagerFactory", ex);
		}
	}

}
