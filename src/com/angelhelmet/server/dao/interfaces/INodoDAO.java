package com.angelhelmet.server.dao.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.NodoEB;

@Local
@Deprecated
public interface INodoDAO
{
	public List<NodoEB> getNodos();
}
