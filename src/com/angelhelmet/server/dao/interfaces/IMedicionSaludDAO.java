package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.persistencia.UmbralesMedicionSaludEB;
import com.angelhelmet.server.util.TipoAlarma;
import com.hcl.iot.smartworker.dto.MediconesDTO;
import com.hcl.iot.smartworker.exception.SmartWorkerDataException;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;

@Local
public interface IMedicionSaludDAO {
	
	public MediconesDTO getMedicion(Integer unidadId) throws  Exception;
	
	public UmbralesMedicionSaludEB getUmbralByUnidadSaludAndTipoMedicion( Integer tipoMedicionId, Integer unidadSaludId) throws  Exception;
}
