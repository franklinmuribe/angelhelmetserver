package com.angelhelmet.server.dao.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.OperarioEB;

@Local
//@Deprecated
public interface IOperarioDAO
{
	public OperarioEB getOperarioPorUnidad(Integer id_unidad);
	public List<OperarioEB> getOperarios() throws Exception;
	public List<OperarioEB> getOperarios(String supervisor) throws Exception;
}
