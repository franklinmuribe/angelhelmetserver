package com.angelhelmet.server.dao.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetZonaException;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.persistencia.ZonaEB;

@Local
public interface IZonaDAO
{
	public List<ZonaEB> getZonasMapa(int id_mapa) throws GetZonasMapaException, Exception;
	public ZonaEB getZona(int id_zona) throws GetZonaException, Exception;
}
