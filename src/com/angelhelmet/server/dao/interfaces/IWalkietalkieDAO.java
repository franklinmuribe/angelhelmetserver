package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.WalkietalkieNotFoundException;
import com.angelhelmet.server.datos.datosWalkietalkieAudio;
import com.angelhelmet.server.datos.datosWalkietalkieImei;

@Local
public interface IWalkietalkieDAO {

	public datosWalkietalkieImei obtenerImeiWT(String imei) throws WalkietalkieNotFoundException, Exception;

	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje) throws WalkietalkieNotFoundException, Exception;
	
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje, Long firmaCasco) throws WalkietalkieNotFoundException, Exception;

	public Long actualizarEstadoWT(Long idMensajeHelmet, Long idEstadoMensaje, Long firma)
			throws WalkietalkieNotFoundException, Exception;

}
