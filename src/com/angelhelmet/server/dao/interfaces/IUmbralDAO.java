package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.rest.service.api.Threshold;

@Local
public interface IUmbralDAO {

	public String addUmbral(datosUmbral datos);

	public Threshold getThresholdConfiguration(Integer configId);

	public Long addUnidadUmbral(Integer id_unidad, Integer sessionid, Integer id_umbral);
	
	public datosUmbral getUmbralPorUnidad(Integer id_unidad );

}
