package com.angelhelmet.server.dao.interfaces;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.persistencia.LogEB;

@Local
public interface ILogDAO {
	public LogEB grabaLog(datosLog datos) throws ParseLogException, GrabaLogException, Exception;

	@Deprecated
	public LogEB getUltimoLog(int id_unidad) throws Exception;

	public List<LogEB> getLogsByFecha(Timestamp inicio, Timestamp fin, List<Integer> unidades, int id_mapa)
			throws Exception;

	public LogEB getLogById(long id_log) throws Exception;

	public LogEB getLogByFirma(Long firma) throws Exception;
}
