package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.EmpresaEB;

@Local
@Deprecated
public interface IOperarioEmpresaDAO
{
	public EmpresaEB getEmpresaPorOperario(int id_operario);
}
