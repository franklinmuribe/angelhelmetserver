package com.angelhelmet.server.dao.interfaces;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.datos.datosUnidadMapa;
import com.angelhelmet.server.negocio.interfaces.UnidadMapaBuilder.IUnidadMapaRealBean;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.persistencia.UnidadEB;

@Local
public interface IEventDAO
{
	public LogEB grabaLog(datosLog datos) throws ParseLogException, GrabaLogException, Exception;

	@Deprecated
	public LogEB getUltimoLog(int id_unidad) throws Exception;

	public List<LogEB> getLogsByFecha(Timestamp inicio, Timestamp fin, List<Integer> unidades, int id_mapa) throws Exception;

	public LogEB getLogById(long id_log) throws Exception;

	//long addAlarmaEvento(datosComunicacion datos, UnidadEB unidad, datosLog datos_log,IUnidadMapaRealBean _cliente_real) throws Exception;

	long addAlarmaEvento(datosComunicacion datos, datosLog datos_log, datosUnidadMapa _unidad_mapa,
			datosAlarma currAlarm) throws Exception;
}
