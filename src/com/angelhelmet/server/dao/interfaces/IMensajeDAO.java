package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.util.TipoMensaje;

@Local
// @Deprecated
public interface IMensajeDAO {
	public MensajeEB getMensaje(int id_mensaje);

	public MensajeEB getMensajeConfiguracion(String serie);

	MensajeEB addMensaje(String texto, TipoMensaje tipo, Integer sessionId, Long unidadUmbrale, Long idFirmaCasco);

	MensajeEB addMensajeMasivos(String texto, TipoMensaje tipo, MensajeMasivo mensajes, Long idFirmaCasco);

	public Long getIdFirmaCasco();

	public String getMensajeByCasco(Long firma);

	public Long getMensajeIdLogByCasco(Long firma);
}
