package com.angelhelmet.server.dao.interfaces;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.util.TipoAlarma;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;

@Local
public interface IAlarmaDAO {
	public AlarmaEB getAlarma(TipoAlarma alarma) throws AlarmaNoEncontradaException, Exception;

	public AlarmSource getAlarma(Long firmaId) throws AlarmaNoEncontradaException, Exception;

	public AlarmEvent getAlarmaEvento(Long firmaId) throws AlarmaNoEncontradaException, Exception;

	public WorkerReadMessage getReadMessage(Long firma) throws AlarmaNoEncontradaException, Exception;
}
