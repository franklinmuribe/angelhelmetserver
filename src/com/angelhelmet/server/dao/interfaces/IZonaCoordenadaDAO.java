package com.angelhelmet.server.dao.interfaces;

import java.util.List;

import javax.ejb.Local;

import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.persistencia.ZonaCoordenadaEB;

@Local
public interface IZonaCoordenadaDAO
{
	public List<ZonaCoordenadaEB> getZonaCoordenadas(int id_zona) throws GetZonaCoordenadas, ZonaSinCoordenadasException, Exception;
}
