package com.angelhelmet.server.dao.interfaces;

import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.persistencia.MapaEB;

import javax.ejb.Local;

@Local
public interface IMapaDAO
{
	public MapaEB getMapa(int id) throws MapaNoEncontradoException, Exception;
	public MapaEB getMapa(double latitud, double longitud) throws MapaNoEncontradoException, Exception;
}
