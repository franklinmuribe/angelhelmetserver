package com.angelhelmet.server.dao;

import java.sql.Timestamp;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.angelhelmet.server.dao.interfaces.IUnidadOperarioDAO;
import com.angelhelmet.server.persistencia.OperarioEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadOperarioEB;

@Stateless
public class UnidadOperarioDAO implements IUnidadOperarioDAO
{
	@PersistenceContext
	private EntityManager _em;
	
	public UnidadOperarioDAO()
	{
	}
	
	@Override
	public UnidadOperarioEB addUnidadOperario(UnidadEB unidad, OperarioEB operario, Timestamp fecha_alta) throws Exception{
		UnidadOperarioEB ret = new UnidadOperarioEB();

		ret.setIdUnidadoperario(null);
		ret.setActivo(true);
		ret.setFechaAlta(fecha_alta);
		ret.setOperario(operario);
		ret.setUnidade(unidad);
		
		_em.persist(ret);
		_em.flush();
		
		return ret;
	}
	
	
}
