package com.angelhelmet.server.dao.excepciones;

public class GetMensajesPurgarException extends Exception
{
	private static final long serialVersionUID = 6005618988828655652L;
	
	public GetMensajesPurgarException() {
	}
	
	public GetMensajesPurgarException(String s) {
		super(s);
	}
	
	public GetMensajesPurgarException(String s, Exception e) {
		super(s, e);
	}

}
