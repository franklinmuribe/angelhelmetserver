package com.angelhelmet.server.dao.excepciones;

public class MapaNoEncontradoException extends Exception
{
	private static final long serialVersionUID = -923564814615846342L;
	
	public MapaNoEncontradoException() {
	}
	
	public MapaNoEncontradoException(String s) {
		super(s);
	}
	
	public MapaNoEncontradoException(String s, Exception e) {
		super(s, e);
	}
}
