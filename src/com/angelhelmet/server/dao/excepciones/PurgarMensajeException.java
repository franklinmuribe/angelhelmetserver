package com.angelhelmet.server.dao.excepciones;

public class PurgarMensajeException extends Exception
{
	private static final long serialVersionUID = -3031658104044128747L;
	
	public PurgarMensajeException() {
	}
	
	public PurgarMensajeException(String s) {
		super(s);
	}
	
	public PurgarMensajeException(String s, Exception e) {
		super(s, e);
	}

}
