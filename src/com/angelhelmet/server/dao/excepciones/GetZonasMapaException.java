package com.angelhelmet.server.dao.excepciones;

public class GetZonasMapaException extends Exception
{
	private static final long serialVersionUID = -38517545280627901L;
	
	public GetZonasMapaException() {
	}
	
	public GetZonasMapaException(String s) {
		super(s);
	}
	
	public GetZonasMapaException(String s, Exception e) {
		super(s, e);
	}

}
