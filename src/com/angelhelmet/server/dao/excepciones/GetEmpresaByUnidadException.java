package com.angelhelmet.server.dao.excepciones;

public class GetEmpresaByUnidadException extends Exception
{
	private static final long serialVersionUID = -1101828618286958929L;
	
	public GetEmpresaByUnidadException() {
	}
	
	public GetEmpresaByUnidadException(String s) {
		super(s);
	}
	
	public GetEmpresaByUnidadException(String s, Exception e) {
		super(s, e);
	}

}
