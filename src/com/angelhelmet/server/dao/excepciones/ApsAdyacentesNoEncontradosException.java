package com.angelhelmet.server.dao.excepciones;

public class ApsAdyacentesNoEncontradosException extends Exception
{
	private static final long serialVersionUID = 4981103156136950873L;
	
	public ApsAdyacentesNoEncontradosException() {
	}
	
	public ApsAdyacentesNoEncontradosException(String s) {
		super(s);
	}
	
	public ApsAdyacentesNoEncontradosException(String s, Exception e) {
		super(s, e);
	}

}
