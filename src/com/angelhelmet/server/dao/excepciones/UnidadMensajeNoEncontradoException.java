package com.angelhelmet.server.dao.excepciones;

public class UnidadMensajeNoEncontradoException extends Exception
{
	private static final long serialVersionUID = 3603950598293309770L;

	public UnidadMensajeNoEncontradoException() {
	}
	
	public UnidadMensajeNoEncontradoException(String s) {
		super(s);
	}
	
	public UnidadMensajeNoEncontradoException(String s, Exception e) {
		super(s, e);
	}
}
