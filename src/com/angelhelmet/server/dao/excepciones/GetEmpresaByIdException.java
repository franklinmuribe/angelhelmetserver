package com.angelhelmet.server.dao.excepciones;

public class GetEmpresaByIdException extends Exception
{
	private static final long serialVersionUID = -3145182237882243051L;
	
	public GetEmpresaByIdException() {
	}
	
	public GetEmpresaByIdException(String s) {
		super(s);
	}
	
	public GetEmpresaByIdException(String s, Exception e) {
		super(s, e);
	}

}
