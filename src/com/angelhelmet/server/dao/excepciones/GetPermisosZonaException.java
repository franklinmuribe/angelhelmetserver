package com.angelhelmet.server.dao.excepciones;

public class GetPermisosZonaException extends Exception
{
	private static final long serialVersionUID = -8209610810301543120L;
	
	public GetPermisosZonaException() {
	}
	
	public GetPermisosZonaException(String s) {
		super(s);
	}
	
	public GetPermisosZonaException(String s, Exception e) {
		super(s, e);
	}

}
