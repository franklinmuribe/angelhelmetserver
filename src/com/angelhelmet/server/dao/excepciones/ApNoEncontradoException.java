package com.angelhelmet.server.dao.excepciones;

public class ApNoEncontradoException extends Exception
{
	private static final long serialVersionUID = 3665744173812453020L;
	
	public ApNoEncontradoException() {
	}
	
	public ApNoEncontradoException(String s) {
		super(s);
	}
	
	public ApNoEncontradoException(String s, Exception e) {
		super(s, e);
	}

}
