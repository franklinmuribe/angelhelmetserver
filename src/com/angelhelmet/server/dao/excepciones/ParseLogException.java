package com.angelhelmet.server.dao.excepciones;

public class ParseLogException extends Exception
{
	private static final long serialVersionUID = 2771802081069116305L;
	
	public ParseLogException() {
	}
	
	public ParseLogException(String s) {
		super(s);
	}
	
	public ParseLogException(String s, Exception e) {
		super(s, e);
	}
}
