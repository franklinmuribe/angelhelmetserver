package com.angelhelmet.server.dao.excepciones;

public class AlarmaNoEncontradaException extends Exception
{
	private static final long serialVersionUID = 5819654162539028516L;

	public AlarmaNoEncontradaException() {
	}
	
	public AlarmaNoEncontradaException(String s) {
		super(s);
	}
	
	public AlarmaNoEncontradaException(String s, Exception e) {
		super(s, e);
	}
}
