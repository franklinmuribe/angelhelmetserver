package com.angelhelmet.server.dao.excepciones;

public class UnidadMensajeSetLeidoException extends Exception
{
	private static final long serialVersionUID = 6002017110063482268L;
	
	public UnidadMensajeSetLeidoException() {
	}
	
	public UnidadMensajeSetLeidoException(String s) {
		super(s);
	}
	
	public UnidadMensajeSetLeidoException(String s, Exception e) {
		super(s, e);
	}

}
