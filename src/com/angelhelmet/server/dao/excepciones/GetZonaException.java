package com.angelhelmet.server.dao.excepciones;

public class GetZonaException extends Exception
{
	private static final long serialVersionUID = -2353544580057203163L;
	
	public GetZonaException() {
	}
	
	public GetZonaException(String s) {
		super(s);
	}
	
	public GetZonaException(String s, Exception e) {
		super(s, e);
	}

}
