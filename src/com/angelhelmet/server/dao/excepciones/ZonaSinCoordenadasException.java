package com.angelhelmet.server.dao.excepciones;

public class ZonaSinCoordenadasException extends Exception
{
	private static final long serialVersionUID = 5840410396090934018L;

	public ZonaSinCoordenadasException() {
	}
	
	public ZonaSinCoordenadasException(String s) {
		super(s);
	}
	
	public ZonaSinCoordenadasException(String s, Exception e) {
		super(s, e);
	}
}
