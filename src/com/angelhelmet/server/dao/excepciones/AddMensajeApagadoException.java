package com.angelhelmet.server.dao.excepciones;

public class AddMensajeApagadoException extends Exception
{
	private static final long serialVersionUID = -8532206781945806864L;

	public AddMensajeApagadoException() {
	}
	
	public AddMensajeApagadoException(String s) {
		super(s);
	}
	
	public AddMensajeApagadoException(String s, Exception e) {
		super(s, e);
	}
}
