package com.angelhelmet.server.dao.excepciones;

public class GrabaLogException extends Exception
{
	private static final long serialVersionUID = 2300611760824842910L;
	
	public GrabaLogException() {
	}
	
	public GrabaLogException(String s) {
		super(s);
	}
	
	public GrabaLogException(String s, Exception e) {
		super(s, e);
	}

}
