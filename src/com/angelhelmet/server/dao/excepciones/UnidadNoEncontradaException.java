package com.angelhelmet.server.dao.excepciones;

public class UnidadNoEncontradaException extends Exception
{
	private static final long serialVersionUID = 3449207035129110466L;
	
	public UnidadNoEncontradaException() {
	}
	
	public UnidadNoEncontradaException(String s) {
		super(s);
	}
	
	public UnidadNoEncontradaException(String s, Exception e) {
		super(s, e);
	}

}
