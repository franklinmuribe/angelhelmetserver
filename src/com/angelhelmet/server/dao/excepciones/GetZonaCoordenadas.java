package com.angelhelmet.server.dao.excepciones;

public class GetZonaCoordenadas extends Exception
{
	private static final long serialVersionUID = -7179395966176286114L;
	
	public GetZonaCoordenadas() {
	}
	
	public GetZonaCoordenadas(String s) {
		super(s);
	}
	
	public GetZonaCoordenadas(String s, Exception e) {
		super(s, e);
	}

}
