package com.angelhelmet.server.dao.excepciones;

public class ApsMapaNoEncontradosException extends Exception
{
	private static final long serialVersionUID = -2232851258506933526L;

	public ApsMapaNoEncontradosException() {
	}
	
	public ApsMapaNoEncontradosException(String s) {
		super(s);
	}
	
	public ApsMapaNoEncontradosException(String s, Exception e) {
		super(s, e);
	}
}
