package com.angelhelmet.server.dao.excepciones;

public class GetUnidadMensajeByIdException extends Exception
{
	private static final long serialVersionUID = 2834692477059746221L;
	
	public GetUnidadMensajeByIdException() {
	}
	
	public GetUnidadMensajeByIdException(String s) {
		super(s);
	}
	
	public GetUnidadMensajeByIdException(String s, Exception e) {
		super(s, e);
	}

}
