package com.angelhelmet.server.dao.excepciones;

public class GetNodosProyeccionException extends Exception
{
	private static final long serialVersionUID = 6223380728716888599L;
	
	public GetNodosProyeccionException() {
	}
	
	public GetNodosProyeccionException(String s) {
		super(s);
	}
	
	public GetNodosProyeccionException(String s, Exception e) {
		super(s, e);
	}

}
