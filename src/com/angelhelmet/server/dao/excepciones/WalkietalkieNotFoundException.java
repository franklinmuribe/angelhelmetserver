package com.angelhelmet.server.dao.excepciones;

public class WalkietalkieNotFoundException extends Exception
{
	private static final long serialVersionUID = 5819654162539028516L;

	public WalkietalkieNotFoundException() {
	}
	
	public WalkietalkieNotFoundException(String s) {
		super(s);
	}
	
	public WalkietalkieNotFoundException(String s, Exception e) {
		super(s, e);
	}
}
