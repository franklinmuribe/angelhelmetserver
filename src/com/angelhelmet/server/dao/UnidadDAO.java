package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.UnidadNoEncontradaException;
import com.angelhelmet.server.dao.interfaces.IUnidadDAO;
import com.angelhelmet.server.persistencia.UnidadEB;

@Stateless
public class UnidadDAO implements IUnidadDAO
{
	@PersistenceContext
	private EntityManager _em;

	public UnidadDAO()
	{
	}

	@Override
	public UnidadEB getUnidad(String serie) throws UnidadNoEncontradaException, Exception
	{
		UnidadEB ret = null;

		TypedQuery<UnidadEB> query = _em.createNamedQuery("Unidad.getBySerie", UnidadEB.class);
		query.setParameter("serie", serie);
		List<UnidadEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new UnidadNoEncontradaException("No se encuentra la Unidad serie:" + serie);
		}
		return ret;
	}

	@Override
	public UnidadEB getUnidad(int id_unidad) throws UnidadNoEncontradaException, Exception
	{
		UnidadEB ret = null;
		TypedQuery<UnidadEB> query = _em.createNamedQuery("Unidad.getById", UnidadEB.class);
		query.setParameter("id", id_unidad);
		List<UnidadEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new UnidadNoEncontradaException("No se encuentra la Unidad id_unidad:" + id_unidad);
		}
		return ret;
	}
}
