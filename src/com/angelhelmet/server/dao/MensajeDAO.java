package com.angelhelmet.server.dao;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.interfaces.IMensajeDAO;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.rest.service.api.MensajeMasivo;
import com.angelhelmet.server.util.ServidorUtil;
import com.angelhelmet.server.util.TipoMensaje;

@Stateless
// @Deprecated
public class MensajeDAO implements IMensajeDAO {
	@PersistenceContext
	private EntityManager _em;

	public MensajeDAO() {
	}

	public Long getIdFirmaCasco() {
		Query q = _em.createNativeQuery("SELECT nextval('public.firmacasco_id_mensaje_seq')");
		return (Long) ((BigInteger) q.getSingleResult()).longValue();
	}

	public String getMensajeByCasco(Long firma) {
		String mensajeCasco = "";
		System.out.println("getMensajeByCasco:" + firma);
		try {
			Query q = _em.createNativeQuery(
					"select texto from mensajes where id_firmacasco in( select firma from logs where id_log in ( select id_log from alarmaseventos where firma = :firma))");
			q.setParameter("firma", firma);
			mensajeCasco = (String) q.getSingleResult();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mensajeCasco;
	}

	public Long getMensajeIdLogByCasco(Long firma) {
		Long mensajeIdLogCasco = 0L;
		try {
			Query q = _em.createNativeQuery("select id_log from alarmaseventos where firma = :firma");
			q.setParameter("firma", firma);
			mensajeIdLogCasco = ((BigInteger) q.getSingleResult()).longValue();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return mensajeIdLogCasco;
	}

	@Override
	public MensajeEB addMensaje(String texto, TipoMensaje tipo, Integer sessionId, Long unidadUmbrale,
			Long idFirmaCasco) {

		MensajeEB ret = null;
		Timestamp t = ServidorUtil.getTimestamp();

		String string_query = "insert into mensajes (texto, fecha_creacion, id_tipomensaje, id_sesion, id_unidadumbral, id_firmacasco) values (:texto, :fecha_creacion, :tipo , :id_sesion, :id_unidadumbral, :id_firmacasco) returning id_mensaje ";
		Query queryMensaje = _em.createNativeQuery(string_query);
		queryMensaje.setParameter("texto", texto).setParameter("fecha_creacion", t).setParameter("tipo",
				tipo.getTipoMensaje());
		queryMensaje.setParameter("id_sesion", sessionId).setParameter("id_unidadumbral", unidadUmbrale)
				.setParameter("id_firmacasco", idFirmaCasco);

		Long id = Long.valueOf(String.valueOf(queryMensaje.getResultList().get(0)));

//		queryMensaje.executeUpdate();
//		string_query = "select max(id_mensaje) as max from mensajes";
//		Query query = _em.createNativeQuery(string_query);
//		Long id = ((Long) query.getResultList().get(0));
		//// commented by Sanjay because no need of get it from db.
		// ret = getMensaje(id.longValue());
		ret = new MensajeEB();
		ret.setIdMensaje(id);
		return ret;
	}

	@Override
	public MensajeEB addMensajeMasivos(String texto, TipoMensaje tipo, MensajeMasivo mensajes, Long idFirmaCasco) {
		Long eventId = 0L;
		if (mensajes != null && mensajes.getFirma() != null && mensajes.getFirma() > 0) {
			String sql = "select id_evento from alarmaseventos where firma = :firma ";
			Query query = _em.createNativeQuery(sql);
			query.setParameter("firma", mensajes.getFirma());
			List lst = query.getResultList();
			if (!lst.isEmpty()) {
				eventId = Long.valueOf(String.valueOf(lst.get(0)));
			}
		}

		MensajeEB ret = null;
		Timestamp t = ServidorUtil.getTimestamp();

		String string_query = "";
		if (eventId > 0) {
			string_query = "insert into mensajes (texto, fecha_creacion,id_evento, id_tipomensaje, id_sesion, id_firmacasco) values (:texto, :fecha_creacion, :id_evento, :tipo, :id_sesion, :id_firmacasco ) returning id_mensaje ";

		} else {
			string_query = "insert into mensajes (texto, fecha_creacion, id_tipomensaje, id_sesion, id_firmacasco) values (:texto, :fecha_creacion, :tipo, :id_sesion, :id_firmacasco ) returning id_mensaje ";

		}
		
		if(mensajes == null) {
			string_query = "insert into mensajes (texto, fecha_creacion, id_tipomensaje, id_firmacasco) values (:texto, :fecha_creacion, :tipo, :id_firmacasco ) returning id_mensaje ";
		}
		Query queryMensaje = _em.createNativeQuery(string_query);
		queryMensaje.setParameter("texto", texto).setParameter("fecha_creacion", t).setParameter("tipo",
				tipo.getTipoMensaje());
		if (mensajes != null) {
			queryMensaje.setParameter("id_sesion", mensajes.getSessionId()).setParameter("id_firmacasco", idFirmaCasco);
		}else {
			queryMensaje.setParameter("id_firmacasco", -1);
		}

		if (eventId > 0) {
			queryMensaje.setParameter("id_evento", eventId);
		}
		
		
		// queryMensaje.executeUpdate();

		// string_query = "select max(id_mensaje) as max from mensajes";
		// Query query = _em.createNativeQuery(string_query);

		// BigInteger id = ((BigInteger) queryMensaje.getResultList().get(0));

		List lst = queryMensaje.getResultList();
		Long id = Long.valueOf(String.valueOf(lst.get(0)));
		//// commented by Sanjay due to no need of getting it from db.
		// ret = getMensaje(id.longValue());
		ret = new MensajeEB();
		ret.setIdMensaje(id);
		return ret;
	}

	public MensajeEB getMensaje(int id_mensaje) {
		MensajeEB ret = null;
		try {
			TypedQuery<MensajeEB> query = _em.createNamedQuery("Mensaje.getMensajeByID", MensajeEB.class)
					.setMaxResults(1);
			query.setParameter("id", id_mensaje);
			List<MensajeEB> lista = query.getResultList();
			if (lista.size() == 1) {
				ret = lista.get(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	@Deprecated
	public MensajeEB getMensaje(long id_mensaje) {
		MensajeEB ret = null;
		try {
			TypedQuery<MensajeEB> query = _em.createNamedQuery("Mensaje.getMensajeByID", MensajeEB.class)
					.setMaxResults(1);
			query.setParameter("id", id_mensaje);
			List<MensajeEB> lista = query.getResultList();
			if (lista.size() == 1) {
				ret = lista.get(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}

	public MensajeEB getMensajeConfiguracion(String serie) {
		MensajeEB ret = null;

		try {
			TypedQuery<MensajeEB> query = _em.createNamedQuery("Mensaje.getLastConfiguration", MensajeEB.class);
			String parametro = "|" + serie + "%";
			query.setParameter("serie", parametro);
			List<MensajeEB> lista = query.getResultList();
			if (lista.size() > 0) {
				ret = lista.get(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
}
