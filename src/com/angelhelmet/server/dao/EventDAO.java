package com.angelhelmet.server.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.dao.interfaces.IEventDAO;
import com.angelhelmet.server.datos.datosAlarma;
import com.angelhelmet.server.datos.datosComunicacion;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.persistencia.LogEB;
import com.angelhelmet.server.util.ModoFuncionamiento;

@Stateless
public class EventDAO implements IEventDAO {
	@PersistenceContext
	private EntityManager _em;

	public EventDAO() {
	}

	@Override
	public long addAlarmaEvento(datosComunicacion datos, datosLog datos_log,
			com.angelhelmet.server.datos.datosUnidadMapa _unidad_mapa, datosAlarma currAlarm) throws Exception {
		Long eventId = 0L;
		// datosAlarma currAlarm = _unidad_mapa.getCurrAlarm();
		/* If same alarm then ignore */
		if (currAlarm == null) {
			return 0L;
		}

		try {

			StringBuilder builder = new StringBuilder();
			builder.append(
					"INSERT INTO alarmaseventos (id_log, leido, coordenada_x, coordenada_y, id_alarma, fecha_evento, ");
			builder.append(" id_unidadmensaje, firma, id_tipoproceso, cancela, id_tipomapa, id_mapa, id_unidad");
			if (_unidad_mapa.getZona().getIdZona() > 0) {
				builder.append(", id_zona ");
			}

			builder.append(" ) VALUES (:id_log, :leido, :coordenada_x, :coordenada_y, :id_alarma,");
			builder.append(
					" :fecha_evento, :id_unidadmensaje, :firma, :id_tipoproceso, :cancela, :id_tipomapa, :id_mapa, :id_unidad ");

			if (_unidad_mapa.getZona().getIdZona() > 0) {
				builder.append(", :id_zona ");
			}

			builder.append(") returning id_evento ");

			Query query = _em.createNativeQuery(builder.toString());
			// TODO set all params

			query.setParameter("id_log", currAlarm.getIdLog()).setParameter("leido", false);
			if (datos.getModo() == ModoFuncionamiento.INTERIOR) {
				query.setParameter("coordenada_x", _unidad_mapa.getPosicion().getCoordenadas().getX())
						.setParameter("coordenada_y", _unidad_mapa.getPosicion().getCoordenadas().getY());
				query.setParameter("id_tipomapa", datos.getModo().getAlarma());
			} else {
				query.setParameter("coordenada_x", datos.getLatitud()).setParameter("coordenada_y",
						datos.getLongitud());
				query.setParameter("id_tipomapa", datos.getModo().getAlarma());
			}

			query.setParameter("id_alarma", currAlarm.getAlarma().getAlarma());
			query.setParameter("fecha_evento", datos_log.getLog().getFechaLog());
			if (_unidad_mapa.getZona().getIdZona() > 0) {
				query.setParameter("id_zona", _unidad_mapa.getZona().getIdZona());// todo

			}

			query.setParameter("id_unidadmensaje", _unidad_mapa.getDatosMensajeWS().getIdUnidadMensaje()); // todo

			Long cancelTo = currAlarm.getCancelaAlarma();

			query.setParameter("firma", currAlarm.getFirma());// todo

			if (currAlarm.getAlarma().getAlarma() == 18) // CANCELADA SISTEMA
			{
				query.setParameter("id_tipoproceso", 3); // CANCELADA POR
															// SISTEMA
			} else if (currAlarm.getAlarma().getAlarma() == 13)// ALARMA OK
			{
				query.setParameter("id_tipoproceso", 5); // HA CANCELADO

			} else {
				query.setParameter("id_tipoproceso", 4); // EN PROCESO
			}
			query.setParameter("id_mapa", _unidad_mapa.getIdMapa());
			query.setParameter("id_unidad", _unidad_mapa.getUnidad().getIdUnidad());

			Long eventIdPrev = 0L;
			if (cancelTo > 0) {
				String sqlEvent = "select id_evento from alarmaseventos WHERE firma = :firma ";
				Query queryEvent = _em.createNativeQuery(sqlEvent);
				queryEvent.setParameter("firma", cancelTo);
				List lstEvent = queryEvent.getResultList();

				if (!lstEvent.isEmpty()) {
					eventIdPrev = Long.valueOf(String.valueOf(lstEvent.get(0)));
				}

				query.setParameter("cancela", eventIdPrev);

				/*
				 * update to previous event if cancel to has value or alarm ok received
				 */
				String sqlUpdate = "UPDATE alarmaseventos SET fecha_respuesta = :fecha_respuesta, leido = :leido WHERE id_evento = :id_evento ";
				Query queryUpdate = _em.createNativeQuery(sqlUpdate);
				queryUpdate.setParameter("fecha_respuesta", new Timestamp(System.currentTimeMillis()));
				queryUpdate.setParameter("leido", true);
				queryUpdate.setParameter("id_evento", eventIdPrev);
				queryUpdate.executeUpdate();

			} else {
				// TODO how to set null value to bigint
				query.setParameter("cancela", 0);
			}
			/* execute query to insert into alarmaseventos */
			List lst = query.getResultList();
			eventId = Long.valueOf(String.valueOf(lst.get(0)));
			return eventId;
		} catch (Exception e) {

			e.printStackTrace();
			throw e;
		}
	}

	@Override
	public LogEB grabaLog(datosLog datos) throws ParseLogException, GrabaLogException, Exception {
		LogEB ret = parseLog(datos);
		if (ret != null) {
			try {
				_em.persist(ret);
				_em.flush();
			} catch (Exception ex) {
				throw new GrabaLogException("No se puede grabar el log", ex);
			}
		}
		return ret;
	}

	@Override
	@Deprecated
	public LogEB getUltimoLog(int id_unidad) throws Exception {
		LogEB ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLastLogByIdUnidad", LogEB.class).setMaxResults(1);
		query.setParameter("id_unidad", id_unidad);
		List<LogEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		}

		return ret;
	}

	@Override
	public List<LogEB> getLogsByFecha(Timestamp inicio, Timestamp fin, List<Integer> unidades, int id_mapa)
			throws Exception {
		List<LogEB> ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLogsByFechas", LogEB.class);
		query.setParameter("inicio", inicio, TemporalType.TIMESTAMP);
		query.setParameter("final", fin, TemporalType.TIMESTAMP);
		query.setParameter("unidades", unidades);
		query.setParameter("id_mapa", id_mapa);
		ret = query.getResultList();

		return ret;
	}

	@Override
	public LogEB getLogById(long id_log) throws Exception {
		LogEB ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLogByIdLog", LogEB.class);
		query.setParameter("id", id_log);
		List<LogEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		}

		return ret;
	}

	private LogEB parseLog(datosLog datos) throws ParseLogException {
		LogEB log = new LogEB();
		try {
			log.setIdUnidad(datos.getUnidad().getIdUnidad());
			log.setIdAlarma(datos.getAlarma().getIdAlarma());

			log.setIdMapa(datos.getMapa().getIdMapa());

			log.setTiempo(datos.getDatos().getTiempo());
			log.setGas(datos.getDatos().getGas());
			log.setSensorX(datos.getDatos().getSensorX());
			log.setSensorY(datos.getDatos().getSensorY());
			log.setSensorZ(datos.getDatos().getSensorZ());
			log.setLatitudC(datos.getDatos().getLatitudC());
			log.setLatitud(datos.getDatos().getLatitud());
			log.setLongitudC(datos.getDatos().getLongitudC());
			log.setLongitud(datos.getDatos().getLongitud());
			log.setExactitud(datos.getDatos().getExactitud());
			log.setSatelites(datos.getDatos().getSatelites());
			log.setMac1(datos.getDatos().getMac1());
			log.setMac2(datos.getDatos().getMac2());
			log.setMac3(datos.getDatos().getMac3());
			log.setMac4(datos.getDatos().getMac4());
			log.setRssi1(datos.getDatos().getRssi1());
			log.setRssi2(datos.getDatos().getRssi2());
			log.setRssi3(datos.getDatos().getRssi3());
			log.setRssi4(datos.getDatos().getRssi4());
			log.setFecha(datos.getDatos().getFecha());
			log.setFechaLog(datos.getDatos().getFechaLog());
			log.setBateria(datos.getDatos().getBateria());
			log.setMensaje(datos.getDatos().getMensaje().ordinal());
			log.setTemperatura(datos.getDatos().getTemperatura());
			log.setVolcado(datos.getDatos().isVolteo());
			log.setDebug(datos.getDatos().getDebug());
			// Walkie Talkie
			log.setCanalWt(datos.getDatos().getCanalWt());
			log.setModoWt(datos.getDatos().getModoWt());
			// Salud
			log.setHealthHeartbeat(datos.getDatos().getHealthHeartbeat());
			log.setHealthOxygenSp02(datos.getDatos().getHealthOxygenSp02());
			log.setHealthPressureDiastole(datos.getDatos().getHealthPressureDiastole());
			log.setHealthPressureSistole(datos.getDatos().getHealthPressureSistole());
			log.setHealthTemperature(datos.getDatos().getHealthTemperature());

		} catch (Exception ex) {
			log = null;
			throw new ParseLogException("No se puede parsear el log", ex);
		}
		return log;
	}
}
