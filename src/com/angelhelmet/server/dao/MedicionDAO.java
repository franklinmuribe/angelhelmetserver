package com.angelhelmet.server.dao;

import java.math.BigInteger;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.angelhelmet.server.dao.interfaces.IMedicionSaludDAO;
import com.angelhelmet.server.persistencia.TipoMedicionEB;
import com.angelhelmet.server.persistencia.UmbralesMedicionSaludEB;
import com.angelhelmet.server.persistencia.UnidadSaludEB;
import com.hcl.iot.smartworker.dto.MediconesDTO;

@Stateless
public class MedicionDAO implements IMedicionSaludDAO {
	@PersistenceContext
	private EntityManager _em;
	private Logger _log;

	public MedicionDAO() {
		_log = LogManager.getLogger(MedicionDAO.class);
	}

	@Override
	public MediconesDTO getMedicion(Integer unidadId) throws Exception {
		
		_log.error(" MedicionDAO getMedicion  implement ************ " );

		String sql = "  select   sum(l.health_temperature)/count(l.health_temperature) as promedio_temp,   "+
				"  sum(l.health_pressure_sistole)/count(l.health_pressure_sistole) as promedio_sistole ,  "+
				"  sum(l.health_pressure_diastole)/count(l.health_pressure_diastole) as promedio_diastole ,  "+
				"  sum(l.health_oxygen_sp02)/count(l.health_oxygen_sp02) as promedio_oxygen_sp02 ,  "+
				"  sum(l.health_heartbeat)/count(l.health_heartbeat) as promedio_heartbeat ,  "+
				"  (select    max(health_temperature) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+
				" 	) AS max_temp,  "+
				" 	(select    min(health_temperature) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+
				" 	) AS min_temp,"+
				" 	(select    max(health_pressure_sistole) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+
				" 	) AS max_sistole,"+
				" 	(select    min(health_pressure_sistole) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+
				" 	) AS min_sistole,"+
				" 	(select    max(health_pressure_diastole) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+
					 " "+
				" 	) AS max_diastole  "+
				" 	,  "+
				" 	(select    min(health_pressure_diastole) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  "+	 
				" 	) AS min_diastole  "+
				" 	,  "+
				" 	(select    max(health_oxygen_sp02) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  					 			 "+
				" 	) AS max_sp02,  "+
				" 	(select    min(health_oxygen_sp02) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  					 			 "+
				" 	) AS min_sp02,"+
				" 	(select    min(health_heartbeat) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  					 			 "+
				" 	) AS min_health_heartbeat,"+
				" 	(select    max(health_heartbeat) from logs where id_unidad =  " +  unidadId +
				" 	AND mensaje = 10  					 			 "+
				" 	) AS max_health_heartbeat								"+
				" 	from logs l 			  "+
				" 	where l.id_unidad =  " +  unidadId +
				" 	AND l.mensaje = 10 ";

		try {

			StringBuilder sqlQuery = new StringBuilder(sql);
			Session sessionObj = (Session) _em.getDelegate();
			_log.error(" getMedicion query ************ " + sqlQuery.toString());
			SQLQuery query = sessionObj.createSQLQuery(sqlQuery.toString());
//			Query q = _em.createNativeQuery(sqlQuery.toString());

			Object results = query.uniqueResult();
			MediconesDTO mediconesDTO = null;

			if (results != null) {
				mediconesDTO = new MediconesDTO();
				Object[] row = (Object[]) results;
				if (row[0] != null) {
					mediconesDTO.setPromedioTemp(Double.valueOf(String.valueOf(row[0])));
				}
				if (row[1] != null) {
					mediconesDTO.setPromedioSistole(Double.valueOf(String.valueOf(row[1])));
				}
				if (row[2] != null) {
					mediconesDTO.setPromedioDiastole(Double.valueOf(String.valueOf(row[2])));
				}
				if (row[3] != null) {
					mediconesDTO.setPromedioOxygenSp02(Double.valueOf(String.valueOf(row[3])));
				}
				if (row[4] != null) {
					mediconesDTO.setPromedioHeartbeat(Double.valueOf(String.valueOf(row[4])));
				}
				if (row[5] != null) {
					mediconesDTO.setMaxTemp(Double.valueOf(String.valueOf(row[5])));
				}
				if (row[6] != null) {
					mediconesDTO.setMinTemp(Double.valueOf(String.valueOf(row[6])));
				}
				if (row[7] != null) {
					mediconesDTO.setMaxSistole(Double.valueOf(String.valueOf(row[7])));
				}
				if (row[8] != null) {
					mediconesDTO.setMinSistole(Double.valueOf(String.valueOf(row[8])));
				}
				if (row[9] != null) {
					mediconesDTO.setMaxDiastole(Double.valueOf(String.valueOf(row[9])));
				}
				if (row[10] != null) {
					mediconesDTO.setMinDiastole(Double.valueOf(String.valueOf(row[10])));
				}
				if (row[11] != null) {
					mediconesDTO.setMaxSp02(Double.valueOf(String.valueOf(row[11])));
				}
				if (row[12] != null) {
					mediconesDTO.setMinSp02(Double.valueOf(String.valueOf(row[12])));
				}
				if (row[13] != null) {
					mediconesDTO.setMinHealthHeartbeat(Double.valueOf(String.valueOf(row[13])));
				}
				if (row[14] != null) {
					mediconesDTO.setMaxHealthHeartbeat(Double.valueOf(String.valueOf(row[14])));
				}

			}
			return mediconesDTO;

		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public UmbralesMedicionSaludEB getUmbralByUnidadSaludAndTipoMedicion(Integer tipoMedicionId, Integer unidadSaludId) throws  Exception {
		_log.error(" MedicionDAO getUmbralByUnidadSaludAndTipoMedicion  implement ************ " );

		String sql = " select * from umbrales_medicion_salud " +
				" where id_unidad_salud =  " + unidadSaludId + 
				" and id_tipo_medicion =  " + tipoMedicionId ;

		try {

			StringBuilder sqlQuery = new StringBuilder(sql);
			Session sessionObj = (Session) _em.getDelegate();
			_log.error(" getUmbralByUnidadSaludAndTipoMedicion query ************ " + sqlQuery.toString());
			SQLQuery query = sessionObj.createSQLQuery(sqlQuery.toString());
//			Query q = _em.createNativeQuery(sqlQuery.toString());

			Object results = query.uniqueResult();
			UmbralesMedicionSaludEB umbralesMedicionSaludEB = null;

			if (results != null) {
				umbralesMedicionSaludEB = new UmbralesMedicionSaludEB();
				Object[] row = (Object[]) results;
				if (row[0] != null) {
					umbralesMedicionSaludEB.setIdUmbralesMedicionSalud(Integer.valueOf(String.valueOf(row[0])));
				}
				if (row[1] != null) {
					TipoMedicionEB tipoMedicionEB = new TipoMedicionEB();
					tipoMedicionEB.setIdTipoMedicion(Integer.valueOf(String.valueOf(row[1])));
					umbralesMedicionSaludEB.setIdTipoMedicion(tipoMedicionEB);
				}
				if (row[2] != null) {
					UnidadSaludEB unidadSaludEB = new UnidadSaludEB();
					unidadSaludEB.setIdUnidadSalud(Integer.valueOf(String.valueOf(row[2])));
					umbralesMedicionSaludEB.setIdUnidadSalud(unidadSaludEB);
				}
				if (row[3] != null) {
					umbralesMedicionSaludEB.setMin(BigInteger.valueOf(Long.valueOf(String.valueOf(row[3]))));
				}
				if (row[4] != null) {
					umbralesMedicionSaludEB.setMax(BigInteger.valueOf(Long.valueOf(String.valueOf(row[4]))));
				}
				
			}
			return umbralesMedicionSaludEB;

		} catch (Exception e) {
			return null;
		}
	}

}
