package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GetEmpresaByIdException;
import com.angelhelmet.server.dao.excepciones.GetEmpresaByUnidadException;
import com.angelhelmet.server.dao.interfaces.IEmpresaDAO;
import com.angelhelmet.server.persistencia.EmpresaEB;

@Stateless
public class EmpresaDAO implements IEmpresaDAO
{
	@PersistenceContext
	private EntityManager _em;

	public EmpresaDAO()
	{
	}

	@Override
	@Deprecated
	public EmpresaEB getEmpresa(int id_empresa) throws GetEmpresaByIdException, Exception
	{
		EmpresaEB ret = null;

		TypedQuery<EmpresaEB> query = _em.createNamedQuery("Empresa.getEmpresaById", EmpresaEB.class);
		query.setParameter("id", id_empresa);
		List<EmpresaEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}

		return ret;
	}

	@Override
	public EmpresaEB getEmpresaPorUnidad(int id_unidad) throws GetEmpresaByUnidadException, Exception
	{
		EmpresaEB ret = null;

		TypedQuery<EmpresaEB> query = _em.createNamedQuery("Empresa.getEmpresaByUnidad", EmpresaEB.class);
		query.setParameter("id_unidad", id_unidad);
		List<EmpresaEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}

		return ret;
	}
}
