package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.MapaNoEncontradoException;
import com.angelhelmet.server.dao.interfaces.IMapaDAO;
import com.angelhelmet.server.persistencia.MapaEB;

@Stateless
public class MapaDAO implements IMapaDAO
{
	@PersistenceContext
	private EntityManager _em;

	public MapaDAO()
	{
	}

	@Override
	public MapaEB getMapa(int id) throws MapaNoEncontradoException, Exception
	{
		MapaEB ret = null;

		TypedQuery<MapaEB> query = _em.createNamedQuery("Mapa.getMapaById", MapaEB.class);
		query.setParameter("id", id);
		List<MapaEB> lista = query.getResultList();
		if(lista.size() > 1)
		{
			String msg="";
			for (MapaEB mapaEB : lista) {
				msg += "map id: " + mapaEB.getIdMapa();
			}
			
			System.out.println("##########ERROR::::::::::::::::::::: More than one mapa found: "+ msg +"################");
		}
		
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new MapaNoEncontradoException("Mapa no encontrado id_mapa: " + id);
		}
		return ret;
	}

	@Override
	public MapaEB getMapa(double latitud, double longitud) throws MapaNoEncontradoException, Exception
	{
		MapaEB ret = null;

		TypedQuery<MapaEB> query = _em.createNamedQuery("Mapa.getMapaExterior", MapaEB.class);
		//TODO HCL SANJAY
		query.setParameter("lon", longitud);
		query.setParameter("lat", latitud);
		List<MapaEB> lista = query.getResultList();
		if(lista.size() > 1)
		{
			String msg="";
			for (MapaEB mapaEB : lista) {
				msg += "map id: " + mapaEB.getIdMapa();
			}
			
			System.out.println("##########ERROR::::::::::::::::::::: More than one mapa found: "+ msg +"################");
		}
		//TODO HCL SANJAY 
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new MapaNoEncontradoException("Mapa no encontrado latidud: " + latitud + ", longitud: " + longitud);
		}
		return ret;
	}

}
