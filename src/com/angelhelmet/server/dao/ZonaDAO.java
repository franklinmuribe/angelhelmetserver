package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GetZonaException;
import com.angelhelmet.server.dao.excepciones.GetZonasMapaException;
import com.angelhelmet.server.dao.interfaces.IZonaDAO;
import com.angelhelmet.server.persistencia.ZonaEB;

@Stateless
public class ZonaDAO implements IZonaDAO
{
	@PersistenceContext
	private EntityManager _em;

	public ZonaDAO()
	{
	}

	@Override
	public List<ZonaEB> getZonasMapa(int id_mapa) throws GetZonasMapaException, Exception
	{
		List<ZonaEB> ret = null;
		try
		{
			TypedQuery<ZonaEB> query = _em.createNamedQuery("ZonaEB.getZonasByIdMapa", ZonaEB.class);
			query.setParameter("id_mapa", id_mapa);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			throw new GetZonasMapaException("No se pruede recuperar las zonas para el mapa " + id_mapa, ex);
		}
		return ret;
	}

	@Override
	public ZonaEB getZona(int id_zona) throws GetZonaException, Exception
	{
		ZonaEB ret = null;
		try
		{
			TypedQuery<ZonaEB> query = _em.createNamedQuery("ZonaEB.getZonaByIdMapa", ZonaEB.class);
			query.setParameter("id_mapa", id_zona);
			List<ZonaEB> lista = query.getResultList();
			if (lista.size() == 1)
			{
				ret = lista.get(0);
			}
		}
		catch (Exception ex)
		{
			throw new GetZonaException("No se puede recuperar la zona " + id_zona, ex);
		}
		return ret;
	}
}
