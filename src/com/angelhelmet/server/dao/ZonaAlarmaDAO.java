package com.angelhelmet.server.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GetPermisosZonaException;
import com.angelhelmet.server.dao.interfaces.IZonaAlarmaDAO;
import com.angelhelmet.server.datos.datosPermisoZona;
import com.angelhelmet.server.persistencia.ZonaAlarmaEB;

@Stateless
public class ZonaAlarmaDAO implements IZonaAlarmaDAO
{
	@PersistenceContext
	private EntityManager _em;
	
	public ZonaAlarmaDAO() {
	}

	@Override
	public List<datosPermisoZona> getPermisosZona(int id_zona, int id_unidad) throws GetPermisosZonaException, Exception
	{
		List<datosPermisoZona> ret = new ArrayList<datosPermisoZona>();
		try
		{
			TypedQuery<ZonaAlarmaEB> query = _em.createNamedQuery("ZonaAlarmaEB.findPermisosUnidad", ZonaAlarmaEB.class);
			query.setParameter("id_zona", id_zona);
			query.setParameter("id_unidad", id_unidad);
			List<ZonaAlarmaEB> lista = query.getResultList();

			for (ZonaAlarmaEB item : lista)
			{
				datosPermisoZona dpz = new datosPermisoZona();
				dpz.setAlarma(item.getAlarma().getIdAlarma());
				dpz.setIdZonaAlarma(item.getIdZonaAlarma());
				ret.add(dpz);
			}

		}
		catch (Exception ex)
		{
			ret = null;
			throw new GetPermisosZonaException("No se puede recuperar los permisos de la zona " + id_zona + " para la unidad " + id_unidad, ex); 
		}

		return ret;
	}

}
