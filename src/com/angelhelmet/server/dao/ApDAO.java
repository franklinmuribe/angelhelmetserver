package com.angelhelmet.server.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.ApNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.ApsAdyacentesNoEncontradosException;
import com.angelhelmet.server.dao.excepciones.ApsMapaNoEncontradosException;
import com.angelhelmet.server.dao.interfaces.IApDAO;
import com.angelhelmet.server.persistencia.ApEB;

@Stateless
public class ApDAO implements IApDAO
{
	@PersistenceContext
	private EntityManager _em;

	public ApDAO()
	{
	}

	@Override
	public ApEB getAp(String mac) throws ApNoEncontradoException, Exception
	{
		ApEB ret = null;

		TypedQuery<ApEB> query = _em.createNamedQuery("Ap.getApByMac", ApEB.class);
		query.setParameter("mac", mac);
		List<ApEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new ApNoEncontradoException("Ap no encontrado mac: " + mac);
		}
		return ret;
	}

	@Override
	public ApEB getAp(int id_ap) throws ApNoEncontradoException, Exception
	{
		ApEB ret = new ApEB();

		TypedQuery<ApEB> query = _em.createNamedQuery("Ap.getApByIdAP", ApEB.class);
		query.setParameter("id_ap", id_ap);
		List<ApEB> lista = query.getResultList();
		if (lista.size() == 1)
		{
			ret = lista.get(0);
		}
		else
		{
			throw new ApNoEncontradoException("Ap no encontrado id_ap: " + id_ap);
		}
		return ret;
	}

	@Override
	public List<ApEB> getAdyacentes(int id_ap) throws ApsAdyacentesNoEncontradosException, Exception
	{
		List<ApEB> ret = new ArrayList<ApEB>();

		TypedQuery<ApEB> query = _em.createNamedQuery("Ap.getApsAdyacentes", ApEB.class);
		query.setParameter("id_ap", id_ap);
		ret = query.getResultList();

		if (ret.size() == 0) { throw new ApsAdyacentesNoEncontradosException("No se han encontrados APs adyacentes para el ap " + id_ap); }

		return ret;
	}

	@Override
	public List<ApEB> getApsMapa(int id_mapa) throws ApsMapaNoEncontradosException, Exception
	{
		List<ApEB> ret = new ArrayList<ApEB>();

		TypedQuery<ApEB> query = _em.createNamedQuery("Mapa.getApsMapa", ApEB.class);
		query.setParameter("id_mapa", id_mapa);
		ret = query.getResultList();

		if (ret.size() == 0) { throw new ApsMapaNoEncontradosException("No se han encontrados APs para el mapa " + id_mapa); }

		return ret;
	}
}
