package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.interfaces.IOperarioEmpresaDAO;
import com.angelhelmet.server.persistencia.EmpresaEB;

@Stateless
@Deprecated
public class OperarioEmpresaDAO implements IOperarioEmpresaDAO
{
	@PersistenceContext
	private EntityManager _em;

	public OperarioEmpresaDAO()
	{
	}

	public EmpresaEB getEmpresaPorOperario(int id_operario)
	{
		EmpresaEB ret = null;
		try
		{
			TypedQuery<EmpresaEB> query = _em.createNamedQuery("OperarioEmpresa.getEmpresaByIdOperario", EmpresaEB.class);
			query.setParameter("id", id_operario);
			List<EmpresaEB> lista = query.getResultList();
			if (lista.size() == 1)
			{
				ret = lista.get(0);
			}
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

}
