package com.angelhelmet.server.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GrabaLogException;
import com.angelhelmet.server.dao.excepciones.ParseLogException;
import com.angelhelmet.server.dao.interfaces.ILogDAO;
import com.angelhelmet.server.datos.datosLog;
import com.angelhelmet.server.persistencia.LogEB;

@Stateless
public class LogDAO implements ILogDAO {
	@PersistenceContext
	private EntityManager _em;

	public LogDAO() {
	}

	@Override
	public LogEB grabaLog(datosLog datos) throws ParseLogException, GrabaLogException, Exception {
		LogEB ret = parseLog(datos);
		if (ret != null) {
			try {
				_em.persist(ret);
				_em.flush();
			} catch (Exception ex) {
				throw new GrabaLogException("No se puede grabar el log", ex);
			}
		}
		return ret;
	}

	@Override
	@Deprecated
	public LogEB getUltimoLog(int id_unidad) throws Exception {
		LogEB ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLastLogByIdUnidad", LogEB.class).setMaxResults(1);
		query.setParameter("id_unidad", id_unidad);
		List<LogEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		}

		return ret;
	}

	@Override
	public List<LogEB> getLogsByFecha(Timestamp inicio, Timestamp fin, List<Integer> unidades, int id_mapa)
			throws Exception {
		List<LogEB> ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLogsByFechas", LogEB.class);
		query.setParameter("inicio", inicio, TemporalType.TIMESTAMP);
		query.setParameter("final", fin, TemporalType.TIMESTAMP);
		query.setParameter("unidades", unidades);
		query.setParameter("id_mapa", id_mapa);
		ret = query.getResultList();

		return ret;
	}

	@Override
	public LogEB getLogById(long id_log) throws Exception {
		LogEB ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLogByIdLog", LogEB.class);
		query.setParameter("id", id_log);
		List<LogEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		}

		return ret;
	}

	@Override
	public LogEB getLogByFirma(Long firma) throws Exception {
		LogEB ret = null;

		TypedQuery<LogEB> query = _em.createNamedQuery("Log.getLogByFirma", LogEB.class).setMaxResults(1);
		query.setParameter("firma", firma);
		List<LogEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		}

		return ret;
	}

	private LogEB parseLog(datosLog datos) throws ParseLogException {
		LogEB log = new LogEB();
		try {
			log.setIdUnidad(datos.getUnidad().getIdUnidad());
			log.setIdAlarma(datos.getAlarma().getIdAlarma());

			log.setIdMapa(datos.getMapa().getIdMapa());

			log.setTiempo(datos.getDatos().getTiempo());
			log.setGas(datos.getDatos().getGas());
			log.setSensorX(datos.getDatos().getSensorX());
			log.setSensorY(datos.getDatos().getSensorY());
			log.setSensorZ(datos.getDatos().getSensorZ());
			log.setLatitudC(datos.getDatos().getLatitudC());
			log.setLatitud(datos.getDatos().getLatitud());
			log.setLongitudC(datos.getDatos().getLongitudC());
			log.setLongitud(datos.getDatos().getLongitud());
			log.setExactitud(datos.getDatos().getExactitud());
			log.setSatelites(datos.getDatos().getSatelites());
			log.setMac1(datos.getDatos().getMac1());
			log.setMac2(datos.getDatos().getMac2());
			log.setMac3(datos.getDatos().getMac3());
			log.setMac4(datos.getDatos().getMac4());
			log.setRssi1(datos.getDatos().getRssi1());
			log.setRssi2(datos.getDatos().getRssi2());
			log.setRssi3(datos.getDatos().getRssi3());
			log.setRssi4(datos.getDatos().getRssi4());
			log.setFecha(datos.getDatos().getFecha());
			log.setFechaLog(datos.getDatos().getFechaLog());
			log.setBateria(datos.getDatos().getBateria());
			log.setMensaje(datos.getDatos().getMensaje().ordinal());
			log.setTemperatura(datos.getDatos().getTemperatura());
			log.setVolcado(datos.getDatos().isVolteo());
			log.setDebug(datos.getDatos().getDebug());
			log.setFirma(datos.getDatos().getFirma());
			log.setRespuesta(datos.getDatos().getRespuesta());
			log.setRespuesta_sms(datos.getDatos().getRespuestaSms());

			// Walkie Talkie
			log.setCanalWt(datos.getDatos().getCanalWt());
			log.setModoWt(datos.getDatos().getModoWt());
			// Salud
			log.setHealthHeartbeat(datos.getDatos().getHealthHeartbeat());
			log.setHealthOxygenSp02(datos.getDatos().getHealthOxygenSp02());
			log.setHealthPressureDiastole(datos.getDatos().getHealthPressureDiastole());
			log.setHealthPressureSistole(datos.getDatos().getHealthPressureSistole());
			log.setHealthTemperature(datos.getDatos().getHealthTemperature());

		} catch (Exception ex) {
			log = null;
			throw new ParseLogException("No se puede parsear el log", ex);
		}
		return log;
	}
}
