package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.AddMensajeApagadoException;
import com.angelhelmet.server.dao.excepciones.GetMensajesPurgarException;
import com.angelhelmet.server.dao.excepciones.GetUnidadMensajeByIdException;
import com.angelhelmet.server.dao.excepciones.PurgarMensajeException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeNoEncontradoException;
import com.angelhelmet.server.dao.excepciones.UnidadMensajeSetLeidoException;
import com.angelhelmet.server.dao.interfaces.IUnidadMensajeDAO;
import com.angelhelmet.server.persistencia.MensajeEB;
import com.angelhelmet.server.persistencia.UnidadEB;
import com.angelhelmet.server.persistencia.UnidadMensajeEB;
import com.angelhelmet.server.util.EstadoMensajeSalida;
import com.angelhelmet.server.util.ServidorUtil;

@Stateless
public class UnidadMensajeDAO implements IUnidadMensajeDAO
{
	@PersistenceContext
	private EntityManager _em;

	public UnidadMensajeDAO()
	{
	}

	@Override
	public void addUnidadMensaje(MensajeEB mensaje, int id_unidad) {
		String string_query = "insert into unidadesmensajes (id_unidad, id_mensaje, id_mensajeestado) values (:unidad, :mensaje, 1)"; 
		_em.createNativeQuery(string_query).setParameter("unidad", id_unidad).setParameter("mensaje", mensaje.getIdMensaje()).executeUpdate();
	}
	
	@Override
	public UnidadMensajeEB getMensajeUnidad(int id_unidad) throws UnidadMensajeNoEncontradoException, Exception
	{
		UnidadMensajeEB ret = null;

		try
		{
			TypedQuery<UnidadMensajeEB> query = _em.createNamedQuery("UnidadMensaje.getMensajesByIdUnidad", UnidadMensajeEB.class).setMaxResults(1);
			query.setParameter("id_unidad", id_unidad);
			List<UnidadMensajeEB> lista = query.getResultList();
			if (lista.size() == 1)
			{
				ret = lista.get(0);
			}
		}
		catch (Exception ex)
		{
			throw new UnidadMensajeNoEncontradoException("No se puede recuperar el mensaje para la unidad " + id_unidad);
		}
		return ret;
	}

	@Override
	public boolean setLeido(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception
	{
		boolean ret = false;
		try
		{
			UnidadMensajeEB unidad_mensaje = null;

			TypedQuery<UnidadMensajeEB> query = _em.createNamedQuery("UnidadMensaje.getUnidadMensajeByIdUnidad", UnidadMensajeEB.class);
			query.setParameter("id_unidadmensaje", id_unidadmensaje);
			unidad_mensaje = query.getSingleResult();

			if (unidad_mensaje != null)
			{
				unidad_mensaje.setIdMensajeEstado(EstadoMensajeSalida.Leido.getEstadoMensage());
				unidad_mensaje.setFechaLectura(ServidorUtil.getTimestamp());

				try
				{
					_em.persist(unidad_mensaje);
					_em.flush();
					ret = true;
				}
				catch (Exception ex)
				{
					throw new UnidadMensajeSetLeidoException("No se puede poner a leido el mensaje, id_unidadmensaje=" + id_unidadmensaje, ex);
				}
			}
		}
		catch (Exception ex)
		{
			throw new GetUnidadMensajeByIdException("No se puede recuperar UnidadMensaje id = " + id_unidadmensaje, ex);
		}
		return ret;
	}
	
	@Override
	public boolean setEntregdoSalud(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception
	{
		boolean ret = false;
		try
		{
			UnidadMensajeEB unidad_mensaje = null;

			TypedQuery<UnidadMensajeEB> query = _em.createNamedQuery("UnidadMensaje.getUnidadMensajeByIdUnidad", UnidadMensajeEB.class);
			query.setParameter("id_unidadmensaje", id_unidadmensaje);
			unidad_mensaje = query.getSingleResult();

			if (unidad_mensaje != null)
			{
				unidad_mensaje.setIdMensajeEstado(EstadoMensajeSalida.EntregadoSalud.getEstadoMensage());
				unidad_mensaje.setFechaLectura(ServidorUtil.getTimestamp());

				try
				{
					_em.persist(unidad_mensaje);
					_em.flush();
					ret = true;
				}
				catch (Exception ex)
				{
					throw new UnidadMensajeSetLeidoException("No se puede poner a leido el mensaje, id_unidadmensaje=" + id_unidadmensaje, ex);
				}
			}
		}
		catch (Exception ex)
		{
			throw new GetUnidadMensajeByIdException("No se puede recuperar UnidadMensaje id = " + id_unidadmensaje, ex);
		}
		return ret;
	}
	@Override
	public boolean setConfirmacionSalud(long id_unidadmensaje) throws GetUnidadMensajeByIdException, UnidadMensajeSetLeidoException, Exception
	{
		boolean ret = false;
		try
		{
			UnidadMensajeEB unidad_mensaje = null;
			
			TypedQuery<UnidadMensajeEB> query = _em.createNamedQuery("UnidadMensaje.getUnidadMensajeByIdUnidad", UnidadMensajeEB.class);
			query.setParameter("id_unidadmensaje", id_unidadmensaje);
			unidad_mensaje = query.getSingleResult();
			
			if (unidad_mensaje != null)
			{
				unidad_mensaje.setIdMensajeEstado(EstadoMensajeSalida.ConfirmacionSalud.getEstadoMensage());
				unidad_mensaje.setFechaLectura(ServidorUtil.getTimestamp());
				unidad_mensaje.setConfirmacion(true);
				
				try
				{
					_em.persist(unidad_mensaje);
					_em.flush();
					ret = true;
				}
				catch (Exception ex)
				{
					throw new UnidadMensajeSetLeidoException("No se puede poner a leido el mensaje, id_unidadmensaje=" + id_unidadmensaje, ex);
				}
			}
		}
		catch (Exception ex)
		{
			throw new GetUnidadMensajeByIdException("No se puede recuperar UnidadMensaje id = " + id_unidadmensaje, ex);
		}
		return ret;
	}

	@Override
	public boolean purgarMensajesSMS(int id_unidad) throws GetMensajesPurgarException, PurgarMensajeException, Exception
	{
		boolean ret = false;
		try
		{
			TypedQuery<UnidadMensajeEB> query = _em.createNamedQuery("UnidadMensaje.purgarMensajes", UnidadMensajeEB.class);
			query.setParameter("id_unidad", id_unidad);
			List<UnidadMensajeEB> resultados = query.getResultList();

			try
			{
				for (UnidadMensajeEB um : resultados)
				{
					um.setIdMensajeEstado(EstadoMensajeSalida.Purgado.getEstadoMensage());
					um.setFechaLectura(ServidorUtil.getTimestamp());
					_em.flush();
				}
				ret = true;
			}
			catch (Exception ex)
			{
				throw new PurgarMensajeException("No se puede prugra los mensajes de la unidad " + id_unidad, ex);
			}

		}
		catch (Exception ex)
		{
			throw new GetMensajesPurgarException("No se puede recuperar los mensajes a purgar para la unidad " + id_unidad, ex);
		}
		return ret;
	}

	@Override
	@Deprecated
	public boolean addMensajeApagadoUnidad(UnidadEB unidad, MensajeEB mensaje) throws AddMensajeApagadoException, Exception
	{
		boolean ret = false;

		try
		{
			UnidadMensajeEB um = new UnidadMensajeEB();
			um.setIdMensajeEstado(1);
			um.setUnidad(unidad);
			um.setMensaje(mensaje);

			_em.persist(um);
			long id = um.getIdMensajeEstado();

			if (id > 0) ret = true;
		}
		catch (Exception ex)
		{
			throw new AddMensajeApagadoException("No se puede crear el mensaje de apagado para la unidad " + unidad.getIdUnidad(), ex);
		}

		return ret;
	}
}
