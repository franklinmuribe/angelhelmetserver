package com.angelhelmet.server.dao;

import java.sql.Timestamp;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.angelhelmet.server.dao.interfaces.IUmbralDAO;
import com.angelhelmet.server.datos.datosUmbral;
import com.angelhelmet.server.negocio.PeticionBean;
import com.angelhelmet.server.rest.service.api.Threshold;
import com.angelhelmet.server.util.ServidorUtil;

@Stateless
public class UmbralDAO implements IUmbralDAO {

	private Logger _log = LogManager.getLogger(UmbralDAO.class);

	@PersistenceContext
	private EntityManager _em;

	@Override
	public String addUmbral(datosUmbral datos) {

		String string_query = "INSERT INTO umbrales (impacto_x, impacto_y, impacto_z, "
				+ "gas, prealarma, estatico, casco_caido, tracking, apn, apn_user, apn_pwd, host_gprs, "
				+ "puerto, ssid, wifi_pwd, id_encriptacion, host_wifi, comunicacion_casset, "// canales,
				+ "telefono, id_tipoumbral, telefono_sos, hw_version) " + "VALUES"
				+ "(:impacto_x, :impacto_y, :impacto_z, :gas, :prealarma, :estatico, :casco_caido, "
				+ ":tracking, :apn, :apn_user, :apn_pwd, :host_gprs, :puerto, :ssid, :wifi_pwd, "
				+ ":id_encriptacion, :host_wifi, :comunicacion_casset, :telefono, :id_tipoumbral, :telefono_sos, :hw_version)";

		_em.createNativeQuery(string_query).setParameter("impacto_x", datos.getImpacto_x())
				.setParameter("impacto_y", datos.getImpacto_y()).setParameter("impacto_z", datos.getImpacto_z())
				.setParameter("gas", datos.getGas()).setParameter("prealarma", 1)
				.setParameter("estatico", datos.getEstatico()).setParameter("casco_caido", datos.isCasco_caido())
				.setParameter("tracking", datos.getTracking()).setParameter("apn", datos.getApn())
				.setParameter("apn_user", datos.getApn_user()).setParameter("apn_pwd", datos.getApn_pwd())
				.setParameter("host_gprs", datos.getHost_gprs()).setParameter("puerto", 18090)
				.setParameter("ssid", datos.getSsid()).setParameter("wifi_pwd", datos.getWifi_pwd())
				.setParameter("id_encriptacion", 1).setParameter("host_wifi", datos.getHost_wifi())
				.setParameter("comunicacion_casset", datos.isComunicacion_casset())
				.setParameter("telefono", datos.getTelefono())
				.setParameter("id_tipoumbral", datos.getId_tipoumbral() + 1)
				.setParameter("telefono_sos", datos.getTelefono()).setParameter("hw_version", 1).executeUpdate();

		string_query = "select max(id_umbral) as max from umbrales";
		Query query = _em.createNativeQuery(string_query);

		String id = query.getResultList().get(0).toString();
		System.out.println("ID" + id);

		return id;
	}

	@Override
	public Long addUnidadUmbral(Integer id_unidad, Integer sessionId, Integer id_umbral) {

		try {

			Timestamp t = ServidorUtil.getTimestamp();

			String string_query = "select id_unidadumbral from unidadesumbrales where id_unidad= :id_unidad and activo = true";
			Query query = _em.createNativeQuery(string_query);
			query.setParameter("id_unidad", id_unidad);
			List data = query.getResultList();
			Integer id_unidadumbral = 0;
			if (!data.isEmpty()) {
				id_unidadumbral = Integer.valueOf(String.valueOf(data.get(0)));
			}

			if (id_unidadumbral > 0) {
				String sqlUpdate = "update unidadesumbrales set fecha_baja = :fecha_baja , activo = false where id_unidadumbral = :id_unidadumbral";
				Query queryUpdate = _em.createNativeQuery(sqlUpdate);
				queryUpdate.setParameter("fecha_baja", t);
				queryUpdate.setParameter("id_unidadumbral", id_unidadumbral);
				queryUpdate.executeUpdate();
			}

			String sqlUnidadUmbral = "INSERT INTO unidadesumbrales (id_unidad, id_umbral, id_sesion, fecha_alta, sustituye, activo) VALUES (:id_unidad, :id_umbral, :id_sesion, :fecha_alta, :sustituye, true) RETURNING id_unidadumbral";

			Query queryUU = _em.createNativeQuery(sqlUnidadUmbral);
			queryUU.setParameter("id_unidad", id_unidad).setParameter("id_umbral", id_umbral).setParameter("id_sesion",
					sessionId);
			queryUU.setParameter("fecha_alta", t).setParameter("sustituye", id_unidadumbral);
			List lst = queryUU.getResultList();
			Long undadeUmbrale = Long.valueOf(String.valueOf(lst.get(0)));

			return undadeUmbrale;
		} catch (Exception e) {

			throw e;
		}

	}

	@Override
	public Threshold getThresholdConfiguration(Integer configId) {

		String string_query = "select cf.id_configuracion, "
				+ "	u.id_umbral, u.impacto_x, u.impacto_y, u.impacto_z, u.gas, u.prealarma, u.estatico, u.casco_caido, u.tracking, "
				+ "	u.apn, u.apn_user, u.apn_pwd, u.host_gprs, u.ssid, u.wifi_pwd, u.host_wifi, "
				+ "	u.comunicacion_casset, u.telefono, u.id_tipoumbral, tu.descripcion communicationMode, "
				+ " u.wt_unit, u.wt_channel, u.wt_blocked, u.coronavirus_proximity virus_proximity, u.ble_health, u.hw_version "
				+ " from configuraciones cf "
				+ " inner join umbralesconfiguraciones ucf on ucf.id_configuracion = cf.id_configuracion and ucf.activo=true "
				+ " inner join umbrales u on u.id_umbral = ucf.id_umbral  "
				+ " inner join 	tipoumbral tu on tu.id_tipoumbral = u.id_tipoumbral where cf.id_configuracion = :id_configuracion ";

		List<?> data = _em.createNativeQuery(string_query).setParameter("id_configuracion", configId).getResultList();

		Threshold configModel = null;

		for (Object object : data) {

			@SuppressWarnings("rawtypes")
			Object[] row = (Object[]) object;

			configModel = new Threshold();
			if (row[0] != null) {
				configModel.setConfigId(Integer.valueOf(String.valueOf(row[0])));
			}
			if (row[1] != null) {
				configModel.setThresholdId(Integer.valueOf(String.valueOf(row[1])));
			}
			if (row[2] != null) {
				configModel.setxImpact(Integer.valueOf(String.valueOf(row[2])));
			}
			if (row[3] != null) {
				configModel.setyImpact(Integer.valueOf(String.valueOf(row[3])));
			}
			if (row[4] != null) {
				configModel.setzImpact(Integer.valueOf(String.valueOf(row[4])));
			}
			if (row[5] != null) {
				configModel.setGasSensor(Integer.valueOf(String.valueOf(row[5])));
			}
			if (row[6] != null) {
				configModel.setPreAlarmTime(Integer.valueOf(String.valueOf(row[6])));
			}
			if (row[7] != null) {
				configModel.setStaticTime(Integer.valueOf(String.valueOf(row[7])));
			}
			if (row[8] != null) {
				configModel.setTurnedDown(Boolean.valueOf(String.valueOf(row[8])));
			}
			if (row[9] != null) {
				configModel.setTrackingTime(Integer.valueOf(String.valueOf(row[9])));
			}
			if (row[10] != null) {
				configModel.setGprsAPN(String.valueOf(row[10]));
			}
			if (row[11] != null) {
				configModel.setGprsUserName(String.valueOf(row[11]));
			}
			if (row[12] != null) {
				configModel.setGprskey(String.valueOf(row[12]));
			}
			if (row[13] != null) {
				configModel.setGprsHost(String.valueOf(row[13]));
			}
			if (row[14] != null) {
				configModel.setWifiSsid(String.valueOf(row[14]));
			}
			if (row[15] != null) {
				configModel.setWifiPassword(String.valueOf(row[15]));
			}
			if (row[16] != null) {
				configModel.setWifiHost(String.valueOf(row[16]));
			}
			if (row[17] != null) {
				configModel.setCasetUnit(Boolean.valueOf(String.valueOf(row[17])));
			}
			if (row[18] != null) {
				configModel.setGprsTelephone(String.valueOf(row[18]));
			}
			if (row[19] != null) {
				configModel.setCommunicationModeId(Integer.valueOf(String.valueOf(row[19])));
			}
			if (row[20] != null) {
				configModel.setCommunicationMode(String.valueOf(row[20]));
			}
			if (row[21] != null) {
				configModel.setWalkieTalkieUnit(Boolean.valueOf(String.valueOf(row[21])));
			}
			if (row[22] != null) {
				configModel.setWalkieTalkieChannel(Integer.valueOf(String.valueOf(row[22])));
			}
			if (row[23] != null) {
				configModel.setWalkieTalkieBlocked(Boolean.valueOf(String.valueOf(row[23])));
			}
			if (row[24] != null) {
				configModel.setCoronavirusProximityNotice(Boolean.valueOf(String.valueOf(row[24])));
			}
			if (row[25] != null) {
				configModel.setBleHealthCommunication(Boolean.valueOf(String.valueOf(row[25])));
			}
			if (row[26] != null) {
				configModel.setHwVersion(Integer.valueOf(String.valueOf(row[26])));
			}
		}
		return configModel;
	}

	@Override
	public datosUmbral getUmbralPorUnidad(Integer id_unidad) {
		datosUmbral datosUmbral = null;
		try {
			String string_query = "select um.id_umbral,um.impacto_x,um.impacto_y, um.impacto_z, "
					+ "um.gas,um.prealarma,um.estatico,um.casco_caido,um.tracking,um.apn,um.apn_user, "
					+ "um.apn_pwd,um.host_gprs,um.puerto,um.ssid,um.wifi_pwd,um.id_encriptacion, "
					+ "um.host_wifi,um.canales,um.comunicacion_casset,um.telefono,um.telefono_sos,um.id_tipoumbral, "
					+ "um.coronavirus_proximity,um.ble_health "
					+ "from unidades u join unidadesumbrales uu on u.id_unidad = uu.id_unidad "
					+ "JOIN umbrales um on um.id_umbral = uu.id_umbral " + "where u.id_unidad = :id_unidad ";

			Object data = _em.createNativeQuery(string_query).setParameter("id_unidad", id_unidad).getSingleResult();

			if (data != null) {
				datosUmbral = new datosUmbral();
				Object[] row = (Object[]) data;
				if (row[0] != null) {
					datosUmbral.setId_umbral(Integer.valueOf(String.valueOf(row[0])));
				}
				if (row[1] != null) {
					datosUmbral.setImpacto_x(Integer.valueOf(String.valueOf(row[1])));
				}
				if (row[2] != null) {
					datosUmbral.setImpacto_y(Integer.valueOf(String.valueOf(row[2])));
				}
				if (row[3] != null) {
					datosUmbral.setImpacto_z(Integer.valueOf(String.valueOf(row[3])));
				}
				if (row[4] != null) {
					datosUmbral.setGas(Integer.valueOf(String.valueOf(row[4])));
				}
				if (row[5] != null) {
					datosUmbral.setPrealarma(Integer.valueOf(String.valueOf(row[5])));
				}
				if (row[6] != null) {
					datosUmbral.setEstatico(Integer.valueOf(String.valueOf(row[6])));
				}
				if (row[7] != null) {
					datosUmbral.setCasco_caido(Boolean.valueOf(String.valueOf(row[7])));
				}
				if (row[8] != null) {
					datosUmbral.setTracking(Integer.valueOf(String.valueOf(row[8])));
				}
				if (row[9] != null) {
					datosUmbral.setApn(String.valueOf(row[9]));
				}
				if (row[10] != null) {
					datosUmbral.setApn_user(String.valueOf(row[10]));
				}
				if (row[11] != null) {
					datosUmbral.setApn_pwd(String.valueOf(row[11]));
				}
				if (row[12] != null) {
					datosUmbral.setHost_gprs(String.valueOf(row[12]));
				}
				if (row[13] != null) {
					datosUmbral.setPuerto(String.valueOf(row[13]));
				}
				if (row[14] != null) {
					datosUmbral.setSsid(String.valueOf(row[14]));
				}
				if (row[15] != null) {
					datosUmbral.setWifi_pwd(String.valueOf(row[15]));
				}
				if (row[16] != null) {
					datosUmbral.setId_encriptacion(Integer.valueOf(String.valueOf(row[16])));
				}
				if (row[17] != null) {
					datosUmbral.setHost_wifi(String.valueOf(row[17]));
				}
				if (row[18] != null) {
					datosUmbral.setCanales(Integer.valueOf(String.valueOf(row[18])));
				}
				if (row[19] != null) {
					datosUmbral.setComunicacion_casset(Boolean.valueOf(String.valueOf(row[19])));
				}
				if (row[20] != null) {
					datosUmbral.setTelefono(String.valueOf(row[20]));
				}
				if (row[21] != null) {
					datosUmbral.setTelefono_sos(String.valueOf(row[21]));
				}
				if (row[22] != null) {
					datosUmbral.setId_tipoumbral(Integer.valueOf(String.valueOf(row[22])));
				}
				if (row[23] != null) {
					datosUmbral.setCoronavirus_proximity(Boolean.valueOf(String.valueOf(row[23])));
				}
				if (row[24] != null) {
					datosUmbral.setBle_health(Boolean.valueOf(String.valueOf(row[24])));
				}

			}

		} catch (Exception e) {
			_log.info("No se puede recuperar el umbral : ************* " + e.getMessage());
			return datosUmbral;
		}

		return datosUmbral;
	}
}
