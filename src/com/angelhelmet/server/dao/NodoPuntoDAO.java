package com.angelhelmet.server.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GetNodosProyeccionException;
import com.angelhelmet.server.dao.interfaces.INodoPuntoDAO;
import com.angelhelmet.server.persistencia.NodoPuntoEB;

@Stateless
public class NodoPuntoDAO implements INodoPuntoDAO
{
	@PersistenceContext
	private EntityManager _em;

	public NodoPuntoDAO()
	{
	}

	public List<NodoPuntoEB> getNodosProyeccion(int id_ap1, int id_ap2) throws GetNodosProyeccionException, Exception
	{
		List<NodoPuntoEB> ret = new ArrayList<NodoPuntoEB>();

		String sql = "select np.* from nodospuntos np " +
				"		inner join vialesnodos vn on np.id_nodo = vn.id_nodo" +
				"	where vn.id_vial in " +
				"			(" +
				"				select id_vial from " +
				"				(" +
				"					select count(id_vial) as max, id_vial from vialesnodos vn " +
				"						inner join nodosaps naps on naps.id_nodo = vn.id_nodo" +
				"					where (naps.id_ap = :ap_min or naps.id_ap = :ap_max) group by id_vial order by max desc limit 1" +
				"				) c1" +
				"			) and np.id_tipopunto=3 order by np.id_nodopunto";

		int ap_max = 0;
		int ap_min = 0;

		if (id_ap1 <= id_ap2)
		{
			ap_max = id_ap2;
			ap_min = id_ap1;
		}
		else
		{
			ap_max = id_ap1;
			ap_min = id_ap2;
		}

		@SuppressWarnings("unchecked")
		TypedQuery<NodoPuntoEB> query = (TypedQuery<NodoPuntoEB>) _em.createNativeQuery(sql, NodoPuntoEB.class);
		query.setParameter("ap_max", ap_max);
		query.setParameter("ap_min", ap_min);
		ret = query.getResultList();

		if (ret.size() == 0) { throw new GetNodosProyeccionException("No se han encontrado nodos para proyectar ap1: " + id_ap1 + " ap2: " + id_ap2); }

		return ret;
	}

	@Override
	@Deprecated
	public List<NodoPuntoEB> getNodosPuntosExtremos(int id_nodo)
	{
		List<NodoPuntoEB> ret = null;
		try
		{
			TypedQuery<NodoPuntoEB> query = _em.createNamedQuery("NodoPuntoEB.getNodosPuntosExtremos", NodoPuntoEB.class);
			query.setParameter("id_nodo", id_nodo);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

}
