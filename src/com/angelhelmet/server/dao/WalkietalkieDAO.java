package com.angelhelmet.server.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.angelhelmet.server.dao.excepciones.WalkietalkieNotFoundException;
import com.angelhelmet.server.dao.interfaces.IWalkietalkieDAO;
import com.angelhelmet.server.datos.datosWalkietalkieAudio;
import com.angelhelmet.server.datos.datosWalkietalkieImei;

@Stateless
public class WalkietalkieDAO implements IWalkietalkieDAO {

	@PersistenceContext
	private EntityManager _em;

	public WalkietalkieDAO() {

	}

	@Override
	public datosWalkietalkieImei obtenerImeiWT(String imei) throws WalkietalkieNotFoundException, Exception {

		System.out.println("obtenerImeiWT");

		String sqlQuery = " select u.id_unidad, u.numero_serie, uo.id_operario, ub.wt_unit, ub.wt_channel, ub.wt_blocked, c.descripcion, c.fecha_alta "
				+ " from public.unidades u " + " join public.unidadesumbrales uu on (u.id_unidad = uu.id_unidad) "
				+ " join public.umbrales ub on (ub.id_umbral = uu.id_umbral) "
				+ " join public.umbralesconfiguraciones uc on (uc.id_umbral = ub.id_umbral) "
				+ " join public.configuraciones c on(uc.id_configuracion = c.id_configuracion) "
				+ " join public.unidadesoperarios uo on (uo.id_unidad = u.id_unidad) "
				+ " where u.activo and u.asignada and uu.activo and uc.activo and uo.activo "
				+ " and u.numero_serie = :imei " + " order by c.fecha_alta limit 1 ";

		datosWalkietalkieImei dataWalkietalkieImei = new datosWalkietalkieImei();

		List<Object[]> rows = null;

		try {

			Session sessionObj = (Session) _em.getDelegate();

			SQLQuery query = sessionObj.createSQLQuery(sqlQuery);

			query.setParameter("imei", imei);

			rows = query.list();

			if (!rows.isEmpty()) {

				for (Object[] row : rows) {
					dataWalkietalkieImei = new datosWalkietalkieImei();
					if (row[0] != null) {
						dataWalkietalkieImei.set_id_unidad(Long.parseLong(row[0].toString()));
						System.out.println("id_unidad: " + dataWalkietalkieImei.get_id_unidad());
					} else {
						System.out.println("id_unidad: ");
					}

					if (row[1] != null) {
						dataWalkietalkieImei.set_nmro_serie(row[1].toString());
						System.out.println("numero_serie: " + dataWalkietalkieImei.get_nmro_serie());
					} else {
						System.out.println("numero_serie: ");
					}

					if (row[2] != null) {
						dataWalkietalkieImei.set_id_operario(Long.parseLong(row[2].toString()));
						System.out.println("id_operario: " + dataWalkietalkieImei.get_id_operario());
					} else {
						System.out.println("id_operario: ");
					}

					if (row[3] != null) {
						dataWalkietalkieImei.set_wt_unit(Boolean.valueOf(row[3].toString()));
						System.out.println("wt_unit: " + dataWalkietalkieImei.is_wt_unit());
					} else {
						System.out.println("wt_unit: ");
					}
					if (row[4] != null) {
						dataWalkietalkieImei.set_wt_channel(Integer.parseInt(row[4].toString()));
						System.out.println("wt_channel: " + dataWalkietalkieImei.get_wt_channel());
					} else {
						System.out.println("wt_channel: ");
					}

					if (row[5] != null) {
						dataWalkietalkieImei.set_wt_blocked(Boolean.valueOf(row[5].toString()));
						System.out.println("wt_blocked: " + dataWalkietalkieImei.is_wt_blocked());
					} else {
						System.out.println("wt_blocked: ");
					}
					if (row[6] != null) {
						dataWalkietalkieImei.set_descripcion(row[6].toString());
						System.out.println("descripcion: " + dataWalkietalkieImei.get_descripcion());
					} else {
						System.out.println("descripcion: ");
					}

					if (row[7] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[7].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieImei.set_fecha_alta(timestamp);
							System.out.println("fecha_alta: " + dataWalkietalkieImei.get_fecha_alta());
						} catch (Exception e) {
							dataWalkietalkieImei.set_fecha_alta(null);
						}

					} else {
						System.out.println("fecha_alta: ");
						dataWalkietalkieImei.set_fecha_alta(null);
					}
				}
			}

		} catch (Exception e) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Imei: " + e.getLocalizedMessage());
		}

		if (rows == null || rows.isEmpty()) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Imei: Sin Información");
		}

		return dataWalkietalkieImei;
	}

	@Override
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje)
			throws WalkietalkieNotFoundException, Exception {

		System.out.println("obtenerAudioWT");

		String sqlQuery = " select um.id_mensaje_helmet_wt, um.id_mensajeaudio_wt, ew.id_estadomensaje_wt, ew.estadomensaje_wt, "
				+ " ma.id_tipomensajeaudio_wt, tma.tipomensajeaudio_wt, ma.nombre_archivo, um.id_unidad, u.numero_serie, uo.id_operario,  "
				+ " um.firma, um.reenvio, um.fecha_creacion, um.fecha_envio, um.fecha_confirmacion, um.fecha_error, um.id_transaccion, um.id_estadoenvio_wt "
				+ " from public.unidadmensaje_wt um "
				+ " join estadomensaje_wt ew on (ew.id_estadomensaje_wt = um.id_estadomensaje_wt) "
				+ " join public.mensajeaudio_wt ma on (ma.id_mensajeaudio_wt = um.id_mensajeaudio_wt) "
				+ " join public.tipomensajeaudio_wt tma on (tma.id_tipomensajeaudio_wt = ma.id_tipomensajeaudio_wt) "
				+ " join public.unidades u on (u.id_unidad = um.id_unidad) "
				+ " join public.unidadesoperarios uo on (uo.id_unidad = u.id_unidad) " + " where "
				+ " u.activo and u.asignada and um.id_estadomensaje_wt = :id_estadomensaje_wt and ma.id_tipomensajeaudio_wt = 'IN' "
				+ " and u.numero_serie = :imei " + " order by um.fecha_creacion limit 1 ";

		datosWalkietalkieAudio dataWalkietalkieAudio = new datosWalkietalkieAudio();

		List<Object[]> rows = null;
		try {
			/*
			 * um.id_mensaje_helmet_wt um.id_mensajeaudio_wt ew.id_estadomensaje_wt
			 * ew.estadomensaje_wt ma.id_tipomensajeaudio_wt tma.tipomensajeaudio_wt
			 * ma.nombre_archivo um.id_unidad u.numero_serie uo.id_operario um.firma
			 * um.fecha_creacion um.fecha_envio um.fecha_confirmacion um.fecha_error
			 * um.id_transaccion um.id_estadoenvio_wt
			 * 
			 */

			Session sessionObj = (Session) _em.getDelegate();

			SQLQuery query = sessionObj.createSQLQuery(sqlQuery);

			query.setParameter("imei", imei);
			query.setParameter("id_estadomensaje_wt", idEstadoMensaje);

			rows = query.list();

			if (!rows.isEmpty()) {

				for (Object[] row : rows) {
					dataWalkietalkieAudio = new datosWalkietalkieAudio();
					if (row[0] != null) {
						dataWalkietalkieAudio.set_id_mensaje_helmet_wt(Long.parseLong(row[0].toString()));
						System.out.println("id_mensaje_helmet_wt: " + dataWalkietalkieAudio.get_id_mensaje_helmet_wt());
					} else {
						System.out.println("id_mensaje_helmet_wt: ");
					}

					if (row[1] != null) {
						dataWalkietalkieAudio.set_id_mensajeaudio_wt(Long.parseLong(row[1].toString()));
						System.out.println("id_mensajeaudio_wt: " + dataWalkietalkieAudio.get_id_mensajeaudio_wt());
					} else {
						System.out.println("id_mensajeaudio_wt: ");
					}

					if (row[2] != null) {
						dataWalkietalkieAudio.set_id_estadomensaje_wt(Long.parseLong(row[2].toString()));
						System.out.println("id_estadomensaje_wt: " + dataWalkietalkieAudio.get_id_estadomensaje_wt());
					} else {
						System.out.println("id_estadomensaje_wt: ");
					}

					if (row[3] != null) {
						dataWalkietalkieAudio.set_estadomensaje_wt(row[3].toString());
						System.out.println("estadomensaje_wt: " + dataWalkietalkieAudio.get_estadomensaje_wt());
					} else {
						System.out.println("estadomensaje_wt: ");
					}

					if (row[4] != null) {
						dataWalkietalkieAudio.set_id_tipomensajeaudio_wt(row[4].toString());
						System.out.println(
								"id_tipomensajeaudio_wt: " + dataWalkietalkieAudio.get_id_tipomensajeaudio_wt());
					} else {
						System.out.println("id_tipomensajeaudio_wt: ");
					}

					if (row[5] != null) {
						dataWalkietalkieAudio.set_tipomensajeaudio_wt(row[5].toString());
						System.out.println("tipomensajeaudio_wt: " + dataWalkietalkieAudio.get_tipomensajeaudio_wt());
					} else {
						System.out.println("tipomensajeaudio_wt: ");
					}
					if (row[6] != null) {
						dataWalkietalkieAudio.set_nombre_archivo(row[6].toString());
						System.out.println("nombre_archivo: " + dataWalkietalkieAudio.get_nombre_archivo());
					} else {
						System.out.println("nombre_archivo: ");
					}

					if (row[7] != null) {
						dataWalkietalkieAudio.set_id_unidad(Long.parseLong(row[7].toString()));
						System.out.println("id_unidad: " + dataWalkietalkieAudio.get_id_unidad());
					} else {
						System.out.println("id_unidad: ");
					}

					if (row[8] != null) {
						dataWalkietalkieAudio.set_numero_serie(row[8].toString());
						System.out.println("numero_serie: " + dataWalkietalkieAudio.get_numero_serie());
					} else {
						System.out.println("numero_serie: ");
					}

					if (row[9] != null) {
						dataWalkietalkieAudio.set_id_operario(Long.parseLong(row[9].toString()));
						System.out.println("id_operario: " + dataWalkietalkieAudio.get_id_operario());
					} else {
						System.out.println("id_operario: ");
					}

					if (row[10] != null) {
						dataWalkietalkieAudio.set_firma(Long.parseLong(row[10].toString()));
						System.out.println("firma: " + dataWalkietalkieAudio.get_firma());
					} else {
						System.out.println("firma: ");
					}

					if (row[11] != null) {
						dataWalkietalkieAudio.set_reenvio(Integer.parseInt(row[11].toString()));
						System.out.println("reenvio: " + dataWalkietalkieAudio.get_reenvio());
					} else {
						System.out.println("reenvio: ");
					}

					if (row[12] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[12].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_creacion(timestamp);
							System.out.println("fecha_creacion: " + dataWalkietalkieAudio.get_fecha_creacion());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_creacion(null);
						}

					} else {
						System.out.println("fecha_creacion: ");
						dataWalkietalkieAudio.set_fecha_creacion(null);
					}

					if (row[13] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[13].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_envio(timestamp);
							System.out.println("fecha_envio: " + dataWalkietalkieAudio.get_fecha_envio());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_envio(null);
						}

					} else {
						System.out.println("fecha_envio: ");
						dataWalkietalkieAudio.set_fecha_envio(null);
					}

					if (row[14] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[14].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_confirmacion(timestamp);
							System.out.println("fecha_confirmacion: " + dataWalkietalkieAudio.get_fecha_confirmacion());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_confirmacion(null);
						}

					} else {
						System.out.println("fecha_confirmacion: ");
						dataWalkietalkieAudio.set_fecha_confirmacion(null);
					}

					if (row[15] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[15].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_error(timestamp);
							System.out.println("fecha_error: " + dataWalkietalkieAudio.get_fecha_error());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_error(null);
						}

					} else {
						System.out.println("fecha_error: ");
						dataWalkietalkieAudio.set_fecha_error(null);
					}

					if (row[16] != null) {
						dataWalkietalkieAudio.set_id_transaccion(Long.parseLong(row[16].toString()));
						System.out.println("id_transaccion: " + dataWalkietalkieAudio.get_id_transaccion());
					} else {
						System.out.println("id_transaccion: ");
					}

					if (row[17] != null) {
						dataWalkietalkieAudio.set_id_estadoenvio_wt(Long.parseLong(row[17].toString()));
						System.out.println("id_estadoenvio_wt: " + dataWalkietalkieAudio.get_id_estadoenvio_wt());
					} else {
						System.out.println("id_estadoenvio_wt: ");
					}

				}
			}

		} catch (Exception e) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Audio: " + e.getLocalizedMessage());
		}

		if (rows == null || rows.isEmpty()) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Audio: Sin Información");
		}

		return dataWalkietalkieAudio;
	}

	@Override
	public datosWalkietalkieAudio obtenerAudioWT(String imei, Long idEstadoMensaje, Long firmaCasco)
			throws WalkietalkieNotFoundException, Exception {

		System.out.println("obtenerAudioWT");

		String sqlQuery = " select um.id_mensaje_helmet_wt, um.id_mensajeaudio_wt, ew.id_estadomensaje_wt, ew.estadomensaje_wt, "
				+ " ma.id_tipomensajeaudio_wt, tma.tipomensajeaudio_wt, ma.nombre_archivo, um.id_unidad, u.numero_serie, uo.id_operario,  "
				+ " um.firma, um.reenvio, um.fecha_creacion, um.fecha_envio, um.fecha_confirmacion, um.fecha_error, um.id_transaccion, um.id_estadoenvio_wt "
				+ " from public.unidadmensaje_wt um "
				+ " join estadomensaje_wt ew on (ew.id_estadomensaje_wt = um.id_estadomensaje_wt) "
				+ " join public.mensajeaudio_wt ma on (ma.id_mensajeaudio_wt = um.id_mensajeaudio_wt) "
				+ " join public.tipomensajeaudio_wt tma on (tma.id_tipomensajeaudio_wt = ma.id_tipomensajeaudio_wt) "
				+ " join public.unidades u on (u.id_unidad = um.id_unidad) "
				+ " join public.unidadesoperarios uo on (uo.id_unidad = u.id_unidad) " + " where "
				+ " u.activo and u.asignada and um.id_estadomensaje_wt = :id_estadomensaje_wt and ma.id_tipomensajeaudio_wt = 'IN' "
				//+ " and u.numero_serie = :imei and um.firma =:firma " + " order by um.fecha_creacion limit 1 ";
		        + " and u.numero_serie = :imei  " + " order by um.fecha_creacion limit 1 ";

		datosWalkietalkieAudio dataWalkietalkieAudio = new datosWalkietalkieAudio();

		List<Object[]> rows = null;
		try {

			Session sessionObj = (Session) _em.getDelegate();

			SQLQuery query = sessionObj.createSQLQuery(sqlQuery);

			query.setParameter("imei", imei);
			query.setParameter("id_estadomensaje_wt", idEstadoMensaje);
			//query.setParameter("firma", firmaCasco);

			rows = query.list();

			if (!rows.isEmpty()) {

				for (Object[] row : rows) {
					dataWalkietalkieAudio = new datosWalkietalkieAudio();
					if (row[0] != null) {
						dataWalkietalkieAudio.set_id_mensaje_helmet_wt(Long.parseLong(row[0].toString()));
						System.out.println("id_mensaje_helmet_wt: " + dataWalkietalkieAudio.get_id_mensaje_helmet_wt());
					} else {
						System.out.println("id_mensaje_helmet_wt: ");
					}

					if (row[1] != null) {
						dataWalkietalkieAudio.set_id_mensajeaudio_wt(Long.parseLong(row[1].toString()));
						System.out.println("id_mensajeaudio_wt: " + dataWalkietalkieAudio.get_id_mensajeaudio_wt());
					} else {
						System.out.println("id_mensajeaudio_wt: ");
					}

					if (row[2] != null) {
						dataWalkietalkieAudio.set_id_estadomensaje_wt(Long.parseLong(row[2].toString()));
						System.out.println("id_estadomensaje_wt: " + dataWalkietalkieAudio.get_id_estadomensaje_wt());
					} else {
						System.out.println("id_estadomensaje_wt: ");
					}

					if (row[3] != null) {
						dataWalkietalkieAudio.set_estadomensaje_wt(row[3].toString());
						System.out.println("estadomensaje_wt: " + dataWalkietalkieAudio.get_estadomensaje_wt());
					} else {
						System.out.println("estadomensaje_wt: ");
					}

					if (row[4] != null) {
						dataWalkietalkieAudio.set_id_tipomensajeaudio_wt(row[4].toString());
						System.out.println(
								"id_tipomensajeaudio_wt: " + dataWalkietalkieAudio.get_id_tipomensajeaudio_wt());
					} else {
						System.out.println("id_tipomensajeaudio_wt: ");
					}

					if (row[5] != null) {
						dataWalkietalkieAudio.set_tipomensajeaudio_wt(row[5].toString());
						System.out.println("tipomensajeaudio_wt: " + dataWalkietalkieAudio.get_tipomensajeaudio_wt());
					} else {
						System.out.println("tipomensajeaudio_wt: ");
					}
					if (row[6] != null) {
						dataWalkietalkieAudio.set_nombre_archivo(row[6].toString());
						System.out.println("nombre_archivo: " + dataWalkietalkieAudio.get_nombre_archivo());
					} else {
						System.out.println("nombre_archivo: ");
					}

					if (row[7] != null) {
						dataWalkietalkieAudio.set_id_unidad(Long.parseLong(row[7].toString()));
						System.out.println("id_unidad: " + dataWalkietalkieAudio.get_id_unidad());
					} else {
						System.out.println("id_unidad: ");
					}

					if (row[8] != null) {
						dataWalkietalkieAudio.set_numero_serie(row[8].toString());
						System.out.println("numero_serie: " + dataWalkietalkieAudio.get_numero_serie());
					} else {
						System.out.println("numero_serie: ");
					}

					if (row[9] != null) {
						dataWalkietalkieAudio.set_id_operario(Long.parseLong(row[9].toString()));
						System.out.println("id_operario: " + dataWalkietalkieAudio.get_id_operario());
					} else {
						System.out.println("id_operario: ");
					}

					if (row[10] != null) {
						dataWalkietalkieAudio.set_id_operario(Long.parseLong(row[10].toString()));
						System.out.println("firma: " + dataWalkietalkieAudio.get_id_operario());
					} else {
						System.out.println("firma: ");
					}

					if (row[11] != null) {
						dataWalkietalkieAudio.set_reenvio(Integer.parseInt(row[11].toString()));
						System.out.println("reenvio: " + dataWalkietalkieAudio.get_reenvio());
					} else {
						System.out.println("reenvio: ");
					}

					if (row[12] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[12].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_creacion(timestamp);
							System.out.println("fecha_creacion: " + dataWalkietalkieAudio.get_fecha_creacion());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_creacion(null);
						}

					} else {
						System.out.println("fecha_creacion: ");
						dataWalkietalkieAudio.set_fecha_creacion(null);
					}

					if (row[13] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[13].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_envio(timestamp);
							System.out.println("fecha_envio: " + dataWalkietalkieAudio.get_fecha_envio());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_envio(null);
						}

					} else {
						System.out.println("fecha_envio: ");
						dataWalkietalkieAudio.set_fecha_envio(null);
					}

					if (row[14] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[14].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_confirmacion(timestamp);
							System.out.println("fecha_confirmacion: " + dataWalkietalkieAudio.get_fecha_confirmacion());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_confirmacion(null);
						}

					} else {
						System.out.println("fecha_confirmacion: ");
						dataWalkietalkieAudio.set_fecha_confirmacion(null);
					}

					if (row[15] != null) {
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[15].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							dataWalkietalkieAudio.set_fecha_error(timestamp);
							System.out.println("fecha_error: " + dataWalkietalkieAudio.get_fecha_error());
						} catch (Exception e) {
							dataWalkietalkieAudio.set_fecha_error(null);
						}

					} else {
						System.out.println("fecha_error: ");
						dataWalkietalkieAudio.set_fecha_error(null);
					}

					if (row[16] != null) {
						dataWalkietalkieAudio.set_id_transaccion(Long.parseLong(row[16].toString()));
						System.out.println("id_transaccion: " + dataWalkietalkieAudio.get_id_transaccion());
					} else {
						System.out.println("id_transaccion: ");
					}

					if (row[17] != null) {
						dataWalkietalkieAudio.set_id_estadoenvio_wt(Long.parseLong(row[17].toString()));
						System.out.println("id_estadoenvio_wt: " + dataWalkietalkieAudio.get_id_estadoenvio_wt());
					} else {
						System.out.println("id_estadoenvio_wt: ");
					}

				}
			}

		} catch (Exception e) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Audio-Firma: " + e.getLocalizedMessage());
		}

		if (rows == null || rows.isEmpty()) {
			throw new WalkietalkieNotFoundException("Walkietalkie-Audio-Firma: Sin Información");
		}

		return dataWalkietalkieAudio;
	}

	@Override
	// @Transactional
	public Long actualizarEstadoWT(Long idMensajeHelmet, Long idEstadoMensaje, Long firma)
			throws WalkietalkieNotFoundException, Exception {

		System.out.println("actualizarEstadoWT");

		System.out.println("idMensajeHelmet: " + idMensajeHelmet);
		System.out.println("idEstadoMensaje: " + idEstadoMensaje);
		System.out.println("firma          : " + firma);
		try {

			// _em.getTransaction().begin();

			String query = "";

			if (idEstadoMensaje == 3) {

				query = "update public.unidadmensaje_wt set fecha_envio = NOW(), reenvio = reenvio + 1, id_estadomensaje_wt = :idEstadoMensaje, firma = :firma where id_mensaje_helmet_wt = :idMensajeHelmet";

				Query queryMensaje = _em.createNativeQuery(query);
				queryMensaje.setParameter("idEstadoMensaje", idEstadoMensaje);
				queryMensaje.setParameter("firma", firma);
				queryMensaje.setParameter("idMensajeHelmet", idMensajeHelmet);
				queryMensaje.executeUpdate();

			} else if (idEstadoMensaje == 4) {

				query = "update public.unidadmensaje_wt set fecha_confirmacion = NOW(), id_estadomensaje_wt = :idEstadoMensaje where id_mensaje_helmet_wt = :idMensajeHelmet";

				Query queryMensaje = _em.createNativeQuery(query);
				queryMensaje.setParameter("idEstadoMensaje", idEstadoMensaje);
				queryMensaje.setParameter("idMensajeHelmet", idMensajeHelmet);
				queryMensaje.executeUpdate();

			} else if (idEstadoMensaje == 6) {
				query = "update public.unidadmensaje_wt set fecha_error = NOW(), reenvio = reenvio + 1, id_estadomensaje_wt = :idEstadoMensaje, firma = :firma where id_mensaje_helmet_wt = :idMensajeHelmet";

				Query queryMensaje = _em.createNativeQuery(query);
				queryMensaje.setParameter("idEstadoMensaje", idEstadoMensaje);
				queryMensaje.setParameter("firma", firma);
				queryMensaje.setParameter("idMensajeHelmet", idMensajeHelmet);
				queryMensaje.executeUpdate();
			}

			// _em.getTransaction().commit();

			return 0L;
		} catch (Exception e) {
			e.printStackTrace();
			// _em.getTransaction().rollback();
			throw new WalkietalkieNotFoundException("Walkietalkie-Update: " + e.getLocalizedMessage());
		}
	}

}
