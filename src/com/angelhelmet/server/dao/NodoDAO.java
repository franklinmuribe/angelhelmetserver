package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.interfaces.INodoDAO;
import com.angelhelmet.server.persistencia.NodoEB;

@Stateless
@Deprecated
public class NodoDAO implements INodoDAO
{
	@PersistenceContext
	private EntityManager _em;

	public NodoDAO() {
	}

	public List<NodoEB> getNodos()
	{
		List<NodoEB> ret = null;
		try
		{
			TypedQuery<NodoEB> query = _em.createNamedQuery("NodoEB.findAll", NodoEB.class);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
		return ret;
	}

}
