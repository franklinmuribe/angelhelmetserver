package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.interfaces.INodoApDAO;
import com.angelhelmet.server.persistencia.NodoApEB;

@Stateless
@Deprecated
public class NodoApDAO implements INodoApDAO
{
	@PersistenceContext
	private EntityManager _em;

	public NodoApDAO() {
	}

	public List<NodoApEB> getNodosAsociados(int id_ap1, int id_ap2)
	{
		List<NodoApEB> ret = null;
		try
		{
			TypedQuery<NodoApEB> query = _em.createNamedQuery("NodoApEB.getNodosAsociados", NodoApEB.class);
			query.setParameter("id_ap1", id_ap1);
			query.setParameter("id_ap2", id_ap2);
			ret = query.getResultList();
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}

		return ret;
	}

}
