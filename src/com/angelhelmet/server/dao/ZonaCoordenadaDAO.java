package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.excepciones.GetZonaCoordenadas;
import com.angelhelmet.server.dao.excepciones.ZonaSinCoordenadasException;
import com.angelhelmet.server.dao.interfaces.IZonaCoordenadaDAO;
import com.angelhelmet.server.persistencia.ZonaCoordenadaEB;

@Stateless
public class ZonaCoordenadaDAO implements IZonaCoordenadaDAO
{
	@PersistenceContext
	private EntityManager _em;

	public ZonaCoordenadaDAO()
	{
	}

	public List<ZonaCoordenadaEB> getZonaCoordenadas(int id_zona) throws GetZonaCoordenadas, ZonaSinCoordenadasException, Exception
	{
		List<ZonaCoordenadaEB> ret = null;

		try
		{
			TypedQuery<ZonaCoordenadaEB> query = _em.createNamedQuery("ZonaCoordenadaEB.getZonaCoordenadaByIdZona", ZonaCoordenadaEB.class);
			query.setParameter("id_zona", id_zona);
			ret = query.getResultList();
			if (ret == null || (ret != null && ret.size() == 0)) { 
				throw new ZonaSinCoordenadasException("Zona sin coordenadas para la zona " + id_zona); 
				}
		}
		catch (Exception ex)
		{
			System.out.println("getZonaCoordenadas - getLocalizedMessage: " + ex.getLocalizedMessage());
			throw new GetZonaCoordenadas("Error al recuperar las coordenadas de la zona " + id_zona);
		}

		return ret;
	}
}
