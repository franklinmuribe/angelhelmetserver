package com.angelhelmet.server.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import com.angelhelmet.server.dao.interfaces.IOperarioDAO;
import com.angelhelmet.server.persistencia.OperarioEB;

@Stateless
//@Deprecated
public class OperarioDAO implements IOperarioDAO
{
	@PersistenceContext
	private EntityManager _em;

	public OperarioDAO()
	{
	}

	public OperarioEB getOperarioPorUnidad(Integer id_unidad)
	{
		OperarioEB ret = null;
		try
		{
			TypedQuery<OperarioEB> query = _em.createNamedQuery("Operario.getOperarioByUnidad", OperarioEB.class);
			query.setParameter("id", id_unidad);
			List<OperarioEB> lista = query.getResultList();
			if (lista.size() == 1)
			{
				ret = lista.get(0);
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return ret;
	}

	@Override
	public List<OperarioEB> getOperarios() throws Exception
	{
		List<OperarioEB> ret = null;

		try
		{
			TypedQuery<OperarioEB> query = _em.createNamedQuery("OperarioEB.findAll", OperarioEB.class);
			ret = query.getResultList();
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
			throw e;
		}

		return ret;
	}

	@Override
	public List<OperarioEB> getOperarios(String supervisor) throws Exception
	{
		List<OperarioEB> ret = null;

		try
		{
			TypedQuery<OperarioEB> query = _em.createNamedQuery("OperarioEB.getBySupervisor", OperarioEB.class);
			query.setParameter("supervisor", supervisor);
			ret = query.getResultList();
		}
		catch (Exception e)
		{
			// TODO: handle exception
			e.printStackTrace();
			throw e;
		}

		return ret;
	}
}
