package com.angelhelmet.server.dao;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;

import com.angelhelmet.server.dao.excepciones.AlarmaNoEncontradaException;
import com.angelhelmet.server.dao.interfaces.IAlarmaDAO;
import com.angelhelmet.server.persistencia.AlarmaEB;
import com.angelhelmet.server.util.TipoAlarma;
import com.hcl.iot.smartworker.geofencingdtos.AlarmEvent;
import com.hcl.iot.smartworker.geofencingdtos.AlarmSource;
import com.hcl.iot.smartworker.geofencingdtos.WorkerReadMessage;

@Stateless
public class AlarmaDAO implements IAlarmaDAO {
	@PersistenceContext
	private EntityManager _em;

	public AlarmaDAO() {
	}

	@Override
	public AlarmaEB getAlarma(TipoAlarma alarma) throws AlarmaNoEncontradaException, Exception {
		AlarmaEB ret = null;

		TypedQuery<AlarmaEB> query = _em.createNamedQuery("Alarma.getAlarmaById", AlarmaEB.class);
		query.setParameter("id", alarma.getAlarma());
		List<AlarmaEB> lista = query.getResultList();
		if (lista.size() == 1) {
			ret = lista.get(0);
		} else {
			throw new AlarmaNoEncontradaException("Alarma no encontrada: " + alarma);
		}

		return ret;
	}

	@Override
	public AlarmSource getAlarma(Long firmaId) throws AlarmaNoEncontradaException, Exception {

		//System.out.println("firmaId: " + firmaId);

		// agregar tipo mapa

		String sqlQuery = " select ae.id_evento eventId, a.id_alarma alarmId, a.descripcion alarmDescription, a.tipo_alarma alarmType, l.id_log logId, "
				+ " fecha_evento eventDate, fecha_respuesta responseDate, l.latitud latitude, l.longitud longitude, "
				+ " l.latitud_c latitudeC, l.longitud_c longitudeC, x, y, ae.id_tipomapa "
				+ " from alarmaseventos ae join alarmas a using (id_alarma) join logs l using(id_log) "
				+ " where ae.firma = :firmaId ";
//				+ " where ae.id_alarma in (13, 18) and ae.cancela > 0 and ae.firma = :firmaId ";
//		        + " where ae.id_evento in (select distinct ae.cancela " 
//				+ " from alarmaseventos ae join alarmas a using (id_alarma) join logs l using(id_log) where ae.firma = :firmaId ) ";

		try {

			Session sessionObj = (Session) _em.getDelegate();

			SQLQuery query = sessionObj.createSQLQuery(sqlQuery);

			query.setParameter("firmaId", firmaId);

			List<Object[]> rows = query.list();
			AlarmSource alarmSouce = new AlarmSource();

			if (!rows.isEmpty()) {

				for (Object[] row : rows) {
					alarmSouce = new AlarmSource();
					if (row[0] != null) {
						System.out.println("eventId: " + row[0].toString());
						alarmSouce.setEventId(Long.parseLong(row[0].toString()));
					} else {
						System.out.println("eventId: ");
					}
					if (row[1] != null) {
						System.out.println("alarmId: " + row[1].toString());
						alarmSouce.setAlarmId(Long.parseLong(row[1].toString()));
					} else {
						System.out.println("alarmId: ");
					}
					if (row[2] != null) {
						System.out.println("alarmDescription: " + row[2].toString());
						alarmSouce.setAlarmDescription(row[2].toString());
					} else {
						System.out.println("alarmDescription: ");
					}
					if (row[3] != null) {
						System.out.println("alarmType: " + row[3].toString());
						alarmSouce.setAlarmType(row[3].toString());
					} else {
						System.out.println("alarmType: ");
					}

					if (row[4] != null) {
						System.out.println("logId: " + row[4].toString());
						alarmSouce.setLogId(Long.parseLong(row[4].toString()));
					} else {
						System.out.println("alarmDescription: ");
					}
					if (row[5] != null) {
						System.out.println("eventDate: " + row[5].toString());
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[5].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							alarmSouce.setEventDate(timestamp);
						} catch (Exception e) {
							alarmSouce.setEventDate(null);
						}

					} else {
						System.out.println("eventDate: ");
						alarmSouce.setEventDate(null);
					}
					if (row[6] != null) {
						System.out.println("responseDate: " + row[6].toString());
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[6].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							alarmSouce.setResponseDate(timestamp);
						} catch (Exception e) {
							alarmSouce.setResponseDate(null);
						}

					} else {
						System.out.println("responseDate: ");
						alarmSouce.setResponseDate(null);
					}
					if (row[7] != null) {
						System.out.println("latitude: " + row[7].toString());
						alarmSouce.setLatitude(Double.parseDouble(row[7].toString()));
					} else {
						System.out.println("latitude: ");
						alarmSouce.setLatitude(0D);
					}
					if (row[8] != null) {
						System.out.println("longitude: " + row[8].toString());
						alarmSouce.setLongitude(Double.parseDouble(row[8].toString()));
					} else {
						System.out.println("longitude: ");
						alarmSouce.setLongitude(0D);
					}
					if (row[9] != null) {
						System.out.println("latitudeC: " + row[9].toString());
						alarmSouce.setLatitudeC(row[9].toString());
					} else {
						System.out.println("latitudeC: ");
						alarmSouce.setLatitudeC("");
					}
					if (row[10] != null) {
						System.out.println("longitudeC: " + row[10].toString());
						alarmSouce.setLongitudeC(row[10].toString());
					} else {
						System.out.println("longitudeC: ");
						alarmSouce.setLongitudeC("");
					}
					if (row[11] != null) {
						System.out.println("x: " + row[11].toString());
						alarmSouce.setX(Double.parseDouble(row[11].toString()));
					} else {
						System.out.println("x: ");
						alarmSouce.setX(0D);
					}
					if (row[12] != null) {
						System.out.println("y: " + row[12].toString());
						alarmSouce.setY(Double.parseDouble(row[12].toString()));
					} else {
						System.out.println("y: ");
						alarmSouce.setY(0D);
					}
					if (row[13] != null) {
						System.out.println("mapTypeId: " + row[13].toString());
						alarmSouce.setMapTypeId(Long.parseLong(row[13].toString()));
					} else {
						System.out.println("mapTypeId: ");
						alarmSouce.setMapTypeId(0L);
					}

				}
			}

			return alarmSouce;

		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public AlarmEvent getAlarmaEvento(Long firmaId) throws AlarmaNoEncontradaException, Exception {
		//System.out.println("firmaId: " + firmaId);

		// agregar tipo mapa

		String sqlQuery = " select id_evento, fecha_evento, fecha_respuesta from alarmaseventos where firma = :firmaId ";

		try {

			Session sessionObj = (Session) _em.getDelegate();

			SQLQuery query = sessionObj.createSQLQuery(sqlQuery);

			query.setParameter("firmaId", firmaId);

			List<Object[]> rows = query.list();
			AlarmEvent alarmEvent = new AlarmEvent();

			if (!rows.isEmpty()) {

				for (Object[] row : rows) {
					alarmEvent = new AlarmEvent();
					if (row[0] != null) {
					//	System.out.println("eventId: " + row[0].toString());
						alarmEvent.setEventId(Long.parseLong(row[0].toString()));
					//} else {
					//	System.out.println("eventId: ");
					}
					if (row[1] != null) {
					//	System.out.println("eventDate: " + row[1].toString());
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[1].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							alarmEvent.setEventDate(timestamp);
						} catch (Exception e) {
							alarmEvent.setEventDate(null);
						}

					} else {
					//	System.out.println("eventDate: ");
						alarmEvent.setEventDate(null);
					}
					if (row[2] != null) {
					//	System.out.println("eventResponseDate: " + row[2].toString());
						try {
							SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
							Date parsedDate = dateFormat.parse(row[2].toString());
							Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
							alarmEvent.setEventResponseDate(timestamp);
						} catch (Exception e) {
							alarmEvent.setEventResponseDate(null);
						}

					} else {
					//	System.out.println("responseDate: ");
						alarmEvent.setEventResponseDate(null);
					}

				}
			}

			return alarmEvent;

		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public WorkerReadMessage getReadMessage(Long firma) throws AlarmaNoEncontradaException, Exception {
		WorkerReadMessage workerReadMessage = new WorkerReadMessage();

		String sqlQuery = "";

		workerReadMessage.setWorkerQuestionMessage("");

		System.out.println("impl-getAlarmDetailsAttended-getMessageRead " + firma);

		Session sessionObj = (Session) _em.getDelegate();

		try {
			sqlQuery = " select texto from mensajes where id_firmacasco in( select firma from logs where id_log in (select id_log from alarmaseventos where firma = :firma ) ) ";

			Query query = sessionObj.createSQLQuery(sqlQuery);
			query.setLong("firma", firma);
			System.out.println("getAlarmDetailsAttended-getWorker");
			System.out.println(sqlQuery);

			query.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);

			List<?> data = query.list();

			for (Object object : data) {

				// AlarmDetailsAttended alarmDetails = new AlarmDetailsAttended();

				@SuppressWarnings("rawtypes")
				Map row = (Map) object;

				if (row.get("texto") != null) {

					String texto = String.valueOf(row.get("texto"));

					String[] mensajCascoLista = texto.split("~");

					if (mensajCascoLista.length > 0) {
						workerReadMessage.setWorkerQuestionMessage(mensajCascoLista[0].substring(9));

					}
				}
			}

		} catch (Exception e) {

		}

		return workerReadMessage;

	}

}
