package com.angelhelmet.server.util;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.nio.ByteBuffer;

/**
 * @author Taylor Moon <https://www.github.com/taaylormoon> <Source code Part of
 *         Xerxes 2 Runescape Emulator>
 */
public class BufferUtils
{

	/**
	 * Reads a string from a specified {@link ByteBuffer}
	 * 
	 * @param buffer
	 *            The {@link ByteBuffer} to read the string from
	 * @return The string read from the buffer
	 */
	public static String readString(ByteBuf buffer)
	{
		StringBuilder sb = new StringBuilder();
		byte b;
		while (buffer.readableBytes() > 0 && (b = buffer.readByte()) != 0)
		{
			sb.append((char) b);
		}
		return sb.toString();
	}

	/**
	 * Writes a string to a specified {@link ByteBuffer}
	 * 
	 * @param s
	 *            The string to write
	 * @param buffer
	 *            The {@link ByteBuffer} to write the string to
	 */
	public static void writeString(String s, ByteBuffer buffer)
	{
		for (char c : s.toCharArray())
		{
			buffer.put((byte) c);
		}
		buffer.put((byte) 0);
	}

	/**
	 * Gets an int from a buffer.
	 * 
	 * @param index
	 *            The index of the buffer to read the value from.
	 * @param buffer
	 *            The buffer containing the value to get.
	 * @return The decoded value.
	 */
	public static int getInt(int index, byte[] buffer)
	{
		return ((buffer[index++] & 0xff) << 24) | ((buffer[index++] & 0xff) << 16) | ((buffer[index++] & 0xff) << 8) | (buffer[index++] & 0xff);
	}

	/**
	 * @param buffer
	 *            The buffer to write to.
	 * @param packet
	 *            The packet ID to write.
	 */
	public static void createPacket(ByteBuffer buffer, int packet)
	{
		if (packet >= 128)
		{
			buffer.put((byte) ((packet >> 8) + 128));
			buffer.put((byte) packet);
		}
		else buffer.put((byte) packet);
	}

	/**
	 * @param buffer
	 * @return
	 */
	public static int readLargeSmart(ByteBuffer buffer)
	{
		if (buffer.get(buffer.position()) > 0)
		{
			int value = buffer.getShort() & 0xFFFF;
			if (value == 32767) { return -1; }
			return value;
		}
		return buffer.getInt() & 0x7fffffff;
	}

	/**
	 * Reads a Smart from the buffer.
	 * 
	 * @param buffer
	 *            The inbound buffer.
	 * @return The Smart read.
	 */
	public static int readSmart(ByteBuffer buffer)
	{
		int value = buffer.get() & 0xff;
		if (value < 128) { return value; }
		int value2 = buffer.get() & 0xff;
		return (value << 8 | value2) - 32768;
	}

	/**
	 * Puts a 5 byte integer into the buffer.
	 * 
	 * @param buf
	 *            The channel buffer
	 * @param value
	 *            The value to be added.
	 */
	public static void put5ByteInteger(ByteBuf buf, long value)
	{
		buf.writeByte((int) (value >> 32));
		buf.writeInt((int) (value & 0xffffffff));
	}

	/**
	 * Puts a 6 byte integer into the buffer.
	 * 
	 * @param buf
	 *            The channel buffer
	 * @param value
	 *            The value to be added.
	 */
	public static void put6ByteInteger(ByteBuf buf, long value)
	{
		buf.writeShort((int) (value >> 32));
		buf.writeInt((int) (value & 0xffffffff));
	}

	/**
	 * Writes a string
	 * 
	 * @param buffer
	 *            The ChannelBuffer
	 * @param string
	 *            The string being wrote.
	 */
	public static void putJagString(ByteBuf buffer, String string)
	{
		buffer.writeByte(0);
		buffer.writeBytes(string.getBytes());
		buffer.writeByte(0);
	}

	/**
	 * Puts a string into a buffer.
	 * 
	 * @param buf
	 *            The buffer.
	 * @param string
	 *            The string.
	 */
	public static void putString(ByteBuf buf, String string)
	{
		for (char c : string.toCharArray())
		{
			// buf.writeByte(c);
			buf.writeByte(Character.getNumericValue(c));
		}
		buf.writeByte(0);
	}

	/**
	 * Writes a 'tri-byte' to the specified buffer.
	 * 
	 * @param buf
	 *            The buffer.
	 * @param value
	 *            The value.
	 */
	public static void putTriByte(ByteBuf buf, int value)
	{
		buf.writeByte(value >> 16);
		buf.writeByte(value >> 8);
		buf.writeByte(value);
	}

	/**
	 * Reads a string from a bytebuffer.
	 * 
	 * @param buf
	 *            The bytebuffer.
	 * @return The decoded string.
	 */
	public static String readString(ByteBuffer buf)
	{
		StringBuilder bldr = new StringBuilder();
		byte b;
		while ((b = buf.get()) != 0)
		{
			bldr.append((char) b);
		}
		return bldr.toString();
	}

	/**
	 * Adds a int into a buffer.
	 * 
	 * @param val
	 *            The value to add.
	 * @param index
	 *            The index to add the value.
	 * @param buffer
	 *            The buffer to add the value to.
	 */
	public static void putInt(int val, int index, byte[] buffer)
	{
		buffer[index++] = (byte) (val >> 24);
		buffer[index++] = (byte) (val >> 16);
		buffer[index++] = (byte) (val >> 8);
		buffer[index++] = (byte) val;
	}

	/**
	 * Encodes a {@link java.lang.String} with CESU8.
	 * 
	 * @param string
	 *            The {@link java.lang.String} to encode.
	 * @return The encoded {@link java.lang.String} in a
	 *         {@link io.netty.buffer.ByteBuf}.
	 */
	public static ByteBuf encodeCESU8(String string)
	{
		int length = string.length();

		// Calculate the amount of bytes for the buffer.
		int size = 0;
		for (int index = 0; index < length; index++)
		{
			int character = string.charAt(index);
			if (character >= 2048) size += 3;
			else if (character >= 128) size += 2;
			else size++;
		}

		// Allocate a new buffer for appending data.
		ByteBuf buffer = Unpooled.buffer(size);

		for (int index = 0; index < length; index++)
		{
			// Get the character at the current index.
			int character = string.charAt(index);

			// A character that is represented more than 1 bytes.
			if (character >= 128)
			{

				// A character that is represented by 3 bytes.
				if (character >= 2048)
				{
					buffer.writeByte((character >> 0xC) | 0xE0);
					buffer.writeByte(((character >> 6) & 0x3F) | 0x80);
					buffer.writeByte((character & 0x3F) | 0x80);

					// A character that is represented by 2 bytes.
				}
				else
				{
					buffer.writeByte((character >> 6) | 0x3015);
					buffer.writeByte((character & 0x3F) | 0x80);
				}
			}
			else
			{
				// A character in which is represented by a single byte.
				buffer.writeByte(character);
			}
		}
		return buffer;
	}
}
