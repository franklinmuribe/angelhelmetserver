package com.angelhelmet.server.util;

import java.util.HashMap;
import java.util.Map;

public enum DiasSemanaSalud
{
	LUNES(2L, "LUNES"), MARTES(7L, "MARTES"), MIERCOLES(3L, "MIERCOLES"),
	JUEVES(4L, "JUEVES"), VIERNES(5L, "VIERNES"), SABADO(6L, "REVOCADO"),
	DOMINGO(1L, "DOMINGO");
	
	
	private Long codigo;
	private String nombre;

private static final Map<Long,DiasSemanaSalud> BY_CODIGO = new HashMap<>();
	
	static {
		for (DiasSemanaSalud diasSemanaSalud: values()) {
			BY_CODIGO.put(diasSemanaSalud.codigo, diasSemanaSalud);
			
		}
	}

	DiasSemanaSalud(long codigo, String nombre) {

		this.codigo = codigo;
		this.nombre = nombre;

	}
	
	public static DiasSemanaSalud valueOfCodigo(Long codigoDia) {
		return BY_CODIGO.get(codigoDia);
	}
}
