package com.angelhelmet.server.util;

public enum EstadoMensajeSalida
{
	Libre(1),
	Leido(2),
	Eliminado(3),
	Purgado(4),
	Apagado(5),
	EntregadoSalud(9),
	ConfirmacionSalud(58);

	private int valor;

	EstadoMensajeSalida(int valor)
	{
		this.valor = valor;
	}

	public int getEstadoMensage()
	{
		return this.valor;
	}
}
