package com.angelhelmet.server.util;

public enum ModoFuncionamiento
{
	ERROR(0),
	INTERIOR(1),
	EXTERIOR(2);

	private int valor;

	ModoFuncionamiento(int valor)
	{
		this.valor = valor;
	}

	public int getAlarma()
	{
		return this.valor;
	}
}
