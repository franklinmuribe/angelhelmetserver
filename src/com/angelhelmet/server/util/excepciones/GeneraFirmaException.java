package com.angelhelmet.server.util.excepciones;

public class GeneraFirmaException extends Exception
{
	private static final long serialVersionUID = -6024813741260610755L;
	
	public GeneraFirmaException() {
	}
	
	public GeneraFirmaException(String s) {
		super(s);
	}
	
	public GeneraFirmaException(String s, Exception e) {
		super(s, e);
	}

}
