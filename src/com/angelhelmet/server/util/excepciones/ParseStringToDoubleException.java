package com.angelhelmet.server.util.excepciones;

public class ParseStringToDoubleException extends Exception
{
	private static final long serialVersionUID = -5471857168825631623L;
	
	public ParseStringToDoubleException() {
	}
	
	public ParseStringToDoubleException(String s) {
		super(s);
	}
	
	public ParseStringToDoubleException(String s, Exception e) {
		super(s, e);
	}

}
