package com.angelhelmet.server.util.excepciones;

public class ParseStringToIntException extends Exception
{
	private static final long serialVersionUID = 1197240256831194157L;

	public ParseStringToIntException() {
	}
	
	public ParseStringToIntException(String s) {
		super(s);
	}
	
	public ParseStringToIntException(String s, Exception e) {
		super(s, e);
	}
}
