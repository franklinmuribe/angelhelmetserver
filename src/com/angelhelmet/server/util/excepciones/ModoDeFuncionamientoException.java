package com.angelhelmet.server.util.excepciones;

public class ModoDeFuncionamientoException extends Exception
{
	private static final long serialVersionUID = 5152261296228977472L;
	
	public ModoDeFuncionamientoException() {
	}
	
	public ModoDeFuncionamientoException(String s) {
		super(s);
	}
	
	public ModoDeFuncionamientoException(String s, Exception e) {
		super(s, e);
	}

}
