package com.angelhelmet.server.util.excepciones;

public class ParseStringToBigIntegerException extends Exception
{
	private static final long serialVersionUID = -8969247541058838434L;
	
	public ParseStringToBigIntegerException() {
	}
	
	public ParseStringToBigIntegerException(String s) {
		super(s);
	}
	
	public ParseStringToBigIntegerException(String s, Exception e) {
		super(s, e);
	}

}
