package com.angelhelmet.server.util.excepciones;

public class DentroDelPoligonoException extends Exception
{
	private static final long serialVersionUID = 4625267705356462183L;
	
	public DentroDelPoligonoException() {
	}
	
	public DentroDelPoligonoException(String s) {
		super(s);
	}
	
	public DentroDelPoligonoException(String s, Exception e) {
		super(s, e);
	}

}
