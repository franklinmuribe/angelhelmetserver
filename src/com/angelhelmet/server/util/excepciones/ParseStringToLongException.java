package com.angelhelmet.server.util.excepciones;

public class ParseStringToLongException extends Exception
{
	private static final long serialVersionUID = 1197240256831194157L;

	public ParseStringToLongException() {
	}
	
	public ParseStringToLongException(String s) {
		super(s);
	}
	
	public ParseStringToLongException(String s, Exception e) {
		super(s, e);
	}
}
