package com.angelhelmet.server.util.excepciones;

public class ParseStringToFloatException extends Exception
{
	private static final long serialVersionUID = 1701132266794403647L;

	public ParseStringToFloatException() {
	}
	
	public ParseStringToFloatException(String s) {
		super(s);
	}
	
	public ParseStringToFloatException(String s, Exception e) {
		super(s, e);
	}
}
