package com.angelhelmet.server.util.excepciones;

public class GetFechaByStringException extends Exception
{
	private static final long serialVersionUID = -8570134345010174892L;
	
	public GetFechaByStringException() {
	}
	
	public GetFechaByStringException(String s) {
		super(s);
	}
	
	public GetFechaByStringException(String s, Exception e) {
		super(s, e);
	}

}
