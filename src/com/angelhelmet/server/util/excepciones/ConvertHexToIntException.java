package com.angelhelmet.server.util.excepciones;

public class ConvertHexToIntException extends Exception
{
	private static final long serialVersionUID = 1905385945254475093L;
	
	public ConvertHexToIntException() {
	}
	
	public ConvertHexToIntException(String s) {
		super(s);
	}
	
	public ConvertHexToIntException(String s, Exception e) {
		super(s, e);
	}

}
