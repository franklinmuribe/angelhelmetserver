package com.angelhelmet.server.util.excepciones;

public class ConvertHexToStringException extends Exception
{
	private static final long serialVersionUID = 1905385945254475093L;
	
	public ConvertHexToStringException() {
	}
	
	public ConvertHexToStringException(String s) {
		super(s);
	}
	
	public ConvertHexToStringException(String s, Exception e) {
		super(s, e);
	}

}
