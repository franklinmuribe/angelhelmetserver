package com.angelhelmet.server.util;

public enum TipoLinea
{
	HORIZONTAL,
	VERTICAL,
	OBLICUA,
	NOALINEADO
}
