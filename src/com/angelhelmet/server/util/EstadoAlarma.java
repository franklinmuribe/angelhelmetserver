package com.angelhelmet.server.util;

public enum EstadoAlarma
{
	NINGUNA(1),
	LANZA(2),
	MANTIENE(3);

	private int valor;

	EstadoAlarma(int valor)
	{
		this.valor = valor;
	}

	public int getEstadoMensage()
	{
		return this.valor;
	}
}
