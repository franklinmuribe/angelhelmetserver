package com.angelhelmet.server.util;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.angelhelmet.server.datos.datosCoordenada;
import com.angelhelmet.server.util.excepciones.ConvertHexToIntException;
import com.angelhelmet.server.util.excepciones.ConvertHexToStringException;
import com.angelhelmet.server.util.excepciones.DentroDelPoligonoException;
import com.angelhelmet.server.util.excepciones.GeneraFirmaException;
import com.angelhelmet.server.util.excepciones.GetFechaByStringException;
import com.angelhelmet.server.util.excepciones.ModoDeFuncionamientoException;
import com.angelhelmet.server.util.excepciones.ParseStringToBigIntegerException;
import com.angelhelmet.server.util.excepciones.ParseStringToDoubleException;
import com.angelhelmet.server.util.excepciones.ParseStringToFloatException;
import com.angelhelmet.server.util.excepciones.ParseStringToIntException;
import com.angelhelmet.server.util.excepciones.ParseStringToLongException;

public class ServidorUtil {
	public ServidorUtil() {
	}

	
	public static String parseIntegerToHex(Integer intValue) {
		String ret = "";

		ret = Integer.toHexString(intValue).toUpperCase();

		ret = String.join("", Collections.nCopies(2 - ret.length(), "0")) + ret;

		return ret;
	}
	
	@Deprecated
	public static String convertStringToHex(String str) {

		char[] chars = str.toCharArray();

		StringBuffer hex = new StringBuffer();
		for (int i = 0; i < chars.length; i++) {
			hex.append(Integer.toHexString((int) chars[i]));
		}

		return hex.toString();
	}

	public static String convertHexToString(String hex, String var) throws ConvertHexToStringException, Exception {
		StringBuilder sb = new StringBuilder();
		StringBuilder temp = new StringBuilder();

		try {
			for (int i = 0; i < hex.length() - 1; i += 2) {
				String output = hex.substring(i, (i + 2));
				int decimal = Integer.parseInt(output, 16);
				sb.append((char) decimal);
				temp.append(decimal);
			}
		} catch (Exception e) {
			throw new ConvertHexToStringException("No se puede convertir el HEX " + var + " = " + hex + " a String", e);
		}
		return sb.toString();
	}

	public static int convertHexToInt(String hex, String var) throws ConvertHexToIntException, Exception {
		int val = 0;
		try {
			String digits = "0123456789ABCDEF";
			hex = hex.toUpperCase();

			for (int i = 0; i < hex.length(); i++) {
				char c = hex.charAt(i);
				int d = digits.indexOf(c);
				val = 16 * val + d;
			}

		} catch (Exception e) {
			throw new ConvertHexToStringException("No se puede convertir el HEX " + var + " = " + hex + " a Int", e);
		}
		return val;
	}

	public static int parseStringToInt(String s, String var) throws ParseStringToIntException {
		int ret = 0;
		try {
			ret = Integer.parseInt(s);
		} catch (Exception e) {
			throw new ParseStringToIntException("No se puede convertir el String " + var + " = " + s + " a int", e);
		}
		return ret;
	}

	public static int parseStringToInt16(String s, String var) throws ParseStringToIntException {
		int ret = 0;
		try {
			ret = Integer.parseInt(s, 16);
		} catch (Exception e) {
			throw new ParseStringToIntException("No se puede convertir el String " + var + " = " + s + " a int", e);
		}
		return ret;
	}
	
	public static long parseStringToLong16(String s, String var) throws ParseStringToLongException {
		long ret = 0;
		try {
			ret = Long.parseLong(s, 16);
		} catch (Exception e) {
			throw new ParseStringToLongException("No se puede convertir el String " + var + " = " + s + " a int", e);
		}
		return ret;
	}


	public static Double parseStringToDouble(String s, String var) throws ParseStringToDoubleException {
		Double ret = null;
		try {
			ret = Double.parseDouble(s);
		} catch (Exception e) {
			throw new ParseStringToDoubleException("No se puede convertir el String " + var + " = " + s + " a Double",
					e);
		}
		return ret;
	}

	public static Float parseStringToFloat(String s, String var) throws ParseStringToFloatException {
		Float ret = null;
		try {
			ret = Float.parseFloat(s);
		} catch (Exception e) {
			throw new ParseStringToFloatException("No se puede convertir el String " + var + " = " + s + " a Float", e);
		}
		return ret;
	}

	public static BigInteger parseStringToBigInteger(String s, String var) throws ParseStringToBigIntegerException {
		BigInteger ret = null;
		try {
			ret = new BigInteger(hexReverse(s), 16);
		} catch (Exception e) {
			throw new ParseStringToBigIntegerException(
					"No se puede convertir el String " + var + " = " + s + " a BigInteger", e);
		}
		return ret;
	}
	
	public static String parseIntToHex(int intValue, String var) throws ParseStringToBigIntegerException {
		String ret = "";
		try {
			ret = Integer.toHexString(intValue);
			
		} catch (Exception e) {
			throw new ParseStringToBigIntegerException(
					"No se puede convertir el String " + var + " = " + intValue + " a Hex", e);
		}
		return ret;
	}
	
	public static Integer parseHexToInt(String hexString, String var) throws ParseStringToBigIntegerException {
		int ret;
		try {
			ret = Integer.valueOf(hexString, 16).intValue();
			
		} catch (Exception e) {
			throw new ParseStringToBigIntegerException(
					"No se puede convertir el String " + var + " = " + hexString + " a Hex", e);
		}
		return ret;
	}

	@Deprecated
	public static Date getTiempo(int tiempo) {
		Calendar calendario = Calendar.getInstance();

		int h = (tiempo / 3600);
		int m = ((tiempo - h * 3600) / 60);
		int s = tiempo - (h * 3600 + m * 60);

		int year = calendario.get(Calendar.YEAR);
		int month = calendario.get(Calendar.MONTH);
		int day = calendario.get(Calendar.DATE);

		calendario.set(year, month, day, h, m, s);
		return calendario.getTime();
	}

	public static Date getFecha(String fecha) throws GetFechaByStringException {
		Calendar calendario = Calendar.getInstance();
		String s_year;
		String s_month;
		String s_day;

		try {

			s_year = fecha.substring(4);
			s_month = fecha.substring(0, 2);
			s_day = fecha.substring(2, 4);

			int year = parseStringToInt16(s_year, "fecha_year") + 2000;
			int month = parseStringToInt16(s_month, "fecha_month") - 1;
			int day = parseStringToInt16(s_day, "fecha_day");

			calendario.set(Calendar.YEAR, year);
			calendario.set(Calendar.MONTH, month);
			calendario.set(Calendar.DAY_OF_MONTH, day);

		} catch (Exception e) {
			throw new GetFechaByStringException("No se puede conseguir la fecha del string: " + fecha, e);
		}
		return calendario.getTime();
	}

	public static Date getFecha(long fecha) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTimeInMillis(fecha);
		return calendario.getTime();
	}

	public static Date sumaSegundos(Date fecha, int segundos) {
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		calendario.add(Calendar.SECOND, segundos);
		return calendario.getTime();
	}

	public static String hexReverse(String hex) {
		StringBuilder sb = new StringBuilder();
		char[] bits_hex = hex.toCharArray();

		for (int j = hex.length() - 1; j > -1; j -= 2) {
			sb.append(bits_hex[j - 1]);
			sb.append(bits_hex[j]);
		}
		return sb.toString();
	}

	public static Timestamp getTimestamp() {
		Date fecha = new Date();
		return (new Timestamp(fecha.getTime()));
	}

	@Deprecated
	public static int getIntConSigno(String hex) {
		int ret;

		byte[] byte_signed = new byte[2];
		String alto = hex.substring(0, 2);
		String bajo = hex.substring(2);

		byte_signed[0] = (byte) Integer.parseInt(alto, 16);
		byte_signed[1] = (byte) Integer.parseInt(bajo, 16);

		ret = (byte_signed[0] << 4) | (byte_signed[1]);
		return ret;
	}

	public static TipoAlarma getTipoAlarma(int alarma) {
		switch (alarma) {
		case 1:
			return TipoAlarma.SOS;
		case 2:
			return TipoAlarma.IMPACTO;
		case 4:
			return TipoAlarma.INMOVIL;
		case 8:
			return TipoAlarma.GAS;
		case 9:
			return TipoAlarma.GASSOS;
		case 10:
			return TipoAlarma.GASIMPACTO;
		case 12:
			return TipoAlarma.GASINMOVIL;
		case 22:
			return TipoAlarma.MENSAJEOPERARIO;
		default: // alarma = 0 o error
			return TipoAlarma.NINGUNA;
		}
	}

	public static EstadoMensajeEntrada getTipoMensaje(int mensaje) {
		switch (mensaje) {
		case 0:
			return EstadoMensajeEntrada.Libre;
		case 1:
			return EstadoMensajeEntrada.Ocupado;
		case 2:
			return EstadoMensajeEntrada.Leido;
		case 3:
			return EstadoMensajeEntrada.Configuracion;
		case 4:
			return EstadoMensajeEntrada.Apagado;
		case 5:
			return EstadoMensajeEntrada.Backdoor;
		case 6:
			return EstadoMensajeEntrada.Lampisteria;
		case 7:
			return EstadoMensajeEntrada.ApagadoSeguro;
		case 8:
			return EstadoMensajeEntrada.MensajeOperario;
		case 9:
			return EstadoMensajeEntrada.EntregaSalud;
		case 58:
			return EstadoMensajeEntrada.ConfirmacionSalud;
		
		default:
			return EstadoMensajeEntrada.Libre;
		}
	}

	public static ModoFuncionamiento getModoFuncionamiento(int modo) throws ModoDeFuncionamientoException {
		ModoFuncionamiento ret = ModoFuncionamiento.ERROR;
		if (modo == 1) {
			ret = ModoFuncionamiento.EXTERIOR;
		} else if (modo == 2) {
			ret = ModoFuncionamiento.INTERIOR;
		} else {
			throw new ModoDeFuncionamientoException("El modo de funcionamiento no es correcto: " + modo);
		}
		return ret;
	}

	public static double redondear(double numero, int decimales) {
		return Math.round(numero * Math.pow(10, decimales)) / Math.pow(10, decimales);
	}

	public static String completaString(String s, char c, int l) {
		String ret = s;
		if (s.length() < l) {
			for (int i = 0; i < l - s.length(); i++) {
				ret = c + ret;
			}
		}
		return ret;
	}

	public static boolean dentroDelPoligono(List<datosCoordenada> poligono, datosCoordenada punto)
			throws DentroDelPoligonoException {
		// La funcion devolvera VERDADERO si el punto x, y esta dentro del
		// poligono, o
		// FALSO si no lo esta. Si el punto esta exactamente en el borde del
		// polagono,
		// entonces la funcion puede devolver VERDADERO o FALSO.

		int i = 0;
		int j = 0;
		boolean c = false;
		// int j = poligono.size() - 1;

		try {
			for (i = 0, j = poligono.size() - 1; i < poligono.size(); j = i++) {
				if (((poligono.get(i).getY() > punto.getY()) != (poligono.get(j).getY() > punto.getY()))
						&& (punto.getX() < (poligono.get(j).getX() - poligono.get(i).getX())
								* (punto.getY() - poligono.get(i).getY())
								/ (poligono.get(j).getY() - poligono.get(i).getY()) + poligono.get(i).getX()))
					c = !c;
			}
		} catch (Exception e) {
			StringBuilder sb = new StringBuilder();
			int k = 1;

			if (poligono.size() > 0) {
				for (datosCoordenada item : poligono) {
					sb.append("Vertice " + k + "\tx=" + item.getX() + " y=" + item.getY() + "\n");
					k++;
				}
			} else {
				sb.append("Poligono sin vertices\n");
			}
			sb.append("Punto: x: " + punto.getX() + " y: " + punto.getY());

			throw new DentroDelPoligonoException(
					"No se puede calcular si el punto esta dentro del poligono" + sb.toString(), e);
		}
		return c;
	}

	public static String generaFirmaString() throws GeneraFirmaException {
		String ret = "";
		try {
			SecureRandom random = new SecureRandom();
			ret = new BigInteger(128, random).toString(32);
		} catch (Exception e) {
			throw new GeneraFirmaException("No se puede gerenar la firma", e);
		}
		return ret;
	}

	public static long generaFirmaLong() throws GeneraFirmaException {
		long ret = 0;
		try {
			Calendar hoy = new GregorianCalendar();
			ret = hoy.getTimeInMillis();
		} catch (Exception e) {
			throw new GeneraFirmaException("No se puede gerenar la firma", e);
		}
		return ret;
	}
}
