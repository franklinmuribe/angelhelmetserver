package com.angelhelmet.server.util;

public enum EstadoMensajeEntrada
{
	Libre(0),
	Ocupado(1),
	Leido(2),
	Configuracion(3),
	Apagado(4),
	Backdoor(5),
	Lampisteria(6),
	ApagadoSeguro(7),
	MensajeOperario(8),
	EntregaSalud(9),
	ConfirmacionSalud(58);

	private int valor;

	EstadoMensajeEntrada(int valor)
	{
		this.valor = valor;
	}

	public int getTipoMensage()
	{
		return this.valor;
	}
}
