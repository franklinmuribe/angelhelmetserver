package com.angelhelmet.server.util;

public enum TipoMensaje {
	SMS(1), CONFIGURACION(2), APAGADO(3), ALARMA(4),SALUD(5);

	private int valor;

	TipoMensaje(int valor) {
		this.valor = valor;
	}

	public int getTipoMensaje() {
		return this.valor;
	}
}
